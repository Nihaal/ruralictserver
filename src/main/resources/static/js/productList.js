website.controller("ProductsCtrl",function($window, $scope, $http, $route, $location, ProductCreate, ProductEdit, ProductDelete) {
		
		var id;
		var prodId = 0;
		var audio_url;
		var image_url;
		var sheet_url;
		var abbr = $('#organizationAbbr').val();
		var counter = 0, hot;
		var prodData;
		var flagModal = 0;
		var imagURL;
		$scope.currProductType = "";
		
		
		
		
		// Initialize the table
		$http.get( API_ADDR + 'api/products/search/byType/'+abbr+'/getList').
		success(function(data, status, headers, config) {
			// Store the data into Angular scope model variable
			for(var i=0; i<data.length; i++)
				data[i].productType = "--"+data[i].productType+"--";
			$scope.products = data;
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});
		
		$http.get(API_ADDR +'web/'+abbr+'/getProductTypeByOrgg/').
		success(function(data, status, headers, config) {
			var list = data.producttyper;
			for(var i=0; i<list.length; i++){
				list[i].name = "--"+list[i].name+"--";
				console.log(list[i].name);
			}
			$scope.productTypeList = list;
			//$scope.productTypeList = data.producttyper;
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server typeORGG.");
		});
		
		//function to save product
		$scope.saveProduct = function(data){
			$scope.newproduct = new ProductCreate();
			$scope.newproduct.name = data.name;
			$scope.newproduct.unitRate = data.unitRate;
		    $scope.newproduct.productType = data.productType;
		    $scope.newproduct.imageUrl= image_url;
		    $scope.newproduct.audioUrlWav = audio_url;
		    $scope.newproduct.quantity = data.quantity;
		    $scope.newproduct.status = 0;
		    console.log(image_url);
		    console.log(audio_url);
			ProductCreate.save($scope.newproduct,function(success){
				//$scope.products.push($scope.newproduct);
				createAlert("Success","The new Product was added Successfully.");
				$http.get( API_ADDR + 'api/products/search/byType/'+abbr+'/getList').
				success(function(data, status, headers, config) {
					// Store the data into Angular scope model variable
					for(var i=0; i<data.length; i++) 
						data[i].productType = "--"+data[i].productType+"--"; 
					$scope.products = data;
				}).
				error(function(data, status, headers, config) {
					createAlert("Error Fetching Data","There was some error in response from the remote server.");
				});
			},function(error){
				if (error.status == "409")
					createAlert("Error Adding Product","Product already added. Add a different product");
			});
		}
		
		
		$('#view-image').on('hidden', function () {
		    $scope.imagURL = "";
		});
		
		$scope.change = function(currProductType){
			product = currProductType;
		}
		
		$scope.refreshTable = function(){
			$http.get( API_ADDR + 'api/products/search/byType/'+abbr+'/getList').
			success(function(data, status, headers, config) {
				// Store the data into Angular scope model variable
				for(var i=0; i<data.length; i++) 
					data[i].productType = "--"+data[i].productType+"--"; 
				$scope.products = data;
			}).
			error(function(data, status, headers, config) {
				createAlert("Error Fetching Data","There was some error in response from the remote server.");
			});
			
		}
		
		
		$scope.setImgURL = function(product)
		{
			$scope.imagURL = product.imageUrl;
			console.log($scope.imagURL);
		}
		
		$scope.uploadSpreadsheet = function() {
			console.log("called");
			if ($scope.mySpreadsheet == undefined)
			{
				createAlert("Error Uploading File","No file added. Please choose a file of '.xlsx' file format")
			}
			else {
				
				var formData=new FormData();
				formData.append("file",$scope.mySpreadsheet); //myFile.files[0] will take the file and append in formData since the name is myFile.
				$http({
					method: 'POST',
					url: API_ADDR + 'web/'+abbr+'/uploadsheet', // The URL to Post.
					headers: {'Content-Type': undefined}, // Set the Content-Type to undefined always.
					data: formData,
				
				})
				.success(function(status) {
					console.log("Sheet successfully uploaded.");
					//createAlert("Success","Please reload page to see changes.");
					angular.element($('#add-new-product')).scope().reload();
				})
				.error(function(status) {
					createAlert("Error Uploading File","error "+status);
					if (status == "500")
						createAlert("Error Uploading File","File already present. Choose a different file before uploading.");
				});
				
				$('#upload-spreadsheet-modal').modal('toggle');
				$('#spreadsheet-upload').val("");
				//angular.element($('#upload-spreadsheet-button')).scope().reload();

			}
		}
		
		//upload file for editing product
		

		
		$scope.uploadFileEdit = function(data, callback){
			if ($scope.myProductImageEdit.type != "image/jpeg")
			{
				createAlert("Error Uploading File","Invalid File!! Please choose again. You can only choose a file of '.jpg' image format");
			}
			else
			{
				var formData=new FormData();
				formData.append("file",$scope.myProductImageEdit); //myFile.files[0] will take the file and append in formData since the name is myFile.
				$http({
					method: 'POST',
					url: API_ADDR + 'web/'+abbr+'/uploadpictureEdit', // The URL to Post.
					headers: {'Content-Type': undefined}, // Set the Content-Type to undefined always.
					data: formData,
					transformRequest: function(data, headersGetterFunction) {
						return data;
					}
				})
				.success(function(data, status) {
					console.log("Image successfully uploaded.");
					image_url=data;
					console.log(image_url);
					//return image_url;
					callback(data);
					})
				.error(function(data, status) {
					createAlert("Error Uploading File","error "+status);
					if (status == "500")
						createAlert("Error Uploading File","File already present. Choose a different file before uploading.");
				});
			}
		}
		
		//// upload for add product
		
		$scope.uploadFile = function(data, callback){
			if ($scope.myProductImage.type != "image/jpeg")
			{
				createAlert("Error Uploading File","Invalid File!! Please choose again. You can only choose a file of '.jpg' image format");
			}
			else
			{
				var formData=new FormData();
				formData.append("file",$scope.myProductImage); //myFile.files[0] will take the file and append in formData since the name is myFile.
				$http({
					method: 'POST',
					url: API_ADDR + 'web/'+abbr+'/uploadpicture', // The URL to Post.
					headers: {'Content-Type': undefined}, // Set the Content-Type to undefined always.
					data: formData,
					transformRequest: function(data, headersGetterFunction) {
						return data;
					}
				})
				.success(function(data, status) {
					console.log("Image successfully uploaded.");
					image_url=data;
					console.log(image_url);
					callback(data);
				})
				.error(function(data, status) {
					createAlert("Error Uploading File","error "+status);
					if (status == "500")
						createAlert("Error Uploading File","File already present. Choose a different file before uploading.");
				});
			}
		}
		
		
		//for uploading audio...
		$scope.uploadSound = function(data, callback){
			var datType = $scope.myProductSound.type;
			if ( datType != "audio/x-wav" && datType != "audio/wav" )
			{
				createAlert("Error Uploading File","Invalid File!! Please choose again. You can only choose a file of '.wav' audio format");
			}
			else
			{
				var formData=new FormData();
				formData.append("file",$scope.myProductSound);
				$http({
					method: 'POST',
					url: API_ADDR + 'web/'+abbr+'/uploadaudio',
					headers: {'Content-Type': undefined},
					data: formData,
					transformRequest: function(data, headersGetterFunction) {
						return data;
					}
				})
				.success(function(data, status) {
					console.log("Audio successfully uploaded.");
					audio_url = data;
					console.log(audio_url);
					callback(data);
					})
				.error(function(data, status) {
					createAlert("Error Uploading File","error "+status);
					if (status == "500")
						createAlert("Error Uploading File","File already present. Choose a different file before uploading.");
				});
			}
		}
		
		
		//for editing audio...
		$scope.uploadSoundEdit = function(data,callback){
			var datType = $scope.myProductSoundEdit.type;			
			if ( datType != "audio/x-wav" && datType != "audio/wav")
			{
				//console.log(datType);
				createAlert("Error Uploading File","Invalid File!! Please choose again. You can only choose a file of '.wav' audio format");
			}
			else
			{
				var formData=new FormData();
				formData.append("file",$scope.myProductSoundEdit);
				$http({
					method: 'POST',
					url: API_ADDR + 'web/'+abbr+'/uploadaudioedit',
					headers: {'Content-Type': undefined},
					data: formData,
					transformRequest: function(data, headersGetterFunction) {
						return data;
					}
				})
				.success(function(data, status) {
					console.log("Audio successfully uploaded.");
					audio_url=data;
					console.log(audio_url);
					//return audio_url;
					callback(data);
					})
				.error(function(data, status) {
					createAlert("Error Uploading File","error "+status);
					if (status == "500")
						createAlert("Error Uploading File","File already present. Choose a different file before uploading.");
				});
			}
		}
		
		$("#new-quantity-input").keypress(function(e) {
		    // between 0 and 9
		    if (e.which < 48 || e.which > 57) {
		        return(false);  // stop processing
		    }
		});

		$("#new-price-input").keypress(function(e) {
		    // 46 is a period
		    if (e.which != 46 && (e.which < 48 || e.which > 57)) {
		        return(false);
		    }
		    if (e.which == 46 && this.value.indexOf(".") != -1) {
		        return(false);   // only one decimal allowed
		    }
		});
		
		
		//add new product on clicking the add button
		$("#page-content").on("click", "#add-new-product", function(e) {
			e.preventDefault();
			var flag = 0;
			var price = $.trim($('#new-price-input').val());
			var productType = $.trim($('#new-product-type-input').val());
			var product = $.trim($('#new-product-input').val());
			var quantity = $.trim($('#new-quantity-input').val());
			var sound = $.trim($('#product-sound-file').val());
			var image = $.trim($('#product-image-file').val());

			for(var x = 0; x < angular.element('#add-new-product').scope().products.length; x++){
				if(angular.element('#add-new-product').scope().products[x].name == product){
					flag = 1;
					break;
				}
				//console.log($scope.productTypeList[0].name);
			}
			
			//org=$('#ProductLists').attr('org');
			if(! $.isNumeric(price)||price<0){
				createAlert("Invalid Input","Please enter valid price input as positive numerical value.");
			}/*
			else if (sound == '')
			{
				createAlert("Error Uploading File","No file added. Please choose a file of '.wav' audio format");
			}
			else if (image == '')
			{
				createAlert("Error Uploading File","No file added. Please choose a file of '.jpg' image format");
			}*/
			else if(flag == 1)
				createAlert("Invalid Input","Product already exists.");
			else if(product == ''){
				createAlert("Invalid Input","Please enter a name for Product.");
			}
			else if(productType == ''){
				createAlert("Invalid Input","Please select one of the Product Type(s).");
			}
			else if (! $.isNumeric(quantity)||quantity<0){
				createAlert("Invalid Input","Please enter valid quantity as positive numerical value.");
			}
			else
			{
				var data = {};
				data.name = product;
				data.unitRate = price;
				data.productType = productType;
				data.quantity = quantity;
				//angular.element($('#add-new-product')).scope().uploadSound(data);
				//angular.element($('#add-new-product')).scope().uploadFile(data);
				//setTimeout(function(){
				angular.element($('#add-new-product')).scope().saveProduct(data);
					//},300);
				$('#add-product-modal').modal('toggle');
				/*$('#new-product-input').val("");
				$('#new-product-type-input').val("");
				$('#new-price-input').val("");
				$('#new-quantity-input').val("");
				$("#product-image-file").attr('src', "");
				$("#product-sound-file").attr('src', "");*/
				$('#productImage1').attr('hidden','hidden');
				$('#productSound1').removeAttr('controls');
				$("#addForm")[0].reset();
				//angular.element($('#add-new-product')).scope().reload();
				$http.get( API_ADDR + 'api/products/search/byType/'+abbr+'/getList').
				success(function(data, status, headers, config) {
					// Store the data into Angular scope model variable
					for(var i=0; i<data.length; i++) 
						data[i].productType = "--"+data[i].productType+"--"; 
					$scope.products = data;
				}).
				error(function(data, status, headers, config) {
					createAlert("Error Fetching Data","There was some error in response from the remote server.");
				});
			}	
		});

		$("#page-content").on("click", "#resetEditForm", function () {
			$("#editForm")[0].reset();
		});
		
		$("#page-content").on("click", "#resetAddForm", function () {
			$('#productImage1').attr('hidden','hidden');
			$('#productSound1').removeAttr('controls');
			$("#addForm")[0].reset();
		});
		
		$("#page-content").on("click", "#add-new-product-multi", function (e) {
			e.preventDefault();
			var gridData = hot.getData();
			var flag=0;
			for (var i =0; i < gridData.length-1;++i)
			{
				var product = $.trim(gridData[i][0]);
				var price = $.trim(gridData[i][1]);
				var prodName = $.trim(gridData[i][2]);
				var quantity = $.trim(gridData[i][3]);
				var prodType;
				for (var x =0; x < prodData.products.length;++x)
					{
					if (prodData.products[x].name == prodName)
						{
						prodType = prodData.products[x].id;
						}
					}

				if (product && price && prodType && quantity)
				{
					if(! $.isNumeric(price)||price<0){
						flag=1;
					}
					else if(product == ''){
						flag=1;
					}
					else if (! $.isNumeric(quantity)||quantity<0) {
						flag=1;
					}
					else if(prodType == ""){
						flag=1;
					}
					else {
						var data = {};
						data.name = product;
						data.unitRate = price;
						data.productType="productTypes/"+prodType;		
						data.quantity = quantity;
						angular.element($('#add-new-product-multi')).scope().saveProduct(data);
					}
				}
			}
			
			$('#add-multiple-product-modal').modal('toggle');
			hot.clear();
		    if (flag == 1)
			{
				console.log("Flag: "+flag);
				createAlert("Invalid Input","Errors were present. Not all products were uploaded!");
			}
		    angular.element($('#add-new-product-multi')).scope().reload();	

		});
		
		$scope.displayStatus = function(product) {
			if (this.product.status == 1){
				return "Disable";
			}
			else {
				return "Enable";
			}
		}
		$scope.displayreverseStatus = function(product) {
			if (this.product.status == 0){
				return "Disabled";
			}
			else {
				return "Enabled";
			}
		}
		$scope.modifyClass = function(product) {
			if(this.product.status == 0){
				return "btn btn-danger btn-xs";
			}
			else if (this.product.status == 1) {
				return "btn btn-success btn-xs";
			}
		}
		
		$scope.displayAudio= function(product) {
			if(this.product.audioUrlWav == null){
				return "audio-hide";
			}
			else {
				return "audio-show";
			}
		}
		
		$scope.editCurrentProductQuantity = function(product) {
			console.log("inside");
			$scope.id = this.product.productId;
			$scope.qty = parseFloat(this.product.quantity);
			$scope.updateQuantity = function() {
				console.log("processing");
				var incr =parseFloat($.trim($('#add-quantity-input').val()));
				var newQty = incr + $scope.qty;
				console.log("new quantity: "+newQty);
				$scope.editproduct = ProductEdit.get({id:$scope.id},function(){
					$scope.editproduct.quantity=newQty;
					$scope.editproduct.$update({id:$scope.id},function(){
						product.quantity = $scope.editproduct.quantity;
					});
				});
				$("#quantity-product-modal").modal('toggle');
				$('#add-quantity-input').val("");
			}
			
		}
		
		$("#update-quantity-input").keypress(function(e) {
		    // between 0 and 9
		    if (e.which < 48 || e.which > 57) {
		        return(false);  // stop processing
		    }
		});

		$("#update-product-input").keypress(function(e) {
		    // 46 is a period
		    if (e.which != 46 && (e.which < 48 || e.which > 57)) {
		        return(false);
		    }
		    if (e.which == 46 && this.value.indexOf(".") != -1) {
		        return(false);   // only one decimal allowed
		    }
		});
		
		//function to edit product
		$scope.editCurrentProduct = function(product){
			
			$scope.id = this.product.productId;
			var stat = parseInt(this.product.status);
			var image = 0;
			var audio = 0;
			/*$(".modal-header #HeadingEdit").html("Edit Product");
			$(".modal-body #update-product-input").html("Price");
			$(".modal-body #update-product-list-input-name").html("Name");
			$(".modal-body #update-quantity-input").html("Modify Quantity");
			if (stat == 1) {
				$(".modal-footer #toggle-product").html("Disable");
			}
			else if (stat == 0) {
				$(".modal-footer #toggle-product").html("Enable");
			}*/
			var oldname = this.product.name;
			var oldprice = this.product.unitRate;
			var oldqty = this.product.quantity;
			var oldimage = this.product.imageUrl;
			var oldaudio = this.product.audioUrlWav;
			$("#update-product-name-input").val(oldname);
			$("#update-price-input").val(oldprice);
			$("#update-product-quantity-input").val(oldqty);
			$("#productImage").attr('src' ,oldimage);
			$("#productSound").attr('src' ,oldaudio);
			
			if(this.product.imageUrl == "" || this.product.imageUrl == null || this.product.imageUrl == undefined){
				//console.log("Wow!   !"+this.product.imageUrl+"!"+(this.product.imageUrl == ""));
				$("#remove-image").attr('style','display:none;');
			}
			
			else{
				//console.log("Wow1!   !"+this.product.imageUrl+"!"+(this.product.imageUrl == ""));
				$("#remove-image").removeAttr('style');
			}
			
			if(this.product.audioUrlWav == null || this.product.audioUrlWav == "" || this.product.audioUrlWav == undefined){
				//console.log("Wow2!  !"+this.product.audioUrlWav+"!"+(this.product.audioUrlWav == ""));
				$("#remove-audio").attr('style','display:none;');
			}


			else{
				//console.log("Wow3!   !"+this.product.audioUrlWav+"!"+(this.product.audioUrlWav == ""));
				$("#remove-audio").removeAttr('style');
			}
			$scope.updateEditCurrentProduct = function() {
				var flag1 = 1;
				var flag2 = 1;
				var flag = 0;
				newprice = $.trim($('#update-price-input').val());
				newname  = $.trim($('#update-product-name-input').val());
				newQty = $.trim($('#update-product-quantity-input').val());
				if(image == 0)
					newurl = $('#productImage').attr('src');	
				else
					newurl = "";
				if(audio == 0)
					newsoundurl = $('#productSound').attr('src');	
				else
					newsoundurl = "";
				console.log("new url: "+newurl);
				console.log("new audio url: "+newsoundurl);	
				/*if($.trim($('#product-image-file-edit').val()).localeCompare('') != 0){
					flag1 = 0;
					angular.element($('#add-new-product')).scope().uploadFileEdit(data,function(data1){
						newurl = data1;
					});
				}
				if($.trim($('#product-sound-file-edit').val()).localeCompare('') != 0){
					flag2 = 0;
					angular.element($('#add-new-product')).scope().uploadSoundEdit(data,function(data1){
						newsoundurl = data1;
					});
				}
				console.log("new qty: "+newQty);*/
				for(var x = 0; x < angular.element('#add-new-product').scope().products.length; x++){
					if(angular.element('#add-new-product').scope().products[x].name == newname){
						flag = 1;
						break;
					}
					//console.log($scope.productTypeList[0].name);
				}
				if(! $.isNumeric(newprice) || newprice < 0){
					createAlert("Invalid Input","Enter valid price.");
				}
				else if(flag == 1 && newname != oldname)
					createAlert("Invalid Input","Product already exists.");
				else if(! $.isNumeric(newQty) || newQty < 0 )
				{
					createAlert("Invalid Input","Enter valid quantity.");
				}
				else if((newname == oldname) &&
						(newprice == oldprice) &&
						(newQty == oldqty) &&
						(newurl == oldimage) &&
						(newsoundurl == oldaudio))
					createAlert("Alert","Nothing to change.");
				else
				{
					var data = {};
					data.name = product;
					$scope.editproduct = ProductEdit.get({id:$scope.id},function(){
						//setTimeout(function() {
							$scope.editproduct.unitRate = newprice;
							$scope.editproduct.name = newname;
							$scope.editproduct.quantity = newQty;
							//if(flag1 == 0)
								$scope.editproduct.imageUrl = newurl;
							//if(flag2 == 0)
								$scope.editproduct.audioUrlWav = newsoundurl;
							$scope.editproduct.$update({id:$scope.id},function(){
								product.unitRate = $scope.editproduct.unitRate;
								//console.log(product.unitRate);
								product.name = $scope.editproduct.name;
								product.quantity = $scope.editproduct.quantity;
								//console.log('1 '+$scope.editproduct.imageUrl);
								//if(flag1 == 0)
									product.imageUrl = $scope.editproduct.imageUrl;
								//if(flag2 == 0)
									product.audioUrlWav = $scope.editproduct.audioUrlWav;
								createAlert('Success','Successfully updated data.');
								$http.get( API_ADDR + 'api/products/search/byType/'+abbr+'/getList').
								success(function(data, status, headers, config) {
									// Store the data into Angular scope model variable
									for(var i=0; i<data.length; i++) 
										data[i].productType = "--"+data[i].productType+"--"; 
									$scope.products = data;
								}).
								error(function(data, status, headers, config) {
									createAlert("Error Fetching Data","There was some error in response from the remote server.");
								});
							});
						//}, 500);
					});
					
					
					$("#edit-product-modal").modal('toggle');
					$('#update-price-input').val("");
					$('#update-product-name-input').val("");
					$("#editForm")[0].reset();
				
					/*$('#productImage').attr('hidden','hidden');
					$('#description').attr('src','');
					$('#productSound').attr('src','');*/
					//angular.element($('#add-new-product')).scope().reload();
				}
			
			}
			
			$scope.setDesc = function(){
				prodId = $scope.id;
				$http.get( API_ADDR + 'api/products/search/byType/'+prodId+'/getDesc').
				success(function(data, status, headers, config) {
					// Store the data into Angular scope model variable
					console.log(data.desc);
					$('#description').val(data.desc);
				}).
				error(function(data, status, headers, config) {
					createAlert("Error Fetching Data","There was some error in response from the remote server.");
				});
			}
			
			$scope.removeAudio = function(){
				$scope.editproduct = ProductEdit.get({id:$scope.id},function(){
					$scope.editproduct.audioUrlWav = null;
					$scope.editproduct.$update({id:$scope.id},function(){
						product.audioUrlWav = $scope.editproduct.audioUrlWav;
					});
					audio = 1;
					flagModal = 1;
					$('#productSound').removeAttr('src');					
					$("#remove-audio").attr('style','display:none;');
				});
			}
			
			$scope.removeImage = function(){
				$scope.editproduct = ProductEdit.get({id:$scope.id},function(){
					$scope.editproduct.imageUrl = null;
					$scope.editproduct.$update({id:$scope.id},function(){
						product.imageUrl = $scope.editproduct.imageUrl;
					});
					image = 1;
					flagModal = 1;
					$('#productImage').removeAttr('src');
					$("#remove-image").attr('style','display:none;');
				});
			}
			
			$("#edit-product-modal").on("hidden", function () {
				if(flagModal == 1){
					$http.get( API_ADDR + 'api/products/search/byType/'+abbr+'/getList').
					success(function(data, status, headers, config) {
						// Store the data into Angular scope model variable
						for(var i=0; i<data.length; i++) 
							data[i].productType = "--"+data[i].productType+"--"; 
						$scope.products = data;
					}).
					error(function(data, status, headers, config) {
						createAlert("Error Fetching Data","There was some error in response from the remote server.");
					});
					flagModal = 0;
				}
			});
			
			
			$scope.toggleCurrentProduct = function() {
				
				
				var currentStat = (stat+1)%2;
				$scope.editproduct = ProductEdit.get({id:$scope.id},function(){
					$scope.editproduct.status=currentStat;
					$scope.editproduct.$update({id:$scope.id},function(){
						product.status = $scope.editproduct.status;
					});
				});
				$("#edit-product-modal").modal('toggle');
			}	
		}
		
		$scope.enableDisableCurrentProduct= function(product){
			var stat=parseInt(this.product.status);
			var currentStat = (stat+1)%2;
			$scope.id = this.product.productId;
			$scope.editproduct = ProductEdit.get({id:$scope.id},function(){
				$scope.editproduct.status=currentStat;
				$scope.editproduct.$update({id:$scope.id},function(){
					product.status = $scope.editproduct.status;
				});
			});

		}
		// Worst try but worth it!
		$scope.enableCurrentProduct= function(product){
			var stat=parseInt(this.product.status);
			var currentStat = (stat+1)%2;
			console.log(currentStat);
			var idProd = this.product.productId;
			$scope.id = this.product.productId;
			$scope.editproduct = ProductEdit.get({id:$scope.id},function(){
				$scope.editproduct.status=currentStat;
				$scope.editproduct.$update({id:$scope.id},function(){
					product.status = $scope.editproduct.status;
					$http.get( API_ADDR + 'api/changeTypeStatus/'+idProd).
					success(function(data, status, headers, config) {
						// Store the data into Angular scope model variable
						console.log("done");
					}).
					error(function(data, status, headers, config) {
						createAlert("Error Fetching Data","There was some error in response from the remote server.");
					});
					console.log(product.status);
				});
				
			});
		}
		$scope.updateDesc = function(){
			var data = {};
			data.desc = $.trim($('#description').val());
			$http.post( API_ADDR + 'api/products/search/byType/'+prodId+'/setDesc', data).
			success(function(data, status, headers, config) {
				// Store the data into Angular scope model variable
				prodId = 0;
				$("#description").val('');				
				$("#set-description").modal('toggle');
			}).
			error(function(data, status, headers, config) {
				createAlert("Error Fetching Data","There was some error in response from the remote server.");
			});
		}
		
		$scope.disableCurrentProduct= function(product){
			var stat=parseInt(this.product.status);
			var currentStat = (stat+1)%2;
			$scope.id = this.product.productId;
			$scope.editproduct = ProductEdit.get({id:$scope.id},function(){
				$scope.editproduct.status=currentStat;
				$scope.editproduct.$update({id:$scope.id},function(){
					product.status = $scope.editproduct.status;
				});
			});
			

		}
		
		// The worst try ends here
		
		$scope.downloadSpreadsheet = function () {
			var abbr = $('#organizationAbbr').val();
			console.log(API_ADDR+'web/'+abbr+'/generatesheet');
			$http.get(API_ADDR+'web/'+abbr+'/generatesheet')
			.success(function () {

				/* //bestB
				window.location="http://best-erp.com/extras/spreadsheets/"+abbr+"-products.xlsx";
				*/ //bestE
				
				//ictB
				window.location="http://ruralict.cse.iitb.ac.in/Downloads/spreadsheets/"+abbr+"-products.xlsx";
				//ictE
				console.log("success!");
				
			})
			.error(function() {
				console.log("error");
			});	
		}
		
		
		$scope.globalEnable = function() {
			var abbr = $('#organizationAbbr').val();
			var toggleStatus=1;
			console.log(API_ADDR+'web/'+abbr+'/statusToggle?status='+toggleStatus)
			$http.get(API_ADDR+'web/'+abbr+'/statusToggle?status='+toggleStatus)
			.success(function (res) {
				$route.reload();
			})
			.error(function() {
				console.log("error");
			});	
		}
		
		$scope.globalDisable = function() {
			var abbr = $('#organizationAbbr').val();
			var toggleStatus=0;
			console.log(API_ADDR+'web/'+abbr+'/statusToggle?status='+toggleStatus)
			$http.get(API_ADDR+'web/'+abbr+'/statusToggle?status='+toggleStatus)
			.success(function (res) {
				$route.reload();
			})
			.error(function() {
				console.log("error");
			});	
		}

		
		////// product enable disable

		$scope.productEnable = function() {
			var abbr = $('#organizationAbbr').val();
			var toggleStatus=1;
			console.log(API_ADDR+'web/'+abbr+'/statusToggle?status='+toggleStatus)
			$http.get(API_ADDR+'web/'+abbr+'/statusToggle?status='+toggleStatus)
			.success(function (res) {
				$route.reload();
			})
			.error(function() {
				console.log("error");
			});	
		}
		
		$scope.productDisable = function() {
			var abbr = $('#organizationAbbr').val();
			var toggleStatus=0;
			console.log(API_ADDR+'web/'+abbr+'/statusToggle?status='+toggleStatus)
			$http.get(API_ADDR+'web/'+abbr+'/statusToggle?status='+toggleStatus)
			.success(function (res) {
				$route.reload();
			})
			.error(function() {
				console.log("error");
			});	
		}

		
		
		
		
		//////
		
		
		$scope.displaySpreadsheet = function() {
			if (counter == 0) {
			var stuff = [[]];
			var container = document.getElementById('spreadsheet');
			var names = [];
			var orgid = $('#organizationId').val();
			var abbr = $('#organizationAbbr').val();
			$http.get(API_ADDR+'web/'+abbr+'/prodtypes')
			.success(function(results){
				prodData = results;
				for (var i=0;i<results.products.length;++i)
					{
					names[i]=results.products[i].name;
					}
				hot = new Handsontable(container, {
					  data: stuff,
					  minRows: 10,
					  minCols: 4,
					  minSpareRows: 1,
					  rowHeaders: false,
					  colHeaders: ['Product Name','Price (Per Unit)', 'Product Type','Quantity'],
					  columns: [
					            {},
					            {type: 'numeric'},
					            {
					              type: 'dropdown',
					              source: names
					            },
					            {type: 'numeric'}
					            
					          ],
					  contextMenu: true,
					  colWidths :120
					  
				}); 
			})
			.error(function() {
			    console.log( "error" );
			});
			++counter;
			}	
		}
		
		//function to delete product
		$scope.deleteCurrentProduct = function(product){
			
			$scope.id = this.product.productId;
			
			$scope.yesButtonDeleteProduct = function()
			{
				$scope.deleteproduct = ProductDelete.get({id:$scope.id},function(){				
					$scope.deleteproduct.$update({id:$scope.id},function(){
						
						// refetch the data
						$http.get( API_ADDR + 'api/products/search/byType/'+abbr+'/getList').
						success(function(data, status, headers, config) {
							// Store the data into Angular scope model variable
							for(var i=0; i<data.length; i++) 
								data[i].productType = "--"+data[i].productType+"--"; 
							$scope.products = data;
						}).
						error(function(data, status, headers, config) {
							createAlert("Error Fetching Data","There was some error in response from the remote server.");
						});
						
						$("#delete-product-modal").modal('toggle');
						
					},function(error){
						if(error.status == "409")
							createAlert("Error Deleting Product","You can't delete this product.");
					});
				});	
			}
		}
		
		//function to fetch list of products
		$scope.getList = function(){
			$http.get( API_ADDR + 'api/products/search/byType/'+abbr+'/getList').
			success(function(data, status, headers, config) {
				// Store the data into Angular scope model variable
				for(var i=0; i<data.length; i++) 
					data[i].productType = "--"+data[i].productType+"--"; 
				$scope.products = data;
			}).
			error(function(data, status, headers, config) {
				createAlert("Error Fetching Data","There was some error in response from the remote server.");
			});
		}
		
		//TODO hard refresh has to be eliminated
		$scope.reload = function(){
			setTimeout($window.location.reload.bind(window.location),2000);
		}
		
		$scope.fileAdded = function(input){	
		    if (input.files && input.files[0]) {
				var reader = new FileReader();
				//var state = reader.readyState;
				reader.onload = function() {
					angular.element($('#add-new-product')).scope().uploadFile('',function(data1){
						$('#productImage1')
		                .attr('src', data1)
		                .width(60)
		                .removeAttr('hidden');
					});
				};
				/*if(state == FileReader.EMPTY){
					$('#productImage1')
	                .attr('alt', 'Please wait...');
				}
				/*if(state == FileReader.Done){
					$('#productImage1')
	                .attr('alt', 'Done');
				}
					*/
		        reader.readAsDataURL(input.files[0]);
		    }
		}
		
		$scope.soundAdded = function(input){	
		    if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function() {
					angular.element($('#add-new-product')).scope().uploadSound('',function(data1){
						$('#productSound1')
		                .attr('src', data1)
		                .attr('controls', 'controls');
					});
				};
		        reader.readAsDataURL(input.files[0]);
		    }
		}
		
		$scope.fileEdited = function(input){
		    if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function() {
					angular.element($('#add-new-product')).scope().uploadFileEdit('',function(data1){
						$('#productImage')
		                .attr('src', data1)
		                .width(60);
					});
				};
		        reader.readAsDataURL(input.files[0]);
		    }
		}
		
		$scope.soundEdited = function(input){	
		    if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function() {
					angular.element($('#add-new-product')).scope().uploadSoundEdit('',function(data1){
						$('#productSound')
		                .attr('src', data1);
					});
				};
		        reader.readAsDataURL(input.files[0]);
		    }
		}
		
		$scope.uploadSheet = function(input){
		    if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function() {
					angular.element($('#add-new-product')).scope().uploadSpreadsheet();
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		
});


/*function readSheetURL(input) {
	if (input.files && input.files[0]) {
	    var reader = new FileReader();
	
	    reader.onload = function (e) {
	    	 $('#spreadsheet-upload')
	    	 	.val(input.files[0].name);
	    };
	    
	    reader.readAsDataURL(input.files[0]);
	}
}

/*function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#productImage')
                .attr('src', e.target.result)
                .width(50);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readSoundURL(ip){
	if (ip.files && ip.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#productSound')
                .attr('src', e.target.result)
                .attr('controls', 'controls');
        };

        reader.readAsDataURL(ip.files[0]);
    }	
}

function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#productImage1')
                .attr('src', e.target.result)
                .width(50);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function readSoundURL1(ip){
	if (ip.files && ip.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#productSound1')
                .attr('src', e.target.result)
                .attr('controls', 'controls');
        };

        reader.readAsDataURL(ip.files[0]);
    }	
}
/*angular.module('myApp', []).config(function($sceDelegateProvider) {
	 $sceDelegateProvider.resourceUrlWhitelist([
	   'http://ruralict.cse.iitb.ac.in/Downloads/audio/*']);
});*/