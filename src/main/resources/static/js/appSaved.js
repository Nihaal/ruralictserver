/**
 * This file contains all the functionalities related to delivery of app
 */


website.controller("AppSaveCtrl", function($scope, $http, $route, $routeParams) {
	
	// Get Organization Abbreviation from Thymeleaf
	var abbr = $('#organizationAbbr').val();
	var product = null;
	var changeFlag = 0;

	// Initialize the table 
	$http.get( API_ADDR + 'api/' + abbr + '/getSavedList').
		success(function(data, status, headers, config) {

			// Store the data into Angular scope model variable
			$scope.manageSavedItems = data;
			console.log("done");
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});
	
	$scope.refreshTable = function(){
		$http.get( API_ADDR + 'api/' + abbr + '/getSavedList').
		success(function(data, status, headers, config) {

			// Store the data into Angular scope model variable
			$scope.manageSavedItems = data;
			console.log("done");
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});
	}
	
	/*$http.get( API_ADDR + 'api/' + abbr + '/getProductList').
		success(function(data, status, headers, config) {
	
			// Store the data into Angular scope model variable
			$scope.manageProducts = data;
			console.log("done");
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});*/
	
	$scope.viewOrder = function($event, manageSavedItem){
		$event.preventDefault();
		$scope.modalItem = manageSavedItem;
		$http.get( API_ADDR + 'api/' + manageSavedItem.orderId + '/getItemsList').
		success(function(data, status, headers, config) {

			// Store the data into Angular scope model variable
			$scope.orderItems = data;
			console.log("done");
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});
	}
	
	$scope.change = function(currProduct){
		product = currProduct;
	}
	
	$scope.changeQty = function($event, orderItem, qty, modalItem){
		$event.preventDefault();
		$http.get( API_ADDR + 'api/order/' + modalItem.orderId + '/getStatus').
		success(function(data, status, headers, config) {
			if(data.value == 'saved'){
				if(qty.length == 0 || isNaN(qty) || qty <= 0 || qty == orderItem.quantity)
					createAlert('Error','Please enter a valid quantity');
				else{
					var pData = {};
					pData.orderId = modalItem.orderId;
					pData.itemId = orderItem.id;
					pData.prodId = orderItem.prodId;
					pData.qty = qty;
					$http.post( API_ADDR + 'api/' + abbr + '/changeOrderItem', pData).
					success(function(data, status, headers, config) {
						// Store the data into Angular scope model variable
						console.log("done");
						if(data.status == 'Success'){
							if(data.val == -1){
								changeFlag = 1;
								$http.get( API_ADDR + 'api/' + modalItem.orderId + '/getItemsList').
								success(function(data, status, headers, config) {
		
									// Store the data into Angular scope model variable
									$scope.orderItems = data;
									console.log("done");
								}).
								error(function(data, status, headers, config) {
									createAlert("Error Fetching Data","There was some error in response from the remote server.");
								});
							}
							else if(data.val == 0){
								createAlert("Alert", "The product is out of stock.");
							}
							else{
								createAlert("Alert", "The maximum quantity of the product is "+data.val);
							}
						}
					}).
					error(function(data, status, headers, config) {
						createAlert("Error Fetching Data","There was some error in response from the remote server.");
					});
				}
			}
			else{
				$('#view-message-modal').modal('hide');
				$http.get( API_ADDR + 'api/' + abbr + '/getSavedList').
				success(function(data, status, headers, config) {

					// Store the data into Angular scope model variable
					$scope.manageSavedItems = data;
					console.log("done");
				}).
				error(function(data, status, headers, config) {
					createAlert("Error Fetching Data","There was some error in response from the remote server.");
				});
				createAlert("Alert","Order status has already been changed.");
			}
		}).
		error(function(data, status, headers, config) {
			createAlert("Error","There was some error in response from the remote server.");
		});
	}
	
	/*$scope.addOrderItem = function($event, modalItem){
		$event.preventDefault();
		console.log(product.prodId+"   "+product.stock);
		var qty = $.trim($("#savedAppQuantity").val());
		//console.log(qty +"   "+isNaN(qty));
		if(product == null)
			createAlert('Error','Please select a product to add');
		else if(qty.length == 0 || isNaN(qty) || qty <= 0)
			createAlert('Error','Please enter a valid quantity');
		else{

			if(product.stock){
				if(qty > product.quantity)
					createAlert('Error','Maximum available quantity for the product is '+product.quantity);
				else{
					var pData = {};
					pData.orderId = modalItem.orderId;
					pData.prodId = product.prodId;
					pData.qty = qty;
					pData.rate = product.rate;
					$http.post( API_ADDR + 'api/' + abbr + '/addOrderItem', pData).
					success(function(data, status, headers, config) {
						// Store the data into Angular scope model variable
						console.log("done");
						$http.get( API_ADDR + 'api/' + modalItem.orderId + '/getItemsList').
						success(function(data, status, headers, config) {

							// Store the data into Angular scope model variable
							$scope.orderItems = data;
							console.log("done");
						}).
						error(function(data, status, headers, config) {
							createAlert("Error Fetching Data","There was some error in response from the remote server.");
						});
					}).
					error(function(data, status, headers, config) {
						createAlert("Error Fetching Data","There was some error in response from the remote server.");
					});
				}
			}
			else{
				var pData = {};
				pData.orderId = modalItem.orderId;
				pData.prodId = product.prodId;
				pData.qty = qty;
				pData.rate = product.rate;
				$http.post( API_ADDR + 'api/' + abbr + '/addOrderItem', pData).
				success(function(data, status, headers, config) {
					// Store the data into Angular scope model variable
					console.log("done");
					$http.get( API_ADDR + 'api/' + modalItem.orderId + '/getItemsList').
					success(function(data, status, headers, config) {

						// Store the data into Angular scope model variable
						$scope.orderItems = data;
						console.log("done");
					}).
					error(function(data, status, headers, config) {
						createAlert("Error Fetching Data","There was some error in response from the remote server.");
					});
				}).
				error(function(data, status, headers, config) {
					createAlert("Error Fetching Data","There was some error in response from the remote server.");
				});				
			}			
		}
	}*/
	
	$scope.removeOrderItem = function($event, orderItem, orderItems, modalItem){
		$event.preventDefault();
		$http.get( API_ADDR + 'api/order/' + modalItem.orderId + '/getStatus').
		success(function(data, status, headers, config) {
			//console.log(orderItems.length);
			if(orderItems.length == 1)
				createAlert("Alert","Removing this item will delete the order. Please delete the order if you wish to do so.");
			else{
				if(data.value == 'saved'){
					$http.get( API_ADDR + 'api/order/' + abbr +'/'+ orderItem.id + '/deleteItem').
					success(function(data, status, headers, config) {
						changeFlag = 1;
						// Store the data into Angular scope model variable
						$http.get( API_ADDR + 'api/' + modalItem.orderId + '/getItemsList').
						success(function(data, status, headers, config) {
			
							// Store the data into Angular scope model variable
							$scope.orderItems = data;
							console.log("done");
						}).
						error(function(data, status, headers, config) {
							createAlert("Error Fetching Data","There was some error in response from the remote server.");
						});
					}).
					error(function(data, status, headers, config) {
						createAlert("Error","There was some error in response from the remote server.");
					});
				}
				else{
					$('#view-message-modal').modal('hide');
					$http.get( API_ADDR + 'api/' + abbr + '/getSavedList').
					success(function(data, status, headers, config) {
	
						// Store the data into Angular scope model variable
						$scope.manageSavedItems = data;
						console.log("done");
					}).
					error(function(data, status, headers, config) {
						createAlert("Error Fetching Data","There was some error in response from the remote server.");
					});
					createAlert("Alert","Order status has already been changed.");
				}
			}
		}).
		error(function(data, status, headers, config) {
			createAlert("Error","There was some error in response from the remote server.");
		});
	}
	
	
	$scope.notifyChange = function(modalItem){
		if(changeFlag == 1){
			var sData = {};
			sData.orderId = modalItem.orderId;
			$http.post( API_ADDR + 'api/' + abbr + '/notifyChange', sData).
			success(function(data, status, headers, config) {
				changeFlag = 0;
				createAlert("Alert","Order no. "+ modalItem.orderId +" has been modified.");
			}).
			error(function(data, status, headers, config) {
				createAlert("Error","There was some error in response from the remote server.");
			});
		}
	}
	
	
	$scope.deleteItem = function($event, manageSavedItem){
		$http.get( API_ADDR + 'api/order/' + manageSavedItem.orderId + '/getStatus').
		success(function(data, status, headers, config) {
			if(data.value == 'saved'){
				$http.get( API_ADDR + 'api/order/' +  abbr +'/'+ manageSavedItem.orderId + '/delete').
				success(function(data, status, headers, config) {
					createAlert("Success","Deleted order no. "+manageSavedItem.orderId);
					manageSavedItem.stat = 1;
				}).
				error(function(data, status, headers, config) {
					createAlert("Error","There was some error in response from the remote server.");
				});
			}
			else{
				manageSavedItem.stat = 1;				
				createAlert("Alert","Order status has already been changed.");
			}
		}).
		error(function(data, status, headers, config) {
			createAlert("Error","There was some error in response from the remote server.");
		});
	}
	
	$scope.processOrder = function($event, manageSavedItem){
		$http.get( API_ADDR + 'api/order/'+ manageSavedItem.orderId + '/getStatus').
		success(function(data, status, headers, config) {
			if(data.value == 'saved'){
				$http.get( API_ADDR + 'api/orders/changestate/processed/' + manageSavedItem.orderId).
				success(function(data, status, headers, config) {
					manageSavedItem.stat = 1;
					createAlert("Success","Order no. "+ manageSavedItem.orderId +" has been processed.");
					console.log("done");
				}).
				error(function(data, status, headers, config) {
					createAlert("Error","There was some error in response from the remote server.");
				});
			}
			else{
				manageSavedItem.stat = 1;				
				createAlert("Alert","Order status has already been changed.");
			}
		}).
		error(function(data, status, headers, config) {
			createAlert("Error","There was some error in response from the remote server.");
		});
	}

});


$("#page-content").on("click", ".saved-app-modal-close", function (e) {
	e.preventDefault();
	$('#savedAppProductName option').prop('selected', function() {
        return this.defaultSelected;
    });
	$('#savedAppQuantity').val('');
});