website.controller('OrderSummaryProductsController', function($scope, $http, $route, ShowOrderSummaryProducts, ShowOrderSummaryOrg) {
		$('#fromProductsDate').datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true
		});
		$('#toProductsDate').datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true
		});
		$scope.orderSummaries = function(data){
		document.getElementById("totalProducts").innerHTML=0;
		$scope.orderSummariesProduct =  ShowOrderSummaryProducts.update(data, function(orderSummaries){
				var locTotal=0;
				for(var i=0; i<orderSummaries.length; i++){
					locTotal+=(parseFloat(orderSummaries[i].collection));
				}
				document.getElementById("totalProducts").innerHTML="₹ " + locTotal;
		  	});
		};
		$scope.orderSummariesByOrg = function(data){
			document.getElementById("totalProducts").innerHTML=0;
			$scope.orderSummariesProduct =  ShowOrderSummaryOrg.update(data, function(orderSummariesByOrg){
				var locTotal=0;
				for(var i=0; i<orderSummariesByOrg.length; i++){
					locTotal+=((parseFloat(orderSummariesByOrg[i].collection)));
				}
				document.getElementById("totalProducts").innerHTML="₹ " + (Math.round(locTotal * 100) / 100);
		  	});
		};
		/*$scope.downloadSheet = function(){
			
		}*/

		$("#page-content").on("click", "#download-sheet", function(e) {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1;
			var yyyy = today.getFullYear();
			if(dd<10) {
			    dd='0'+dd
			} 
			if(mm<10) {
			    mm='0'+mm
			} 
			today = yyyy+'-'+mm+'-'+dd;
		    console.log(today);
			var organization = $('#orgAbbrevation').attr('organizationId');
		    console.log(organization);
			$http.get(API_ADDR+'web/'+organization+'/'+today+'/generateSummary')
			.success(function(){
				/* //bestB
				window.location="http://best-erp.com/extras/spreadsheets/"+organization+"-summary.xlsx";
				*/ //bestE
				//ictB
				window.location="http://ruralict.cse.iitb.ac.in/Downloads/spreadsheets/"+organization+"-summary.xlsx";
				//ictE
			})
			.error(function() {
				console.log("error");
			});		
		});
});

      
$("#page-content").on("click", "#submitProducts", function(e) {
    e.preventDefault();
    var date = new Date();
    var product= $.trim($('#product').val());
    var from= $.trim($('#fromProductsDate').val());
    var to= $.trim($('#toProductsDate').val());
    var to2= new Date(to);
    var to1 = new Date(to2.getFullYear(),to2.getMonth(),to2.getDate(),0,0,0,0);
    if(from=="") createAlert("Invalid Input","Please select(type) a valid From date in yyyy-mm-dd format");
    else if(to=="") createAlert("Invalid Input","Please select(type) a valid To date in yyyy-mm-dd format");
    else if(validatedate(from)==false)	createAlert("Invalid Input","Please select(type) a valid From date in yyyy-mm-dd format");
    else if(validatedate(to)==false)	createAlert("Invalid Input","Please select(type) a valid To date in yyyy-mm-dd format");
    else if(to<from)	createAlert("Invalid Input","To date should be ahead of From date!");
    else if(to1>date) createAlert("Invalid Input","To date cannot be ahead of today.");
    else if(product == "") createAlert("Invalid Input","Please select a product from Product Name!");
    else if(product == "All"){
    	var data={};
	    data.fromDate=from;
	    data.toDate=to;
    	data.organization=$('#orgAbbrevation').attr('organizationId');
    	angular.element($('#submitProducts')).scope().orderSummariesByOrg(data);
    }
    else
    {
    	var data={};
	    data.product=product;
	    data.fromDate=from;
	    data.toDate=to;
	    angular.element($('#submitProducts')).scope().orderSummaries(data);
    }
    
});