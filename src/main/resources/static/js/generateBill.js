function redirectForGenerateBill(){
    //e.preventDefault();
    var date = new Date();
    var product= $.trim($('#product').val());
    var fromDate= $.trim($('#fromBillDate').val());
    var toDate= $.trim($('#toBillDate').val());
    var to2= new Date(toDate);
    var to1 = new Date(to2.getFullYear(),to2.getMonth(),to2.getDate(),0,0,0,0);
    //console.log(to1);
    if(fromDate=="") createAlert("Invalid Input","Please select(type) a valid From date in yyyy-mm-dd format");
    else if(toDate=="") createAlert("Invalid Input","Please select(type) a valid To date in yyyy-mm-dd format");
    else if(validatedate(fromDate)==false)	createAlert("Invalid Input","Please select(type) a valid From date in yyyy-mm-dd format");
    else if(validatedate(toDate)==false)	createAlert("Invalid Input","Please select(type) a valid To date in yyyy-mm-dd format");
    else if(toDate<fromDate)	createAlert("Invalid Input","To date should be ahead of From date!");
    else if(new Date(to1)>new Date()) createAlert("Invalid Input","To date cannot be ahead of today.");
    else{
		var url = "generateBillGroup/"+fromDate+"/"+toDate;
		var win = window.open(url, '_blank');
		win.focus();
    }	
}
