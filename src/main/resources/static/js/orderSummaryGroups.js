website.controller('OrderSummaryGroupsController', function($scope, $route, ShowOrderSummaryGroups, ShowOrderSummaryOrg) {
	$('#fromGroupsDate').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true
	});
	$('#toGroupsDate').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true
	});
	$scope.orderSummaries = function(data){
		document.getElementById("totalAmount").innerHTML=0;
		$scope.orderSummariesGroup =  ShowOrderSummaryGroups.update(data, function(orderSummaries){
			var locTotal=0;
			for(var i=0; i<orderSummaries.length; i++){
				locTotal+=((parseFloat(orderSummaries[i].collection)));
			}
			document.getElementById("totalAmount").innerHTML="₹ " + (Math.round(locTotal * 100) / 100);
	  	});
	};
	$scope.orderSummariesByOrg = function(data){
		document.getElementById("totalAmount").innerHTML=0;
		$scope.orderSummariesGroup =  ShowOrderSummaryOrg.update(data, function(orderSummariesByOrg){
				var locTotal=0;
				for(var i=0; i<orderSummariesByOrg.length; i++){
					locTotal+=((parseFloat(orderSummariesByOrg[i].collection)));
				}
				document.getElementById("totalAmount").innerHTML="₹ " + (Math.round(locTotal * 100) / 100);
		  	});
		};
});
      
$("#page-content").on("click", "#submitGroups", function(e) {
    e.preventDefault();
    var date = new Date();
    var product= $.trim($('#product').val());
    var from= $.trim($('#fromGroupsDate').val());
    var to= $.trim($('#toGroupsDate').val());
    var to2= new Date(to);
    var to1 = new Date(to2.getFullYear(),to2.getMonth(),to2.getDate(),0,0,0,0);
    if(from=="") createAlert("Invalid Input","Please select(type) a valid From date in yyyy-mm-dd format");
    else if(to=="") createAlert("Invalid Input","Please select(type) a valid To date in yyyy-mm-dd format");
    else if(validatedate(from)==false)	createAlert("Invalid Input","Please select(type) a valid From date in yyyy-mm-dd format");
    else if(validatedate(to)==false)	createAlert("Invalid Input","Please select(type) a valid To date in yyyy-mm-dd format");
    else if(to<from)	createAlert("Invalid Input","To date should be ahead of From date!");
   // else if(group == "") createAlert("Invalid Input","Please select a group from Group Name!");
    else if(to1>date) createAlert("Invalid Input","To date cannot be ahead of today.");
    else{
    	var data={};
	    //data.group=group;
	    data.fromDate=from;
	    data.toDate=to;
    	data.organization=$('#OrderSummaryGroup').attr('organizationId');
    	angular.element($('#submitGroups')).scope().orderSummariesByOrg(data);
    }
   });