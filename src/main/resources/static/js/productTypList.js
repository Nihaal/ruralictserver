website.controller("ProductsTypeCtrl",function($window, $scope, $http, $route, $location, ProductTypeCreate, ProductTypeEdit, ProductTypeDelete) 
{		
		var id;
		var index;
		var abbr = $('#organizationAbbr').val();
		var productTypeList= [];
		//function to save product
		$scope.saveProductType = function(data){
			$scope.productType = new ProductTypeCreate();
			$scope.productType.name = data.name;
			$scope.productType.organization=data.organization;
			$scope.productType.sequence=data.sequence;
			
			//console.log('enters save func');
			//console.log("data"+ data.organizationId);
			ProductTypeCreate.save($scope.productType,function(success){
				$scope.productTypeList.push($scope.productType);
				$http.get(API_ADDR +'web/'+abbr+'/getProductTypeByOrgg/').
				success(function(data, status, headers, config) {
					$scope.productTypeList = data.producttyper;
				}).
				error(function(data, status, headers, config) {
					createAlert("Error Fetching Data","There was some error in response from the remote server.");
				});
				createAlert("Success","The new Product type was added Successfully.");
			},function(error){
				if (error.status == "409")
					createAlert("Error","Product type already exists.");
			});
		}
		//function to edit product type
		$scope.editProductType = function(value){
			//console.log(productTypeId);
			if($scope.productType.name == value)
				createAlert("Alert","Nothing to change.");
			else{
				$scope.productType = ProductTypeEdit.get({id:$scope.id},function(){
					$scope.productType.name = value;
					//console.log($scope.id+"  "+value);
					$scope.productType.$update({id:$scope.id},function(success){
						createAlert("Success","Changed product type.");
					},function(error){
						createAlert("Error Fetching Data","There was some error in response from the remote server.");
					});
				});
			}
		}

		//function to edit product type
		$scope.editName = function(producttype, productTypeList){
			//console.log(productTypeId);
			$("#update-type-input").val(producttype.name);		
			$scope.updateType = function() {
				var flag = 0;	
				var value = $.trim($('#update-type-input').val());
				for(var x = 0; x < $scope.productTypeList.length; x++){
					if($scope.productTypeList[x].name == value){
						flag = 1;
						break;
					}
					//console.log($scope.productTypeList[0].name);
				}
				if(value == producttype.name)
					createAlert("Alert", "Nothing to change.");
				else if(flag == 1)
					createAlert("Alert", "Product type already exists.");
				else if(value.length == 0)
					createAlert("Alert", "Please enter a valid name.");
				else{
					$scope.productType = ProductTypeEdit.get({id:producttype.id},function(){
						$scope.productType.name = value;
						//console.log('-->'+value);
						$scope.productType.$update({id:producttype.id},function(success){
							producttype.name = value;
							createAlert("Success","Changed product type.");
						},function(error){
							createAlert("Error Fetching Data","There was some error in response from the remote server.");
						});
					});
					$('#update-type-input').val("");
					$("#edit-type-modal").modal('toggle');
				}
			}
		}
		//function to delete product
		$scope.deleteProductType = function(producttype){
			$scope.deleteType = function() {
				$http.get(API_ADDR+'web/'+abbr+'/prodtypedelete/'+producttype.id)
				.success(function(data, stat){
					if(data.status == 'success'){
						createAlert("Success","Product type successfully deleted.");
						//producttype.stat = 1;
						$http.get(API_ADDR +'web/'+abbr+'/getProductTypeByOrgg/').
						success(function(data, status, headers, config) {
							$scope.productTypeList = data.producttyper;
						}).
						error(function(data, status, headers, config) {
							createAlert("Error Fetching Data","There was some error in response from the remote server typeORGG.");
						});
					}
					else if(data.status == 'error')
						createAlert("Error","Product Type cannot be deleted as products of this type have been ordered before.");
				})
				.error(function(){
					createAlert("Error","Couldn't delete product.");
				});
				$("#delete-type-modal").modal('toggle');
			}
			/*$scope.productType = ProductTypeDelete.get({id:$scope.id},function(){	
				//console.log('scope.product type= '+$scope.productType);
				$scope.productType.$update({id:$scope.id},function(){
				},function(error){
					if(error.status == "409")
						createAlert("Error Deleting Product type","You can't delete this product type.");
				});
			});*/	
		}
		//function to set id attribute
		$scope.setId = function(productTypeId){
			$scope.id=productTypeId; 
			//console.log("Assigned value id "+ productTypeId);
		}
		
		
		
		
		
		//function to fetch list of products
	/*	$scope.getList = function(){
			$scope.producttypelists = ProductTypeCreate.query(function() {
			    
			  }); 
		}*/
		
		
		
		
		
		$scope.enableDisableCurrentProduct= function(productType){
			//console.log(productTypeId+"producttypeid!!!!!!");
			//console.log(API_ADDR);
			console.log(productType.id+"  "+productType.status);
			//var stat=parseInt(this.product.status);
			//var currentStat = (stat+1)%2;
		//	setTimeout(function(){$http.get(API_ADDR + 'api/products/search/byType/toggleproducttype/'+productTypeId).
			$http.get(API_ADDR +'api/products/search/byType/toggleproducttype/'+productType.id).
				success(function(data, status, headers, config) {
					createAlert("Confirmation","The changes will impact all the products inside the product type ");
					productType.status = (productType.status+1)%2;
					// Store the data into Angular scope model variable
					//console.log("ajax query fired again");
					//$route.reload();
					//console.log($scope.productTypeList);
				}).
				error(function(data, status, headers, config) {
					createAlert("Error Fetching Data","There was some error in response from the remote server.");
				});
			// /api/products/search/byType/toggleproducttype/{prodTypeID}
			
		}
			
			
		
		
		//TODO hard refresh has to be eliminated
		$scope.reload = function(){
			setTimeout($window.location.reload.bind(window.location),2000);
		}
		
		/*$http.get( API_ADDR + 'api/productTypes/search/findByorganization_abbreviationIgnoreCase?abbr=' + abbr +'&projection=producttype').
		success(function(data, status, headers, config) {

			// Store the data into Angular scope model variable
			//$scope.productTypeList = data._embedded.productTypes;
			//console.log($scope.productTypeList);
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});*/
		
		//var xyz='http:/';
		$scope.refreshTable = function(){
			$http.get(API_ADDR +'web/'+abbr+'/getProductTypeByOrgg/').
			success(function(data, status, headers, config) {
				$scope.productTypeList = data.producttyper;
			}).
			error(function(data, status, headers, config) {
				createAlert("Error Fetching Data","There was some error in response from the remote server typeORGG.");
			});
		}
		
		$http.get(API_ADDR +'web/'+abbr+'/getProductTypeByOrgg/').
		success(function(data, status, headers, config) {
			$scope.productTypeList = data.producttyper;
			//console.log($scope.productTypeList);
			//console.log(data+"here");
			// Store the data into Angular scope model variable
		//	$scope.productTypeList = data._embedded.responseJsonObject;
		//	console.log($scope.productTypeList);
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server typeORGG.");
		});
		
		/*$scope.displayStatus = function(productTypeList) {
			if (this.productTypeList.status_type == 1){
				return "Disable";
			}
			else {
				return "Enable";
			}
		}*/
		
		
		$scope.displayStatus = function(xx) {
			//console.log("hello!!!!")
			if (xx){
				return "Enabled";
			}
			else {
				return "Disabled";
			}
		}
		
		
		
});

$("#page-content").on("click", "#producttable #checkall", function () {
	if ($("#producttable #checkall").is(':checked')) {
	    $("#producttable input[type=checkbox]").each(function () {
	        $(this).prop("checked", true);
	    });
	} else {
	    $("#producttable input[type=checkbox]").each(function () {
	        $(this).prop("checked", false);
	    });
	}
});

//add new product type on clicking the add button
$("#page-content").on("click", "#add-new-type", function(e) {
	e.preventDefault();
	var name = $.trim($('#new-product-type-input').val());
	var organizationId= $('#product-type').attr('organizationId');
	var organization = "organizations/"+organizationId;
	var data = {};
	var flag = 0;
	var seq = 0;
	data.name = name;
	data.organization = organization;
	data.organizationId= organizationId;
	for(var x = 0; x < angular.element('#add-new-type').scope().productTypeList.length; x++){
		if(angular.element('#add-new-type').scope().productTypeList[x].name == name){
			flag = 1;
		}
		if(angular.element('#add-new-type').scope().productTypeList[x].sequence > seq){
			seq = angular.element('#add-new-type').scope().productTypeList[x].sequence;
		}
		//console.log($scope.productTypeList[0].name);
	}
	seq = seq+1;
	data.sequence = seq;
	if(flag == 1)
		createAlert("Error", "Product type already exists.");
	else if(name.length == 0)
		createAlert("Error", "Please enter a valid name.");
	else{
		angular.element('#add-new-type').scope().saveProductType(data);
		$('#add-type-modal').modal('toggle');
		$('#new-product-type-input').val("");
	}
});

//capture the id of product on clicking the edit button
$("#page-content").on("click", "#btn-edit-type", function () {
	productTypeId = $(this).attr("productTypeId");
	type=$(this).attr("productTypeName");
	angular.element(this).scope().setId(productTypeId);
	//console.log("product type name= "+type);
	$(".modal-header #HeadingEdit").html("Edit product type "+type);
	$(".modal-body #update").html(type);
});

//update the product type on clicking the update button in edit modal
$/*("#page-content").on("click","#update-type",function(e){
	value = $.trim($('#update-type-input').val());
	//console.log("1  "+value);
	angular.element(this).scope().editProductType(value);
	$("#edit-type-modal").modal('toggle');
	//TODO Eliminating this function doing hard refresh
	angular.element($("#producttypetable")).scope().reload();
});*/

//capture the id of product type on clicking the delete button
$("#page-content").on("click", "#btn-delete-type", function(e) {  
	//e.preventDefault();
	productTypeId = $(this).attr("productTypeId");
	//console.log('id'+productTypeId);
	angular.element(this).scope().setId(productTypeId);
});

//delete a product type entry on clicking the 'yes' delete button
/*$("#page-content").on("click","#delete-type",function(e){
	angular.element(this).scope().deleteProductType();
	$("#delete-type-modal").modal('toggle');
	//TODO Eliminating this function doing hard refresh
	angular.element(this).scope().reload();
});*/







