/**
 * This file contains all the functionalities related to users
 */


website.controller("UsersCtrl", function($scope, $http, $route, $routeParams) {
	
	// Get Organization Abbreviation from Thymeleaf
	var abbr = $('#organizationAbbr').val();
	var url = "";
	
	// By Default display all members
	$scope.selectedRole = "";
	
	// Initialize the table
	$http.get( API_ADDR + 'api/' + abbr + '/manageUsers/getUserList').
		success(function(data, status, headers, config) {

			// Store the data into Angular scope model variable
			$scope.manageUserItems = data;
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});
	
	$scope.refreshTable = function(){
		$http.get( API_ADDR + 'api/' + abbr + '/manageUsers/getUserList').
		success(function(data, status, headers, config) {

			// Store the data into Angular scope model variable
			$scope.manageUserItems = data;
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});
	}
	
	$scope.userPropertySearchQuery = function (manageUserItem){
		    
			if(!$scope.userpropertysearch)
			{
				return true;
			}
			else if($scope.userpropertysearch) 
			{
				if (manageUserItem.name.toLowerCase().indexOf($scope.userpropertysearch.toLowerCase())!=-1 || manageUserItem.phone.indexOf($scope.userpropertysearch)!=-1) {
					return true;
				}
				else
					return false;
			}
	        
	};

	$scope.removeMember = function($event, manageUserItem) {
		$event.preventDefault();
		var userDetails = {};
		userDetails.userid = this.manageUserItem.manageUserID;
		userDetails.phno = this.manageUserItem.phone;
		$http.post( API_ADDR + 'api/' + abbr + '/manageUsers/adminTerminates', userDetails).
		success(function(data, status, headers, config) {
			//console.log(data.response);
			if(data.response == "Success"){
				createAlert("Deleted","User deleted.");
				manageUserItem.stat = 1;
			}
			else if(data.response == "Error")
				createAlert("Error","Cannot delete users with pending orders.");
		}).
		error(function(data, status, headers, config) {
			//console.log(data);
			createAlert("Error Deleting  Member","There was some error in response from the remote server.");
		});
			//$scope.reload();
	};
	
	$scope.sendBroadcast = function() {
		if (!$scope.inputMessage) {
			createAlert("Invalid Input","Please enter a message !");
		}
		
		else {
			var dataSend = {};
			dataSend.orgabbr = abbr;
			dataSend.message = $scope.inputMessage;
			$http.post( API_ADDR + 'api/sendbroadcast', dataSend).
			success(function(data, status, headers, config) {
				createAlert("Success","Broadcast sent!");
				$('#send-broadcast-modal').modal('hide');

			}).
			error(function(data, status, headers, config) {
				createAlert("Error Updating Member","There was some error in response from the remote server.");
				$('#send-broadcast-modal').modal('hide');

			});

		}

	}
	
	// For uploading from XLS
	
	$scope.uploadSpreadsheet = function() {
		console.log("called");
		if ($scope.read_Xls == undefined)
		{
			createAlert("Error Uploading File","No file added. Please choose a file of '.xlsx' file format")
		}
		else if($scope.read_Xls.type != 'application/vnd.ms-excel'){
			console.log($scope.read_Xls.type);
			createAlert("Error Uploading File","Please choose a file of '.xls' file format");
		}
		else {
			
			var formData = new FormData();
			formData.append("file",$scope.read_Xls); //myFile.files[0] will take the file and append in formData since the name is myFile.
			$http({
				method: 'POST',
				url: API_ADDR + 'api/uploadsheet/'+abbr, // The URL to Post.
				headers: {'Content-Type': undefined}, // Set the Content-Type to undefined always.
				data: formData,
				transformRequest: function(data, headersGetterFunction) {
					return data;
				}
			})
			.success(function(data, status) {
				console.log("Sheet successfully uploaded.");
				url = data;
				console.log(url);
				//("#uploadData")[0].reset(); 
				if(url == ""){
	        		createAlert("Success","All members referred successfully");
					/*setTimeout(function(){
		        		angular.element("#read_xls").scope().reload();
					},800);*/
	        	}
	        	else if(url == 'nil')
	        		createAlert("Alert","No valid number found. Please check the file.");
	        	else{
	        		var str = url.split(',');
	        		var total = "";
	        		for(i=0; i<str.length-1; i++)
	        			total += str[i] +", ";
	        		total = total.substring(0,total.length-2);
	        		createAlert("Error", "The numbers " +total+" were not added. Please check for any redundant number.");	
				}
			})
			.error(function(status) {
				console.log("Error uploading file.");
			});
		}

		$("#uploadData")[0].reset();
	}
	
		
	$scope.readXLS = function(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
	        	$scope.uploadSpreadsheet();
	        	//console.log(url);
			};
			reader.readAsDataURL(input.files[0]); 
		}
	}
	
	
	
	
	// Click event handler for the 'Add' Modal Button
	$scope.addNewUserModalButton = function() {
		// Check if required text fields are blank or not
		if(!$scope.inputUserPhone)
		{
			createAlert("Invalid Input","Please Enter a Phone Number !");
		}
		else if(!validateEmail($scope.inputUserEmail) && $scope.inputUserEmail)
		{
			createAlert("Invalid Input","Invalid Email ID !");
		}
		else
		{
			// Get the attributes of the new user
			var newUserDetails = {};
			newUserDetails.name = $scope.inputUserName;
			newUserDetails.lastname = $scope.inputUserLastName;
			newUserDetails.email = $scope.inputUserEmail;
			newUserDetails.phonenumber = $scope.inputUserPhone;
			newUserDetails.role = "Member";                   // New User is by default a Member
			newUserDetails.address = $scope.inputUserAddress;
			newUserDetails.abbr = abbr;
			if(normalizePhoneNumber(newUserDetails.phonenumber) == false)
			{
				createAlert("Invalid Input","Please enter a valid phone number !");
			}
			else
			{
				// Normalize the phone number to database format
				newUserDetails.phonenumber = normalizePhoneNumber(newUserDetails.phonenumber);
				console.log(newUserDetails);
				// Add the User as we have validated the number
				
				//$http.post( API_ADDR + 'api/' + abbr + '/manageUsers/addNewUser', newUserDetails).
				$http.post( API_ADDR + 'api/refer', newUserDetails).	
					success(function(data, status, headers, config) {
						
						if(data.response == "failure")
						{
							createAlert("Invalid", data.error+". Please try again.");
						}
						else
						{
							// Push the new object in the ng-repeat variable for for table
							// This Automatically updates the table
							
							//console.log(data);
		
							// Hide the modal dialog box after successful operation
							$('#add-new-user-modal').modal('hide');
							
							// Display modal conveying that the new user has been successfully added
							createAlert("Success","The user was sucessfully referred.");
							
							// Clear the contents of scope variables
							$scope.inputUserName = '';
							$scope.inputUserLastName = '';
							$scope.inputUserEmail = '';
							$scope.inputUserPhone = '';
							$scope.inputUserAddress = '';
							$("#add-member-new")[0].reset();  
						}
					}).
					error(function(data, status, headers, config) {
						createAlert("Error Adding Member","There was some error in response from the remote server.");
						$("#add-member-new")[0].reset();  
					});
			}
			
			
		}
	};

	// Utility functions used in ng-if to check the roles
	$scope.detectIfAdmin = function(manageUserItem) {
		if(manageUserItem)
		{
			if(this.manageUserItem.role.search("Admin") != -1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	};

	$scope.detectIfPublisher = function(manageUserItem) {
		if(manageUserItem)
		{
			if(this.manageUserItem.role.search("Publisher") != -1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	};

	// makeRole functions are called when user does not have the specified roles
	$scope.makeRoleAdmin = function($event, manageUserItem) {
		$event.preventDefault();

		var userDetails = {};
		userDetails.userid = this.manageUserItem.manageUserID;
		userDetails.addRole = "Admin";

		console.log(manageUserItem.role);
		$http.post( API_ADDR + 'api/' + abbr + '/manageUsers/addUserRole', userDetails).
			success(function(data, status, headers, config) {

				var previousRole = manageUserItem.role;
				if(previousRole === "Member")
				{
					manageUserItem.role = "Admin";
				}
				else
				{
					manageUserItem.role = "Admin Publisher";
				}
			}).
			error(function(data, status, headers, config) {
				createAlert("Error Updating Member","There was some error in response from the remote server.");
			});
	};

	$scope.makeRolePublisher = function($event, manageUserItem) {
		$event.preventDefault();

		var userDetails = {};
		userDetails.userid = this.manageUserItem.manageUserID;
		userDetails.addRole = "Publisher";

		$http.post( API_ADDR + 'api/' + abbr + '/manageUsers/addUserRole', userDetails).
			success(function(data, status, headers, config) {

				var previousRole = manageUserItem.role;
				if(previousRole === "Member")
				{
					manageUserItem.role = "Publisher";
				}
				else
				{
					manageUserItem.role = "Admin Publisher";
				}
			}).
			error(function(data, status, headers, config) {
				createAlert("Error Updating Member","There was some error in response from the remote server.");
			});
	};

	// removeRole functions are called when we want to remove a specific role
	// they assume that the user is necessarily having roles admin or publisher or both
	$scope.removeRoleAdmin = function($event, manageUserItem) {
		$event.preventDefault();

		var userDetails = {};
		userDetails.userid = this.manageUserItem.manageUserID;
		userDetails.removeRole = "Admin";

		$http.post( API_ADDR  + 'api/' + abbr + '/manageUsers/removeUserRole', userDetails).
			success(function(data, status, headers, config) {

				var previousRole = manageUserItem.role;
				if(previousRole === "Admin")
				{
					manageUserItem.role = "Member";
				}
				else
				{
					manageUserItem.role = "Publisher";
				}
			}).
			error(function(data, status, headers, config) {
				createAlert("Error Updating Member","There was some error in response from the remote server.");
			});
	};

	$scope.removeRolePublisher = function($event, manageUserItem) {
		$event.preventDefault();

		var userDetails = {};
		userDetails.userid = this.manageUserItem.manageUserID;
		userDetails.removeRole = "Publisher";

		$http.post( API_ADDR  + 'api/' + abbr + '/manageUsers/removeUserRole', userDetails).
			success(function(data, status, headers, config) {

				var previousRole = manageUserItem.role;
				if(previousRole === "Publisher")
				{
					manageUserItem.role = "Member";
				}
				else
				{
					manageUserItem.role = "Admin";
				}
			}).
			error(function(data, status, headers, config) {
				createAlert("Error Updating Member","There was some error in response from the remote server.");
			});
	};

	// Function to handle events for edit user anchor button
	$scope.editRowUser = function($event, manageUserItem) {
		$event.preventDefault();

		var oldname = this.manageUserItem.name;
		var oldlastname = this.manageUserItem.lastname;
		var oldemail = this.manageUserItem.email;
		var oldphone = this.manageUserItem.phone;
		var oldaddress = this.manageUserItem.address;
		
		$scope.editUserName = oldname;
		$scope.editUserLastName = oldlastname;
		$scope.editUserPhone = oldphone;
		$scope.editUserAddress = oldaddress;
		$scope.editUserEmail = oldemail;

		$('#edit-user-modal').modal('show');

		// click event handler for edit user modal save button
		$scope.editUserModalAnchorButton = function() {
			
			// Check if required text fields are blank or not
			if(!$scope.editUserName)
			{
				createAlert("Invalid Input","Please Enter a Name for the User !");
			}
			else if(!$scope.editUserPhone)
			{
				createAlert("Invalid Input","Please Enter a Phone Number !");
			}
			else if(!validateEmail($scope.editUserEmail))
			{
				createAlert("Invalid Input","Invalid Email ID !");
			}
			else
			{
				
				// Get the attributes of the new user
				var newUserDetails = {};
				newUserDetails.userid = manageUserItem.manageUserID;
				newUserDetails.name = $scope.editUserName;
				newUserDetails.lastname = $scope.editUserLastName;
				newUserDetails.email = $scope.editUserEmail;
				newUserDetails.phone = $scope.editUserPhone;
				newUserDetails.address = $scope.editUserAddress;
				
				
				if(normalizePhoneNumber(newUserDetails.phone) == false)
				{
					createAlert("Invalid Input","Please enter a valid phone number !");
				}
				else
				{
					// Normalize the phone number to database format
					newUserDetails.phone = normalizePhoneNumber(newUserDetails.phone);
					
					// If phone Number has been changed, update phone number as well as user details
					if(newUserDetails.phone != manageUserItem.phone)
					{
						$http.post( API_ADDR + 'api/' + abbr + '/manageUsers/editUserWithPhoneNumber', newUserDetails).
							success(function(data, status, headers, config) {
								
								if(data == "-1")
								{
									createAlert("Invalid Input","The Entered Phone Number already exists in the database");
								}
								else
								{
									manageUserItem.name = $scope.editUserName;
									manageUserItem.lastname = $scope.editUserLastName;
									manageUserItem.phone = newUserDetails.phone;  // Normalized phone number
									manageUserItem.address = $scope.editUserAddress;
									manageUserItem.email = $scope.editUserEmail;
			
									// Hide the edit user modal dialog box after successful operation
									$('#edit-user-modal').modal('hide');
									
									// Display modal conveying that the user details have been changed successfully
									createAlert("Success","The user details were changed successfully.");
									
									// clear the contents of scope variables
									$scope.editUserName = '';
									$scope.editUserLastName = '';
									$scope.editUserEmail = '';
									$scope.editUserPhone = '';
									$scope.editUserAddress = '';
								}
		
							}).
							error(function(data, status, headers, config) {
								createAlert("Error Updating Member","There was some error in response from the remote server.");
							});
					}
					// Or else just update the user details
					else
					{
						if( newUserDetails.name == oldname &&
							newUserDetails.lastname == oldlastname &&
							newUserDetails.email == oldemail &&
							newUserDetails.phone == oldphone &&
							newUserDetails.address == oldaddress)
							createAlert("Alert","Nothing to change.");
						else{
							$http.post( API_ADDR + 'api/' + abbr + '/manageUsers/editUserOnly', newUserDetails).
							success(function(data, status, headers, config) {
								
								manageUserItem.name = $scope.editUserName;
								manageUserItem.lastname = $scope.editUserLastName;
								manageUserItem.address = $scope.editUserAddress;
								manageUserItem.email = $scope.editUserEmail;
		
								// Hide the edit user modal dialog box after successful operation
								$('#edit-user-modal').modal('hide');
								
								// Display modal conveying that the user details have been changed successfully
								createAlert("Success","The user details were changed successfully.");
								
								// clear the contents of scope variables
								$scope.editUserName = '';
								$scope.editUserLastName = '';
								$scope.editUserEmail = '';
								$scope.editUserPhone = '';
								$scope.editUserAddress = '';
		
							}).
							error(function(data, status, headers, config) {
								createAlert("Error Updating Member","There was some error in response from the remote server.");
							});
						}
					}
					
				}
			}

		};

	};

	// Event handler for get user details anchor link
	$scope.getRowUserDetails = function($event, manageUserItem) {
		$event.preventDefault();

		var userid = manageUserItem.manageUserID;

		$http.post( API_ADDR  + 'api/' + abbr + '/manageUsers/getUserDetails', userid).
			success(function(data, status, headers, config) {

				$scope.userDetails = data;

				$('#get-user-details-modal').modal('show');

			}).
			error(function(data, status, headers, config) {
				createAlert("Error Fetching Data","There was some error in response from the remote server.");
			});
	};

});

/*function readXLS(input){
	if(input.files && input.files[0]){
		var reader = new FileReader();
		reader.onload = function(e){
			console.log('isis');
		}
		reader.readAsDataURL(input.files[0]);
	}
}*/