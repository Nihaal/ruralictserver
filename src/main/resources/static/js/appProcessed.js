/**
 * This file contains all the functionalities related to processing of app
 */


website.controller("AppProcessCtrl", function($scope, $http, $route, $routeParams, UpdateOrder) {
	
	// Get Organization Abbreviation from Thymeleaf
	var abbr = $('#organizationAbbr').val();
	
	// Initialize the table
	$http.get( API_ADDR + 'api/' + abbr + '/getProcessedList').
		success(function(data, status, headers, config) {

			// Store the data into Angular scope model variable
			$scope.manageProcessedItems = data;
			console.log("done");
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});
	$scope.refreshTable = function(){
		$http.get( API_ADDR + 'api/' + abbr + '/getProcessedList').
		success(function(data, status, headers, config) {

			// Store the data into Angular scope model variable
			$scope.manageProcessedItems = data;
			console.log("done");
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});
	}
	$scope.saveOrder = function($event, manageProcessedItem) {
		$event.preventDefault();
		$http.get( API_ADDR + 'api/order/' + manageProcessedItem.orderId + '/getStatus').
		success(function(data, status, headers, config) {
			if(data.value == 'processed'){
				$scope.order = UpdateOrder.get({id:manageProcessedItem.orderId},function(){
					//$scope.order.status = "delivered";
					var request = {};
					request.status = "delivered";
					request.comments = null;
					request.orderItems = null;
					$scope.order.$update({id:manageProcessedItem.orderId},function(){
						$http.post( API_ADDR + 'api/orders/update/' + manageProcessedItem.orderId, request).
						success(function(data, status, headers, config) {
							createAlert("Success","Order no. "+ manageProcessedItem.orderId +" has been processed.");
							manageProcessedItem.stat = 1;
							//angular.element($('#btn-process')).scope().reload();
						});
					});
				});
			}
			else{
				manageProcessedItem.stat = 1;				
				createAlert("Alert","Order status has already been changed.");
			}
		}).
		error(function(data, status, headers, config) {
			createAlert("Error","There was some error in response from the remote server.");
		});
		//console.log(API_ADDR + 'api/orders/update/' + manageProcessedItem.orderId);		
	};
	
	$scope.viewBill = function($event, manageProcessedItem) {
		$event.preventDefault();
		$http.get( API_ADDR + 'api/order/' + manageProcessedItem.orderId + '/getStatus').
		success(function(data, status, headers, config) {
			if(data.value == 'processed'){
				window.open("generateBill/"+manageProcessedItem.orderId);
			}
			else{
				manageProcessedItem.stat = 1;				
				createAlert("Alert","Order status has already been changed.");
			}
		}).
		error(function(data, status, headers, config) {
			createAlert("Error","There was some error in response from the remote server.");
		});
	};

});