/**
 * This file contains all the functionalities related to delivery of app
 */


website.controller("AppDeliverCtrl", function($scope, $http, $route, $routeParams) {
	
	// Get Organization Abbreviation from Thymeleaf
	var abbr = $('#organizationAbbr').val();
	
	// Initialize the table
	$http.get( API_ADDR + 'api/' + abbr + '/getOrderList').
		success(function(data, status, headers, config) {

			// Store the data into Angular scope model variable
			//data.reverse();
			$scope.manageOrderItems = data;
			console.log("done");
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});
	
	$scope.refreshTable = function(){
		$http.get( API_ADDR + 'api/' + abbr + '/getOrderList').
		success(function(data, status, headers, config) {

			// Store the data into Angular scope model variable
			//data.reverse();
			$scope.manageOrderItems = data;
			console.log("done");
		}).
		error(function(data, status, headers, config) {
			createAlert("Error Fetching Data","There was some error in response from the remote server.");
		});
		
	}
	
	$scope.paidOrder= function($event, manageOrderItem){
		$event.preventDefault();
		$http.get( API_ADDR + 'api/order/' + manageOrderItem.orderId + '/getPaid').
		success(function(data, status, headers, config) {
			console.log(data.value);
			if(data.value == false){
				$http.get( API_ADDR + 'api/orders/paid/' + manageOrderItem.orderId).
				success(function(data, status, headers, config) {
					//this.manageOrderItem.text = "Paid";
					manageOrderItem.isPaid = true;
					manageOrderItem.text = "Paid";
					createAlert("Success","Order no. "+ manageOrderItem.orderId +" has been paid.");
				});
			}
			else{
				manageOrderItem.isPaid = true;
				manageOrderItem.text = "Paid";		
				createAlert("Alert","Order status has already been changed.");
			}
		}).
		error(function(data, status, headers, config) {
			createAlert("Error","There was some error in response from the remote server.");
		});
		
		/*console.log("Order ID: " + manageOrderItem.orderId);
		console.log("isPaid: " + manageOrderItem.isPaid);*/
		
	};

});