package app.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import javax.persistence.OneToMany;


/**
 * The persistent class for the order_item database table.
 * 
 */
@Entity
@Table(name="lcp_order_item")
public class LcpOrderItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="order_item_id")
	private int orderItemId;

	private int quantity;

	@Column(name="status")
	private String status;
	
	@Column(name="message")
	private String message;
	
	/*private String productName;*/

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name="unit_rate")
	private float unitRate;

	@Column(name="del_charges")
	private int deliveryCharges;
	
	public int getDeliveryCharges() {
		return deliveryCharges;
	}

	public void setDeliveryCharges(int deliveryCharges) {
		this.deliveryCharges = deliveryCharges;
	}

	@Column(name="delivery")
	private String delivery;
	
	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	@Column(name="is_paid")
	@Type(type="org.hibernate.type.NumericBooleanType")
	private boolean isPaid;

	//bi-directional many-to-one association to Order
	@ManyToOne
	@JoinColumn(name="order_id")
	private LcpOrder lcpOrder;

	//bi-directional many-to-one association to Product
	@ManyToOne
	@JoinColumn(name="lcp_product_id")
	private LcpProduct product;

	//bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name="organization_id")
	private Organization organization;
	
	public LcpOrderItem() {
	}

	public LcpOrderItem(LcpOrder order, LcpProduct product, int quantity, float unitRate) {
		this.lcpOrder = order;
		this.product = product;
		this.quantity = quantity;
		this.unitRate = unitRate;
	}
	
	/*public OrderItem(Order order, String productName, int quantity, float unitRate) {
		this.order = order;
		this.productName = productName;
		this.quantity = quantity;
		this.unitRate = unitRate;
	}*/

	public LcpOrderItem(LcpOrder order, LcpProduct product, int quantity2, float unitRate2, Organization orgId) {
		// TODO Auto-generated constructor stub
		this.lcpOrder = order;
		this.product = product;
		this.quantity = quantity2;
		this.unitRate = unitRate2;
		this.organization = orgId;
		this.status = "placed";
		this.deliveryCharges = orgId.getDelCharge();
	}

	public int getOrderItemId() {
		return this.orderItemId;
	}

	public void setOrderItemId(int orderItemId) {
		this.orderItemId = orderItemId;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getUnitRate() {
		return this.unitRate;
	}

	public void setUnitRate(float unitRate) {
		this.unitRate = unitRate;
	}

	public LcpOrder getOrder() {
		return this.lcpOrder;
	}

	public void setOrder(LcpOrder order) {
		this.lcpOrder = order;
	}

	public LcpProduct getProduct() {
		return this.product;
	}

	public void setProduct(LcpProduct product) {
		this.product = product;
	}
	


	public boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}

	public LcpOrder getLcpOrder() {
		return lcpOrder;
	}

	public void setLcpOrder(LcpOrder lcpOrder) {
		this.lcpOrder = lcpOrder;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	/*public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
*/
}
