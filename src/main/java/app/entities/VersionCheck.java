package app.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="version_check")
public class VersionCheck implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name = "version")
	private float version;
	
	@Column(name = "img_url")
	private String imgUrl;
	
	@Column(name = "link_to")
	private String linkTo;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Column(name = "mandatory")
	private int mandatory;
	
	public VersionCheck() {
		
	}
	
	public VersionCheck(float version, int mandatory) {
		this.version = version;
		this.mandatory = mandatory;
	}
	
	public VersionCheck(float version, int mandatory, String imgUrl, String linkTo) {
		this.version = version;
		this.mandatory = mandatory;
		this.imgUrl = imgUrl;
		this.linkTo = linkTo;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public float getVersion() {
		return this.version;
	}
	
	public void setVersion(float version) {
		this.version = version;
	}
	
	public int getMandatory() {
		return this.mandatory;
	}
	
	public void setMandatory(int mandatory){
		this.mandatory = mandatory;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	
	public String getLinkTo() {
		return linkTo;
	}
	
	public void setLinkTo(String linkTo) {
		this.linkTo = linkTo;
	}
}
