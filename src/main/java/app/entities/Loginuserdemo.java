package app.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Columns;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="userdemo")
public class Loginuserdemo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="iduserdemo")
	private int loginid;
	
	@Column(name="email")
	private String emailid;
	
	@Column(name="password")
	private String password;
	
	@Column(name="phone")
	private int phone;

	//constructors
	public Loginuserdemo() {
		
	}
	
	public Loginuserdemo(int loginid, String emailid, String password, int phone) {
		this.loginid = loginid;
		this.emailid = emailid;
		this.password = password;
		this.phone = phone;
	}
	
	//getters and setters
	public int getLoginid() {
		return loginid;
	}

	public void setLoginid(int loginid) {
		this.loginid = loginid;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}
	
	
}