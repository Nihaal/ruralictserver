package app.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the product database table.
 * 
 */
@Entity
@Table(name="lcp_product")
public class LcpProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="product_id")
	private int productId;

	@Column(name = "description")
	private String description;

	@Column(name="image_url")
	private String imageUrl;

	@Column(name="moq")
	private int moq;
	
	@Column(name="si_unit")
	private String siUnit;
	
	@Column(name="logistics_availability")
	private int logAvailability;

	@Column(name="name")
	private String name;

	@Column(name="quantity")
	private int quantity;
	
	@Column(name="status")
	private int status;

	@Column(name="unit_rate")
	private float unitRate;

	@Column(name="unit")
	private int unit;

	public int getUnit() {
		return unit;
	}

	public void setUnit(int unit) {
		this.unit = unit;
	}

	//bi-directional many-to-one association to LcpProductType
	@ManyToOne
	@JoinColumn(name="product_type_id")
	private ProductType productType;

	@ManyToOne
	@JoinColumn(name="organization_id")
	private Organization organization;
	
	public int getMoq() {
		return moq;
	}

	public void setMoq(int moq) {
		this.moq = moq;
	}

	public String getSiUnit() {
		return siUnit;
	}

	public void setSiUnit(String siUnit) {
		this.siUnit = siUnit;
	}

	public int getLogAvailability() {
		return logAvailability;
	}

	public void setLogAvailability(int logAvailability) {
		this.logAvailability = logAvailability;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	public LcpProduct() {
		
	}

	public LcpProduct(Organization org, String name, ProductType productType, float unitRate, int quantity, String description,
			String imageUrl, int status) {
		//this.productId = id;
		this.moq = 5;
		this.status = 1;
		this.siUnit = "Kg";
		this.organization = org;
		this.name = name;
		this.productType = productType;
		this.unitRate = unitRate;
		this.quantity = 899;
		this.description = description;
		this.imageUrl = imageUrl;
	}
	
	public LcpProduct(String name, String imgUrl, float unitRate, int quantity){
		this.name = name;
		this.imageUrl = imgUrl;
		this.unitRate = unitRate;
		this.quantity = quantity;
	}

	public int getStatus(){
		return this.status;
	}
	
	public void setStatus(int status){
		this.status = status;
	}
	
	
	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getUnitRate() {
		return this.unitRate;
	}

	public void setUnitRate(float unitRate) {
		this.unitRate = unitRate;
	}

	public ProductType getProductType() {
		return this.productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

}
