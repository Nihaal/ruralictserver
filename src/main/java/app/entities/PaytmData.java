
package app.entities;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Type;

@SuppressWarnings("serial")
@Entity
@Table(name="paytm_data")
public class PaytmData implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@OneToOne
	@JoinColumn(name="organization_id")
	private Organization organization;
	
	@Column(name="deficit")
	private float deficit;
	
	@Column(name="balance")
	private float balance;

	public PaytmData(){
		
	}
	
	public PaytmData(Organization organization, float deficit, float balance){
		this.organization = organization;
		this.deficit = deficit;
		this.balance = balance;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public float getDeficit() {
		return deficit;
	}

	public void setDeficit(float deficit) {
		this.deficit = deficit;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}
	
}
