package app.entities;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Type;

@Entity
@Table(name="org_enquiry")
public class OrgEnquiry implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="username")
	private String userName;
	
	@Column(name="org_name")
	private String orgName;

	@Column(name="contact")
	private String contact;
	
	@Column(name="pincode")
	private String pincode;
	
	@Column(name="products")
	private String products;
	
	@Column(name="address")
	private String address;
	
	@Column(name="email")
	private String email;

	@Column(name="trad_lic")
	@Type(type="org.hibernate.type.NumericBooleanType")
	private boolean tradLic;
	
	@Column(name="organic_lic")
	@Type(type="org.hibernate.type.NumericBooleanType")
	private boolean organicLic;
	
	@Column(name="reference")
	private String reference;
	
	@Column(name="loka_pref")
	private int lokaPref;
	
	@Column(name="organic_lic_url")
	private String orgLicUrl;
	
	@Column(name="traders_lic_no")
	private String licNo;
	
	@Column(name="pan_no")
	private String panNo;
	
	@Column(name="aadhar_no")
	private String aadharNo;

	public OrgEnquiry(){
		
	}
	
	public OrgEnquiry(String userName, String orgName, String contact, String address, String email, boolean tradLic, boolean organicLic, String reference, int lokaPref, String pincode, String products, String orgLicUrl, String licNo, String panNo, String aadharNo){
		this.userName = userName;
		this.orgName = orgName;
		this.contact = contact;
		this.address = address;
		this.email = email;
		this.tradLic = tradLic;
		this.organicLic = organicLic;
		this.reference = reference;
		this.lokaPref = lokaPref;
		this.pincode = pincode;
		this.products = products;
		this.orgLicUrl = orgLicUrl;
		this.licNo = licNo;
		this.panNo = panNo;
		this.aadharNo = aadharNo;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}
	
	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isTradLic() {
		return tradLic;
	}

	public void setTradLic(boolean tradLic) {
		this.tradLic = tradLic;
	}

	public boolean isOrganicLic() {
		return organicLic;
	}

	public void setOrganicLic(boolean organicLic) {
		this.organicLic = organicLic;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getProducts() {
		return products;
	}

	public void setProducts(String products) {
		this.products = products;
	}

	public int getLokaPref() {
		return lokaPref;
	}

	public void setLokaPref(int lokaPref) {
		this.lokaPref = lokaPref;
	}
	
	public String getOrgLicUrl() {
		return orgLicUrl;
	}

	public void setOrgLicUrl(String orgLicUrl) {
		this.orgLicUrl = orgLicUrl;
	}

	public String getLicNo() {
		return licNo;
	}

	public void setLicNo(String licNo) {
		this.licNo = licNo;
	}
}
