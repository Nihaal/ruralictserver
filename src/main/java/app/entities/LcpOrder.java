package app.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import app.entities.message.Message;


/**
 * The persistent class for the order database table.
 * 
 */
@Entity
@Table(name="lcp_order")
public class LcpOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="order_id")
	private int orderId;
	
	@OneToMany
	@JoinColumn(name="organization_id")
	private List<Organization> organization;
	
	@Column(name="status")
	private String status;
	
	@Column(name="total_items")
	private int total;
	
	@Column(name="amount")
	private float amount;
	
	@Column(name="delivery")
	private String delivery;
	
	@Column
	private String message;
	
	@Column(name="placed_date")
	private String placeDate;

	public String getPlaceDate() {
		return placeDate;
	}

	public void setPlaceDate(String placeDate) {
		this.placeDate = placeDate;
	}

	@Column(name="is_paid")
	@Type(type="org.hibernate.type.NumericBooleanType")
	private boolean isPaid;
	
	//bi-directional many-to-one association to OrderItem
	@OneToMany(mappedBy="lcpOrder")
	private List<LcpOrderItem> orderItems;

	@OneToMany
	@JoinColumn(name="product_id")
	private List<LcpProduct> lcp_product;
	
	@OneToOne
	@JoinColumn(name="user_id")
	private Registration registration;
	
	
//	//bi-directional many-to-one association to Message
//	@OneToOne(mappedBy="lcp_order")
//	private Message message;

	public LcpOrder() {
	}

	public LcpOrder(int total, Registration reg, float amount, String message, String date) {
		// TODO Auto-generated constructor stub
		this.total = total;
		this.registration = reg;
		this.status = "placed";
		this.amount = amount;
		this.message = message;
		this.isPaid = false;
		this.placeDate = date;
	}

	public LcpOrder(int total, Registration reg, float amount, String message, String delivery, String date) {
		// TODO Auto-generated constructor stub
		this.total = total;
		this.registration = reg;
		this.status = "placed";
		this.amount = amount;
		this.message = message;
		this.isPaid = false;
		this.delivery = delivery;
		this.placeDate = date;
	}
	
	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<LcpOrderItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<LcpOrderItem> orderItems) {
		this.orderItems = orderItems;
	}

	public List<LcpProduct> getLcp_product() {
		return lcp_product;
	}

	public void setLcp_product(List<LcpProduct> lcp_product) {
		this.lcp_product = lcp_product;
	}

	public Registration getRegistration() {
		return registration;
	}

	public void setRegistration(Registration registration) {
		this.registration = registration;
	}

	public void setPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}

	public int getOrderId() {
		return this.orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	
	public boolean getIsPaid() {
		return this.isPaid;
	}
	
	public void setIsPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getDelivery() {
		return this.delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}
	

//	public List<LcpOrderItem> getOrderItems() {
//		return this.orderItems;
//	}
//
//	public void setOrderItems(List<LcpOrderI tem> orderItems) {
//		this.orderItems = orderItems;
//	}


//	public Message getMessage() {
//		return this.message;
//	}
//
//	public void setMessage(Message message) {
//		this.message = message;
//	}

//	public LcpOrderItem addOrderItem(LcpOrderItem orderItem) {
//		getOrderItems().add(orderItem);
//		orderItem.setOrder(this);
//
//		return orderItem;
//	}
//
//	public LcpOrderItem removeOrderItem(LcpOrderItem orderItem) {
//		getOrderItems().remove(orderItem);
//		orderItem.setOrder(null);
//
//		return orderItem;
//	}
	
//	public List<HashMap<String,String>> getOrderItemsHashMap()
//	{
//		/*Marshalling the order items in a string hashmap since included version of the spring data rest doesnt support 
//		 * nested projection in collection containers. Upgrading to newer version requires rework in code.
//		 * */
//		List<HashMap<String,String>> response= new ArrayList<HashMap<String,String>>();
//		for(LcpOrderItem orderItem:orderItems)
//		{
//			HashMap<String, String> temp = new HashMap<String, String>();
//			temp.put("quantity",Float.toString(orderItem.getQuantity()));
//			temp.put("unitrate",Float.toString(orderItem.getUnitRate()));
//			temp.put("productname",orderItem.getProduct().getName());
//			temp.put("productId", Integer.toString(orderItem.getProduct().getProductId()));
//			temp.put("stockquantity",Float.toString(orderItem.getProduct().getQuantity()));
//			response.add(temp);
//		}
//		return response;
//	}

}
