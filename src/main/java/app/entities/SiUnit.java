package app.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the si_unit database table.
 * 
 */
@Entity
@Table(name="si_unit")
public class SiUnit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name="si_unit")
	private String siUnit;
	
	@Column(name="lc_group")
	private int lcGroup;
	
	@Column(name="lcp_group")
	private int lcpGroup;
	
	public SiUnit(){}

	public SiUnit(String siUnit, int lcGroup, int lcpGroup){
		this.siUnit = siUnit;
		this.lcGroup = lcGroup;
		this.lcpGroup = lcpGroup;
	}

	public String getSiUnit() {
		return siUnit;
	}

	public void setSiUnit(String siUnit) {
		this.siUnit = siUnit;
	}

	public int getLcGroup() {
		return lcGroup;
	}

	public void setLcGroup(int lcGroup) {
		this.lcGroup = lcGroup;
	}

	public int getLcpGroup() {
		return lcpGroup;
	}

	public void setLcpGroup(int lcpGroup) {
		this.lcpGroup = lcpGroup;
	}

}