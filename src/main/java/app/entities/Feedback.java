package app.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="feedback")
public class Feedback implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="content")
	private String content;
	
	@Column(name="destination")
	private String destination;
	
	@Column(name = "user_id")
	private int userId;;
	
	public Feedback() {
	}
	
	public Feedback(String content, String destination, int userId) {
		this.content = content;
		this.destination = destination;
		this.userId = userId;
	}
	
	public int getId()  {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getContent() {
		return this.content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getDestination() {
		return this.destination;
	}
	
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public int getUser() {
		return this.userId;
	}
}
