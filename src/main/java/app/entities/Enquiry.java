package app.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="enquiry")
public class Enquiry implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name = "email")
	private String email;
	
	@Column (name = "name")
	private String name;
	
	@Column (name = "phonenumber")
	private String phonenumber;
	
	@Column (name = "address")
	private String address;
	
	@Column (name = "message")
	private String message;
	
	public Enquiry() {
		
	}
	
	public Enquiry(String email, String name, String phonenumber, String address, String message) {
		this.email = email;
		this.phonenumber = phonenumber;
		this.address = address;
		this.message = message;
	}
	
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPhonenumber() {
		return this.phonenumber;
	}
	
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public void setMessage(String message) {
		this.setMessage(message);
	}
	
	public String getMessage() {
		return this.getMessage();
	}
	
	
 	
}
