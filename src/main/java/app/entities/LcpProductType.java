package app.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the product_type database table.
 * 
 */
@Entity
@Table(name="lcp_product_type")
public class LcpProductType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="product_type_id")
	private int productTypeId;

	@Column(name="description")
	private String description;

	@Column(name="image_url")
	private String imageUrl;

	@Column(name="name")
	private String name;

	@Column(name="status")
	private int status;
	
	
	@Column(name="sequence")
	private int sequence;

	//bi-directional many-to-one association to Product
/*	@OneToMany(fetch = FetchType.EAGER, mappedBy="productType")
	private List<Product> products;*/

	//bi-directional many-to-one association to ProductType
	/*@ManyToOne
	@JoinColumn(name="parent_product_type_id")
	private ProductType parentProductType;

	//bi-directional many-to-one association to ProductType
	@OneToMany(mappedBy="parentProductType")
	private List<ProductType> subProductTypes;*/

	
	
	public LcpProductType() {
	}

	/*public LcpProductType(String name, ProductType parentProductType, String description,
			String imageUrl) {

		this.name = name;
		this.parentProductType = parentProductType;
		this.description = description;
		this.imageUrl = imageUrl;
	}

	public LcpProductType(String name, ProductType parentProductType, String description,
			String imageUrl, int sequence) {

		this.name = name;
		this.parentProductType = parentProductType;
		this.description = description;
		this.imageUrl = imageUrl;
		this.sequence = sequence;
	}*/
	
	public LcpProductType(String name, String description,
			String imageUrl, int sequence, int status) {

		this.name = name;
		//this.parentProductType = parentProductType;
		this.description = description;
		this.imageUrl = imageUrl;
		this.sequence = sequence;
		this.status = status;
	}
	public int getProductTypeId() {
		return this.productTypeId;
	}

	public void setProductTypeId(int productTypeId) {
		this.productTypeId = productTypeId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/*public List<PresetQuantity> getPresetQuantities() {
		return this.presetQuantities;
	}

	public void setPresetQuantities(List<PresetQuantity> presetQuantities) {
		this.presetQuantities = presetQuantities;
	}





	public List<Product> getProducts() {
		return this.products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
*/
	//public LcpProduct addProduct(LcpProduct product) {
	//	getProducts().add(product);
	//	product.setProductType(this);

	//	return product;
	//}

	/*public Product removeProduct(Product product) {
		getProducts().remove(product);
		product.setProductType(null);

		return product;
	}
*/

	/*public ProductType getParentProductType() {
		return this.parentProductType;
	}

	public void setParentProductType(ProductType parentProductType) {
		this.parentProductType = parentProductType;
	}*/
	
	public int getSequence() {
		return this.sequence;
	}
	
	public void setSequence(int sequence) {
		this.sequence = sequence;
	} 

	/*public List<ProductType> getSubProductTypes() {
		return this.subProductTypes;
	}

	public void setSubProductTypes(List<ProductType> subProductTypes) {
		this.subProductTypes = subProductTypes;
	}

//	public ProductType addSubProductType(ProductType subProductType) {
//		getSubProductTypes().add(subProductType);
//		subProductType.setParentProductType(this);
//
//		return subProductType;
//	}

	public ProductType removeSubProductType(ProductType subProductType) {
		getSubProductTypes().remove(subProductType);
		subProductType.setParentProductType(null);

		return subProductType;
	}*/

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
