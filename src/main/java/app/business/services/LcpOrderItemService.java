package app.business.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.LcpOrderItemRepository;
import app.entities.Group;
import app.entities.LcpOrder;
import app.entities.LcpOrderItem;
import app.entities.LcpProduct;

@Service
public class LcpOrderItemService {

	@Autowired
	LcpOrderItemRepository orderItemRepository;

//	public List<LcpOrderItem> getOrderItemListByProductAndTime(LcpProduct product, Date fromDate, Date toDate) {
//		Calendar c = Calendar.getInstance();
//
//		c.setTime(toDate);
//		c.add(Calendar.DATE, 1);
//		toDate = c.getTime();
//		System.out.println("fromDate" + fromDate + "toDate" + toDate);
//		return orderItemRepository.findByProductAndOrder_Message_TimeBetween(product, fromDate, toDate);
//	}

//	public List<LcpOrderItem> getOrderItemListByGroupAndTime(Group group, Date fromDate, Date toDate) {
//		Calendar c = Calendar.getInstance();
//		c.setTime(toDate);
//		c.add(Calendar.DATE, 1);
//		toDate = c.getTime();
//		System.out.println("fromDate" + fromDate + "toDate" + toDate);
//		return orderItemRepository.findByOrder_Message_GroupAndOrder_Message_TimeBetween(group, fromDate, toDate);
//	}

	public LcpOrderItem addOrderItem(LcpOrderItem orderItem) {
		return orderItemRepository.save(orderItem);
	}
	
	public void changeToCancel(LcpOrderItem orderItem) {
		// TODO Auto-generated method stub
		orderItem.setStatus("cancelled");
		orderItemRepository.save(orderItem);
		
	}
	
	public void changeToProcess(LcpOrderItem orderItem){
		orderItem.setStatus("processed");
		orderItemRepository.save(orderItem);
	}
	
	public List<LcpOrderItem> getOrderItemsByOrderId(LcpOrder order) {
		// TODO Auto-generated method stub
		return orderItemRepository.findByLcpOrderOrderByOrderItemIdDesc(order);
	}
}
