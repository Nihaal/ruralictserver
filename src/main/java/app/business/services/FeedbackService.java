package app.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.FeedbackRepository;
import app.entities.Feedback;

@Service
public class FeedbackService {

	@Autowired 
	FeedbackRepository feedbackRepository;
	
	public void addFeedback(Feedback feedback) {
		feedbackRepository.save(feedback);
	
		
	}
}
