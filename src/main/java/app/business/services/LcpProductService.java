package app.business.services;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.LcpProductRepository;
import app.data.repositories.ProductTypeRepository;
import app.entities.LcpProduct;
import app.entities.Organization;
import app.entities.Product;
import app.entities.ProductType;

@Service
public class LcpProductService{
	
	@Autowired
	LcpProductRepository lcpProductRepository;
	
	@Autowired
	ProductTypeRepository productTypeRepository;
	
	public List<LcpProduct> getAllProductList(){
		return lcpProductRepository.findAll();
	}
	
	public void addProduct(LcpProduct lcpproduct){
		lcpProductRepository.save(lcpproduct);
	}
	
	public List<ProductType> getProductTypeList(Organization organization){
		return organization.getProductTypes(); 
	}
	
	public List<ProductType> getProTypeList(Organization organization){
		return productTypeRepository.findByOrganizationOrderByProductTypeIdAsc(organization);
	}
	
	public List<LcpProduct> getProductList(List<ProductType> productTypes){
		List<LcpProduct> products = new ArrayList<LcpProduct>();
		for (ProductType productType : productTypes){
			products.addAll(productType.getLcpProducts());
		}
		return products;
	}
	
//	public List<LcpProduct> getAscProductList(ProductType productTypes){
//		lcpProductRepository.findByAndSort(productType, sort)
//	}

	public List<LcpProduct> getLcpProductList(Organization organization){
		return getProductList(getProductTypeList(organization));
	}
	
	public List<LcpProduct> getLcpProductListAdmin(Organization organization){
		return lcpProductRepository.findByOrganization(organization);
	}
	
	public List<LcpProduct> getSearchedProducts(String arg){
		return lcpProductRepository.findByNameContaining(arg);
	}

	public List<LcpProduct> getSearchedProducts(String arg, int from, int to){
		return lcpProductRepository.findByNameContainingAndQuantityBetween(arg, from, to);
	}

	public List<LcpProduct> getSearchedProductsAsc(String arg, int from, int to){
		return lcpProductRepository.findByNameContainingAndQuantityBetweenOrderByUnitRateAsc(arg, from, to);
	}	

	public List<LcpProduct> getSearchedProductsDesc(String arg, int from, int to){
		return lcpProductRepository.findByNameContainingAndQuantityBetweenOrderByUnitRateDesc(arg, from, to);
	}		
	
	public List<LcpProduct> getSearchedProductsSortedByPrice(String arg){
		return lcpProductRepository.findByNameContainingOrderByUnitRateAsc(arg);
	}
	
	public List<LcpProduct> getSearchedProductsSortedByPriceDesc(String arg){
		return lcpProductRepository.findByNameContainingOrderByUnitRateDesc(arg);
	}
//	public List<LcpProduct> getSearchPriceFilteredProducts(String arg, float from, float to){
//		return lcpProductRepository.findByNameContaining(arg);
//	}
	
	public List<LcpProduct> getFilteredPrice(float from, float to){
		
		return lcpProductRepository.findByUnitRateBetween(from, to);
	}
		
	public List<LcpProduct> getOrderedFilteredPrice(){
		return lcpProductRepository.findAllByOrderByUnitRateAsc();
	}
	
	public List<LcpProduct> getOrderedFilteredPriceDesc(){
		return lcpProductRepository.findAllByOrderByUnitRateDesc();
	}
	
	public LcpProduct getMinQuantity(String name){
		return lcpProductRepository.findFirstByNameOrderByQuantityAsc(name);
	}
	
	public LcpProduct getMaxQuantity(String name){
		return lcpProductRepository.findFirstByNameOrderByQuantityDesc(name);
	}
	
	public List<LcpProduct> getProductWithQuantity(String name,int from,int to){
		return lcpProductRepository.findAllByNameAndQuantityBetween(name, from, to);
	}
	
	public LcpProduct getProductById(int id){
		return lcpProductRepository.findByProductId(id);
	}

	public LcpProduct getGlobalMaxQuantity(ProductType productType) {
		return lcpProductRepository.findFirstByProductTypeOrderByQuantityAsc(productType);
	}

	public LcpProduct getGlobalMinQuantity(ProductType productType) {
		return lcpProductRepository.findFirstByProductTypeOrderByQuantityDesc(productType);
	}

}