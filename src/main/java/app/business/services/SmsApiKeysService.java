package app.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.SmsApiKeysRepository;
import app.entities.Organization;
import app.entities.SmsApiKeys;

@Service
public class SmsApiKeysService {
	
	@Autowired
	SmsApiKeysRepository smsApiKeysRepository;

	public SmsApiKeys getApiKeyByOrganization(Organization organization){
		return smsApiKeysRepository.findByOrganization(organization);
	}
}
