package app.business.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.FcmTokensRepository;
import app.entities.FcmTokens;

@Service
public class FcmTokensService {
	
	@Autowired
	FcmTokensRepository gcmTokensRepository;
	
	public FcmTokens getByPhoneNumber(String number) {
		return gcmTokensRepository.findByNumber(number);
	}
	
	public List<FcmTokens> getListByPhoneNumber(String number) {
		return gcmTokensRepository.findAllByNumber(number);
	}
	public void addToken(FcmTokens gcmToken) {
		gcmTokensRepository.save(gcmToken);
	}
	
	public void removeToken(FcmTokens gcmToken) {
		gcmTokensRepository.delete(gcmToken);
	}
	
	public FcmTokens getByToken(String token){
		return gcmTokensRepository.findByToken(token);
	}
}
