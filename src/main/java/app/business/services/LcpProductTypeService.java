package app.business.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.LcpProductTypeRepository;
import app.entities.LcpProductType;
import app.entities.ProductType;

@Service
public class LcpProductTypeService {
	@Autowired
	LcpProductTypeRepository lcpproductTypeRepository;
	
	public List<LcpProductType> getAllProductTypeList(){
		return lcpproductTypeRepository.findAll();
	}
}