package app.business.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.LcpOrderRepository;
import app.entities.Group;
import app.entities.LcpOrder;
import app.entities.Organization;
import app.entities.Registration;
import app.entities.User;
import app.entities.message.Message;

@Service
public class LcpOrderService {
	
	@Autowired
	LcpOrderRepository orderRepository;
	
	/*
	 * Returns list of Orders by an Organization  
	 */
//	public List<LcpOrder> getOrderByOrganizationProcessed(Organization organization) {
//		return orderRepository.findByOrganizationAndStatus(organization, "processed");
//	}
//	
//	public List<LcpOrder> getOrderByOrganizationProcessedSorted(Organization organization) {
//		return orderRepository.findByOrganizationAndStatusOrderByOrderIdDesc(organization, "processed");
//	}
//	
//	public List<LcpOrder> getOrderByOrganizationSaved(Organization organization) {
//		return orderRepository.findByOrganizationAndStatus(organization, "saved");
//	}
//	
//	public List<LcpOrder> getOrderByOrganizationSavedSorted(Organization organization) {
//		return orderRepository.findByOrganizationAndStatusOrderByOrderIdDesc(organization, "saved");
//	}
//	
//	public List<LcpOrder> getOrderByOrganizationCancelled(Organization organization) {
//		return orderRepository.findByOrganizationAndStatus(organization, "cancelled");
//	}
//	
//	public List<LcpOrder> getOrderByOrganizationCancelledSorted(Organization organization) {
//		return orderRepository.findByOrganizationAndStatusOrderByOrderIdDesc(organization, "cancelled");
//	}
//	
//	public List<LcpOrder> getOrderByOrganizatonDeliveredSorted(Organization organization) {
//		return orderRepository.findByOrganizationAndStatusOrderByOrderIdDesc(organization, "delivered");
//
//	}
//	public List<LcpOrder> getOrderByOrganizationRejected(Organization organization) {
//		return orderRepository.findByOrganizationAndStatus(organization, "rejected");
//	}
//	
//	public List<LcpOrder> getOrderByOrganizationRejectedSorted(Organization organization) {
//		return orderRepository.findByOrganizationAndStatusOrderByOrderIdDesc(organization, "rejected");
//	}

//	public List<LcpOrder> getOrderByGroupProcessed(Group group)
//	{
//		return orderRepository.findByMessage_groupAndStatus(group,"processed");
//	}
//	
	/*
	 * changes the status of an order to 'saved'
	 */
	public void saveOrder(LcpOrder order)
	{
		order.setStatus("saved");
		orderRepository.save(order);
	}
	
	/*
	 * changes the status of an order to 'accepted'
	 */
	public void acceptOrder(LcpOrder order)
	{
		order.setStatus("accepted");
		orderRepository.save(order);
	}
	
	
	public void changeToCancel(LcpOrder order) {
		// TODO Auto-generated method stub
		order.setStatus("cancelled");
		orderRepository.save(order);
	}
	
	
	/*
	 * changes the status of an order to 'rejected'
	 */
	public void rejectOrder(LcpOrder order)
	{
		order.setStatus("rejected");
		orderRepository.save(order);
	}
	
	/*
	 * changes the status of an order to 'processed'
	 */
	public void processOrder(LcpOrder order)
	{
		order.setStatus("processed");
		orderRepository.save(order);
	}
	
	/*
	 * Return message for a particular order
	 */
//	public Message getMessageByOrder(LcpOrder order)
//	{
//		return order.getMessage();
//	}
	
//	public User getMessageUserByOrder(LcpOrder order)
//	{
//		return order.getMessage().getUser();
//	}
	
	public LcpOrder getOrder(int orderId) {
		
		return orderRepository.findOne(orderId);
	}
	
	public LcpOrder addOrder(LcpOrder order) {
		
		return orderRepository.save(order);
	}

	public void removeOrder(LcpOrder order) {
	
		orderRepository.delete(order);
}

	public void cancelOrder(LcpOrder order) {
		order.setStatus("cancelled");
		orderRepository.save(order);
		
	}
	
	public void changeToProcessed(LcpOrder order){
		order.setStatus("processed");
		orderRepository.save(order);
	}
	
	public List<LcpOrder> getOrdersByUsers(Registration reg){
		return orderRepository.findByRegistrationOrderByOrderIdDesc(reg);
	}
//	public List<LcpOrder> getOrderByOrganization(Organization organization) {
//		return orderRepository.findByOrganization(organization); 
//	}
//	
//	public List<LcpOrder> getOrderByOrganizationSorted(Organization organization) {
//		return orderRepository.findByOrganizationOrderByOrderIdDesc(organization); 
//	}
	
	//public List <Order> getOrderByOrganization
}