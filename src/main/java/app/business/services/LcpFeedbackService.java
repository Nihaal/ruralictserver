package app.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.LcpFeedbackRepository;
import app.entities.LcpFeedback;

@Service
public class LcpFeedbackService {

	@Autowired 
	LcpFeedbackRepository feedbackRepository;
	
	public void addFeedback(LcpFeedback feedback) {
		feedbackRepository.save(feedback);
	
		
	}
}
