package app.business.services;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.data.repositories.ProductRepository;
import app.data.repositories.RegistrationRepository;
import app.entities.Registration;

@Service
public class RegistrationService{
	@Autowired
	RegistrationRepository registrationrepository;
	
	public Registration getUserByPhoneAndPassword(String phone, String password){
		return registrationrepository.findByPhoneAndPassword(phone, password);
	}
	
	public void addUser(Registration registration){
		registrationrepository.save(registration);
	}
}

