package app.business.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.websocket.server.PathParam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.entities.Organization;
import app.entities.Product;
import app.entities.ProductType;


@Controller
@RequestMapping("/web/{org}")
public class ProductTypeController 
{

	@Autowired
	OrganizationService organizationService;
	@Autowired
	ProductTypeService productTypeService;
	@Autowired
	ProductService productService;
	

	@Transactional
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@RequestMapping(value="/productTypePage",method = RequestMethod.GET)
	public String productsPage(@PathVariable String org, Model model) {
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		List<ProductType> productTypes = new ArrayList<ProductType>(organization.getProductTypes());
		model.addAttribute("organization",organization);
		model.addAttribute("productTypes", productTypes);
		//No change in model here yet
		return "productTypeList";
	}
	
	@PreAuthorize("hasRole('ADMIN'+#org)") 
	@RequestMapping(value="/prodtypes",method = RequestMethod.GET)
	public @ResponseBody String productList(@PathVariable String org) {
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		List<ProductType> prod= productTypeService.getAllByOrganisation(organization);
		Iterator<ProductType> iterator = prod.iterator();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray prodArray=new JSONArray();
		while(iterator.hasNext())
		{
			
			ProductType p = iterator.next();
			JSONObject obj = new JSONObject();
			try{
			obj.put("id",p.getProductTypeId());
			obj.put("name", p.getName());
			}
			catch(JSONException e)
			{
			}
			prodArray.put(obj);
		}
		try {
			responseJsonObject.put("products",prodArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return responseJsonObject.toString();
	}
	@PreAuthorize("hasRole('ADMIN'+#org)") 
	@RequestMapping(value="/prodtypedelete/{id}",method = RequestMethod.GET)

	public @ResponseBody String productDelete(@PathVariable String org, @PathVariable int id) {
		//Organization organization = organizationService.getOrganizationByAbbreviation(org);
		ProductType productType = productTypeService.getReturnProductTypeById(id);
		JSONObject responseJsonObject = new JSONObject();
		try{
		List  <Product> listProduct = productType.getProducts();
		if (!listProduct.isEmpty())
			throw new Exception();
		productTypeService.removeProductType(productType);
		}
		catch(Exception e) {
			/*
			try {
				responseJsonObject.put("message", "Please remove products under this product type");
				return responseJsonObject.toString();
			} catch (JSONException e3) {
				e3.printStackTrace();
			} */
			try {	
				deleteProducts(productType);
				ProductType productType2 = productTypeService.getReturnProductTypeById(id);
				productTypeService.removeProductType(productType2);
			}
			catch(Exception e1){
				try {
					responseJsonObject.put("status", "error");
				} catch (JSONException e2) {
					e2.printStackTrace();
				}
				return responseJsonObject.toString();
			}
		}
		try {
			responseJsonObject.put("status","success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	@Transactional
	public void deleteProducts(ProductType productType) throws Exception {
		List<ProductType> productTypeList = new ArrayList<ProductType>();
		productTypeList.add(productType);
		List <Product> products = productType.getProducts();
		Iterator<Product>iterator = products.iterator();
		while(iterator.hasNext()) {
			Product product = iterator.next();
			productService.removeProduct(product);
		}
		productType.setProducts(null);
		productTypeService.addProductType(productType);
	}
	
	
	@PreAuthorize("hasRole('ADMIN'+#org)") 
	@RequestMapping(value="/prodtypesid",method = RequestMethod.GET)

	public @ResponseBody String productId(@PathVariable String org, @RequestParam("name") String name) {
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		ProductType product = productTypeService.getByOrganizationAndName(organization, name);
		return "productTypes/"+product.getProductTypeId();
	}
	//Aug 2 Anu
	
	@RequestMapping(value="/getProductTypeByOrgg/",method = RequestMethod.GET)
	public @ResponseBody String producttype(@PathVariable String org) {
		System.out.println("Reached here!@!#!@@!$#@#@!$!");
		System.out.println(org);
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		List<ProductType> productTypes = new ArrayList<ProductType>(organization.getProductTypes());
		//List<Integer> producttypestatus=new ArrayList<Integer>();
		
		//int[] producttypestatus=new int[productTypes.size()];
		JSONObject responseJsonObject = new JSONObject();
		JSONArray prodTArray=new JSONArray();
		Iterator<ProductType> iterator=productTypes.iterator();
		
		
		while(iterator.hasNext())
		{
			ProductType p=iterator.next();
			JSONObject obj = new JSONObject();
			try{
				obj.put("stat", 0);
				obj.put("id", p.getProductTypeId());
				obj.put("status",p.getStatus() );
				obj.put("name", p.getName());
				obj.put("imageUrl",p.getImageUrl());
				obj.put("sequence", p.getSequence());
				obj.put("parentProductType",p.getParentProductType());
				obj.put("description", p.getDescription());
			}catch(Exception e)
			{
			}
			prodTArray.put(obj);
			
			
		}
		System.out.println("reached the end");
		
		try{
			responseJsonObject.put("producttyper", prodTArray);
		}catch(Exception e){
			System.out.println("problem exists in json!!!");
			//e.printStackTrace();
		}
		
		//No change in model here yet
		System.out.println(responseJsonObject);
		return responseJsonObject.toString();
	}
	
}

