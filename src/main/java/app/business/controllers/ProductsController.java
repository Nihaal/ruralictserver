package app.business.controllers;


import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.business.services.OrderSummaryService;
import app.business.services.OrderSummaryService.OrderSummary;
import app.entities.Organization;
import app.entities.Product;
import app.entities.ProductType;
import app.util.SpreadsheetParser;
import app.util.Utils;

@Controller
@RequestMapping("/web/{org}")
public class ProductsController {

	@Autowired
	OrganizationService organizationService;
	@Autowired
	ProductService productService;
	
	@Autowired
	OrderSummaryService orderSummaryService;
	
	@Autowired
	ProductTypeService productTypeService;
	
	@Transactional
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@RequestMapping(value="/productsPage",method = RequestMethod.GET)
	public String productsPageInitial(@PathVariable String org, Model model) {
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		List<ProductType> productTypes = productService.getProductTypeList(organization);
		List<Product> products = productService.getProductList(productTypes);
		model.addAttribute("organization",organization);
		model.addAttribute("productTypes",productTypes);
		model.addAttribute("products",products);
		return "productList";
	}
	
	
	@Transactional
	@RequestMapping (value="/uploadsheet", method = RequestMethod.POST, produces = "text/plain")
	public @ResponseBody String handleSheetUpload(HttpServletRequest request, @PathVariable String org) {
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mRequest.getFileNames();
		MultipartFile mFile = mRequest.getFile(itr.next());
		String fileName = mFile.getOriginalFilename();
		File temp = Utils.saveFile("temp.xlsx", Utils.getSpreadsheetDir(), mFile);
		File serverFile = new File(Utils.getSpreadsheetDir() +File.separator+ fileName);
		if (serverFile.exists())
			serverFile.delete();
		int flag =1;
		do{

			try 
			{
				Files.copy(temp.toPath(), serverFile.toPath());
				flag=1;
			}	
			catch(Exception e){
				e.printStackTrace();
			}
		}while(flag==0);
		String url = Utils.getSpreadsheetDirURL() + fileName;
		System.out.println(url);
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		HashMap<Integer, String []> data = SpreadsheetParser.parseProductSheet(organization.getAbbreviation(), serverFile);
		System.out.println("Data Size: "+data.size());
		for (int i =0;i<data.size();i++) {
			String []contents = data.get(i);
			int id = Integer.parseInt(contents[0]);
			String name = contents[1];
			String type = contents[2];
			float rate = Float.parseFloat(contents[3]);
			int quantity = Integer.parseInt(contents[4]);
			if (id ==0) {
				//new product
				System.out.println("New product");
				Product product = new Product();
				ProductType productType = productTypeService.getByOrganizationAndName(organization, type);
				product.setName(name);
				product.setUnitRate(rate);
				product.setProductType(productType);
				product.setQuantity(quantity);
				try {
					productService.addProduct(product);
				}
				catch(Exception e) {
					System.out.println("cannot add");
				}
				
			}
			else if ( id != 0) {
				//Edit product
				System.out.println("in edit prodcut");
				Product product = productService.getProductById(id);
				int flg = 0;
				if (!name.equals(product.getName())) {
					product.setName(name);
					flg =1;
				}
				if (rate != product.getUnitRate()){
					product.setUnitRate(rate);
					flg=1;
				}
				if (quantity != product.getQuantity()){
					product.setQuantity(quantity);
					flg=1;
				}
				if (flg == 1 ) {
					try {
						productService.addProduct(product);
					}
					catch(Exception e) {
						System.out.println("cannot update");
					}
				}
			}
			
			
			
		}
		return url;
	
	}
	
	@Transactional
	@RequestMapping(value="/uploadpicture", method=RequestMethod.POST, produces = "text/plain")
	public @ResponseBody String handleFileUpload(HttpServletRequest request) {
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mRequest.getFileNames();
		
		//only one iteration i.e itr.next() as it has only one file
		MultipartFile mFile = mRequest.getFile(itr.next());
		String fileName = mFile.getOriginalFilename();
		System.out.println("File is "+fileName);
		File temp = Utils.saveFile("temp.jpg", Utils.getImageDir(), mFile);
		File serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
		System.out.println(Utils.getImageDir() +File.separator+ fileName);
		Random randomint = new Random();
		int flag=1;
		do{
			try 
			{
				Files.copy(temp.toPath(), serverFile.toPath());
				flag=1;
				System.out.println("Copied.");
			}	
			catch (FileAlreadyExistsException e)
			{
				System.out.println("File already exist. Renaming file and trying again.");
				fileName = fileName.substring(0,fileName.length()-4);
				fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".jpg";
				serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
				flag=0;
			}
			catch (IOException e) {
				e.printStackTrace();
				flag=1;
			}
		}while(flag==0);
		
		
		String url = Utils.getImageDirURL() + fileName;
		System.out.println(url);
		return url;
	    
	}
	
	//upload image for editing product
	
	@Transactional
	@RequestMapping(value="/uploadpictureEdit", method=RequestMethod.POST, produces = "text/plain")
	public @ResponseBody String handleFileUploadForEdit(HttpServletRequest request) {
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mRequest.getFileNames();
		
		//only one iteration i.e itr.next() as it has only one file
		MultipartFile mFile = mRequest.getFile(itr.next());
		String fileName = mFile.getOriginalFilename();
		System.out.println("File is "+fileName);
		File temp = Utils.saveFile("temp.jpg", Utils.getImageDir(), mFile);
		File serverFile = new File(Utils.getImageDir() +File.separator+ fileName);

		System.out.println(Utils.getImageDir() +File.separator+ fileName);
		Random randomint = new Random();
		int flag=1;
		do{
			try 
			{
				Files.copy(temp.toPath(), serverFile.toPath());
				flag=1;
			}	
			catch (FileAlreadyExistsException e)
			{
				System.out.println("File already exist. Renaming file and trying again.");
				fileName = fileName.substring(0,fileName.length()-4);
				fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".jpg";
				serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
				flag=0;
			}
			catch (IOException e) {
				e.printStackTrace();
				flag=1;
			}
		}while(flag==0);
		
		
		String url = Utils.getImageDirURL() + fileName;
		System.out.println(url);
		return url;
	    
	}
	




	@Transactional
	@RequestMapping(value="/uploadaudio", method=RequestMethod.POST, produces = "text/plain")
	public @ResponseBody String handleSoundUpload(HttpServletRequest request) {
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mRequest.getFileNames();
		
		//only one iteration i.e itr.next() as it has only one file
		MultipartFile mFile = mRequest.getFile(itr.next());
		String fileName = mFile.getOriginalFilename();
		System.out.println("File is "+fileName);
		File temp = Utils.saveFile("temp.mp3", Utils.getAudioDir(), mFile);
		File serverFile = new File(Utils.getAudioDir() +File.separator+ fileName);

		System.out.println(Utils.getAudioDir() +File.separator+ fileName);
		Random randomint = new Random();
		int flag=1;
		do{
			try 
			{
				Files.copy(temp.toPath(), serverFile.toPath());
				flag=1;
				System.out.println("Copied.");
			}	
			catch (FileAlreadyExistsException e)
			{
				System.out.println("File already exist. Renaming file and trying again.");
				fileName = fileName.substring(0,fileName.length()-4);
				fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".wav";
				serverFile = new File(Utils.getAudioDir() +File.separator+ fileName);
				flag=0;
			}
			catch (IOException e) {
				e.printStackTrace();
				flag=1;
			}
		}while(flag==0);
		
		
		String url = Utils.getAudioDirURL() + fileName;
		System.out.println(url);
		return url;
	    
	}
	
	
	
	@Transactional
	@RequestMapping(value="/uploadaudioedit", method=RequestMethod.POST, produces = "text/plain")
	public @ResponseBody String handleSoundUploadForEdit(HttpServletRequest request) {
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mRequest.getFileNames();
		
		//only one iteration i.e itr.next() as it has only one file
		MultipartFile mFile = mRequest.getFile(itr.next());
		String fileName = mFile.getOriginalFilename();
		System.out.println("File is "+fileName);
		File temp = Utils.saveFile("temp.mp3", Utils.getAudioDir(), mFile);
		File serverFile = new File(Utils.getAudioDir() +File.separator+ fileName);

		System.out.println(Utils.getAudioDir() +File.separator+ fileName);
		Random randomint = new Random();
		int flag=1;
		do{
			try 
			{
				Files.copy(temp.toPath(), serverFile.toPath());
				flag=1;
				System.out.println("Copied.");
			}	
			catch (FileAlreadyExistsException e)
			{
				System.out.println("File already exist. Renaming file and trying again.");
				fileName = fileName.substring(0,fileName.length()-4);
				fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".wav";
				serverFile = new File(Utils.getAudioDir() +File.separator+ fileName);
				flag=0;
			}
			catch (IOException e) {
				e.printStackTrace();
				flag=1;
			}
		}while(flag==0);
		
		
		String url = Utils.getAudioDirURL() + fileName;
		System.out.println(url);
		return url;
	    
	}
	
	
	@RequestMapping(value="/statusToggle",method = RequestMethod.GET)
	public @ResponseBody String globalStatusChange(@PathVariable String org, @RequestParam(value="status") int status)
	{
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		List<Product> products = productService.getProductList(organization);
		Iterator <Product> iterator = products.iterator();
		while (iterator.hasNext()) {
			Product product = iterator.next();
			product.setStatus(status);
			productService.addProduct(product);
		}
		return null;
	}
	
	
	// worst try mapping API
	@RequestMapping(value="/statusEnableToggle",method = RequestMethod.GET)
	public @ResponseBody String statusEnableToggle(@PathVariable String org, @RequestParam(value="productId") int productId)
	{ 
		
			Product product = productService.getProductById(productId);
			product.setStatus(1);
			productService.addProduct(product);
		
		return null;
	}
	@RequestMapping(value="/statusDisableToggle",method = RequestMethod.GET)
	public @ResponseBody String statusDisableToggle(@PathVariable String org, @RequestParam(value="productId") int productId)
	{ 
		
			Product product = productService.getProductById(productId);
			product.setStatus(0);
			productService.addProduct(product);
		
		return null;
	}
	//
	
	
	@Transactional
	@RequestMapping(value="/generatesheet", method=RequestMethod.GET)
	
	public @ResponseBody File generateSheet(@PathVariable String org) {
		System.out.println("Controller hit");
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
	//	response.setHeader("Content-disposition","attachment; filename=" + organization.getAbbreviation()+"-product.xlsx");
      //  response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");


		List<ProductType> productTypes = productService.getProductTypeList(organization);
		List<Product> products = productService.getProductList(productTypes);
		Iterator <Product> iterator = products.iterator();
		List <String> prodNames = new ArrayList<String>();
		List <Integer> prodId = new ArrayList<Integer>();
		List <String> prodType = new ArrayList<String>();
		List <Float> unitRate = new ArrayList<Float>();
		List <Integer> quantity = new ArrayList<Integer>();
		List <ProductType> productType = productTypeService.getAllByOrganisation(organization);

		 do{
			Product product = iterator.next();
			prodNames.add(product.getName());
			prodId.add(product.getProductId());
			prodType.add(String.valueOf(product.getProductType().getName()));
			unitRate.add(product.getUnitRate());
			quantity.add(product.getQuantity());
		}while(iterator.hasNext());

			prodNames.add("0");
			prodId.add(0);
			prodType.add("0");
			unitRate.add(0.0f);
			quantity.add(0);
			
		File file = SpreadsheetParser.generateProductSheet(prodId, prodNames, prodType, unitRate, quantity, productType, organization.getAbbreviation());
		/*InputStream is;
		try {
			 is = new FileInputStream(file.getAbsolutePath());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
	      response.flushBuffer();
	    } catch (IOException ex) {
	      throw new RuntimeException("IOError writing file to output stream");
	    }
		*/
		
		if(file.exists()) {
			System.out.println("file exists");
		}
		System.out.println("File path: "+file.getAbsolutePath());
		return file;
		}

		
		@Transactional
		@RequestMapping(value="/{date_t}/generateSummary", method=RequestMethod.GET)
		
		public @ResponseBody File generateSummary(@PathVariable String org, @PathVariable String date_t) {
			System.out.println("Controller hit");
			String date_f = "2015-01-01";
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	        Date fromDate = null;
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
	        Date toDate = null;
			try {
				fromDate = df.parse(date_f);
				toDate = dt.parse(date_t);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(fromDate);
			List<OrderSummary> orders = orderSummaryService.getOrderSummaryListForOrganization(organizationService.getOrganizationByAbbreviation(org), fromDate, toDate);
			Iterator <OrderSummary> iterator = orders.iterator();
			List <String> prodNames = new ArrayList<String>();
			List <Float> quantity = new ArrayList<Float>();
			List <Float> unitRate = new ArrayList<Float>();
	
			while(iterator.hasNext()) {
				OrderSummary order = iterator.next();
				prodNames.add(order.getProductName());
				unitRate.add(order.getunitRate()*order.getQuantity());
				quantity.add(order.getQuantity());
			}
			File file = SpreadsheetParser.generateSummary(prodNames, quantity, unitRate, organizationService.getOrganizationByAbbreviation(org).getAbbreviation());
			return file; 
		}
		
		
		
		
		/*String mimeType = new MimetypesFileTypeMap().getContentType(organization.getAbbreviation()+"-product.xlsx");
        System.out.println("MIME Type: "+mimeType);
        response.setContentType(mimeType);
        response.setContentLength((int) file.length());
        
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		OutputStream out = null;
		try {
			out = response.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] buffer= new byte[81920];
		int length = 0;

		try {
			while ((length = in.read(buffer)) > 0){
			     out.write(buffer, 0, length);
			     System.out.println("length: "+length);
			}
			in.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		*/

	

}