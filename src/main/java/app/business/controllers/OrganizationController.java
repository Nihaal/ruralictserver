package app.business.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import app.business.services.OrganizationService;
import app.business.services.VoiceService;
import app.data.repositories.WelcomeMessageRepository;
import app.entities.WelcomeMessage;

@Controller
@RequestMapping("/web/{org}")
public class OrganizationController {
	
	@Autowired
	WelcomeMessageRepository messageRepository;	
	@Autowired
	OrganizationService orgService;	
	@Autowired
	VoiceService voiService;

	@RequestMapping(value="/organizationPage")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	public String organizationPage(@PathVariable String org, Model model) {
		messageRepository.save(new WelcomeMessage(orgService.getOrganizationByAbbreviation("HKART"), "en", voiService.getVoice(1)));
		messageRepository.save(new WelcomeMessage(orgService.getOrganizationByAbbreviation("HKART"), "hi", voiService.getVoice(2)));
		messageRepository.save(new WelcomeMessage(orgService.getOrganizationByAbbreviation("HKART"), "mr", voiService.getVoice(3)));
		return "organization";
	}

}
