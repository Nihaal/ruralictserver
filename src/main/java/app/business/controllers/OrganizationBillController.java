package app.business.controllers;



import java.util.Date;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import java.util.List;


import javax.transaction.Transactional;

import org.apache.http.impl.cookie.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;

import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.business.services.BillLayoutSettingsService;

import app.business.services.GroupService;

import app.business.services.OrderService;

import app.business.services.OrganizationService;

import app.business.services.UserPhoneNumberService;
import app.data.repositories.OrderRepository;
import app.data.repositories.OrdersStoreRepository;
import app.data.repositories.UserRepository;

import app.entities.BillLayoutSettings;

import app.entities.Group;

import app.entities.Order;

import app.entities.OrderItem;
import app.entities.OrdersStore;
import app.entities.Organization;

import app.entities.User;

import app.entities.UserPhoneNumber;

import app.entities.message.Message;
import app.util.SpreadsheetParser;
import app.util.Utils;


@Controller

@RequestMapping("/web/{org}")

public class OrganizationBillController {


	@Autowired

	UserRepository userrepository;

	
	@Autowired

	OrdersStoreRepository ordersstorerepository;

	

	@Autowired

	OrderService orderservice;	

	@Autowired

	OrderRepository orderrepo;

	

	@Autowired

	OrganizationService organizationservice;

	

	@Autowired

	BillLayoutSettingsService billLayoutSettingsService;

	

	@Autowired

	GroupService groupservice;

	

	@Autowired

	UserPhoneNumberService userPhoneNumberService;

	

public static  class OrderWithPhoneNumber {

		

		private String phone;

		private Order order;

		private String timeInHoursAndMinutes;





	public OrderWithPhoneNumber(Order order, String phone,String timeInHoursAndMinutes)

	{

		

		this.order = order;

		this.phone = phone;

		this.timeInHoursAndMinutes = timeInHoursAndMinutes;

	}


	public Order getOrder()

	{

		return order;

	}


	public String getPhone()

	{

		return phone;

	}

	

	public String getTimeInHoursAndMinutes()

	{

		return timeInHoursAndMinutes;

	}

	

	public void setTimeInHoursAndMinutes(String timeInHoursAndMinutes)

	{

		this.timeInHoursAndMinutes = timeInHoursAndMinutes;

	}

	

	public void setOrder(Order order)

	{

		this.order = order;

	}


	public void setPhone(String phone)

	{

		this.phone = phone;

	}

	

	}


	@RequestMapping(value="/generateOrganizationBill")

	@PreAuthorize("hasRole('ADMIN'+#org)")

	@Transactional

	public String generateBill(@PathVariable String org, Model model) {

		List<Order> orderList = orderservice.getOrderByOrganizationProcessed(organizationservice.getOrganizationByAbbreviation(org));

		//Removing orders with No message associated to it.

		List<Order> nullOrders=new ArrayList<Order>();

		List<Float> totalCost= new ArrayList<Float>();

 		for(Order order:orderList){

			if(order.getMessage()==null)

				nullOrders.add(order);

			else

			{

				for(OrderItem orderItem : order.getOrderItems()) {

					totalCost.add(orderItem.getUnitRate() * orderItem.getQuantity());

				}

			}

		}

		orderList.removeAll(nullOrders);

		

		BillLayoutSettings billLayoutSetting = billLayoutSettingsService.getBillLayoutSettingsByOrganization(organizationservice.getOrganizationByAbbreviation(org));

		model.addAttribute("billLayout",billLayoutSetting);

		List<Integer> indexes= new ArrayList<Integer>(orderList.size());

		for(int index=0;index<orderList.size();index++)

		{

			indexes.add(index);

		}

		model.addAttribute("total",totalCost);

		model.addAttribute("orders",orderList);

		return "generateBillMultiple";

	}

	
	@RequestMapping(value="/generateBillGroup/{fromDate}/{toDate}")

//	@RequestMapping(value="/generateBillGroup/{groupId}")
	@PreAuthorize("hasRole('ADMIN'+#org)")

	@Transactional
	public String generateBillGroup(@PathVariable String org, @PathVariable String fromDate, @PathVariable String toDate, Model model) {
		
		User user;
		UserPhoneNumber userPhoneNumber;

		Message message;

		List<Order> orderProcessed = orderrepo.findByOrganizationAndStatus(organizationservice.getOrganizationByAbbreviation(org), "processed");

		List<Order> orderDelivered = orderrepo.findByOrganizationAndStatus(organizationservice.getOrganizationByAbbreviation(org), "delivered");
		
		
		List<Order> orderList = new ArrayList<Order>(); //getOrderByGroupProcessed(groupservice.getGroup(groupId));

		orderList.addAll(orderDelivered);
		orderList.addAll(orderProcessed);
		//Removing orders with No message associated to it.

		List<Order> nullOrders=new ArrayList<Order>();

		List<Float> totalCost= new ArrayList<Float>(0);

 		for(Order order:orderList){

			if(order.getMessage()==null)

				nullOrders.add(order);

			else if(order.getStatus().equals("processed"))

			{

				float sum=0;

				for(OrderItem orderItem : order.getOrderItems()) {

					System.out.println(orderItem.getProduct().getName());
					sum+=orderItem.getUnitRate() * orderItem.getQuantity();

				}

				totalCost.add(sum);

			}
			
			else if(order.getStatus().equals("delivered"))

			{

				float sum=0;
				int flag = 0;
				for(OrderItem orderItem : order.getOrderItems()) {

					if(orderItem.getProduct() == null){
						flag = 1;
						break;
					}
					else{
						sum+=orderItem.getUnitRate() * orderItem.getQuantity();
					}
				}
				
				if(flag == 0)
					totalCost.add(sum);
				else
					nullOrders.add(order);

			}


			/*else if(order.getStatus().equals("delivered"))

			{

				float sum=0;

				for(OrdersStore orderStore : ordersstorerepository.findByOrder(order)) {

					sum+=orderStore.getUnitRate() * orderStore.getQuantity();

				}

				totalCost.add(sum);

			}*/

		}

		orderList.removeAll(nullOrders);

		

		BillLayoutSettings billLayoutSetting = billLayoutSettingsService.getBillLayoutSettingsByOrganization(organizationservice.getOrganizationByAbbreviation(org));

		model.addAttribute("billLayout",billLayoutSetting);

		List<Integer> indexes= new ArrayList<Integer>(orderList.size());

		for(int index=0;index<orderList.size();index++)

		{

			indexes.add(index);

		}

		List<OrderWithPhoneNumber> orderWithPhoneNumberList = new ArrayList<OrderWithPhoneNumber>(orderList.size());

		Iterator<Order> iterator = orderList.iterator();
		int val = 0;
		while(iterator.hasNext()) {
		   Order order = iterator.next();

			//Test
			
			List<OrderItem> orderItems = order.getOrderItems();
			
			Iterator<OrderItem> itr = orderItems.iterator();
			
			while(itr.hasNext()){
				OrderItem oItem = itr.next();
				System.out.println(oItem.getProduct().getDescription());
			}

			
			//Test
		   message = order.getMessage();

		   user = message.getUser();

		   userPhoneNumber = userPhoneNumberService.getUserPrimaryPhoneNumber(user);

		   String timeInHoursAndMinutes = order.getMessage().getTime().toString().substring(0, 16);
		   
		   // To be tested
		   
		   String formattedDate = order.getMessage().getTime().toString().substring(0,10);		   
		   //OrderWithPhoneNumber orderWithPhoneNumber = new  OrderWithPhoneNumber(order,userPhoneNumber.getPhoneNumber(),timeInHoursAndMinutes);
		   //orderWithPhoneNumberList.add(orderWithPhoneNumber);
				
		   Date dateBill = new Date(Integer.parseInt(formattedDate.substring(0,4)),Integer.parseInt(formattedDate.substring(5,7)),Integer.parseInt(formattedDate.substring(8,10)));
		   Date dateFrom = new Date(Integer.parseInt(fromDate.substring(0,4)),Integer.parseInt(fromDate.substring(5,7)),Integer.parseInt(fromDate.substring(8,10)));
		   Date dateTo = new Date(Integer.parseInt(toDate.substring(0,4)),Integer.parseInt(toDate.substring(5,7)),Integer.parseInt(toDate.substring(8,10)));
		   Calendar c =  Calendar.getInstance(); 
		   c.setTime(dateTo);
		   c.add(Calendar.DATE, 1);
		   dateTo = c.getTime();
		   //System.out.println(dateTo +"  "+dateFrom);
		   if(dateBill.compareTo(dateTo)<=0 && dateBill.compareTo(dateFrom)>=0) {
			   OrderWithPhoneNumber orderWithPhoneNumber = new  OrderWithPhoneNumber(order,userPhoneNumber.getPhoneNumber(),timeInHoursAndMinutes);
			   orderWithPhoneNumberList.add(orderWithPhoneNumber);
			   val++;
	       }
	       else{
	    	   totalCost.remove(val);
	       }
		}
		model.addAttribute("total",totalCost);

		model.addAttribute("orderWithPhoneNumbers",orderWithPhoneNumberList);

		return "generateBillMultiple";

	}

	
	
	
	@RequestMapping(value="/generateBillGroupNew/{fromDate}/{toDate}")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String generateBillGroupNew(@PathVariable String org, @PathVariable String fromDate, @PathVariable String toDate, Model model) {
		
		User user;
		UserPhoneNumber userPhoneNumber;
		Message message;
		List<Order> orderProcessed = orderrepo.findByOrganizationAndStatus(organizationservice.getOrganizationByAbbreviation(org), "processed");
		List<Order> orderDelivered = orderrepo.findByOrganizationAndStatus(organizationservice.getOrganizationByAbbreviation(org), "delivered");
		List<Order> orderList = new ArrayList<Order>(); //getOrderByGroupProcessed(groupservice.getGroup(groupId));
		//orderList.addAll(orderDelivered);
		orderList.addAll(orderProcessed);
		//Removing orders with No message associated to it.
		List<Order> nullOrders=new ArrayList<Order>();
		List<Float> totalCost= new ArrayList<Float>(0);
 		for(Order order:orderList){
			if(order.getMessage()==null)
				nullOrders.add(order);
			else if(order.getStatus().equals("processed"))
			{
				float sum=0;
				for(OrderItem orderItem : order.getOrderItems()) {
					sum+=orderItem.getUnitRate() * orderItem.getQuantity();
				}
				totalCost.add(sum);
			}
			/*else if(order.getStatus().equals("delivered"))
			{
				float sum=0;
				for(OrdersStore orderStore : ordersstorerepository.findByOrder(order)) {
					sum+=orderStore.getUnitRate() * orderStore.getQuantity();
				}
				totalCost.add(sum);
			}*/
		}
		orderList.removeAll(nullOrders);
		BillLayoutSettings billLayoutSetting = billLayoutSettingsService.getBillLayoutSettingsByOrganization(organizationservice.getOrganizationByAbbreviation(org));
		model.addAttribute("billLayout",billLayoutSetting);
		List<Integer> indexes= new ArrayList<Integer>(orderList.size());
		for(int index=0;index<orderList.size();index++)
		{
			indexes.add(index);
		}
		List<OrderWithPhoneNumber> orderWithPhoneNumberList = new ArrayList<OrderWithPhoneNumber>(orderList.size());
		Iterator<Order> iterator = orderList.iterator();
		int val = 0;
		while(iterator.hasNext()) {
			   Order order = iterator.next();
		       message = order.getMessage();
			   user = message.getUser();
			   userPhoneNumber = userPhoneNumberService.getUserPrimaryPhoneNumber(user);
			   String timeInHoursAndMinutes = order.getMessage().getTime().toString().substring(0, 16);
		   
			   // To be tested			   
			   String formattedDate = order.getMessage().getTime().toString().substring(0,10);		   
			   //OrderWithPhoneNumber orderWithPhoneNumber = new  OrderWithPhoneNumber(order,userPhoneNumber.getPhoneNumber(),timeInHoursAndMinutes);
			   //orderWithPhoneNumberList.add(orderWithPhoneNumber);
			   Date dateBill = new Date(Integer.parseInt(formattedDate.substring(0,4)),Integer.parseInt(formattedDate.substring(5,7)),Integer.parseInt(formattedDate.substring(8,10)));
			   Date dateFrom = new Date(Integer.parseInt(fromDate.substring(0,4)),Integer.parseInt(fromDate.substring(5,7)),Integer.parseInt(fromDate.substring(8,10)));
			   Date dateTo = new Date(Integer.parseInt(toDate.substring(0,4)),Integer.parseInt(toDate.substring(5,7)),Integer.parseInt(toDate.substring(8,10)));
			   Calendar c =  Calendar.getInstance(); 
			   c.setTime(dateTo);
			   c.add(Calendar.DATE, 1);
			   dateTo = c.getTime();
			   //System.out.println(dateTo +"  "+dateFrom);
			   if(dateBill.compareTo(dateTo)<=0 && dateBill.compareTo(dateFrom)>=0) {
				   OrderWithPhoneNumber orderWithPhoneNumber = new  OrderWithPhoneNumber(order,userPhoneNumber.getPhoneNumber(),timeInHoursAndMinutes);
				   orderWithPhoneNumberList.add(orderWithPhoneNumber);
				   val++;
		       }
		       else{
		    	   totalCost.remove(val);
		       }
		}
		model.addAttribute("total",totalCost);
		model.addAttribute("orderWithPhoneNumbers",orderWithPhoneNumberList);
		return "generateBillMultiple";
	}
	
	
	@RequestMapping(value="/generateBill")	
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String generateBillPage(@PathVariable String org,Model model) {
		Organization organization=organizationservice.getOrganizationByAbbreviation(org);
		List<Group> groupList = organization.getGroups();
		model.addAttribute("groups", groupList);
		return "generateBillLandingPage";
	}

	

}
