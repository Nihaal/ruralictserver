package app.business.controllers.rest;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import app.entities.Group;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.services.EnquiryService;
import app.business.services.FeedbackService;
import app.business.services.GcmTokensService;
import app.business.services.GroupMembershipService;
import app.business.services.GroupService;
import app.business.services.LcpProductService;
import app.business.services.OrderService;
import app.business.services.OrdersStoreService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.business.services.ReferralService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.business.services.UserViewService;
import app.business.services.VoiceService;
import app.business.services.UserViewService.UserView;
import app.business.services.message.MessageService;
import app.data.repositories.BillLayoutSettingsRepository;
import app.data.repositories.BinaryMessageRepository;
import app.data.repositories.GroupMembershipRepository;
import app.data.repositories.GroupRepository;
import app.data.repositories.MessageRepository;
import app.data.repositories.OrderItemRepository;
import app.data.repositories.OrderRepository;
import app.data.repositories.OrgEnquiryRepository;
import app.data.repositories.OrganizationMembershipRepository;
import app.data.repositories.OrganizationRepository;
import app.data.repositories.UserPhoneNumberRepository;
import app.data.repositories.UserRepository;
import app.data.repositories.VersionCheckRepository;
import app.data.repositories.WelcomeMessageRepository;
import app.entities.BillLayoutSettings;
import app.entities.FcmTokens;
import app.entities.Feedback;
import app.entities.GcmTokens;
import app.entities.Group;
import app.entities.GroupMembership;
import app.entities.LcpFeedback;
import app.entities.LcpProduct;
import app.entities.Order;
import app.entities.OrderItem;
import app.entities.OrdersStore;
import app.entities.OrgEnquiry;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.Product;
import app.entities.ProductType;
import app.entities.Referral;
import app.entities.Registration;
import app.entities.SmsApiKeys;
import app.entities.User;
import app.entities.UserPhoneNumber;
import app.entities.WelcomeMessage;
import app.entities.message.BinaryMessage;
import app.entities.message.Message;
import app.util.GcmRequest;
import app.util.SendContent;
import app.util.SendMail;
import app.util.Utils;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@RestController
@RequestMapping("/app")
public class WebController {

	@Autowired
	UserPhoneNumberRepository userPhoneNumberRepository;
	
	@Autowired
	OrgEnquiryRepository orgEnquiryRepository;

	@Autowired
	OrganizationRepository organizationRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	OrganizationMembershipRepository organizationMemberRepository;

	@Autowired
	GroupMembershipRepository groupMembershipRepository;

	@Autowired
	GroupRepository groupRepository;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	OrderService orderService;

	@Autowired
	GroupService groupService;
	
	@Autowired
	OrdersStoreService ordersStoreService;
	
	@Autowired
	GroupMembershipService groupMembershipService;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	MessageRepository messageRepo;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Autowired
	FeedbackService feedbackService;

	@Autowired
	UserPhoneNumberService userPhoneNumberService;

	@Autowired
	VersionCheckRepository versionCheckRepository;

	@Autowired
	GcmTokensService gcmTokensService;

	@Autowired
	UserService userService;

	@Autowired
	MessageService messageService;

	@Autowired
	SmsApiKeysService smsApiKeysService;

	@Autowired
	ReferralService referralService;

	@Autowired
	ProductTypeService productTypeService;

	@Autowired
	EnquiryService enquiryService;

	@Autowired
	ProductService productService;
	

	@Autowired
	UserService userservice;
	
	@Autowired
	LcpProductService lcpProductService;
	
	@Autowired
	WelcomeMessageRepository messageRepository;	
	
	@Autowired
	VoiceService voiService;
	
	@Autowired
	BillLayoutSettingsRepository billLayoutSettingRepo;
	
	@Autowired
	OrderItemRepository orderItemRepository;
	
	@Autowired
	BinaryMessageRepository binaryMessageRepository;
	
	public HashMap<String, Integer> dashBoardLocal(String orgabbr) throws ParseException {
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		Group g= organizationService.getParentGroup(organization);
		List<Order> paidOrders = orderRepository.findByOrganizationAndStatusAndText(organization, "delivered", "Paid");
		List<Order> unpaidOrders = orderRepository.findByOrganizationAndStatusAndText(organization, "delivered", "Unpaid");
		List<Order> deliveredOrders = orderService.getOrderByOrganizatonDeliveredSorted(organization);
		List<Message> messageapppro = messageService.getMessageListByOrderStatus(g, "binary", "processed");
		List<Message> messageappnew = messageService.getMessageListByOrderStatus(g, "binary", "saved");
		List<Message> messageappcan = messageService.getMessageListByOrderStatus(g, "binary", "cancelled");
		HashMap<String, Integer> dashmap = new HashMap<String, Integer>();
		dashmap.put("saved", messageappnew.size());
		dashmap.put("processed", messageapppro.size());
		dashmap.put("cancelled", messageappcan.size());
		dashmap.put("paid", paidOrders.size());
		dashmap.put("unpaid", unpaidOrders.size());
		dashmap.put("delivered", deliveredOrders.size());		
		List<OrganizationMembership> membershipListpending = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 0);
		List<OrganizationMembership> membershipListapproved = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 1);
		dashmap.put("totalUsers", membershipListapproved.size());
		dashmap.put("pendingUsers", membershipListpending.size());
		int todayUsers=0;
		for(OrganizationMembership membership : membershipListpending)
		{
			User user = membership.getUser();		
			try
			{
				Timestamp time = user.getTime();				
				Calendar cal= Calendar.getInstance();
				cal.clear(Calendar.HOUR_OF_DAY);
				cal.clear(Calendar.AM_PM);
				cal.clear(Calendar.MINUTE);
				cal.clear(Calendar.SECOND);
				cal.clear(Calendar.MILLISECOND);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			    java.util.Date dateWithoutTime = sdf.parse(sdf.format(new java.util.Date()));
				if(time.after(dateWithoutTime))
				{
					todayUsers=todayUsers+1;
				}
			}
			catch(NullPointerException | ParseException e)
			{
				System.out.println("User name not having his timestamp recorded is: " + user.getName() + " having userID: " + user.getUserId());
			}
		}
		dashmap.put("newUsersToday",todayUsers);
		return dashmap;
	}
	
	
	public List <String> getTargetConsumerDevices(String phonenumber) {
		List <String>androidTargets = new ArrayList<String>();
		try{
			List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(phonenumber);
			Iterator <GcmTokens> iter = gcmTokens.iterator();
			while(iter.hasNext()) {
				androidTargets.add(iter.next().getToken());
			}
			}
			catch(Exception e){
				System.out.println("no token for number: "+phonenumber);
			}
		return androidTargets;
	}	
	
	
	public List <String> getTargetDevices (Organization organization)  {
			
		List<OrganizationMembership> organizationMembership = organizationMembershipService.getOrganizationMembershipListByIsAdmin(organization, true);
		List<String> phoneNumbers = new ArrayList<String>();
		Iterator <OrganizationMembership> membershipIterator = organizationMembership.iterator();
		while (membershipIterator.hasNext()) {
			OrganizationMembership membership = membershipIterator.next();
			User user = membership.getUser();
			phoneNumbers.add(userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber());
		}
		Iterator <String> iterator = phoneNumbers.iterator();
		List <String>androidTargets = new ArrayList<String>();
		while(iterator.hasNext()) {
			String number = iterator.next();
			try{
			List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(number);
			Iterator <GcmTokens> iter = gcmTokens.iterator();
			while(iter.hasNext()) {
			
			androidTargets.add(iter.next().getToken());
			}
			}
			catch(Exception e){
				System.out.println("no token for number: "+number);
			}
		}
		return androidTargets;
	}
	@Transactional
	@RequestMapping(value = "/getinfo",method = RequestMethod.GET)
	

	public @ResponseBody String details(@RequestParam  (value="orgabbr") String orgabbr) throws ParseException{

		List<Organization> organs = organizationService.getAllOrganizationList();
		Iterator<Organization> iter = organs.iterator(); 
		 
		while(iter.hasNext()){
			Organization organization = iter.next();
			Group g =organizationService.getParentGroup(organization);
			List<ProductType> products = productTypeService.getAllByOrganisation(organization);
			int product1 = products.size();
			List<Message> messagesave = messageService.getMessageListByOrderStatus(g, "binary","saved");
			int messagesave1 = messagesave.size();
			int users = userService.getUserCount(organization);
            List<Message> messagepro = messageService.getMessageListByOrderStatus(g,"binary","processed");
            int messagepro1 = messagepro.size();     
            List<Message> messagecan=messageService.getMessageListByOrderStatus(g, "binary", "cancelled");
			int messagecan1 = messagecan.size();
			
		}
		return orgabbr;
		
		
		
	
			
	}
	
	
	
	
	
	@Transactional
	@RequestMapping(value = "/newUser",method = RequestMethod.POST)
	public String newUserEnquiry(@RequestBody String request) throws JSONException{
		JSONObject mRequest = new JSONObject(request);		
		JSONObject response = new JSONObject();
		String firstname = null;
		String lastname = null;
		String address = null;
		String email = null;
		String fullName = null;
		String phoneNumber = null;
		String password = null;
		String webPassword = null;
		User user = new User();
		Random random = new Random();
		UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
		try{
				firstname = mRequest.getString("firstname");
				lastname = mRequest.getString("lastname");
				address = mRequest.getString("address");
				email = mRequest.getString("email");
				phoneNumber = mRequest.getString("phoneNumber");
				webPassword = mRequest.getString("password");
				fullName = firstname+" "+lastname;
				if(userService.getUserFromEmail(email) != null){
					response.put("status", "error");
					response.put("reason", "Email already exists."); 
					return response.toString();
				}
				
				
				//// New addition					
				user.setAddress(address);
				user.setCallLocale("en");
				user.setEmail(email);
				password = Integer.toString(random.nextInt(10));
				for (int count = 0; count < 9; ++count)
					password = password + random.nextInt(10);
				user.setPassToken(password);
				user.setSha256Password(passwordEncoder.encode(password));
				user.setWebPassword(passwordEncoder.encode(webPassword));
				user.setName(fullName);
				user.setTextbroadcastlimit(0);
				user.setVoicebroadcastlimit(0);
				user = userRepository.save(user);
				phoneNumber = "91" + phoneNumber;
				userPhoneNumber.setPhoneNumber(phoneNumber);
				userPhoneNumber.setPrimary(true);
				userPhoneNumber.setUser(user);
				userPhoneNumber = userPhoneNumberRepository.save(userPhoneNumber);	
				response.put("status", "Success");		
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		return response.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/newUserRef",method = RequestMethod.POST)
	public String newUserRefEnquiry(@RequestBody String request) throws JSONException{
		JSONObject mRequest = new JSONObject(request);		
		JSONObject response = new JSONObject();
		String firstname = null;
		String lastname = null;
		String address = null;
		String email = null;
		String fullName = null;
		String phoneNumber = null;
		String password = null;
		String refCode = null;
		Organization organization = null;
		String webPassword = null;
		User user = new User();
		Random random = new Random();
		UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
		try{
				firstname = mRequest.getString("firstname");
				lastname = mRequest.getString("lastname");
				address = mRequest.getString("address");
				email = mRequest.getString("email");
				phoneNumber = mRequest.getString("phoneNumber");
				webPassword = mRequest.getString("password");
				refCode = mRequest.getString("refcode");
				fullName = firstname+" "+lastname;
				if(userService.getUserFromEmail(email) != null){
					response.put("status", "error");
					response.put("reason", "Email already exists."); 
					return response.toString();
				}
				
				
				//// New addition					
				user.setAddress(address);
				user.setCallLocale("en");
				user.setEmail(email);
				password = Integer.toString(random.nextInt(10));
				for (int count = 0; count < 9; ++count)
					password = password + random.nextInt(10);
				user.setPassToken(password);
				user.setSha256Password(passwordEncoder.encode(password));
				user.setWebPassword(passwordEncoder.encode(webPassword));
				user.setName(fullName);
				user.setTextbroadcastlimit(0);
				user.setVoicebroadcastlimit(0);
				user = userRepository.save(user);
				phoneNumber = "91" + phoneNumber;
				userPhoneNumber.setPhoneNumber(phoneNumber);
				userPhoneNumber.setPrimary(true);
				userPhoneNumber.setUser(user);
				userPhoneNumber = userPhoneNumberRepository.save(userPhoneNumber);	
				organization = organizationService.getOrganizationByRefCode(refCode);
				groupMembershipService.addParentGroupMembership(organization, user);
				OrganizationMembership orgMembership = new OrganizationMembership(organization, user, false, false, 1);
				organizationMembershipService.addOrganizationMembership(orgMembership);
				response.put("status", "Success");		
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		return response.toString();
	}
	
	
	@Transactional
	@RequestMapping(value = "/getProductMap",method = RequestMethod.GET )
	public String productListByTypeNew(@RequestParam(value="orgabbr") String orgabbr, @RequestParam(value="email") String email, HttpServletResponse  response){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getUserFromEmail(email);
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		
		if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
			try {
				responseJsonObject.put("status", "Failure");
				responseJsonObject.put("error", "You are no longer a member");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		
		for (ProductType ptype: productTypeList)
		{
			JSONArray jsonArray = new JSONArray();				
			for(Product product: productList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) && (product.getStatus() == 1)) {
					System.out.println(ptype.getName());
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
					jsonObject.put("imageUrl", product.getImageUrl());
					jsonObject.put("audioUrl", product.getAudioUrlWav());	
					jsonObject.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						jsonObject.put("description", "");
					else
						jsonObject.put("description", product.getDescription());
					jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				
			}
			}
			try {
				JSONObject temp = new JSONObject();
				temp.put(ptype.getName(), jsonArray);
				temp.put("productId", ptype.getProductTypeId());
				temp.put("status", ptype.getStatus());
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			//paytm detail added
			responseJsonObject.put("paytm", (organization.isPaytm() ? 1 : 0));
			responseJsonObject.put("products", details);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
		return responseJsonObject.toString();  
	}

	@Transactional	
	@RequestMapping(value = "/orders/add",method = RequestMethod.POST )
	public @ResponseBody String addOrders(@RequestBody String requestBody, HttpServletResponse  httpResponse){
	//	HashMap<String,String> response= new HashMap<String, String>();
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		String organizationabbr = null;
		String comment = null;
		Organization organization= null;
		User currentUser = null;
		float netAmount =0 ,total =0;
		int flag =0, countProd=0, ct = 0, flagForDisabled = 0;
		ArrayList <String> errorProduct = new ArrayList<String> ();
		String remProduct = "";
		try {
			JSONArray orderProducts = null;
			jsonObject = new JSONObject(requestBody);
			organizationabbr = jsonObject.getString("orgabbr");
			comment = jsonObject.getString("comment");
			currentUser = userService.getUserFromEmail(jsonObject.getString("email"));
			JSONArray updatePrice = new JSONArray();
			JSONArray disabledProducts = new JSONArray();
			String disabledProductsStr ="";
			String prodName ="";
			organization= organizationRepository.findByAbbreviation(organizationabbr);
			Boolean stockManagement = organization.getStockManagement();
			if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null) {
				response.put("status", "Failure");
				response.put("error", "You are no longer a member");
				return response.toString();
			}
			//Quantity verification
			orderProducts = jsonObject.getJSONArray("orderItems");
			for (int i = 0; i < orderProducts.length(); i++) {
				  
				JSONObject row = orderProducts.getJSONObject(i);
			    String productname=row.getString("name");
			    int productQuantity =row.getInt("quantity");
			    float price = -1;
			    try {
			    price= (float) row.getDouble("price");
			    total += price * productQuantity;
			    System.out.println(price);
			    }
			    catch(Exception e) {
			    	//uncaught, but not untamed!
			    }
			    Product product=productService.getProductByNameAndOrg(productname,organization);
			    if (product == null) {
			    	if (ct != 0)
			    		remProduct += ", ";
			    	remProduct += (productname);
			    	++ct;
			    	continue;
			    }
			    //algo for price changed in checkout
			    if (product.getUnitRate() != price && price != -1) {
			    	if(countProd != 0)
			    		prodName += ", ";
			    	flag =1;
			    	++countProd;
			    	prodName += product.getName();
			    	JSONObject object = new JSONObject();
			    	object.put("id", Integer.toString(product.getProductId()));
			    	object.put("name", product.getName());
			    	object.put("quantity", Integer.toString((product.getQuantity())));
			    	object.put("unitRate", Float.toString(product.getUnitRate()));
			    	object.put("imageUrl", product.getImageUrl());
			    	object.put("audioUrl", product.getAudioUrl());
			    	object.put("stockManagement", stockManagement.toString());
			    	object.put("description", product.getDescription());
			    	updatePrice.put(object);
			    }
			    
			    //algo for disabled products in checkout
			    if (product.getStatus()==0) {
			    	if(flagForDisabled>0)
			    	{
			    	disabledProductsStr=disabledProductsStr+" , "+product.getName();
			    	}
			    	else
			    	{
			    		disabledProductsStr=" "+product.getName();
			    	}
			    	flagForDisabled++;
			    }
			    
			    
				if(organization.getStockManagement() == true) {

			    int currentQuantity = product.getQuantity();
			    if (currentQuantity < productQuantity) {
			    	errorProduct.add(product.getName() + " - Max: "+ currentQuantity+ " units");
			    }
				}
			}
			if (!remProduct.isEmpty()) {
				response.put("error", "Products have been removed by the supplier: "+ remProduct);
				response.put("status", "Failure");
				return response.toString();
			}
			
			if (flag == 1){
				response.put("status", "price increased");
				response.put("products", updatePrice);
				return response.toString();
			}
			
			if (flagForDisabled!=0){
				response.put("status", "products has been disabled");
				response.put("error", "The following products have been deleted by the admin:"+disabledProductsStr);
				return response.toString();
			}
			
			if(!errorProduct.isEmpty()){
				throw new Exception();
			}
			if (total < organization.getOrderThreshold()) {
				response.put("error", "Minimum order for this organization is: Rs."+organization.getOrderThreshold());
				response.put("status", "Failure");
				return response.toString();
			}
			
			
		} 
			catch (JSONException e) {
			e.printStackTrace();
		}
		catch(Exception e1) {
			e1.printStackTrace();
			try {
				response.put("status", "Failure");
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			String error = new String();
			JSONArray errorArray = new JSONArray();
			Iterator <String> iterator = errorProduct.iterator();
			String output = "";
			int count =0;
			while(iterator.hasNext()) {
				if (count > 0)
					output += ", ";
				output = output + iterator.next();
				++count;
			}
			try {
				response.put("error", "Insufficient stock for: "+output);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		Order order = new Order();
		order.setOrganization(organization);
		order.setStatus("saved");
		order.setTrans_id("NA");
		order = orderRepository.save(order);
	    List<Integer> prods = new ArrayList<Integer>();
		List<OrderItem> orderItems= new ArrayList<OrderItem>();
		try {
			JSONArray orderItemsJSON = jsonObject.getJSONArray("orderItems");
			for (int i = 0; i < orderItemsJSON.length(); i++) {
			  
				JSONObject row = orderItemsJSON.getJSONObject(i);
			    String productname=row.getString("name");
			    int productQuantity =row.getInt("quantity");
			    Product product=productService.getProductByNameAndOrg(productname,organization);
			    int currentQuantity = product.getQuantity();
			    OrderItem orderItem= new OrderItem();
			    orderItem.setProduct(product);
			    orderItem.setQuantity(productQuantity);	
			    //
			    if (organization.getStockManagement() == true) {
			    	int newQty = currentQuantity - productQuantity;
			    	product.setQuantity(newQty);
			    	productService.addProduct(product);
			    	if(newQty < 10){
			    		prods.add(product.getProductId());
			    	}
			    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
			    	if(prod != null){
						prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
						lcpProductService.addProduct(prod);
			    	}
			    }
			    netAmount = netAmount + (productQuantity * product.getUnitRate());
			    orderItem.setUnitRate(product.getUnitRate());
			    orderItem.setOrder(order);
			    orderItem=orderItemRepository.save(orderItem);
			    orderItems.add(orderItem);
			}
			
			System.out.println(orderItemsJSON.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	
		order.setOrderItems(orderItems);
		BinaryMessage bmessage= new BinaryMessage();
		bmessage.setTime(new Timestamp((new Date()).getTime()));
		bmessage.setOrder(order);
		bmessage.setGroup(organizationService.getParentGroup(organization));
		bmessage.setUser(currentUser);
		bmessage.setMode("app");
		bmessage.setType("order");
		bmessage.setComments(comment);
		binaryMessageRepository.save(bmessage);
		order.setMessage(bmessage);
		orderRepository.save(order);
		try {
			response.put("orderId",new Integer(order.getOrderId()).toString());
			response.put("status", "Success");

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		String email=order.getMessage().getUser().getEmail();
		String phonenumber = order.getMessage().getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
		List <String> androidTargets = getTargetDevices(organization);
		if(androidTargets.size()>0) {
			GcmRequest gcmRequest = new GcmRequest();
			HashMap<String,Integer>dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(currentUser.getName()+" has placed an order", "New order", androidTargets,0);
			System.out.println(organizationabbr);
			for(int i = 0; i < prods.size(); i++){
				gcmRequest.broadcast(productService.getProductById(prods.get(i)).getName()+" is running low on stock", "Low stock", androidTargets, 1);
			}
			gcmRequest.broadcast(androidTargets,organizationabbr,dashData);
		}
		String orderString = "";
		Iterator <OrderItem> itemsIterator = orderItems.iterator();
		orderString = "<ol>";
		while (itemsIterator.hasNext()) {
			OrderItem orderItem = itemsIterator.next();
			orderString += "<li>";
			orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units<br />";
			orderString += "</li>";
		}
		orderString+= "</ol>";
		if(order.getMessage().getUser().getEmailVerified()==1)
		SendMail.sendMail(email, "Lokacart: Order Placed Successfully" , "<p>Dear User <br /><br />Your order has been placed successfully with order ID: " + new Integer(order.getOrderId()).toString()+".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+" .</p>");
		if(organization.getAbbreviation().equals("NatG"))
			SendMail.sendMail("vishalghodke@gmail.com", "Lokacart: Order Placed", "Dear Admin,\nCustomer "+order.getMessage().getUser().getName()+" has placed an order of order id "+order.getOrderId()+".\n\nThankyou\nLokacart Team\n");
		
		SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(organization);
		String authId = smsApiKeys.getAuthId();
		String authToken = smsApiKeys.getAuthToken();
		String sourceNumber = smsApiKeys.getSourceNumber();
		RestAPI api = new RestAPI(authId, authToken, "v1");
		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("src", sourceNumber);
		parameters.put("dst", phonenumber);
		parameters.put("text", "Hello from Lokacart! \nThank you for placing an order with "+organization.getName()+". The bill amount is Rs."+String.format("%.2f", netAmount)+".");
		parameters.put("method", "GET");
		try {
			MessageResponse msgResponse = api.sendMessage(parameters);
			System.out.println(msgResponse);
			System.out.println("Api ID : " + msgResponse.apiId);
			System.out.println("Message : " + msgResponse.message);
			if (msgResponse.serverCode == 202) {
				System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
				response.put("text", "Confirmation sent");

			} else {
				System.out.println(msgResponse.error);
			}
		} catch (PlivoException e) {
			System.out.println(e.getLocalizedMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}		
		httpResponse.setHeader("Access-Control-Allow-Origin", "*");
		return response.toString();
	}	

	@Transactional
	@RequestMapping(value = "/webfeedback",method = RequestMethod.POST )
	public @ResponseBody String submitFeedback(@RequestBody String requestBody, HttpServletResponse response) {
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		String orgabbr=null, content=null, phonenumber=null;
		try {
			object = new JSONObject(requestBody);
			content = object.getString("content");
			phonenumber = object.getString("phonenumber");
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "Error in uploading");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		User user = userRepository.findByuserPhoneNumbers_phoneNumber("91"+phonenumber);
		Feedback feedback = new Feedback(content, "Test2", user.getUserId());
		feedbackService.addFeedback(feedback);
		//ictB
		String email = "lokacart@cse.iitb.ac.in";
		//ictE
		
		/* //bestB
		String email = "lokacart@itaakash.com";
		*/ //bestE
		String subject = "User Feedback";
		String body = "<html><head></head><body><b>"+user.getName()+"</b> has submitted feedback.<br /><br /><br />"+
				"<b>Timestamp: </b>" + String.valueOf(new Timestamp((new Date()).getTime())) + "<br />" +"<b>Email ID: </b>"+user.getEmail()+ "<br />" + "<b>Phonenumber: </b>"+userPhoneNumberService.getUserPrimaryPhoneNumber(user)+
				"<br /><b>Feedback: </b><br /><br /><p>"+content+"</p><br /><br /><br /></body></html>";
		SendContent.sendMail(email, subject, body);
		try {
			responseJsonObject.put("response","success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
		return responseJsonObject.toString();
	}
	
	
	
	@Transactional
	@RequestMapping(value="/orgDetails", method= RequestMethod.POST)
	public @ResponseBody String getOrgDetails(@RequestBody String requestbody){
		JSONObject response = new JSONObject();
		JSONObject request = null;
		String email = null;
		User user = null;
		JSONArray orgArray = new JSONArray(); 
		List<OrganizationMembership> orgMembershipList = new ArrayList<OrganizationMembership>();
		try{
			request = new JSONObject(requestbody);
			email = request.getString("email");
			user = userService.getUserFromEmail(email);
			orgMembershipList = user.getOrganizationMemberships();
			Iterator<OrganizationMembership> iter = orgMembershipList.iterator();
			while(iter.hasNext()){
				Organization organization = iter.next().getOrganization();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("name", organization.getName());
				jsonObject.put("abbr", organization.getAbbreviation());
				jsonObject.put("desc", organization.getOrganizationDesc());
				orgArray.put(jsonObject);
			}
			response.put("OrgDetails", orgArray);
		} catch(JSONException e){
			e.printStackTrace();
		}
		return response.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/profileView",method = RequestMethod.POST)
	public String profileView(@RequestBody String requestBody) throws JSONException{
		JSONObject request = null;
		JSONObject response = new JSONObject();
		JSONObject Profile = new JSONObject();
		String email = null;
		User user = new User();
		JSONArray orderDetails = null;
		JSONObject orderObj = null;
		JSONArray orderArray = new JSONArray();
		String orgabbr = null;
		try{
			request = new JSONObject(requestBody);
			email = request.getString("email");
			orgabbr = request.getString("orgabbr");
			user = userService.getUserFromEmail(email);	
			if(user == null){
				response.put("error", "User doesn't exist.");
				return response.toString();
			}
			try {
				Profile.put("username", user.getName() + " " + user.getLastname());
			} catch (Exception e) {
				Profile.put("username", "NA");
			}	
			try {
				Profile.put("profilePicUrl", user.getProfilePic());
			} catch (Exception e) {
				Profile.put("profilePicUrl", "NA");
			}	
			try {
				Profile.put("phoneNumber", userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber());
			} catch (Exception e) {
				Profile.put("phoneNumber", "NA");
			}
			try {
				Profile.put("emailID", user.getEmail());
			} catch (Exception e) {
				Profile.put("emailID", "NA");
			}
			try {
				Profile.put("address", user.getAddress());
			} catch (Exception e) {
				Profile.put("address", "NA");
			}
			try {
				Profile.put("pincode", user.getPincode());		
			} catch (Exception e) {
				Profile.put("pincode", "NA");
			}
			List<Message> ordersList = messageRepo.findByUserAndTypeAndFormat(user, "order", "binary");
			Iterator<Message> iterator = ordersList.iterator();
			while(iterator.hasNext()) {
				Order order = iterator.next().getOrder();
				if(order.getOrganization().getAbbreviation().equals(orgabbr)){
					orderDetails = new JSONArray();
					orderObj = new JSONObject();
					orderObj.put("orderId", order.getOrderId());
					if(order.getDelivery() != null)
						orderObj.put("deliveryDate", order.getDelivery());
					else
						orderObj.put("deliveryDate", "NA");
					if(order.getMessage() != null)
						orderObj.put("placedDate", order.getMessage().getTime());
					else
						orderObj.put("placedDate", "NA");					
					orderObj.put("status", order.getStatus());
					orderObj.put("isPaid", order.getIsPaid());
					orderObj.put("orderId", order.getOrderId());
					if(order.getStatus().equals("saved") || order.getStatus().equals("processed")){
						List<OrderItem> orderItems = order.getOrderItems();
						Iterator<OrderItem> iter = orderItems.iterator();
						while(iter.hasNext()){
							OrderItem orderItem = iter.next();
							if(orderItem.getProduct() != null){
								JSONObject itemObj = new JSONObject();
								itemObj.put("name", orderItem.getProduct().getName());
								itemObj.put("quantity", orderItem.getQuantity());
								itemObj.put("rate", orderItem.getUnitRate());
								orderDetails.put(itemObj);
							}
							orderObj.put("orderItems", orderDetails);
						}
						orderArray.put(orderObj);
					}
					else if(order.getStatus().equals("cancelled") || order.getStatus().equals("deleted") || order.getStatus().equals("delivered")){
						List<OrdersStore> orderItems = ordersStoreService.getStoredItems(order);
						Iterator<OrdersStore> iter = orderItems.iterator();
						while(iter.hasNext()){
							OrdersStore orderItem = iter.next();
							if(orderItem.getProductName() != null){
								JSONObject itemObj = new JSONObject();
								itemObj.put("name", orderItem.getProductName());
								itemObj.put("quantity", orderItem.getQuantity());
								itemObj.put("rate", orderItem.getUnitRate());
								orderDetails.put(itemObj);
							}
							orderObj.put("orderItems", orderDetails);
						}
						orderArray.put(orderObj);
					}
				}
			}
			response.put("Profile", Profile);	
			response.put("Orders", orderArray);
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		return response.toString();
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/deleteMembership", method = RequestMethod.POST )
	public @ResponseBody String deleteMembership (@RequestBody String requestBody) {
		JSONObject responseJsonObject = new JSONObject();
		String organizationabbr = null;
		Organization organization = null;
		String email = null;
		try{
			JSONObject object = new JSONObject(requestBody);
			organizationabbr = object.getString("abbr");
			email = object.getString("email");
		}
		catch(JSONException e) {
			e.printStackTrace();
		}
		try {
		organization = organizationService.getOrganizationByAbbreviation(organizationabbr);
		User user = userService.getUserFromEmail(email);
		
		//remove group memberships
		List <Order> list = new ArrayList<Order>();
		List <Message> messages = messageService.getMessagesForUser(user, "order", "binary");
		Iterator <Message> iterator = messages.iterator();
		while (iterator.hasNext()) {
			Message message = iterator.next();
			Order order = message.getOrder();
			if ((order.getOrganization() == organization)){
				if(order.getStatus().equals("saved") || order.getStatus().equals("processed"))
				{
					list.add(order);
				}
				if(order.getStatus().equals("delivered"))
				{
					if(!order.getIsPaid())
					list.add(order);
				
				}
			}
		}
		if(!list.isEmpty()) {
			System.out.println(list.size());
			responseJsonObject.put("response", "Cannot be deleted");
			return responseJsonObject.toString();
		}
		for(GroupMembership groupMembership: user.getGroupMemberships()) {
			if(groupMembership.getGroup().getOrganization().getName().equals(organization.getName()))  
				groupMembershipService.removeGroupMembership(groupMembership); 	
		}
		
		//remove membership
		OrganizationMembership organizationMembership= organizationMembershipService.getUserOrganizationMembership(user, organization);
		organizationMembershipService.removeOrganizationMembership(organizationMembership);
		
		//proceed to deactivate referral
		List <Referral> referrals = referralService.getRemoveReferralsList(email, organization);
		Iterator <Referral> referralIter = referrals.iterator();
		while (referralIter.hasNext()) {
			referralService.removeReferral(referralIter.next());
			System.out.println("Referral removed");
		}
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "error");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		try {
			responseJsonObject.put("response", "Membership removed");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();	
	}
	

	@Transactional
	@RequestMapping(value = "/editProfile", method = RequestMethod.POST )
	public @ResponseBody String editProfile (@RequestBody String requestBody) {
		JSONObject request = null;
		JSONObject response = new JSONObject();
		String name = null, lastname = null, phonenumber = null, email = null, address = null, pincode = null;
		try{
			request = new JSONObject(requestBody);
			phonenumber = request.getString("phonenumber");
			name = request.getString("firstname");
			lastname = request.getString("lastname");
			email = request.getString("email");
			address = request.getString("address");
			pincode = request.getString("pincode");
			User user = userRepository.findByuserPhoneNumbers_phoneNumber("91"+phonenumber);
			if(user != null){
				user.setAddress(address);
				if(userService.getUserFromEmail(email) == null ){
					user.setEmail(email);
				}
				else if(user.getEmail().equals(email))
					System.out.println("Same ID");
				else{
					try {	
						response.put("status", "error");
						response.put("error", "Email already exists.");
						return response.toString();	
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				user.setName(name);
				user.setLastname(lastname);
				user.setPincode(pincode);
				response.put("status", "success");				
			}
			else{
				try {	
					response.put("status", "error");
					response.put("error", "User doesn't exist.");
					return response.toString();	
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch(Exception e){
			try {	
				response.put("status", "error");
				response.put("error", "Please retry.");
				return response.toString();	
			} catch (JSONException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
		return response.toString();
	}
	
	
	@SuppressWarnings("unused")
	@Transactional
	@RequestMapping(value = "/cancelOrder", method = RequestMethod.POST )
	public @ResponseBody String cancelOrder (@RequestBody String requestBody) {
		JSONObject request = null;
		JSONObject response = new JSONObject();
		Order order = null;
		String email = null, comments = null, orgabbr = null;
		int orderId = 0;
		try{
			request = new JSONObject(requestBody);
			orderId = request.getInt("orderId");
			email = request.getString("email");
			comments = request.getString("comments");
			orgabbr = request.getString("orgabbr");			
		} catch (Exception e) {
			e.printStackTrace();
		}
		order = orderService.getOrder(orderId);
		User user = userService.getUserFromEmail(email);
		if (user == order.getMessage().getUser()){
			if (order == null) {
				try {	
					response.put("status", "error");
					response.put("error", "Order was not found");
					return response.toString();	
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (order.getStatus().equals("processed")) {
				try {	
					response.put("status", "error");
					response.put("error", "Order has already been processed by the organisation");
					return response.toString();	
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (order.getStatus().equals("delivered")) {
				try {	
					response.put("status", "error");
					response.put("error", "Order has already been delivered to your address");
					return response.toString();	
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (order.getStatus().equals("deleted")) {
				try {	
					response.put("status", "error");
					response.put("error", "Order has already been deleted by Admin");
					return response.toString();	
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				response.put("status", "Success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			order.setStatus("cancelled");
			Message message = order.getMessage();
			message.setComments(comments);
			messageService.addMessage(message);
			String orderString = "";
			
			List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
			float netAmount=0;
			orderString ="<ol>";
			while (itemsIterator.hasNext()) {
				OrderItem orderItem = itemsIterator.next();
				OrdersStore ordersStore = new OrdersStore();
				ordersStore.setOrder(order);
				ordersStore.setProductName(orderItem.getProduct().getName());
				ordersStore.setQuantity(orderItem.getQuantity());
				ordersStore.setUnitRate(orderItem.getUnitRate());
				ordersStoreService.addOrdersStore(ordersStore);
				orderString += "<li>";
				orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units\n";
				orderString += "</li>";
				netAmount += (orderItem.getQuantity() * orderItem.getUnitRate());
			}
			orderString += "</ol>";
			List<OrderItem> orderItems = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> iterator = orderItems.iterator();
			while (iterator.hasNext()) {
				OrderItem orderItem = iterator.next();
				Product product = orderItem.getProduct();
		    	int newQty = product.getQuantity() + orderItem.getQuantity();
		    	product.setQuantity(newQty);
				productService.addProduct(product);
		    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
		    	if(prod != null){
					prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
					lcpProductService.addProduct(prod);
		    	}
				System.out.println("Updating product on cancellation - "+product.getName());
				orderItem.setProduct(null);
			}
			if(order.getMessage().getUser().getEmailVerified()==1)
			SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Cancellation Acknowledgement", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been successfully cancelled by "+ organizationService.getOrganizationByAbbreviation(orgabbr).getName() +".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+". <br /><br />We hope to serve you again!</p>");
			orderRepository.save(order);
		}
		else{
			try {
				response.put("status", "error");
				response.put("error", "Please retry...");
				return response.toString();	
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return response.toString();
	}

	@Transactional
	@RequestMapping(value = "/refExists", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String refExists(@RequestBody String requestBody) {
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String refCode = null;
		try {
			jsonObject = new JSONObject(requestBody);
			refCode = jsonObject.getString("refCode");
			Organization organization = organizationService.getOrganizationByRefCode(refCode);
			if(organization == null) {
				responseJsonObject.put("status", "failure");
			}
			else {
				responseJsonObject.put("status", "success");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value = "/referWeb", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String newReferral(@RequestBody String requestBody, HttpServletResponse  response) {
		
		int noEmail = 0;
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null, branchResponse = null, shortenerResponse = null;
		String API_KEY = "AIzaSyBU0JpWx49ZIJHw5lF-lBJeAnL5ZpXktL0";
		String phonenumber = null, email = null, refCode = null, refreeEmail = null, password = null, pincode = null, webPass = null,
				generatedUrl = null, address = null, name = null, lastname = null;
		String shortendUrl = null;
		Random random = new Random();
		List<UserPhoneNumber> userPhoneNumbers = new ArrayList<UserPhoneNumber>();
		Organization destOrganization = null;
		Organization organization = null;
		try {
			jsonObject = new JSONObject(requestBody);
			System.out.println("JSON Object refer: " + jsonObject);
			phonenumber = jsonObject.getString("phonenumber");
			System.out.println(phonenumber);			
			refCode = jsonObject.getString("refCode");
			try {
				organization = organizationService.getOrganizationByRefCode(refCode);
				System.out.println(organization.getName());
				List<OrganizationMembership> list = organizationMembershipService
						.getOrganizationMembershipListByIsAdmin(organization, true);
				refreeEmail = list.get(0).getUser().getEmail();
			} catch (Exception e) {
				// fetch
				responseJsonObject.put("response", "No such code");
				return responseJsonObject.toString();				
			}
			/*
			email = userRepository.findByuserPhoneNumbers_phoneNumber(phonenumber).getEmail();
			email = phonenumber+"@gmail.com";	*/
			
			
			try {
				email = jsonObject.getString("email");
				System.out.println(email);
				if(email.equals("")){
					noEmail=1;
					email=phonenumber+"@gmail.com";					
				}
			} catch (Exception e11) {
				
				// Do nothing
			}
			try {
				address = jsonObject.getString("address");
			} catch (Exception e1) {
				// Do nothing
			}
			try {
				pincode = jsonObject.getString("pincode");
			} catch (Exception e1) {
				// Do nothing
			}
			try {
				webPass = jsonObject.getString("password");
			} catch (Exception e1) {
				// Do nothing
			}
			try {
				name = jsonObject.getString("name");
			} catch (Exception e2) {
				// Do nothing
			}
			try {
				lastname = jsonObject.getString("lastname");
			} catch (Exception e3) {
				// Do nothing
			}
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				System.out.println("JSON exception caught");
				responseJsonObject.put("response", "Format error");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		System.out.println("JSON passed");

		System.out.println(userPhoneNumberService.findPreExistingPhoneNumber(phonenumber)+"   "+userService.getUserFromEmail(email));
		destOrganization = organizationService.getOrganizationByRefCode(refCode);
		if ((userPhoneNumberService.findPreExistingPhoneNumber(phonenumber) == true)) {
			// New user scenario
			if(userService.getUserFromEmail(email) != null){
				try {
					responseJsonObject.put("response", "failure");
					responseJsonObject.put("error", "User with same email already exists");
					return responseJsonObject.toString();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("inside");
			User user = new User();
			user.setEmail(email);
			userService.addUser(user);
			System.out.println("user with only email added");

			UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
			userPhoneNumber.setPhoneNumber(phonenumber);
			userPhoneNumber.setPrimary(true);
			userPhoneNumber.setUser(user);
			userPhoneNumberService.addUserPhoneNumber(userPhoneNumber);
			userPhoneNumbers.add(userPhoneNumber);
			user.setUserPhoneNumbers(userPhoneNumbers);
			java.util.Date date = new java.util.Date();
			Timestamp currentTimestamp = new Timestamp(date.getTime());
			user.setTime(currentTimestamp);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			user.setWebLocale("en");
			user.setCallLocale("en");
			if (address != null)
				user.setAddress(address);
			if (name != null)
				user.setName(name);
			if (lastname != null)
				user.setLastname(lastname);
			String fname;
			if(noEmail!=1)
			{
				int i = email.indexOf("@");
				fname = email.substring(0, i);
			 
			}
			else
			{
				fname=" ";
			}
			password = fname+random.nextInt(1000);
			password = Integer.toString(random.nextInt(10));
			for (int count = 0; count < 9; ++count)
				password = password + random.nextInt(10);
			user.setPassToken(password);
			user.setSha256Password(passwordEncoder.encode(password));
			user.setWebPassword(passwordEncoder.encode(webPass));
			user.setPincode(pincode);
			userService.addUser(user);
			System.out.println("user with only password added");

			/*User refUser = userService.getUserFromEmail(refreeEmail);
			System.out.println("User name: " + refUser.getName());
			Referral referral = new Referral();
			System.out.println("Should start if-else");
			if(userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail()!=null)
			{email = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail();
			System.out.println("I found this" + userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail());
			}
			System.out.println("see this" + email);
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser); // This is the referee
			String referralCode = String.valueOf(random.nextInt(999999));
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);*/
			/*String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String android_uri_scheme = "lokacart://";
			String android_package_name = "com.mobile.ict.cart";
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
			// String reqBody =
			// "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\"phonenumber\":\""+phonenumber+"\",\"email\":\""+email+"\",\"password\":\""+password+"\",\"referralCode\":\""+referralCode+"\"}\"";
			String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
					+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
					+ referralCode + "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\"" + user.getPassToken()
					+ "\\\"}\"\n    \n}";*/
			/*
			 * String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+
			 * "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+
			 * "\\\", \\\"email\\\":\\\""+email+
			 * "\\\", \\\"referralCode\\\":\\\""+referralCode+
			 * "\\\", \\\"password\\\":\\\""+ password+
			 * "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\""
			 * +user.getPassToken()+"\\\",\\\"organization\\\":\\\""+
			 * destOrganization.getName()+ "\\\", \\\"abbr\\\":\\\""
			 * +destOrganization.getAbbreviation()+"\\\"}\"\n    \n}";
			 */
			/*System.out.println("body: " + reqBody);

			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: " + generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			String reqBodyUrl = "{\n    \"longUrl\": \"http://best-erp.com/ruralict/app/code/" + referralCode
					+ "\"\n\n}";*/
			groupMembershipService.addParentGroupMembership(organization, user);
			OrganizationMembership orgMembership = new OrganizationMembership(organization, user, false, false, 1);
			organizationMembershipService.addOrganizationMembership(orgMembership);
			//String refUrl = "http://ruralict.cse.iitb.ac.in/ruralict/app/code/" + referralCode;
			/*OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: " + shortendUrl);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}*/
			/*SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");
			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + destOrganization.getName()
					+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			} */
			try {
				//responseJsonObject.put("url", refUrl);
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			int flag = 0;
			User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
			// Existing user
			int details = 0;
			if (user.getEmail() == null || user.getLastname() == null || user.getName() == null
					|| user.getAddress() == null || user.getName() == null || user.getPincode() == null) {
				details = 1;
			}
			List<OrganizationMembership> list = user.getOrganizationMemberships();
			Iterator<OrganizationMembership> iterator = list.iterator();
			while (iterator.hasNext()) {
				OrganizationMembership organizationMembership = iterator.next();
				if (organizationMembership.getIsAdmin() == true)
					flag = 1;
			}
			if (flag == 1) {
				try {
					responseJsonObject.put("response", "failure");
					responseJsonObject.put("error", "Admin accounts cannot be used on the consumer app");
					return responseJsonObject.toString();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (user.getPassToken() == null) {
				// Resetting password
				password = Integer.toString(random.nextInt(10));
				for (int count = 0; count < 9; ++count)
					password = password + random.nextInt(10);
				user.setPassToken(password);
				user.setSha256Password(passwordEncoder.encode(password));
				userService.addUser(user);
				System.out.println("pass token: " + password);
			}
			List<OrganizationMembership> memberList = user.getOrganizationMemberships();
			if (memberList.isEmpty() == false) {
				Iterator<OrganizationMembership> memberIterator = memberList.iterator();
				while (memberIterator.hasNext()) {
					OrganizationMembership organizationMembership = memberIterator.next();
					if (destOrganization == organizationMembership.getOrganization()) {
						try {
							responseJsonObject.put("response", "failure");
							responseJsonObject.put("error", "Already a member");
						} catch (JSONException e) {
							e.printStackTrace();
						}
						return responseJsonObject.toString();
					}
				}
			}
			
			
			///// New implementation
			
			groupMembershipService.addParentGroupMembership(organization, user);
			OrganizationMembership orgMembership = new OrganizationMembership(organization, user, false, false, 1);
			organizationMembershipService.addOrganizationMembership(orgMembership);
			
			///// End of new implementation
			
			
			/*User refUser = userService.getUserFromEmail(refreeEmail);
			Referral referral = new Referral();
			System.out.println("Should start if-else");
			if(userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail()!=null)
			{email = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail();
			System.out.println("I found this" + userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail());
			}
			System.out.println("see this" + email);
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser);
			String referralCode = String.valueOf(random.nextInt(999999));
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
			String android_uri_scheme = "lokacart://";
			String android_package_name = "com.mobile.ict.cart";
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);*/
			/*String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
					+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
					+ referralCode + "\\\", \\\"details\\\":\\\"" + details + "\\\", \\\"token\\\":\\\""
					+ user.getPassToken() + "\\\"}\"\n    \n}";

			System.out.println("body: " + reqBody); */
			
			/*OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();*/
			/*
			 * MediaType mediaType = MediaType.parse("application/json");
			 * okhttp3.RequestBody body =
			 * okhttp3.RequestBody.create(mediaType,
			 * "{\n\"branch_key\":\"key_live_feebgAAhbH9Tv85H5wLQhpdaefiZv5Dv\",\n\"data\":\"{\\\"name\\\": \\\"Alex\\\"}\"\n    \n}"
			 * ); Request request = new Request.Builder()
			 * .url("https://api.branch.io/v1/url") .post(body)
			 * .addHeader("content-type", "application/json")
			 * .addHeader("cache-control", "no-cache")
			 * .addHeader("postman-token",
			 * "7a4da102-dc81-31b9-3028-aedee4a30025") .build();
			 */
			/*try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: " + generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}*/

			/*String reqBodyUrl = "{\n    \"longUrl\": \"http://best-erp.com/ruralict/app/code/"
					+ referralCode + "\"\n\n}";*/
			//String referUrl = "http://ruralict.cse.iitb.ac.in/ruralict/app/code/"+ referralCode;
			/*OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: " + shortendUrl);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}*/

			/*SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + destOrganization.getName()
					+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			System.out.println("http://best-erp.com/ruralict/app/code/" + referralCode);
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}*/
			try {/*
				responseJsonObject.put("url", referUrl);*/
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/feedback",method = RequestMethod.POST )
	public @ResponseBody String submitFeedback(@RequestBody String requestBody) {
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		String orgabbr=null, content=null, phonenumber=null;
		try {
			object = new JSONObject(requestBody);
			orgabbr = object.getString("abbr");
			content = object.getString("content");
			phonenumber = object.getString("phonenumber");
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "Error in uploading");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
		Feedback feedback = new Feedback(content, orgabbr, user.getUserId());
		feedbackService.addFeedback(feedback);
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		List<OrganizationMembership> list =  organizationMembershipService.getOrganizationMembershipListByIsAdmin(organization, true);
		Iterator <OrganizationMembership> iterator = list.iterator();
		while(iterator.hasNext()) {
		String email = iterator.next().getUser().getEmail();
		String subject = "User Feedback";
		String body = "<html><head></head><body><b>"+user.getName()+"</b> has submitted feedback.<br /><br /><br />"+
				"<b>Timestamp: </b>" + String.valueOf(new Timestamp((new Date()).getTime())) + "<br />" +"<b>Email ID: </b>"+user.getEmail()+ "<br />" + "<b>Phonenumber: </b>"+user.getUserPhoneNumbers().get(0).getPhoneNumber()+
				"<br /><b>Feedback: </b><br /><br /><p>"+content+"</p><br /><br /><br /></body></html>";
		SendContent.sendMail(email, subject, body);
		}
		try {
			responseJsonObject.put("response","success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();

	}
	
}
