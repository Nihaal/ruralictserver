package app.business.controllers.rest;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;
import app.util.FcmRequest;
import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.services.BillLayoutSettingsService;
import app.business.services.FcmTokensService;
import app.business.services.GcmTokensService;
import app.business.services.LcpOrderItemService;
import app.business.services.LcpOrderService;
import app.business.services.LcpProductService;
import app.business.services.OrderItemService;
import app.business.services.OrderService;
import app.business.services.OrdersStoreService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.PresetQuantityService;
import app.business.services.ProductService;
import app.business.services.RegistrationService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.business.services.message.BinaryMessageService;
import app.business.services.message.MessageService;
import app.data.repositories.BillLayoutSettingsRepository;
import app.data.repositories.BinaryMessageRepository;
import app.data.repositories.FcmTokensRepository;
import app.data.repositories.GroupRepository;
import app.data.repositories.LcpOrderItemRepository;
import app.data.repositories.LcpOrderRepository;
import app.data.repositories.MessageRepository;
import app.data.repositories.OrderItemRepository;
import app.data.repositories.OrderRepository;
import app.data.repositories.OrdersStoreRepository;
import app.data.repositories.OrganizationRepository;
import app.data.repositories.PaytmDataRepository;
import app.data.repositories.RegistrationRepository;
import app.entities.BillLayoutSettings;
import app.entities.FcmTokens;
import app.entities.GcmTokens;
import app.entities.Group;
import app.entities.LcpOrder;
import app.entities.LcpOrderItem;
import app.entities.LcpProduct;
import app.entities.Order;
import app.entities.OrderItem;
import app.entities.OrdersStore;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.PaytmData;
import app.entities.PresetQuantity;
import app.entities.Product;
import app.entities.SmsApiKeys;
import app.entities.User;
import app.entities.message.BinaryMessage;
import app.entities.message.Message;
import app.util.GcmRequest;
import app.util.OrderItemManage;
import app.util.OrderManage;
import app.util.SendBill;
import app.util.SendMail;
import app.util.SpreadsheetParser;
import app.util.UserManage;
import app.util.Utils;

@RestController
@RequestMapping("/api")
public class OrderRestController {

	@Autowired
	RegistrationService registrationService;
	
	@Autowired
	FcmTokensRepository fcmTokensRepository;
	
	@Autowired
	RegistrationRepository registrationRepository;

	@Autowired
	BinaryMessageService binaryMessageService;

	@Autowired
	LcpOrderService lcpOrderService;
	
	@Autowired
	LcpProductService lcpProductService;

	@Autowired
	LcpOrderItemRepository lcpOrderItemRepository;
	
	@Autowired
	LcpOrderItemService lcpOrderItemService;

	@Autowired
	PaytmDataRepository paytmDatarepository;
	
	@Autowired
	OrganizationService organizationService;
	
	@Autowired
	MessageService messageService;
	
	@Autowired
	PresetQuantityService presetQuantityService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	OrganizationRepository organizationRepository;
	
	@Autowired
	OrderItemRepository orderItemRepository;
	
	@Autowired
	BinaryMessageRepository binaryMessageRepository;
	
	@Autowired
	GroupRepository groupRepository;

	@Autowired
	UserService userService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	BillLayoutSettingsRepository billLayoutSettingsRepository;
	
	@Autowired 
	UserPhoneNumberService userPhoneNumberService;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Autowired
	GcmTokensService gcmTokensService;
	
	@Autowired
	BillLayoutSettingsService billLayoutSettingsService;
	
	@Autowired
	OrderItemService orderItemService;
	
	@Autowired
	SmsApiKeysService smsApiKeysService;

	@Autowired
	OrdersStoreService ordersStoreService; 

	@Autowired
	OrdersStoreRepository ordersStoreRepository; 
	
	
	public HashMap<String, Integer> dashBoardLocal(String orgabbr) throws ParseException {
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		Group g= organizationService.getParentGroup(organization);
		List<Order> paidOrders = orderRepository.findByOrganizationAndStatusAndText(organization, "delivered", "Paid");
		List<Order> unpaidOrders = orderRepository.findByOrganizationAndStatusAndText(organization, "delivered", "Unpaid");
		List<Order> deliveredOrders = orderService.getOrderByOrganizatonDeliveredSorted(organization);
		List<Message> messageapppro = messageService.getMessageListByOrderStatus(g, "binary", "processed");
		List<Message> messageappnew = messageService.getMessageListByOrderStatus(g, "binary", "saved");
		List<Message> messageappcan = messageService.getMessageListByOrderStatus(g, "binary", "cancelled");
		HashMap<String, Integer> dashmap = new HashMap<String, Integer>();
		dashmap.put("saved", messageappnew.size());
		dashmap.put("processed", messageapppro.size());
		dashmap.put("cancelled", messageappcan.size());
		dashmap.put("paid", paidOrders.size());
		dashmap.put("unpaid", unpaidOrders.size());
		dashmap.put("delivered", deliveredOrders.size());		
		List<OrganizationMembership> membershipListpending = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 0);
		List<OrganizationMembership> membershipListapproved = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 1);
		dashmap.put("totalUsers", membershipListapproved.size());
		dashmap.put("pendingUsers", membershipListpending.size());
		int todayUsers=0;
		for(OrganizationMembership membership : membershipListpending)
		{
			User user = membership.getUser();		
			try
			{
				Timestamp time = user.getTime();				
				Calendar cal= Calendar.getInstance();
				cal.clear(Calendar.HOUR_OF_DAY);
				cal.clear(Calendar.AM_PM);
				cal.clear(Calendar.MINUTE);
				cal.clear(Calendar.SECOND);
				cal.clear(Calendar.MILLISECOND);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			    java.util.Date dateWithoutTime = sdf.parse(sdf.format(new java.util.Date()));
				if(time.after(dateWithoutTime))
				{
					todayUsers=todayUsers+1;
				}
			}
			catch(NullPointerException | ParseException e)
			{
				System.out.println("User name not having his timestamp recorded is: " + user.getName() + " having userID: " + user.getUserId());
			}
		}
		dashmap.put("newUsersToday",todayUsers);
		return dashmap;
	}
		
	
	
	public List <String> getTargetConsumerDevices(String phonenumber) {
		List <String>androidTargets = new ArrayList<String>();
		try{
			List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(phonenumber);
			Iterator <GcmTokens> iter = gcmTokens.iterator();
			while(iter.hasNext()) {
				androidTargets.add(iter.next().getToken());
			}
			}
			catch(Exception e){
				System.out.println("no token for number: "+phonenumber);
			}
		return androidTargets;
	}	
	
	public List <String> getTargetConsumerDevicesLcp(String phonenumber) {
		List <String>androidTargets = new ArrayList<String>();
		try{
			List<FcmTokens> fcmTokens = fcmTokensRepository.findAllByNumberOrderByIdDesc(phonenumber);
			Iterator <FcmTokens> iter = fcmTokens.iterator();
			while(iter.hasNext()) {
				androidTargets.add(iter.next().getToken());
			}
			}
			catch(Exception e){
				System.out.println("no token for number: "+phonenumber);
			}
		return androidTargets;
	}
	
	public List <String> getTargetDevices (Organization organization)  {
			
		List<OrganizationMembership> organizationMembership = organizationMembershipService.getOrganizationMembershipListByIsAdmin(organization, true);
		List<String> phoneNumbers = new ArrayList<String>();
		Iterator <OrganizationMembership> membershipIterator = organizationMembership.iterator();
		while (membershipIterator.hasNext()) {
			OrganizationMembership membership = membershipIterator.next();
			User user = membership.getUser();
			phoneNumbers.add(userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber());
		}
		Iterator <String> iterator = phoneNumbers.iterator();
		List <String>androidTargets = new ArrayList<String>();
		while(iterator.hasNext()) {
			String number = iterator.next();
			try{
			List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(number);
			Iterator <GcmTokens> iter = gcmTokens.iterator();
			while(iter.hasNext()) {
			
			androidTargets.add(iter.next().getToken());
			}
			}
			catch(Exception e){
				System.out.println("no token for number: "+number);
			}
		}
		return androidTargets;
	}
	
	@RequestMapping(value = "/generatePaytmRefunds", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public FileSystemResource generatePaytmRefunds(){
		File paytmFile = null;		
		List<Order> orderList = orderRepository.findByStatus("cancelled");
		List<String> transactionList = new ArrayList<String>();
		List<Float> refundList = new ArrayList<Float>();
		Iterator<Order> iter = orderList.iterator();
		while(iter.hasNext()){
			Order order = iter.next();
			if(order.isRefunded() == false && order.getIsPaid() == true && !order.getTrans_id().equals("NA")){
				float total = 0;
				transactionList.add(order.getTrans_id());	
				List<OrderItem> oItems = order.getOrderItems();
				Iterator<OrderItem> iterator = oItems.iterator();
				while(iterator.hasNext()){
					total += iterator.next().getUnitRate(); 
				}
				refundList.add(total);
			}
		}
		paytmFile = SpreadsheetParser.generatePaytmRefund(transactionList, refundList);
		return new FileSystemResource(paytmFile);
	}
	
	
	
	@RequestMapping(value = "/generateBillStats/{fromDate}/{toDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public FileSystemResource generateBillStats(@PathVariable String fromDate, @PathVariable String toDate){
		List<Order> orderList = orderRepository.findAll();
		Date dateFrom = new Date(Integer.parseInt(fromDate.substring(0,4)),Integer.parseInt(fromDate.substring(5,7)),Integer.parseInt(fromDate.substring(8,10)));
		Date dateTo = new Date(Integer.parseInt(toDate.substring(0,4)),Integer.parseInt(toDate.substring(5,7)),Integer.parseInt(toDate.substring(8,10)));
		Iterator<Order> iter = orderList.iterator();
		List<String> nameList = new ArrayList<String>();
		List<String> timeList = new ArrayList<String>();
		List<Integer> orderIdList = new ArrayList<Integer>();
		List<String> statusList = new ArrayList<String>();
		List<String> orgList = new ArrayList<String>();		
		while(iter.hasNext()){
			try{
				Order order = iter.next();
				String date = order.getMessage().getTime().toString();
				System.out.println(order.getMessage().getTime().toString());
				Date dateBill = new Date(Integer.parseInt(date.substring(0,4)),Integer.parseInt(date.substring(5,7)),Integer.parseInt(date.substring(8,10)));
				if(dateBill.compareTo(dateTo)<=0 && dateBill.compareTo(dateFrom)>=0) {
					nameList.add(order.getMessage().getUser().getName()+" "+order.getMessage().getUser().getLastname());
					timeList.add(order.getDelivery());
					orderIdList.add(order.getOrderId());
					statusList.add(order.getStatus());
					orgList.add(order.getOrganization().getName());
				}
			}
			catch(Exception e){}
		}
		File file = SpreadsheetParser.generateSummaryforRiha(nameList, timeList, orderIdList, statusList, orgList);
		return new FileSystemResource(file);
	}
	
	/*@RequestMapping(value = "/generatePaytmRefundData/{fromDate}/{toDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public FileSystemResource generateBillStats(@PathVariable String fromDate, @PathVariable String toDate){
		List<Order> orderList = orderRepository.findAll();
		Date dateFrom = new Date(Integer.parseInt(fromDate.substring(0,4)),Integer.parseInt(fromDate.substring(5,7)),Integer.parseInt(fromDate.substring(8,10)));
		Date dateTo = new Date(Integer.parseInt(toDate.substring(0,4)),Integer.parseInt(toDate.substring(5,7)),Integer.parseInt(toDate.substring(8,10)));
		Iterator<Order> iter = orderList.iterator();
		List<String> nameList = new ArrayList<String>();
		List<String> timeList = new ArrayList<String>();
		List<Integer> orderIdList = new ArrayList<Integer>();
		List<String> statusList = new ArrayList<String>();
		List<String> orgList = new ArrayList<String>();		
		List<String> phoneNumberList = new ArrayList<String>();
		List<String> transactionIdList = new ArrayList<String>();

		while(iter.hasNext()){
			try{
				Order order = iter.next();
				Boolean isPaid = order.getIsPaid();
				String status = order.getSatus();
				String orderId = order.getOrderId();
				String transactionId = order.getTrans_id();
				String is_Refunded = order.get;
				System.out.println(order.getMessage().getTime().toString());
				Date dateBill = new Date(Integer.parseInt(date.substring(0,4)),Integer.parseInt(date.substring(5,7)),Integer.parseInt(date.substring(8,10)));
				if(isPaid==True && status=="cancelled" && is_Refunded==False) {
					transactionIdList.add(order.getTrans_id());
					phoneNumberList.add(order.getOrganization().getName());
				}
			}
			catch(Exception e){}
		}
		File file = SpreadsheetParser.generatePaytmRefund(phoneNumberList,transactionIdList);
		return new FileSystemResource(file);
	}*/
	
	
	@Transactional
	@RequestMapping(value="/{org}/getOrderList", method=RequestMethod.GET, produces = "application/json")

	public List<OrderManage> getOrderList(@PathVariable String org) {

		List<OrderManage> orderrows = new ArrayList<OrderManage>();

		//Organization organization = organizationService.getOrganizationByAbbreviation(org);

		Group group = organizationService.getParentGroup(organizationService.getOrganizationByAbbreviation(org));
		List<Message> appDeliveredMessageList=binaryMessageService.getDeliveredBinaryMessageList(group);	

		//List<Order> orderList = orderService.getOrderByOrganization(organization);

		for(Message message : appDeliveredMessageList)
		{			
			try
			{
				// Get required attributes for each user
				int stat = 0;
				int orderId = message.getOrder().getOrderId();
				String name = message.getUser().getName();
				String text = message.getOrder().getText();
				Boolean isPaid = message.getOrder().getIsPaid();
				String status = message.getOrder().getStatus();
				String time = message.getOrder().getMessage().getTime().toString();
				// Create the UserManage Object and add it to the list
				OrderManage orderrow = new OrderManage(stat, orderId, name, text, isPaid, status, time);
				orderrows.add(orderrow);
			}
			catch(NullPointerException e)
			{
				//System.out.println("Order ID: " + order.getOrderId() + " has no name.");
			}
		}
		return orderrows;
	}
	
	
	@Transactional
	@RequestMapping(value="/{orderId}/getItemsList", method=RequestMethod.GET, produces = "application/json")

	public List<OrderItemManage> getItemsList(@PathVariable int orderId) {

		List<OrderItemManage> orderitemrows = new ArrayList<OrderItemManage>();

		List<OrderItem> orderItemList = orderItemRepository.findByOrder_OrderId(orderId);

		for(OrderItem orderItem : orderItemList)
		{
			try
			{
				int stat = 0;
				int id = orderItem.getOrderItemId();
				int prodId = orderItem.getProduct().getProductId();
				String name = orderItem.getProduct().getName();
				int qty = orderItem.getQuantity();
				float rate = orderItem.getUnitRate();

				OrderItemManage orderitemrow = new OrderItemManage(stat, id, prodId, name, qty, rate);
				orderitemrows.add(orderitemrow);
			}
			catch(NullPointerException e)
			{
				//System.out.println("Order ID: " + order.getOrderId() + " has no name.");
			}
		}
		return orderitemrows;
	}
	

	@Transactional
	@RequestMapping(value="/{org}/getSavedList", method=RequestMethod.GET, produces = "application/json")

	public List<OrderManage> getSavedList(@PathVariable String org) {

		List<OrderManage> orderrows = new ArrayList<OrderManage>();

		//Organization organization = organizationService.getOrganizationByAbbreviation(org);

		//List<Order> orderList = orderService.getOrderByOrganizationSavedSorted(organization);
		
		Group group = organizationService.getParentGroup(organizationService.getOrganizationByAbbreviation(org));
		List<Message> appSavedMessageList= binaryMessageService.getSavedBinaryMessageList(group);
		//List<Product> productList= productService.getProductList(organizationService.getOrganizationByAbbreviation(org));
		//List<PresetQuantity> presetQuantityList= presetQuantityService.getPresetQuantityList(organizationService.getOrganizationByAbbreviation(org));
		
		
		

		for(Message message : appSavedMessageList)
		{
			try
			{
				System.out.println(message.getMessageId());
				int stat = 0;
				int orderId = message.getOrder().getOrderId();
				String name = message.getUser().getName();
				String time = message.getTime().toString();
				OrderManage orderrow = new OrderManage(stat, orderId, name, time);
				orderrows.add(orderrow);
			}
			catch(NullPointerException e)
			{
				//System.out.println("Order ID: " + order.getOrderId() + " has no name.");
			}			
		}
		return orderrows;
	}
	
	
	@Transactional
	@RequestMapping(value="/{org}/getProcessedList", method=RequestMethod.GET, produces = "application/json")

	public List<OrderManage> getProcessedList(@PathVariable String org) {

		List<OrderManage> orderrows = new ArrayList<OrderManage>();

		Organization organization = organizationService.getOrganizationByAbbreviation(org);

		List<Order> orderList = orderService.getOrderByOrganizationProcessedSorted(organization);

		for(Order order : orderList)
		{		
			try
			{
				// Get required attributes for each user
				int stat = 0;
				int orderId = order.getOrderId();
				String name = order.getMessage().getUser().getName();
			
				// Create the UserManage Object and add it to the list
				OrderManage orderrow = new OrderManage(stat, orderId, name);
				orderrows.add(orderrow);
			}
			catch(NullPointerException e)
			{
				//System.out.println("Order ID: " + order.getOrderId() + " has no name.");
			}
		}
		return orderrows;
	}
	

	@Transactional
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@RequestMapping(value = "/{org}/addOrderItem",method = RequestMethod.POST )
	public @ResponseBody String addOrderItems(@PathVariable String org, @RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		int orderId = 0;
		int prodId = 0;
		int cQty = 0;
		float rate = 0;
		Organization organization= organizationService.getOrganizationByAbbreviation(org);
		Boolean stockMgmt = organization.getStockManagement();
		OrderItem orderItem = null;
		try{
			jsonObject = new JSONObject(requestBody);
			orderId = jsonObject.getInt("orderId");
			prodId = jsonObject.getInt("prodId");
			cQty = jsonObject.getInt("qty");
			rate = (float) jsonObject.getDouble("rate");
			orderItem = new OrderItem(orderService.getOrder(orderId), productService.getProductById(prodId), cQty, rate);
			if(stockMgmt){
				Product product = productService.getProductById(prodId);
				int pQty = product.getQuantity();
				if(cQty <= pQty){
			    	int newQty = pQty-cQty;
			    	product.setQuantity(newQty);
					productService.addProduct(product);
			    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
			    	if(prod != null){
						prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
						lcpProductService.addProduct(prod);
			    	}
				}
				else{
					try {
						response.put("status", "Failure");
					} catch (JSONException e2) {
						e2.printStackTrace();
					}
				}
			}
			orderItemService.addOrderItem(orderItem);
			orderItemRepository.count();
			try {
				response.put("status", "Success");

			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		catch(Exception e){
			try {
				response.put("status", "Failure");
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
		}
		return response.toString();
	}
	
	

	/*@Transactional
	@RequestMapping(value = "/checkOrderItem",method = RequestMethod.GET )
	public @ResponseBody String checkOrderItems(@RequestBody String requestBody){
		
	}*/
	
	@Transactional
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@RequestMapping(value = "/{org}/notifyChange",method = RequestMethod.POST )
	public @ResponseBody String notifyChange(@PathVariable String org, @RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		String orderString = "";
		try{
			jsonObject = new JSONObject(requestBody);
			int orderId = jsonObject.getInt("orderId");
			Order order = orderService.getOrder(orderId);
			Organization organization = order.getOrganization();
			List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
			float netAmount=0;
			orderString ="<ol>";
			while (itemsIterator.hasNext()) {
				OrderItem orderItem = itemsIterator.next();
				OrdersStore ordersStore = new OrdersStore();
				ordersStore.setOrder(order);
				ordersStore.setProductName(orderItem.getProduct().getName());
				ordersStore.setQuantity(orderItem.getQuantity());
				ordersStore.setUnitRate(orderItem.getUnitRate());
				ordersStoreService.addOrdersStore(ordersStore);
				orderString += "<li>";
				orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units\n";
				orderString += "</li>";
				netAmount += (orderItem.getQuantity() * orderItem.getUnitRate());
			}
			orderString += "</ol>";
	
			if(order.getMessage().getUser().getEmailVerified()==1)
			SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Modification Acknowledgement", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been successfully modified on "+order.getMessage().getTime()+ " by "+ organization.getName() +".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+" .</p>");
			System.out.println("---------------You reached here----------------");
			//GcmRequest gcmRequest= new GcmRequest();
			List <String> androidConsumerTargets = getTargetConsumerDevices(userPhoneNumberService.getUserPrimaryPhoneNumber(order.getMessage().getUser()).getPhoneNumber());
			if (androidConsumerTargets.size() > 0) {
				System.out.println("Broadcasts sent");
				GcmRequest gcmRequest = new GcmRequest();
				gcmRequest.consumerBroadcastWithMessage("Changed Order", "Order ID: "+ orderId +" has been modified" ,androidConsumerTargets, "7");
					}
			
			order.getMessage().setTime(new Timestamp((new Date()).getTime()));
			binaryMessageRepository.save((BinaryMessage)order.getMessage());
			if(organization.getAbbreviation().equals("NatG"))
				SendMail.sendMail("vishalghodke@gmail.com", "Lokacart: Order Modification", "Dear Admin ,<br /><br />Customer "+order.getMessage().getUser().getName()+" has modified the order of order ID "+order.getOrderId()+".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+" .</p>");
			List<OrdersStore> osList = ordersStoreService.getStoredItems(order);
			for(OrdersStore oStore : osList){
				ordersStoreRepository.delete(oStore);
			}
			try {
				response.put("status", "Success");
	
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		catch(Exception e){
			try {
				response.put("status", "Failure");
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
		}
		return response.toString();
	}
	
	
	
	@Transactional
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@RequestMapping(value = "/{org}/changeOrderItem",method = RequestMethod.POST )
	public @ResponseBody String changeOrderItems(@PathVariable String org, @RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		int itemId = 0;
		int newQty = 0;
		int currentQty = 0;
		int productId = 0;
		Organization organization= organizationService.getOrganizationByAbbreviation(org);
		Boolean stockMgmt = organization.getStockManagement();
		try{
			jsonObject = new JSONObject(requestBody);
			itemId = jsonObject.getInt("itemId");
			newQty = jsonObject.getInt("qty");
			productId = jsonObject.getInt("prodId");
			OrderItem oItem = orderItemRepository.findOne(itemId);
			float price = productService.getProductById(productId).getUnitRate();
			currentQty = oItem.getQuantity();
			try {
				response.put("val", -1);
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			if(stockMgmt){
				Product product = oItem.getProduct();
				int prodQty = product.getQuantity();
				if(newQty < currentQty){
					int newqty = prodQty + (currentQty - newQty);
					product.setQuantity(newqty);
					productService.addProduct(product);
			    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
			    	if(prod != null){
						prod.setQuantity((int)(newqty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
						lcpProductService.addProduct(prod);
			    	}
					oItem.setQuantity(newQty);
					oItem.setUnitRate(price);
					//orderItemService.addOrderItem(oItem);
				}
				else{
					if(newQty <= prodQty){
						int newqty = prodQty - (newQty-currentQty);
						product.setQuantity(newqty);
						productService.addProduct(product);
				    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
				    	if(prod != null){
							prod.setQuantity((int)(newqty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
							lcpProductService.addProduct(prod);
				    	}
						oItem.setQuantity(newQty);
						oItem.setUnitRate(price);
						//orderItemService.addOrderItem(oItem);
					}
					else{
						try {
							response.put("val", prodQty);
						} catch (JSONException e2) {
							e2.printStackTrace();
						}
					}
				}
			}
			else{
				oItem.setQuantity(newQty);
				oItem.setUnitRate(price);
				//orderItemService.addOrderItem(oItem);
			}
			try {
				response.put("status", "Success");

			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		catch(Exception e){
			try {
				response.put("status", "Failure");
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
		}
		return response.toString();
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/order/{orderId}/getStatus",method = RequestMethod.GET )
	public String getStatus(@PathVariable int orderId)
	{
		JSONObject jsonResponseObject = new JSONObject();
		String status = orderService.getOrder(orderId).getStatus();
		try {
			jsonResponseObject.put("value", status);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value = "/order/{orderId}/getPaid",method = RequestMethod.GET )
	public String getPaid(@PathVariable int orderId)
	{
		JSONObject jsonResponseObject = new JSONObject();
		Boolean status = orderService.getOrder(orderId).getIsPaid();
		try {
			jsonResponseObject.put("value", status);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value = "/order/{org}/{orderId}/deleteItem",method = RequestMethod.GET )
	public String delItem(@PathVariable int orderId, @PathVariable String org)
	{
		Boolean stock = organizationService.getOrganizationByAbbreviation(org).getStockManagement();
		JSONObject jsonResponseObject = new JSONObject();
		OrderItem orderItem = orderItemRepository.findOne(orderId);
		//orderService.removeOrder(order);
		//orderRepository.delete(order);
		if(stock){
			Product product = orderItem.getProduct();
	    	int newQty = product.getQuantity() + orderItem.getQuantity();
	    	product.setQuantity(newQty);
			productService.addProduct(product);
	    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
	    	if(prod != null){
				prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
				lcpProductService.addProduct(prod);
	    	}
			System.out.println("Updating product on deletion - "+product.getName());
		}
		orderItemRepository.delete(orderItem);	//List<Order> orderList = orderService.getOrderByOrganizationSavedSorted(organization);
		try {
			jsonResponseObject.put("Status", "Success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return jsonResponseObject.toString();		
	}
	

	@Transactional
	@RequestMapping(value="/testgcm", method = RequestMethod.POST)
	public String testGcm(@RequestBody String requestBody){
		JSONObject req = null;
		JSONObject response = new JSONObject();
		String organizationabbr = null;
		Organization organization = null;
		try {
			req = new JSONObject(requestBody);
			organizationabbr = req.getString("orgabbr");
			organization = organizationService.getOrganizationByAbbreviation(organizationabbr);
			List <String> androidTargets = getTargetDevices(organization);
			if(androidTargets.size()>0) {
			GcmRequest gcmRequest = new GcmRequest();
			HashMap<String,Integer>dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(userService.getCurrentUser().getName()+" has placed an order", "New order", androidTargets,0);
			System.out.println(organizationabbr);
			//gcmRequest.broadcast(androidTargets,organizationabbr,dashData);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return response.toString();
	}
	
	@Transactional
	
	@RequestMapping(value = "/orders/add",method = RequestMethod.POST )
	public @ResponseBody String addOrders(@RequestBody String requestBody){
	//	HashMap<String,String> response= new HashMap<String, String>();
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		String organizationabbr = null;
		String groupname = null;
		String comment = null;
		Organization organization= null;
		User currentUser = userService.getCurrentUser();
		float netAmount =0 ,total =0;
		int flag =0, countProd=0, ct = 0, flagForDisabled = 0;
		ArrayList <String> errorProduct = new ArrayList<String> ();
		String remProduct = "";
		try {
			JSONArray orderProducts = null;
			jsonObject = new JSONObject(requestBody);
			organizationabbr = jsonObject.getString("orgabbr");
			groupname = jsonObject.getString("groupname");
			comment = jsonObject.getString("comment");
			JSONArray updatePrice = new JSONArray();
			JSONArray disabledProducts = new JSONArray();
			String disabledProductsStr ="";
			String prodName ="";
			organization= organizationRepository.findByAbbreviation(organizationabbr);
			Boolean stockManagement = organization.getStockManagement();
			if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null) {
				response.put("status", "Failure");
				response.put("error", "You are no longer a member");
				return response.toString();
			}
			//Quantity verification
			orderProducts = jsonObject.getJSONArray("orderItems");
			for (int i = 0; i < orderProducts.length(); i++) {
				  
				JSONObject row = orderProducts.getJSONObject(i);
			    String productname=row.getString("name");
			    int productQuantity =row.getInt("quantity");
			    float price = -1;
			    try {
			    price= (float) row.getDouble("price");
			    total += price * productQuantity;
			    System.out.println(price);
			    }
			    catch(Exception e) {
			    	//uncaught, but not untamed!
			    }
			    Product product=productService.getProductByNameAndOrg(productname,organization);
			    if (product == null) {
			    	if (ct != 0)
			    		remProduct += ", ";
			    	remProduct += (productname);
			    	++ct;
			    	continue;
			    }
			    //algo for price changed in checkout
			    if (product.getUnitRate() != price && price != -1) {
			    	if(countProd != 0)
			    		prodName += ", ";
			    	flag =1;
			    	++countProd;
			    	prodName += product.getName();
			    	JSONObject object = new JSONObject();
			    	object.put("id", Integer.toString(product.getProductId()));
			    	object.put("name", product.getName());
			    	object.put("quantity", Integer.toString((product.getQuantity())));
			    	object.put("unitRate", Float.toString(product.getUnitRate()));
			    	object.put("imageUrl", product.getImageUrl());
			    	object.put("audioUrl", product.getAudioUrl());
			    	object.put("stockManagement", stockManagement.toString());
			    	object.put("description", product.getDescription());
			    	updatePrice.put(object);
			    }
			    
			    //algo for disabled products in checkout
			    if (product.getStatus()==0) {
			    	//anytime you want the JSONArray to be the output, uncomment it
			    /*	flagForDisabled++;
			    	JSONObject object = new JSONObject();
			    	object.put("id", Integer.toString(product.getProductId()));
			    	object.put("name", product.getName());
			    	object.put("quantity", Integer.toString((product.getQuantity())));
			    	object.put("unitRate", Float.toString(product.getUnitRate()));
			    	object.put("imageUrl", product.getImageUrl());
			    	object.put("audioUrl", product.getAudioUrl());
			    	object.put("stockManagement", stockManagement.toString());
			    	object.put("description", product.getDescription());
			    	disabledProducts.put(object);*/
			    	if(flagForDisabled>0)
			    	{
			    	disabledProductsStr=disabledProductsStr+" , "+product.getName();
			    	}
			    	else
			    	{
			    		disabledProductsStr=" "+product.getName();
			    	}
			    	flagForDisabled++;
			    }
			    
			    
				if(organization.getStockManagement() == true) {

			    int currentQuantity = product.getQuantity();
			    if (currentQuantity < productQuantity) {
			    	errorProduct.add(product.getName() + " - Max: "+ currentQuantity+ " units");
			    }
				}
			}
			if (!remProduct.isEmpty()) {
				response.put("error", "Products have been removed by the supplier: "+ remProduct);
				response.put("status", "Failure");
				return response.toString();
			}
			
			if (flag == 1){
				response.put("status", "price increased");
				response.put("products", updatePrice);
				return response.toString();
			}
			
			if (flagForDisabled!=0){
				response.put("status", "products has been disabled");
				response.put("error", "The following products have been deleted by the admin:"+disabledProductsStr);
				return response.toString();
			}
			
			if(!errorProduct.isEmpty()){
				throw new Exception();
			}
			if (total < organization.getOrderThreshold()) {
				response.put("error", "Minimum order for this organization is: Rs."+organization.getOrderThreshold());
				response.put("status", "Failure");
				return response.toString();
			}
			
			
		} 
			catch (JSONException e) {
			e.printStackTrace();
		}
		catch(Exception e1) {
			e1.printStackTrace();
			try {
				response.put("status", "Failure");
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			String error = new String();
			JSONArray errorArray = new JSONArray();
			Iterator <String> iterator = errorProduct.iterator();
			/*while(iterator.hasNext()) {
			//	error = error + iterator.next() +", ";
				errorArray.put(iterator.next());
			} */
			String output = "";
			int count =0;
			while(iterator.hasNext()) {
				if (count > 0)
					output += ", ";
				output = output + iterator.next();
				++count;
			}
			try {
				response.put("error", "Insufficient stock for: "+output);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		Order order = new Order();
		//Organization organization= organizationRepository.findByAbbreviation(organizationabbr);
		order.setOrganization(organization);
		order.setStatus("saved");
		order.setTrans_id("NA");
		order = orderRepository.save(order);
	    List<Integer> prods = new ArrayList<Integer>();
		List<OrderItem> orderItems= new ArrayList<OrderItem>();
		try {
			JSONArray orderItemsJSON = jsonObject.getJSONArray("orderItems");
			for (int i = 0; i < orderItemsJSON.length(); i++) {
			  
				JSONObject row = orderItemsJSON.getJSONObject(i);
			    String productname=row.getString("name");
			    int productQuantity =row.getInt("quantity");
			    Product product=productService.getProductByNameAndOrg(productname,organization);
			    int currentQuantity = product.getQuantity();
			    OrderItem orderItem= new OrderItem();
			    orderItem.setProduct(product);
			    orderItem.setQuantity(productQuantity);	
			    //
			    if (organization.getStockManagement() == true) {
			    	int newQty = currentQuantity - productQuantity;
			    	product.setQuantity(newQty);
			    	productService.addProduct(product);
			    	if(newQty < 10){
			    		prods.add(product.getProductId());
			    	}
			    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
			    	if(prod != null){
						prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
						lcpProductService.addProduct(prod);
			    	}
			    }
			    //
			    netAmount = netAmount + (productQuantity * product.getUnitRate());
			    orderItem.setUnitRate(product.getUnitRate());
			    orderItem.setOrder(order);
			    orderItem=orderItemRepository.save(orderItem);
			    orderItems.add(orderItem);
			}
			
			System.out.println(orderItemsJSON.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	
		order.setOrderItems(orderItems);
		BinaryMessage bmessage= new BinaryMessage();
		bmessage.setTime(new Timestamp((new Date()).getTime()));
		bmessage.setOrder(order);
		bmessage.setGroup(groupRepository.findByNameAndOrganization(groupname, organization));
		bmessage.setUser( userService.getCurrentUser());
		bmessage.setMode("app");
		bmessage.setType("order");
		bmessage.setComments(comment);
		binaryMessageRepository.save(bmessage);
		order.setMessage(bmessage);
		orderRepository.save(order);
		try {
			response.put("orderId",new Integer(order.getOrderId()).toString());
			response.put("status", "Success");

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		String email=order.getMessage().getUser().getEmail();
		String phonenumber = order.getMessage().getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
		List <String> androidTargets = getTargetDevices(organization);
		if(androidTargets.size()>0) {
			GcmRequest gcmRequest = new GcmRequest();
			HashMap<String,Integer>dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(userService.getCurrentUser().getName()+" has placed an order", "New order", androidTargets,0);
			System.out.println(organizationabbr);
			for(int i = 0; i < prods.size(); i++){
				gcmRequest.broadcast(productService.getProductById(prods.get(i)).getName()+" is running low on stock", "Low stock", androidTargets, 1);
			}
			gcmRequest.broadcast(androidTargets,organizationabbr,dashData);
		}
		String orderString = "";
		Iterator <OrderItem> itemsIterator = orderItems.iterator();
		orderString = "<ol>";
		while (itemsIterator.hasNext()) {
			OrderItem orderItem = itemsIterator.next();
			orderString += "<li>";
			orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units<br />";
			orderString += "</li>";
		}
		orderString+= "</ol>";
		if(order.getMessage().getUser().getEmailVerified()==1)
		SendMail.sendMail(email, "Lokacart: Order Placed Successfully" , "<p>Dear User <br /><br />Your order has been placed successfully with order ID: " + new Integer(order.getOrderId()).toString()+".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+" .</p>");
		if(organization.getAbbreviation().equals("NatG"))
			SendMail.sendMail("vishalghodke@gmail.com", "Lokacart: Order Placed", "Dear Admin,\nCustomer "+order.getMessage().getUser().getName()+" has placed an order of order id "+order.getOrderId()+".\n\nThankyou\nLokacart Team\n");
		
		SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(organization);
		String authId = smsApiKeys.getAuthId();
		String authToken = smsApiKeys.getAuthToken();
		String sourceNumber = smsApiKeys.getSourceNumber();
		RestAPI api = new RestAPI(authId, authToken, "v1");

		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("src", sourceNumber);
		parameters.put("dst", phonenumber);
		parameters.put("text", "Hello from Lokacart! \nThank you for placing an order with "+organization.getName()+". The bill amount is Rs."+String.format("%.2f", netAmount)+".");
		parameters.put("method", "GET");
		try {
			MessageResponse msgResponse = api.sendMessage(parameters);
			System.out.println(msgResponse);
			System.out.println("Api ID : " + msgResponse.apiId);
			System.out.println("Message : " + msgResponse.message);
			if (msgResponse.serverCode == 202) {
				System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
				response.put("text", "Confirmation sent");

			} else {
				System.out.println(msgResponse.error);
			}
		} catch (PlivoException e) {
			System.out.println(e.getLocalizedMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}		
		return response.toString();
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/orders/addPG",method = RequestMethod.POST )
	public @ResponseBody String addOrdersPG(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		String organizationabbr = null;
		String groupname = null, comment = null;
		Organization organization = null;
		User currentUser = userService.getCurrentUser();
		float netAmount = 0 ,total = 0;
		int flag = 0, countProd = 0, ct = 0, flagForDisabled = 0;
		ArrayList <String> errorProduct = new ArrayList<String> ();
		String remProduct = "";
		try {
			JSONArray orderProducts = null;
			jsonObject = new JSONObject(requestBody);
			organizationabbr=jsonObject.getString("orgabbr");
			groupname = jsonObject.getString("groupname");
			comment = jsonObject.getString("comment");
			JSONArray updatePrice = new JSONArray();
			JSONArray disabledProducts = new JSONArray();
			String disabledProductsStr ="";
			String prodName ="";
			organization= organizationRepository.findByAbbreviation(organizationabbr);
			Boolean stockManagement = organization.getStockManagement();
			if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null) {
				response.put("status", "Failure");
				response.put("error", "You are no longer a member");
				return response.toString();
			}
			//Quantity verification
			orderProducts = jsonObject.getJSONArray("orderItems");
			for (int i = 0; i < orderProducts.length(); i++) {
				  
				JSONObject row = orderProducts.getJSONObject(i);
			    String productname=row.getString("name");
			    int productQuantity =row.getInt("quantity");
			    float price = -1;
			    try {
			    price= (float) row.getDouble("price");
			    total += price * productQuantity;
			    System.out.println(price);
			    }
			    catch(Exception e) {
			    	//uncaught, but not untamed!
			    }
			    Product product=productService.getProductByNameAndOrg(productname,organization);
			    if (product == null) {
			    	if (ct != 0)
			    		remProduct += ", ";
			    	remProduct += (productname);
			    	++ct;
			    	continue;
			    }
			    //algo for price changed in checkout
			    if (product.getUnitRate() != price && price != -1) {
			    	if(countProd != 0)
			    		prodName += ", ";
			    	flag =1;
			    	++countProd;
			    	prodName += product.getName();
			    	JSONObject object = new JSONObject();
			    	object.put("id", Integer.toString(product.getProductId()));
			    	object.put("name", product.getName());
			    	object.put("quantity", Integer.toString((product.getQuantity())));
			    	object.put("unitRate", Float.toString(product.getUnitRate()));
			    	object.put("imageUrl", product.getImageUrl());
			    	object.put("audioUrl", product.getAudioUrl());
			    	object.put("stockManagement", stockManagement.toString());
			    	object.put("description", product.getDescription());
			    	updatePrice.put(object);
			    }
			    
			    //algo for disabled products in checkout
			    if (product.getStatus()==0) {
			    	//anytime you want the JSONArray to be the output, uncomment it
			    /*	flagForDisabled++;
			    	JSONObject object = new JSONObject();
			    	object.put("id", Integer.toString(product.getProductId()));
			    	object.put("name", product.getName());
			    	object.put("quantity", Integer.toString((product.getQuantity())));
			    	object.put("unitRate", Float.toString(product.getUnitRate()));
			    	object.put("imageUrl", product.getImageUrl());
			    	object.put("audioUrl", product.getAudioUrl());
			    	object.put("stockManagement", stockManagement.toString());
			    	object.put("description", product.getDescription());
			    	disabledProducts.put(object);*/
			    	if(flagForDisabled>0)
			    	{
			    	disabledProductsStr=disabledProductsStr+" , "+product.getName();
			    	}
			    	else
			    	{
			    		disabledProductsStr=" "+product.getName();
			    	}
			    	flagForDisabled++;
			    }
			    
			    
				if(organization.getStockManagement() == true) {

			    int currentQuantity = product.getQuantity();
			    if (currentQuantity < productQuantity) {
			    	errorProduct.add(product.getName() + " -Max: "+ currentQuantity+ " units");
			    }
				}
			}
			if (!remProduct.isEmpty()) {
				response.put("error", "Products have been removed by the supplier: "+ remProduct);
				response.put("status", "Failure");
				return response.toString();
			}
			
			if (flag == 1){
				response.put("status", "price increased");
				response.put("products", updatePrice);
				return response.toString();
			}
			
			if (flagForDisabled!=0){
				response.put("status", "products has been disabled");
				response.put("error", "The following products have been deleted by the admin:"+disabledProductsStr);
				return response.toString();
			}
			
			if(!errorProduct.isEmpty()){
				throw new Exception();
			}
			if (total < organization.getOrderThreshold()) {
				response.put("error", "Minimum order for this organization is: Rs."+organization.getOrderThreshold());
				response.put("status", "Failure");
				return response.toString();
			}
			
			
		} 
			catch (JSONException e) {
			e.printStackTrace();
		}
		catch(Exception e1) {
			e1.printStackTrace();
			try {
				response.put("status", "Failure");
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			String error = new String();
			JSONArray errorArray = new JSONArray();
			Iterator <String> iterator = errorProduct.iterator();
			/*while(iterator.hasNext()) {
			//	error = error + iterator.next() +", ";
				errorArray.put(iterator.next());
			} */
			String output = "";
			int count =0;
			while(iterator.hasNext()) {
				if (count > 0)
					output += ", ";
				output = output + iterator.next();
				++count;
			}
			try {
				response.put("error", "Insufficient stock for: "+output);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		Order order = new Order();
		//Organization organization= organizationRepository.findByAbbreviation(organizationabbr);
		order.setOrganization(organization);
		order.setStatus("none");
		order.setTrans_id("NA");
		order = orderRepository.save(order);
		List<OrderItem> orderItems= new ArrayList<OrderItem>();
		try {
			JSONArray orderItemsJSON = jsonObject.getJSONArray("orderItems");
			for (int i = 0; i < orderItemsJSON.length(); i++) {
			  
				JSONObject row = orderItemsJSON.getJSONObject(i);
			    String productname=row.getString("name");
			    int productQuantity =row.getInt("quantity");
			    Product product=productService.getProductByNameAndOrg(productname,organization);
			    int currentQuantity = product.getQuantity();
			    OrderItem orderItem= new OrderItem();
			    orderItem.setProduct(product);
			    orderItem.setQuantity(productQuantity);	
			    if (organization.getStockManagement() == true) {
			    	int newQty = currentQuantity - productQuantity;
			    	product.setQuantity(newQty);
			    	productService.addProduct(product);
			    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
			    	if(prod != null){
						prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
						lcpProductService.addProduct(prod);
			    	}
			    }
			    netAmount = netAmount + (productQuantity * product.getUnitRate());
			    orderItem.setUnitRate(product.getUnitRate());
			    orderItem.setOrder(order);
			    orderItem = orderItemRepository.save(orderItem);
			    orderItems.add(orderItem);
			}
			
			System.out.println(orderItemsJSON.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	
		order.setOrderItems(orderItems);
		BinaryMessage bmessage= new BinaryMessage();
		bmessage.setTime(new Timestamp((new Date()).getTime()));
		bmessage.setOrder(order);
		bmessage.setGroup(groupRepository.findByNameAndOrganization(groupname, organization));
		bmessage.setUser( userService.getCurrentUser());
		bmessage.setMode("app");
		bmessage.setType("order");
		bmessage.setComments(comment);
		binaryMessageRepository.save(bmessage);
		order.setMessage(bmessage);
		orderRepository.save(order);
		try {
			response.put("orderId", new Integer(order.getOrderId()).toString());
			response.put("merchantId", organization.getMerchantId());
			response.put("userId", new Integer(userService.getCurrentUser().getUserId()).toString());
			response.put("status", "Success");

		} catch (JSONException e1) {
			e1.printStackTrace();
		}		
		return response.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/confirmPG",method = RequestMethod.POST )
	public @ResponseBody String confirmOrdersPG(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		PaytmData paytmdata = null;
		JSONObject response = new JSONObject();
		Order order = null;
		List<OrderItem> orderItems = null;
		int orderId;
		float netAmount = 0;
		String trans_id = null;
		Organization organization = null;
		String organizationabbr = null;
	    List<Integer> prods = new ArrayList<Integer>();
		try{
			jsonObject = new JSONObject(requestBody);
			orderId = jsonObject.getInt("orderId");
			netAmount = jsonObject.getLong("net");
			trans_id = jsonObject.getString("txnId");
			order = orderService.getOrder(orderId);
			orderItems = order.getOrderItems();
			for(int i=0; i<orderItems.size(); i++){
				if(orderItems.get(i).getProduct().getQuantity() < 10)
					prods.add(orderItems.get(i).getProduct().getProductId());
			}
			organization = order.getOrganization();
			organizationabbr =  organization.getAbbreviation();
			paytmdata = paytmDatarepository.findByOrganization(organization);
			try{
				order.setTrans_id(trans_id);
				order.setStatus("saved");
				order.setIsPaid(true);
				paytmdata.setDeficit(paytmdata.getDeficit() + netAmount);
				response.put("status", "Success");
			}
			catch (Exception e) {
				// TODO: handle exception
				response.put("status", "Failure");
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		String email=order.getMessage().getUser().getEmail();
		String phonenumber = order.getMessage().getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
		List <String> androidTargets = getTargetDevices(organization);
		if(androidTargets.size()>0) {
			GcmRequest gcmRequest = new GcmRequest();
			HashMap<String,Integer>dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(userService.getCurrentUser().getName()+" has placed an order", "New order", androidTargets,0);
			System.out.println(organizationabbr);
			for(int i = 0; i < prods.size(); i++){
				System.out.println("Low stock for "+productService.getProductById(prods.get(i)).getName());
				gcmRequest.broadcast(productService.getProductById(prods.get(i)).getName()+" is running low on stock", "Low stock", androidTargets, 0);
			}
			gcmRequest.broadcast(androidTargets,organizationabbr,dashData);
		}
		String orderString = "";
		Iterator <OrderItem> itemsIterator = orderItems.iterator();
		orderString = "<ol>";
		while (itemsIterator.hasNext()) {
			OrderItem orderItem = itemsIterator.next();
			orderString += "<li>";
			orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units<br />";
			orderString += "</li>";
		}
		orderString+= "</ol>";
		if(order.getMessage().getUser().getEmailVerified()==1)
		SendMail.sendMail(email, "Lokacart: Order Placed Successfully" , "<p>Dear User <br /><br />Your order has been placed successfully with order ID: " + new Integer(order.getOrderId()).toString()+".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+" .</p>");
		if(organization.getAbbreviation().equals("NatG"))
			SendMail.sendMail("vishalghodke@gmail.com", "Lokacart: Order Placed", "Dear Admin,\nCustomer "+order.getMessage().getUser().getName()+" has placed an order of order id "+order.getOrderId()+".\n\nThankyou\nLokacart Team\n");
		
		SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(organization);
		String authId = smsApiKeys.getAuthId();
		String authToken = smsApiKeys.getAuthToken();
		String sourceNumber = smsApiKeys.getSourceNumber();
		RestAPI api = new RestAPI(authId, authToken, "v1");

		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("src", sourceNumber);
		parameters.put("dst", phonenumber);
		parameters.put("text", "Hello from Lokacart! \nThank you for placing an order with "+organization.getName()+". The bill amount is Rs."+String.format("%.2f", netAmount)+".");
		parameters.put("method", "GET");
		try {
			MessageResponse msgResponse = api.sendMessage(parameters);
			System.out.println(msgResponse);
			System.out.println("Api ID : " + msgResponse.apiId);
			System.out.println("Message : " + msgResponse.message);
			if (msgResponse.serverCode == 202) {
				System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());
				response.put("text", "Confirmation sent");

			} else {
				System.out.println(msgResponse.error);
			}
		} catch (PlivoException e) {
			System.out.println(e.getLocalizedMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return response.toString();
	}
	
	@Transactional
	@RequestMapping(value = "{org}/viewbill/{orderId}",method = RequestMethod.GET)
	public String viewBill(@PathVariable String org, @PathVariable int orderId){
		
		Order order = orderRepository.findOne(orderId);
		List <OrdersStore> ordersStore = null;
		Organization organization= order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
		String msg = null;
		if (order.getStatus().equals("delivered")) {
			ordersStore = ordersStoreService.getStoredItems(order);
			msg = SendBill.returnDeliveredBill(order,ordersStore, organization, billLayoutSetting);	
		}
		else {
			msg = SendBill.returnBill(order, organization, billLayoutSetting);
		}
		System.out.println(msg);
		return msg;
	}
	
	
	@Transactional
	@RequestMapping(value = "{org}/viewbilllcp/{orderId}",method = RequestMethod.GET)
	public String viewBillLcp(@PathVariable String org, @PathVariable int orderId){		
		LcpOrderItem orderItem = lcpOrderItemRepository.getOne(orderId);
		Organization organization = orderItem.getOrganization();
		BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
		String msg = null;
		msg = SendBill.returnBillLcp(orderItem, organization, billLayoutSetting);
		System.out.println(msg);
		return msg;
	}
	
	
	@Transactional
	@RequestMapping(value = "{org}/viewbilldelivered/{orderId}",method = RequestMethod.GET)
	public String viewBillDelivered(@PathVariable String org, @PathVariable int orderId){		
		Order order = orderRepository.findOne(orderId);
		List <OrdersStore> ordersStore = ordersStoreService.getStoredItems(order);
		Organization organization= order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
		String msg = SendBill.returnDeliveredBill(order,ordersStore, organization, billLayoutSetting);
		System.out.println(msg);
		return msg;
	}
	
	@Transactional
	@RequestMapping(value = "/orders/paid/{orderId}",method = RequestMethod.GET ,produces="application/json" )
	public String updatePaidOrder(@PathVariable int orderId) {
		JSONObject jsonResponseObject = new JSONObject();
		System.out.println("Hell yeah");
		Order order = orderService.getOrder(orderId);
		if (order == null){
			try {
				jsonResponseObject.put("response", "Failure");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		order.setIsPaid(true);
		order.setText("Paid");
		orderService.addOrder(order);
		try {
			jsonResponseObject.put("response", "Success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	
	@SuppressWarnings("unused")
	@Transactional
	@RequestMapping(value = "/orders/editOrderLcp/{orderId}", method = RequestMethod.POST, produces = "application/json")
	public String editOrderLcp(@PathVariable int orderId, @RequestBody String requestBody){
		JSONObject response = new JSONObject();
		JSONObject jsonObject = null;
		LcpOrderItem orderItem = null;
		int currQty = 0, modQty = 0;
		try{
			jsonObject = new JSONObject(requestBody);
			//
			//currQty = jsonObject.getInt("currQty");
			//
			//modQty = jsonObject.getInt("modQty");
			modQty = jsonObject.getJSONArray("orderItems").getJSONObject(0).getInt("quantity");
			orderItem = lcpOrderItemRepository.getOne(orderId);
			currQty = orderItem.getQuantity();
			
			if(orderItem == null){
				response.put("code", "Failure");
				response.put("error", "No Order of the following Id found");
				return response.toString();
			}
			else if(orderItem.getProduct().getQuantity() < (modQty - currQty)){
				response.put("code", "Failure");
				response.put("error", "Maximum quantity can be "+orderItem.getProduct().getQuantity());
				return response.toString();
			}
			else if(orderItem.getStatus().equals("cancelled")){
				response.put("code", "Failure");
				response.put("error", "Order cannot be modified as it is deleted");
				return response.toString();
			}
			else if(orderItem.getStatus().equals("processed") || orderItem.getStatus().equals("delivered")){
				response.put("code", "Failure");
				response.put("error", "Order cannot be modified. Please sync app immediately");
				return response.toString();
			}
			else if(modQty < orderItem.getProduct().getMoq()){
				response.put("code", "Failure");
				response.put("error", "Minimum quantity has to be "+orderItem.getProduct().getMoq());
				return response.toString();
			}
			else{
				orderItem.setQuantity(modQty);
				JSONObject orderObj = new JSONObject();
				orderObj.put("quantity", modQty);
				orderObj.put("rate", orderItem.getUnitRate());
				orderObj.put("productname", orderItem.getProduct().getName());
				orderObj.put("moq", orderItem.getProduct().getMoq());
				orderObj.put("stockQuantity", orderItem.getProduct().getQuantity());
				JSONObject orderResponse = new JSONObject();
				JSONArray orderArray = new JSONArray();
				orderArray.put(orderObj);
				orderResponse.put("orderid", orderItem.getOrderItemId());
				orderResponse.put("items", orderArray);
				orderResponse.put("timestamp", orderItem.getOrder().getPlaceDate());
				orderResponse.put("username", orderItem.getOrder().getRegistration().getFirstname());
				orderResponse.put("comment", orderItem.getOrder().getMessage());
				response.put("order", orderResponse);
				response.put("code", "2");
				response.put("comment", "success");
				FcmRequest.send_FCM_Broadcast(getTargetConsumerDevicesLcp(orderItem.getOrder().getRegistration().getPhone()), "Order Modified", "Order ID "+orderItem.getOrderItemId()+" has been modified by the admin", 0);
			}
		}
		catch(Exception e){}
		return response.toString();
	}

	
	@Transactional
	@RequestMapping(value = "/orders/update/{orderId}",method = RequestMethod.POST ,produces="application/json" )
	public String updateOrders(@PathVariable int orderId, @RequestBody String requestBody)
	{
		System.out.println("inside here");
		JSONObject response= new JSONObject();
		JSONObject jsonObject = null;
		String status=null;
		String comments=null;
		String orgabr=null;
		String delivery = null;
		//int flag = 0;
		List<String> updated = new ArrayList<String>();
		JSONArray orderItemsJSON = null;
		try {
			jsonObject = new JSONObject(requestBody);
			status=jsonObject.getString("status");
			comments=jsonObject.getString("comments");
			orderItemsJSON = jsonObject.getJSONArray("orderItems");

			//orgabr= jsonObject.getString("orgabbr");
		} catch (JSONException e) {
			//Uncaught but not untamed :-)			
			//return "Error";
		}	
		System.out.println(status);

		if(orderRepository.findOne(orderId)==null)
		{
			try {
				response.put("status", "error");
				response.put("error", "No Order of the following Id found");
				return response.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}	

		Order order = orderRepository.findOne(orderId);
		Organization organization= order.getOrganization();
		
		//delivered state procedure
		if (status.equals("delivered")) {
			if (order.getStatus().equals("delivered")) {
				try {
					response.put("status", "error");
					response.put("error", "Order has already been delivered");
					return response.toString();		
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			} 
			else if (order.getStatus().equals("cancelled")) {
				try {
					response.put("status", "error");
					response.put("error", "Order has already been cancelled");
					return response.toString();		
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println("Inside delivery part");
			order.setStatus("delivered");
			order.setText("Unpaid");

			try {
				delivery = jsonObject.getString("deldate");
			} catch (JSONException e) {
				delivery = "Not Applicable";
			}
			order.setDelivery(delivery);
			List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
			while(itemsIterator.hasNext()) {
				OrderItem orderItem = itemsIterator.next();
				OrdersStore ordersStore = new OrdersStore();
				ordersStore.setOrder(order);
				ordersStore.setProductName(orderItem.getProduct().getName());
				ordersStore.setQuantity(orderItem.getQuantity());
				ordersStore.setUnitRate(orderItem.getUnitRate());
				ordersStoreService.addOrdersStore(ordersStore);
				//orderItem.setProduct(null);

			}
			
			orderService.addOrder(order);
			if(order.getMessage().getUser().getEmailVerified()==1)
			SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order #"+order.getOrderId()+" has been Delivered", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been delivered to your address by "+ organization.getName() +".<br /><br />We hope to serve you again!</p>");
			List <String> androidConsumerTargets = getTargetConsumerDevices(userPhoneNumberService.getUserPrimaryPhoneNumber(order.getMessage().getUser()).getPhoneNumber());
			if (androidConsumerTargets.size() > 0) {
				System.out.println("Broadcasts sent");
				GcmRequest gcmRequest = new GcmRequest();
				gcmRequest.consumerBroadcastWithMessage("Delivered Order", "Order ID: "+ orderId +" has been delivered" ,androidConsumerTargets, "6");
			}
			
			try {
				response.put("status", "Success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		System.out.println(order.getOrderItems().iterator().next().getProduct().getName());
		//cancellation procedure
		if (status.equals("cancelled")) {
			if (order.getStatus().equals("processed")) {
				try {	
				response.put("status", "error");
					response.put("error", "Order has already been processed by the organisation");
					return response.toString();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (order.getStatus().equals("delivered")) {
				try {	
				response.put("status", "error");
					response.put("error", "Order has already been delivered to your address");
					return response.toString();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (order.getStatus().equals("deleted")) {
				try {	
				response.put("status", "error");
					response.put("error", "Order has already been deleted by Admin");
					return response.toString();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				response.put("status", "Success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			order.setStatus(status);
			Message message = order.getMessage();
			message.setComments(comments);
			messageService.addMessage(message);
			String orderString = "";
			
			List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
			float netAmount=0;
			orderString ="<ol>";
			while (itemsIterator.hasNext()) {
				OrderItem orderItem = itemsIterator.next();
				OrdersStore ordersStore = new OrdersStore();
				ordersStore.setOrder(order);
				ordersStore.setProductName(orderItem.getProduct().getName());
				ordersStore.setQuantity(orderItem.getQuantity());
				ordersStore.setUnitRate(orderItem.getUnitRate());
				ordersStoreService.addOrdersStore(ordersStore);
				orderString += "<li>";
				orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units\n";
				orderString += "</li>";
				netAmount += (orderItem.getQuantity() * orderItem.getUnitRate());
			}
			orderString += "</ol>";
			List<OrderItem> orderItems = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> iterator = orderItems.iterator();
			while (iterator.hasNext()) {
				OrderItem orderItem = iterator.next();
				Product product = orderItem.getProduct();
		    	int newQty = product.getQuantity() + orderItem.getQuantity();
		    	product.setQuantity(newQty);
				productService.addProduct(product);
		    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
		    	if(prod != null){
					prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
					lcpProductService.addProduct(prod);
		    	}
				System.out.println("Updating product on cancellation - "+product.getName());
				orderItem.setProduct(null);
			}
			if(order.getMessage().getUser().getEmailVerified()==1)
			SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Cancellation Acknowledgement", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been successfully cancelled by "+ organization.getName() +".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+". <br /><br />We hope to serve you again!</p>");
			orderRepository.save(order);
			return response.toString();
		}
		
		
		
		
		
		//deletion procedure
		if (status.equals("deleted")) {
			if (order.getStatus().equals("processed")) {
				try {	
				response.put("status", "error");
					response.put("error", "Order has already been processed by you");
					return response.toString();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (order.getStatus().equals("delivered")) {
				try {	
				response.put("status", "error");
					response.put("error", "Order has already been delivered to the users address");
					return response.toString();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (order.getStatus().equals("deleted")) {
				try {	
				response.put("status", "error");
					response.put("error", "Order has already been deleted by you");
					return response.toString();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (order.getStatus().equals("cancelled")) {
				try {	
				response.put("status", "error");
					response.put("error", "Order has already been deleted by the User");
					return response.toString();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				response.put("status", "Success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			//check
			order.setStatus("cancelled");
			Message message = order.getMessage();
			message.setComments(comments);
			messageService.addMessage(message);
			String orderString = "";
			
			List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
			float netAmount=0;
			orderString ="<ol>";
			while (itemsIterator.hasNext()) {
				OrderItem orderItem = itemsIterator.next();
				OrdersStore ordersStore = new OrdersStore();
				ordersStore.setOrder(order);
				ordersStore.setProductName(orderItem.getProduct().getName());
				ordersStore.setQuantity(orderItem.getQuantity());
				ordersStore.setUnitRate(orderItem.getUnitRate());
				ordersStoreService.addOrdersStore(ordersStore);
				orderString += "<li>";
				orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units\n";
				orderString += "</li>";
				netAmount += (orderItem.getQuantity() * orderItem.getUnitRate());
			}
			orderString += "</ol>";
			List<OrderItem> orderItems = orderService.getOrder(orderId).getOrderItems();
			Iterator <OrderItem> iterator = orderItems.iterator();
			while (iterator.hasNext()) {
				OrderItem orderItem = iterator.next();
				Product product = orderItem.getProduct();
		    	int newQty = product.getQuantity() + orderItem.getQuantity();
		    	product.setQuantity(newQty);
				productService.addProduct(product);
		    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
		    	if(prod != null){
					prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
					lcpProductService.addProduct(prod);
		    	}
				System.out.println("Updating product on deletion - "+product.getName());
				orderItem.setProduct(null);
			}
			
			List <String> androidConsumerTargets = getTargetConsumerDevices(userPhoneNumberService.getUserPrimaryPhoneNumber(order.getMessage().getUser()).getPhoneNumber());
			if (androidConsumerTargets.size() > 0) {
				System.out.println("Broadcasts sent");
				GcmRequest gcmRequest = new GcmRequest();
				gcmRequest.consumerBroadcastWithMessage("Order Deletion", "Order ID: "+ orderId +" has been deleted by the admin" ,androidConsumerTargets, "10");
					}
			
			if(order.getMessage().getUser().getEmailVerified()==1)
			SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Cancellation Acknowledgement", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been deleted by "+ organization.getName() +".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+". <br /><br />We hope to serve you again!</p>");
			orderRepository.save(order);
			return response.toString();
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		String organizationabbr = organization.getAbbreviation();
		if( order.getStatus().equals("processed")) {
			try {
				response.put("status", "Failure");
				response.put("error","Order cannot be modified. Please sync app immediately");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		//Will be used later when comments are added while ordering.
		if(comments != null) {
		if(!comments.equals("null"))
		{
			BinaryMessage message=(BinaryMessage)order.getMessage();
			message.setComments(comments);
			binaryMessageRepository.save(message);
		}
		}
		System.out.println(order.getStatus());
		if( order.getStatus().equals("processed")) {
			try {
				System.out.println("inside");
				response.put("status", "Order cannot be modified. Please sync app immediately");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		if( order.getStatus().equals("deleted")) {
			try {
				response.put("status", "Failure");
				response.put("error","Order cannot be modified as it is deleted");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		else if ( order.getStatus().equals("delivered")) {
			try {
				System.out.println("inside");
				response.put("status", "Order cannot be modified. Please sync app immediately");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		else if ( order.getStatus().equals("cancelled")) {
			try {
				System.out.println("inside");
				response.put("result", "Failure");
				response.put("status", "Order cannot be modified. The order has been deleted by the User");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return response.toString();
		}
		if(orderItemsJSON != null)
		{
			List <OrderItem> list = orderService.getOrder(orderId).getOrderItems();
			ArrayList <String> errorProduct = new ArrayList<String> ();
			try{
				if(organization.getStockManagement() == true) {
					//Quantity verification
					JSONArray orderProducts = jsonObject.getJSONArray("orderItems");
					for (int i = 0; i < orderProducts.length(); i++) {
						 OrderItem orderItem = null;
						
						JSONObject row = orderProducts.getJSONObject(i);
					    String productname=row.getString("name");
					    int productQuantity =row.getInt("quantity");
					    for(int j=0;j<list.size();++j) {
							if(list.get(j).getProduct().getName().equals(productname))
								orderItem = list.get(j);
						}
					    Product product=productService.getProductByNameAndOrg(productname,organization);
					    int currentQuantity = product.getQuantity();
					    if (currentQuantity < (productQuantity - orderItem.getQuantity())) {
					    	errorProduct.add(product.getName()+ " Max: "+ (currentQuantity+ orderItem.getQuantity())+ " units");
					    }
					}
					if(!errorProduct.isEmpty()){
						throw new Exception();
					}
				}
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			catch(Exception e1) {
				e1.printStackTrace();
				try {
					response.put("status", "Failure");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				String error = new String();
				JSONArray errorArray = new JSONArray();
				Iterator <String> iterator = errorProduct.iterator();
				String output = "";
				int count =0;
				while(iterator.hasNext()) {
				//	error = error + iterator.next() +", ";
					//errorArray.put(iterator.next());
					if (count > 0)
						output += ", ";
					output = output + iterator.next();
					++count;
				}
				try {
					response.put("error",  "Insufficient stock for: "+output);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return response.toString();
			}	
			try {
			if(organization.getStockManagement() == true) {
				//Quantity verification
				JSONArray orderProducts = jsonObject.getJSONArray("orderItems");
				for (int i = 0; i < orderProducts.length(); i++) {
					 OrderItem orderItem = null;
					
					JSONObject row = orderProducts.getJSONObject(i);
				    String productname=row.getString("name");
				    int productQuantity =row.getInt("quantity");
				    for(int j=0;j<list.size();++j) {
						if(list.get(j).getProduct().getName().equals(productname))
						{
							orderItem = list.get(j);
							updated.add(list.get(j).getProduct().getName());
						}	
					}
				    
				    /*  
				    if (orderProducts.length() < list.size() ) {
						Iterator <OrderItem> itemIter = list.iterator();
						while (itemIter.hasNext()) {
							OrderItem item = itemIter.next();
							f
						}
				    }
				    */
				    
				    
				    
				    Product product=productService.getProductByNameAndOrg(productname,organization);
				    int currentQuantity = product.getQuantity();
				   // if (currentQuantity < productQuantity) {
				   // 	errorProduct.add(product.getName());
				   // }
				    if (productQuantity < orderItem.getQuantity()){
				    	//reduce order qty
				    	int difference = orderItem.getQuantity() - productQuantity;
				    	int newQty = product.getQuantity()+difference;
				    	product.setQuantity(newQty);
				    	productService.addProduct(product);
				    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
				    	if(prod != null){
							prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
							lcpProductService.addProduct(prod);
				    	}
				    	System.out.println("increased qty of " + product.getName());
				    }
				    else if (productQuantity == orderItem.getQuantity()) {
				    	// Do nothing.. :P
				    	System.out.println("product qty is same:" + product.getName());
				    }
				    else if (currentQuantity >= (productQuantity- orderItem.getQuantity())) {
				    	//increase product order qty
				    	System.out.println("increased product order qty");
				    	int difference = productQuantity - orderItem.getQuantity();
				    	int newQty = product.getQuantity() - difference;
				    	product.setQuantity(newQty);
				    	productService.addProduct(product);
				    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
				    	if(prod != null){
							prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
							lcpProductService.addProduct(prod);
				    	}
				    }
				    else if (currentQuantity < (productQuantity - orderItem.getQuantity())) {
				    	errorProduct.add(product.getName()+" -Max: "+ (currentQuantity+ orderItem.getQuantity())+ " units");
				    }
				}
				// I don't want to modify working logic
				if (orderProducts.length() < list.size()) {
					Iterator <OrderItem> itemIter = list.iterator();
					while (itemIter.hasNext()) {
						OrderItem item = itemIter.next();
						Iterator <String> iterProd = updated.iterator();
						int flag =0;
						while (iterProd.hasNext()) {
							String updatedProd = iterProd.next();
							if (item.getProduct().getName().equals(updatedProd)) {
								flag =1;
							}
						}
						if (flag == 0) {
							Product prodUpdated = item.getProduct();
					    	int newQty = prodUpdated.getQuantity() + item.getQuantity();
					    	prodUpdated.setQuantity(newQty);
							productService.addProduct(prodUpdated);
					    	LcpProduct prod = lcpProductService.getProductById(prodUpdated.getProductId());
					    	if(prod != null){
								prod.setQuantity((int)(newQty*(Utils.conversionFact(prodUpdated.getSiUnit(), prod.getSiUnit())*prodUpdated.getUnitQty())));
								lcpProductService.addProduct(prod);
					    	}
						}
					}			
				}
				if(!errorProduct.isEmpty()){
					throw new Exception();
				}
				}
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			catch(Exception e1) {
				e1.printStackTrace();
				try {
					response.put("status", "Failure");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				String error = new String();
				JSONArray errorArray = new JSONArray();
				Iterator <String> iterator = errorProduct.iterator();
				String output = "";
				int count =0;
				while(iterator.hasNext()) {
				//	error = error + iterator.next() +", ";
					//errorArray.put(iterator.next());
					if (count > 0)
						output += ", ";
					output = output + iterator.next();
					++count;
				}
				try {
					response.put("error",  "Insufficient stock for: "+output);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return response.toString();
			}
			float unitRates[] = new float [order.getOrderItems().size()];
			int count =0;
			
			List <OrderItem> oldOrdersList = order.getOrderItems();
			for( OrderItem orderitem : order.getOrderItems())
			{
				orderItemRepository.delete(orderitem);
			}
			List<OrderItem> orderItems= new ArrayList<OrderItem>();
			try {
				orderItemsJSON = jsonObject.getJSONArray("orderItems");
				for (int i = 0; i < orderItemsJSON.length(); i++) {
				    OrderItem orderItem= new OrderItem();
					JSONObject row = orderItemsJSON.getJSONObject(i);
				    String productName=row.getString("name");
				    int productQuantity =row.getInt("quantity");
				    System.out.println(productName + " ---- "+productQuantity);
				    Product product=productService.getProductByNameAndOrg(productName, organization);
				    orderItem.setProduct(product);
					orderItem.setQuantity(productQuantity);
				    float oldRate = 0;
				    int oldQuantity = 0;
				    Iterator <OrderItem> oldOrders = oldOrdersList.iterator();
				    while(oldOrders.hasNext()) {
					    OrderItem item = oldOrders.next();
					    if (item.getProduct().getName().equals(productName)) {
						    oldRate = item.getUnitRate();
						    oldQuantity = item.getQuantity();
					    }
				    }
				    //orderItem.setUnitRate(oldRate);
				    if(oldQuantity == productQuantity){	
				   	    orderItem.setUnitRate(oldRate);
				   	    System.out.println("No change "+oldRate);
				    }
				    else{
				   	    orderItem.setUnitRate(product.getUnitRate());
				   	    System.out.println("Yes change "+product.getUnitRate());
				    }
				    orderItem.setOrder(order);
				    orderItem = orderItemRepository.save(orderItem);
				    orderItems.add(orderItem);
				}
				System.out.println(orderItemsJSON.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			order.setOrderItems(orderItems);
		}
		String orderString = "";
		List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
		Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
		float netAmount=0;
		orderString +="<ol>";
		while (itemsIterator.hasNext()) {
			OrderItem orderItem = itemsIterator.next();
			orderString += "<li>";
			orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units<br />";
			orderString += "</li>";
			netAmount += (orderItem.getQuantity() * orderItem.getUnitRate());
		}
		orderString += "</ol>";
		System.out.println(orderString);
		try {
			// EditOrder
			response.put("status", "Success]");

			
			
			
			
			
			
			//JSONObject jsonResponseObject = new JSONObject();
		//	Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		//	List<Order> orderList = orderService.getOrderByOrganizationSavedSorted(organization);
		//	JSONArray orderArray = new JSONArray();
		//	Iterator<Order> iterator = orderList.iterator();
		//	Boolean stockManagement = organization.getStockManagement();
			//findByOrder_OrderId
			Order orderOne = orderRepository.findOne(orderId);
			List<OrderItem> oit = orderItemRepository.findByOrder_OrderId(orderId);
				JSONObject orderObject = new JSONObject();
				
				List<Integer> quantityList=new ArrayList<Integer>();
				JSONArray orderProducts = jsonObject.getJSONArray("orderItems");
				for (int i = 0; i < orderProducts.length(); i++) {
					 OrderItem orderItem = null;
					
					JSONObject row = orderProducts.getJSONObject(i);
				    
				    int productQuantity =row.getInt("quantity");
				    quantityList.add(productQuantity);
				    
				}
				
				
				Message message = messageService.getMessageFromOrder(order);
				if(message != null){
				try {
					orderObject.put("orderid",orderOne.getOrderId());
					orderObject.put("timestamp", message.getTime().toString());
					orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
					orderObject.put("comment", message.getComments());
					List<OrderItem> OneOrder = orderOne.getOrderItems();
					int ordersCount=0;
					JSONArray items = new JSONArray();
					
					while(ordersCount<oit.size())
					{
						JSONObject orderObjects = new JSONObject();
					Product product = oit.get(ordersCount).getProduct();
					orderObjects.put("productname",(product.getName().toString()));
					//System.out.println(oit.get(ordersCount).getQuantity() +"   "+ quantityList.get(ordersCount));
					/*if(oit.get(ordersCount).getQuantity() != quantityList.get(ordersCount)){
						orderObjects.put("quantity", quantityList.get(ordersCount).toString());
						orderObjects.put("rate", Float.toString(product.getUnitRate()));
						System.out.println("Yes change");
					}
					else{*/
						orderObjects.put("quantity", Integer.toString(oit.get(ordersCount).getQuantity()));
						orderObjects.put("rate", Float.toString(oit.get(ordersCount).getUnitRate()));
						//System.out.println("No change");						
					//}
					Product products = oit.get(ordersCount).getProduct();
					orderObjects.put("stockQuantity", Integer.toString(products.getQuantity()));
					items.put(orderObjects);
					ordersCount++;
					}	
					orderObject.put("items",items);
					response.put("order", orderObject);
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				}
			

			//return response.toString();
			
		

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			order.setStatus(status);
	
			if (status.equals("cancelled")) {
				List<OrderItem> orderItems = orderService.getOrder(orderId).getOrderItems();
				Iterator <OrderItem> iterator = orderItems.iterator();
				while (iterator.hasNext()) {
					OrderItem orderItem = iterator.next();
					Product product = orderItem.getProduct();
			    	int newQty = product.getQuantity() + orderItem.getQuantity();
			    	product.setQuantity(newQty);
					productService.addProduct(product);
			    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
			    	if(prod != null){
						prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
						lcpProductService.addProduct(prod);
			    	}
					System.out.println("Updating product on cancellation - "+product.getName());
				}
			}
			if(status.equals("cancelled"))
			{
				if(order.getMessage().getUser().getEmailVerified()==1)
				SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Cancellation Acknowledgement", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been successfully cancelled by "+ organization.getName() +".<br />The items ordered were: <br /><br />"+orderString+"<br />We hope to serve you again.</p>");
				if(organization.getAbbreviation().equals("NatG"))
					SendMail.sendMail("vishalghodke@gmail.com", "Lokacart: Order Cancellation", "Dear Admin\n\nCustomer "+order.getMessage().getUser().getName()+" has cancelled the order of order ID "+order.getOrderId()+".<br />Thank you <br />Lokacart Team");
			}
			else {
				if(order.getMessage().getUser().getEmailVerified()==1)
				SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Modification Acknowledgement", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been successfully modified on "+order.getMessage().getTime()+ " by "+ organization.getName() +".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+" .</p>");
				System.out.println("---------------You reached here----------------");
				//GcmRequest gcmRequest= new GcmRequest();
				List <String> androidConsumerTargets = getTargetConsumerDevices(userPhoneNumberService.getUserPrimaryPhoneNumber(order.getMessage().getUser()).getPhoneNumber());
				if (androidConsumerTargets.size() > 0) {
					System.out.println("Broadcasts sent");
					GcmRequest gcmRequest = new GcmRequest();
					gcmRequest.consumerBroadcastWithMessage("Changed Order", "Order ID: "+ orderId +" has been modified" ,androidConsumerTargets, "7");
						}
				
				order.getMessage().setTime(new Timestamp((new Date()).getTime()));
				binaryMessageRepository.save((BinaryMessage)order.getMessage());
				if(organization.getAbbreviation().equals("NatG"))
					SendMail.sendMail("vishalghodke@gmail.com", "Lokacart: Order Modification", "Dear Admin ,<br /><br />Customer "+order.getMessage().getUser().getName()+" has modified the order of order ID "+order.getOrderId()+".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+" .</p>");
			}
			orderRepository.save(order);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		List <String> androidTargets = getTargetDevices(organization);
		if(androidTargets.size() > 0) {
			GcmRequest gcmRequest = new GcmRequest();
	
			HashMap<String, Integer> dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(androidTargets,organizationabbr,dashData);			
			}
		return response.toString();
	}
	
	
	@Transactional
	@RequestMapping(value = "/orders/changelcp/delete", method = RequestMethod.POST)
	public String deleteLcpOrder(@RequestParam(value="orderId") String orderId, @RequestBody String requestBody){
		JSONObject response = new JSONObject();
		LcpOrderItem order = null;
		try{
			order = lcpOrderItemRepository.getOne(Integer.parseInt(orderId));
			if(order == null){
				response.put("status", "error");
				response.put("error", "No Order of the following Id found");
				return response.toString();
			}
			else if(order.getStatus().equals("processed")){
				response.put("status", "error");
				response.put("error", "Order has already been processed by you");
				return response.toString();
			}
			else if(order.getStatus().equals("delivered")){
				response.put("status", "error");
				response.put("error", "Order has already been delivered to the users address");
				return response.toString();
			}
			else if(order.getStatus().equals("cancelled")){
				response.put("status", "error");
				response.put("error", "Order has already been deleted by you");
				return response.toString();
			}
			else{
				order.setStatus("cancelled");
				order.setMessage("Order deleted by admin.");
				// TODO - Sending message for cancellation
				FcmRequest.send_FCM_Broadcast(getTargetConsumerDevicesLcp(order.getOrder().getRegistration().getPhone()), "Order Cancelled", "Order ID "+order.getOrderItemId()+" has been cancelled by the admin", 1);
				response.put("status", "Success");
			}
		}
		catch(Exception e){	}
		return response.toString();
	}
	
	
	@Transactional
	@RequestMapping(value = "/orders/changelcp/processed", method = RequestMethod.GET)
	public String processLcpOrder(@RequestParam(value="orderId") String orderId){
		JSONObject response = new JSONObject();
		LcpOrderItem order = null;
		try{
			order = lcpOrderItemRepository.getOne(Integer.parseInt(orderId));
			if(order == null){
				response.put("status", "error");
				response.put("error", "Order was not found.");
				return response.toString();
			}
			else if(order.getStatus().equals("processed")){
				response.put("status", "error");
				response.put("error", "Order was already processed.");
				return response.toString();
			}
			else if(order.getStatus().equals("delivered")){
				response.put("status", "error");
				response.put("error", "Order was already delivered.");
				return response.toString();
			}
			else if(order.getStatus().equals("cancelled")){
				response.put("status", "error");
				response.put("error", "Order was already cancelled by user.");
				return response.toString();
			}
			else if(order.getStatus().equals("deleted")){
				response.put("status", "error");
				response.put("error", "Order was already deleted by admin.");
				return response.toString();
			}
			else if(order.getStatus().equals("placed")){
				order.setStatus("processed");
				order.setMessage("Order processed by admin.");
				// TODO - Sending message for processed
				FcmRequest.send_FCM_Broadcast(getTargetConsumerDevicesLcp(order.getOrder().getRegistration().getPhone()), "Order Processed", "Order ID "+order.getOrderItemId()+" has been processed by the admin", 2);
				response.put("status", "Success");
			}
		}
		catch(Exception e){	}
		return response.toString();
	}

	
	@Transactional
	@RequestMapping(value = "/orders/changelcp/delivered", method = RequestMethod.POST)
	public String deliveredLcpOrder(@RequestParam(value="orderId") String orderId){
		JSONObject response = new JSONObject();
		LcpOrderItem order = null;
		try{
			order = lcpOrderItemRepository.getOne(Integer.parseInt(orderId));
			if(order == null){
				response.put("status", "error");
				response.put("error", "No Order of the following Id found");
				return response.toString();
			}
			else if(order.getStatus().equals("placed")){
				response.put("status", "error");
				response.put("error", "Order not processed yet");
				return response.toString();
			}
			else if(order.getStatus().equals("delivered")){
				response.put("status", "error");
				response.put("error", "Order has already been delivered");
				return response.toString();
			}
			else if(order.getStatus().equals("cancelled")){
				response.put("status", "error");
				response.put("error", "Order has already been cancelled");
				return response.toString();
			}
			else if(order.getStatus().equals("deleted")){
				response.put("status", "error");
				response.put("error", "Order has already been cancelled");
				return response.toString();
			}
			else if(order.getStatus().equals("processed")){
				order.setStatus("delivered");
				order.setMessage("Order delivered by admin.");
				// TODO - Sending message for processed
				FcmRequest.send_FCM_Broadcast(getTargetConsumerDevicesLcp(order.getOrder().getRegistration().getPhone()), "Order Delivered", "Order ID "+order.getOrderItemId()+" has been delivered by the admin", 3);
				response.put("status", "Success");
			}
		}
		catch(Exception e){	}
		return response.toString();
	}
	

	@Transactional
	@RequestMapping(value = "/orders/changelcp/paid", method = RequestMethod.GET)
	public String paidLcpOrder(@RequestParam(value="orderId") String orderId){
		JSONObject response = new JSONObject();
		LcpOrderItem order = null;
		try{
			order = lcpOrderItemRepository.getOne(Integer.parseInt(orderId));
			if(order == null){
				response.put("response", "failure");
				response.put("reason", "Order was not found.");
				return response.toString();
			}
			else if(order.getStatus().equals("placed")){
				response.put("response", "failure");
				response.put("reason", "Order not processed yet.");
				return response.toString();
			}
			else if(order.getStatus().equals("processed")){
				response.put("response", "failure");
				response.put("reason", "Order not delivered yet.");
				return response.toString();
			}
			else if(order.getStatus().equals("cancelled")){
				response.put("response", "failure");
				response.put("reason", "Order was already cancelled by user.");
				return response.toString();
			}
			else if(order.getStatus().equals("deleted")){
				response.put("response", "failure");
				response.put("reason", "Order was already deleted by admin.");
				return response.toString();
			}
			else if(order.getStatus().equals("delivered")){
				if(order.getIsPaid() == true){
					response.put("response", "failure");
					response.put("reason", "Order was already paid.");
					return response.toString();					
				}
				else{
					order.setIsPaid(true);
					order.setMessage("Order paid by user.");
					// TODO - Sending message for processed
					
					response.put("response", "Success");
				}
			}
		}
		catch(Exception e){	}
		return response.toString();
	}	
	
	
	@Transactional
	@RequestMapping(value = "/orders/updatestatus/{orderId}",method = RequestMethod.GET, produces="text/plain" )
	public String updateOrderStatus(@PathVariable int orderId)
	{
		System.out.println("In update status with order id = "+orderId);
		Order order = orderRepository.findOne(orderId);
		Organization organization= order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		if(order.getStatus().equals("processed"))
		{
			System.out.println("In processed");
			BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
			SendBill.sendMail(order, organization, billLayoutSetting);
		}
		else if (order.getStatus().equals("saved"))
		{
			System.out.println("In saved");
			if(order.getMessage().getUser().getEmailVerified()==1)
			SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order recieved", "<p>Dear User <br /><br />Organization "+organization.getName()+" has recieved your order. Order processing might take couple of days. Once your order is processed, you will receive an email confirming the same.<br /><br /> Thank you for shopping with us.</p>");
		}
		System.out.println("Mail sent");
		List <String> androidTargets = getTargetDevices(organization);
		if(androidTargets.size() > 0) {
			GcmRequest gcmRequest = new GcmRequest();

			HashMap<String, Integer> dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(androidTargets,organizationabbr,dashData);			}
		return "Success";
	}
	
	
	@Transactional
	@RequestMapping(value="/orders/get", method = RequestMethod.GET)
	public String displayAllOrders(@RequestParam(value="orgabbr") String orgabbr)
	{
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray savedArray = new JSONArray();
		JSONArray processedArray = new JSONArray();
		JSONArray cancelledArray = new JSONArray();
		JSONArray deliveredArray = new JSONArray();

		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizationSorted(organization);
		Iterator <Order> iterator = orderList.iterator();
		while(iterator.hasNext()) {
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			JSONObject orderObject = new JSONObject();
			if(message != null){
				try {
					orderObject.put("orderid",order.getOrderId());
					orderObject.put("timestamp", message.getTime().toString());
					orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
					orderObject.put("comment", message.getComments());
					if(order.getStatus().equals("saved")){
						savedArray.put(orderObject);
					}
					else if (order.getStatus().equals("processed")){
						processedArray.put(orderObject);
					}
					else if (order.getStatus().equals("cancelled")) {
						cancelledArray.put(orderObject);
					}
					else if (order.getStatus().equals("delivered")) {
						deliveredArray.put(orderObject);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				}
			
		}
		try{
		jsonResponseObject.put("saved", savedArray);
		jsonResponseObject.put("processed", processedArray);
		jsonResponseObject.put("cancelled", cancelledArray);
		jsonResponseObject.put("delivered", deliveredArray);
		jsonResponseObject.put("response", "success");
		}
		catch(JSONException e){
			e.printStackTrace();
			try {
				jsonResponseObject.put("response", "error");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/saved",method = RequestMethod.GET )
	public String displaySavedOrders(@RequestParam(value="orgabbr") String orgabbr)
	{
		JSONObject jsonResponseObject = new JSONObject();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizationSavedSorted(organization);
		JSONArray orderArray = new JSONArray();
		Iterator<Order> iterator = orderList.iterator();
		Boolean stockManagement = organization.getStockManagement();
		while(iterator.hasNext())
		{
			JSONObject orderObject = new JSONObject();
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			if(message != null){
			try {
				orderObject.put("orderid",order.getOrderId());
				orderObject.put("timestamp", message.getTime().toString());
				orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
				orderObject.put("comment", message.getComments());
				orderObject.put("delivery", "saved");
				orderObject.put("transId", order.getTrans_id());
				JSONArray items = new JSONArray();
				List<OrderItem> orderItems = order.getOrderItems();
				Iterator<OrderItem> iter = orderItems.iterator();
				while(iter.hasNext()){
					OrderItem orderItem = iter.next();
					JSONObject item = new JSONObject();
					try {
				
						item.put("productname", orderItem.getProduct().getName());
						item.put("quantity", Integer.toString(orderItem.getQuantity()));
						Product product = orderItem.getProduct();
						item.put("stockQuantity", Integer.toString(product.getQuantity()));
						item.put("rate", Float.toString(orderItem.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
				}
				orderObject.put("items", items);
				orderArray.put(orderObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
		}
		try {
			jsonResponseObject.put("orders",orderArray);
			jsonResponseObject.put("stockManagement", stockManagement.toString());
			jsonResponseObject.put("minimumBillOrder",organization.getOrderThreshold());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	

	@Transactional
	@RequestMapping(value = "/orders/savedLcp",method = RequestMethod.GET )
	public String displaySavedOrdersLCP(@RequestParam(value="orgabbr") String orgabbr)
	{
		JSONObject jsonResponseObject = new JSONObject();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		List<LcpOrderItem> orderList = lcpOrderItemRepository.findByOrganizationOrderByOrderItemIdDesc(organization);
		JSONArray lcpOrderArray = new JSONArray();
		Iterator<LcpOrderItem> iterator = orderList.iterator();
		Boolean stockManagement = organization.getStockManagement();
		while(iterator.hasNext())
		{
			LcpOrderItem orderItem = iterator.next();
			JSONObject orderObject = new JSONObject();
			if(orderItem.getStatus().equals("placed")){
				try {
					orderObject.put("orderid",orderItem.getOrderItemId());
					orderObject.put("timestamp", orderItem.getOrder().getPlaceDate());
					orderObject.put("transId", "NA");
					orderObject.put("username", orderItem.getOrder().getRegistration().getFirstname());
					try{
						orderObject.put("comment", orderItem.getOrder().getMessage());
					}
					catch(Exception e){
						orderObject.put("comment", "null");
					}
					orderObject.put("delivery", "placed");
					JSONObject item = new JSONObject();
					JSONArray items = new JSONArray();
					try {
						item.put("productname", orderItem.getProduct().getName());
						item.put("quantity", Integer.toString(orderItem.getQuantity()));
						LcpProduct product = orderItem.getProduct();
						item.put("stockQuantity", Integer.toString(product.getQuantity()));
						item.put("moq", Integer.toString(product.getMoq()));
						item.put("rate", Float.toString(orderItem.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}				
					orderObject.put("items", items);
					lcpOrderArray.put(orderObject);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			jsonResponseObject.put("orders",lcpOrderArray);
			jsonResponseObject.put("stockManagement", stockManagement.toString());
			jsonResponseObject.put("minimumBillOrder",organization.getOrderThreshold());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/orders/processedLcp",method = RequestMethod.GET )
	public String displayProcessedOrdersLCP(@RequestParam(value="orgabbr") String orgabbr)
	{
		JSONObject jsonResponseObject = new JSONObject();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		List<LcpOrderItem> orderList = lcpOrderItemRepository.findByOrganizationOrderByOrderItemIdDesc(organization);
		JSONArray lcpOrderArray = new JSONArray();
		Iterator<LcpOrderItem> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			LcpOrderItem orderItem = iterator.next();
			JSONObject orderObject = new JSONObject();
			if(orderItem.getStatus().equals("processed")){
				try {
					orderObject.put("orderid",orderItem.getOrderItemId());
					orderObject.put("timestamp", orderItem.getOrder().getPlaceDate());
					orderObject.put("username", orderItem.getOrder().getRegistration().getFirstname());
					try{
						orderObject.put("comment", orderItem.getOrder().getMessage());
					}
					catch(Exception e){
						orderObject.put("comment", "null");
					}
					orderObject.put("delivery", orderItem.getDelivery());
					JSONObject item = new JSONObject();
					JSONArray items = new JSONArray();
					try {
						item.put("productname", orderItem.getProduct().getName());
						item.put("quantity", Integer.toString(orderItem.getQuantity()));
						LcpProduct product = orderItem.getProduct();
						item.put("stockQuantity", Integer.toString(product.getQuantity()));
						item.put("rate", Float.toString(orderItem.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}				
					orderObject.put("items", items);
					lcpOrderArray.put(orderObject);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			jsonResponseObject.put("orders",lcpOrderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value = "/orders/deliveredLcp",method = RequestMethod.GET )
	public String displayDeliveredOrdersLCP(@RequestParam(value="orgabbr") String orgabbr)
	{
		JSONObject jsonResponseObject = new JSONObject();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		List<LcpOrderItem> orderList = lcpOrderItemRepository.findByOrganizationOrderByOrderItemIdDesc(organization);
		JSONArray lcpOrderArray = new JSONArray();
		Iterator<LcpOrderItem> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			LcpOrderItem orderItem = iterator.next();
			JSONObject orderObject = new JSONObject();
			if(orderItem.getStatus().equals("delivered")){
				try {
					orderObject.put("orderid",orderItem.getOrderItemId());
					orderObject.put("timestamp", orderItem.getOrder().getPlaceDate());
					orderObject.put("username", orderItem.getOrder().getRegistration().getFirstname());
					try{
						orderObject.put("comment", orderItem.getOrder().getMessage());
					}
					catch(Exception e){
						orderObject.put("comment", "null");
					}
					orderObject.put("timestamp", orderItem.getOrder().getPlaceDate());
					orderObject.put("delivery", orderItem.getDelivery());
					orderObject.put("paid", orderItem.getIsPaid());
					JSONObject item = new JSONObject();
					JSONArray items = new JSONArray();
					try {
						item.put("productname", orderItem.getProduct().getName());
						item.put("quantity", Integer.toString(orderItem.getQuantity()));
						LcpProduct product = orderItem.getProduct();
						item.put("stockQuantity", Integer.toString(product.getQuantity()));
						item.put("rate", Float.toString(orderItem.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}				
					orderObject.put("items", items);
					lcpOrderArray.put(orderObject);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			jsonResponseObject.put("orders",lcpOrderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value = "/orders/cancelledLcp",method = RequestMethod.GET )
	public String displayCancelledOrdersLCP(@RequestParam(value="orgabbr") String orgabbr)
	{
		JSONObject jsonResponseObject = new JSONObject();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		List<LcpOrderItem> orderList = lcpOrderItemRepository.findByOrganizationOrderByOrderItemIdDesc(organization);
		JSONArray lcpOrderArray = new JSONArray();
		Iterator<LcpOrderItem> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			LcpOrderItem orderItem = iterator.next();
			JSONObject orderObject = new JSONObject();
			if(orderItem.getStatus().equals("cancelled")){
				try {
					orderObject.put("orderid",orderItem.getOrderItemId());
					orderObject.put("transId", "NA");
					orderObject.put("timestamp", orderItem.getOrder().getPlaceDate());
					orderObject.put("username", orderItem.getOrder().getRegistration().getFirstname());
					try{
						orderObject.put("comment", orderItem.getOrder().getMessage());
					}
					catch(Exception e){
						orderObject.put("comment", "null");
					}
					orderObject.put("timestamp", orderItem.getOrder().getPlaceDate());
					orderObject.put("delivery", "cancelled");
					orderObject.put("paid", orderItem.getOrder().getIsPaid());
					JSONObject item = new JSONObject();
					JSONArray items = new JSONArray();
					try {
						item.put("productname", orderItem.getProduct().getName());
						item.put("quantity", Integer.toString(orderItem.getQuantity()));
						LcpProduct product = orderItem.getProduct();
						item.put("stockQuantity", Integer.toString(product.getQuantity()));
						item.put("rate", Float.toString(orderItem.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}				
					orderObject.put("items", items);
					lcpOrderArray.put(orderObject);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			jsonResponseObject.put("orders",lcpOrderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}	
	
	
	@Transactional
	@RequestMapping(value = "/orders/processed",method = RequestMethod.GET )
	public String displayProcessedOrders(@RequestParam(value="orgabbr") String orgabbr)
	{
		
		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizationProcessedSorted(organization);
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		Iterator<Order> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			JSONObject orderObject = new JSONObject();
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			if(message != null){
			try {
				orderObject.put("orderid",order.getOrderId());
				orderObject.put("timestamp", message.getTime().toString());
				orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
				orderObject.put("comment", message.getComments());
				orderObject.put("delivery", order.getDelivery());
				JSONArray items = new JSONArray();
				List<OrderItem> orderItems = order.getOrderItems();
				Iterator<OrderItem> iter = orderItems.iterator();
				while(iter.hasNext()){
					OrderItem orderItem = iter.next();
					JSONObject item = new JSONObject();
					try {
						Product product = orderItem.getProduct();
						item.put("stockQuantity", Integer.toString(product.getQuantity()));
						item.put("productname", orderItem.getProduct().getName());
						item.put("quantity", Integer.toString(orderItem.getQuantity()));
						item.put("rate", Float.toString(orderItem.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					items.put(item);
				}
				orderObject.put("items", items);
				orderArray.put(orderObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
		}
		try {
			jsonResponseObject.put("orders",orderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/cancelled",method = RequestMethod.GET )
	public String displayCancelledOrders(@RequestParam(value="orgabbr") String orgabbr)
	{		
		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizationCancelledSorted(organization);
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		Iterator<Order> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			JSONObject orderObject = new JSONObject();
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			if(message != null){
			try {
				orderObject.put("orderid",order.getOrderId());
				orderObject.put("timestamp", message.getTime().toString());
				orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
				orderObject.put("comment", message.getComments());
				orderObject.put("delivery", "cancelled");
				orderObject.put("transId", order.getTrans_id());
				JSONArray items = new JSONArray();
				List<OrdersStore> ordersStoreList = ordersStoreService.getStoredItems(order);
				Iterator <OrdersStore> ordersStoreIter = ordersStoreList.iterator();
				List<OrderItem> orderItems = order.getOrderItems();
				Iterator<OrderItem> iter = orderItems.iterator();
				while (ordersStoreIter.hasNext()) {
					OrdersStore ordersStore =  ordersStoreIter.next();
					JSONObject item = new JSONObject();
					try {
						item.put("productname", ordersStore.getProductName());
						item.put("quantity", Integer.toString(ordersStore.getQuantity()));
						item.put("rate", Float.toString(ordersStore.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					items.put(item);
				}
				/*	while(iter.hasNext()){
					OrderItem orderItem = iter.next();
					JSONObject item = new JSONObject();
					try {
						item.put("productname", orderItem.getProduct().getName());
						item.put("quantity", Integer.toString(orderItem.getQuantity()));
						item.put("rate", Float.toString(orderItem.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					items.put(item);
				} */
				orderObject.put("items", items);
				orderArray.put(orderObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
		}
		try {
			jsonResponseObject.put("orders",orderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/delivered",method = RequestMethod.GET )
	public String returnDeliveredOrders(@RequestParam(value="orgabbr") String orgabbr)
	{		
		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizatonDeliveredSorted(organization);
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		Iterator<Order> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			JSONObject orderObject = new JSONObject();
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			if(message != null){
			try {
				orderObject.put("orderid",order.getOrderId());
				orderObject.put("timestamp", message.getTime().toString());
				orderObject.put("delivery", order.getDelivery());
				orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
				orderObject.put("paid", String.valueOf(order.getIsPaid()));

				JSONArray items = new JSONArray();
				List<OrdersStore> ordersStoreList = ordersStoreService.getStoredItems(order);
				Iterator <OrdersStore> ordersStoreIter = ordersStoreList.iterator();
				List<OrderItem> orderItems = order.getOrderItems();
				Iterator<OrderItem> iter = orderItems.iterator();
				while (ordersStoreIter.hasNext()) {
					OrdersStore ordersStore =  ordersStoreIter.next();
					JSONObject item = new JSONObject();
					try {
						item.put("productname", ordersStore.getProductName());
						item.put("quantity", Integer.toString(ordersStore.getQuantity()));
						item.put("rate", Float.toString(ordersStore.getUnitRate()));
						items.put(item);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					items.put(item);
					
					
				}
				orderObject.put("items", items);
				orderArray.put(orderObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
		}
		try {
			jsonResponseObject.put("orders",orderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	
	
	@Transactional
	@RequestMapping(value = "/orders/rejected",method = RequestMethod.GET )
	public String displayRejectedOrders(@RequestParam(value="orgabbr") String orgabbr)
	{
		
		Organization organization =organizationService.getOrganizationByAbbreviation(orgabbr);
		List<Order> orderList = orderService.getOrderByOrganizationRejectedSorted(organization);
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		Iterator<Order> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			JSONObject orderObject = new JSONObject();
			Order order = iterator.next();
			Message message = messageService.getMessageFromOrder(order);
			if(message != null){
			try {
				orderObject.put("orderid",order.getOrderId());
				orderObject.put("timestamp", message.getTime().toString());
				orderObject.put("username", userService.getUser(message.getUser().getUserId()).getName());
				orderObject.put("comment", message.getComments());
				orderArray.put(orderObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			}
		}
		try {
			jsonResponseObject.put("orders",orderArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/changestate/processed/{orderId}",method = RequestMethod.GET )
	public String changeToProcessedState(@PathVariable int orderId) {
		//System.out.println("Called processed");
		JSONObject responseJsonObject = new JSONObject();
		Order order = orderService.getOrder(orderId);
		if (order.getStatus().equals("processed")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already processed");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("delivered")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already delivered");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("cancelled")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already cancelled");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("deleted")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "Order has already been deleted");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		int id = order.getOrderId();
		order.setStatus("processed");
		Organization organization = order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		String phonenumber = order.getMessage().getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
		System.out.println("Phonenumber: "+phonenumber);
		try {
		orderService.processOrder(order);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try {
				return responseJsonObject.put("result", "failure").toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	try {
		BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
		if(order.getMessage().getUser().getEmailVerified()==1){
		SendBill.sendMail(order, organization, billLayoutSetting);
		System.out.println("mail sent");
		}
		else{
			System.out.println("mail not sent as it is not verified");
		}
		responseJsonObject.put("result", "success");
	} catch (JSONException e) {
		e.printStackTrace();
	}	
	List <String> androidTargets = getTargetDevices(organization);
	if(androidTargets.size() > 0) {
		GcmRequest gcmRequest = new GcmRequest();
		HashMap<String, Integer> dashData = null;
		try {
			dashData = dashBoardLocal(organizationabbr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		gcmRequest.broadcast(androidTargets,organizationabbr,dashData);		}
	
	List <String> androidConsumerTargets = getTargetConsumerDevices(phonenumber);
	if (androidConsumerTargets.size() > 0) {
		System.out.println("Broadcasts sent");
		GcmRequest gcmRequest = new GcmRequest();
		if(organizationabbr.equals("HHB"))
			gcmRequest.consumerBroadcastWithMessage("Order Processed", "Order "+order.getOrderId()+" has been processed and will be delivered tommorrow", androidConsumerTargets, "4");
		else
			gcmRequest.consumerBroadcast(androidConsumerTargets, String.valueOf(id));
	}
	return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/changestate/cancelled/{orderId}",method = RequestMethod.GET )
	public String changeToCancelledState(@PathVariable int orderId) {
		JSONObject responseJsonObject = new JSONObject();
		Order order = orderService.getOrder(orderId);
		order.setStatus("cancelled");
		Organization organization = order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		List <OrderItem> items = order.getOrderItems();
		Iterator <OrderItem> iterator = items.iterator();
		if(organization.getStockManagement()) {
		while(iterator.hasNext()) {
			OrderItem orderItem = iterator.next();
			Product product = orderItem.getProduct();
			int quantity = orderItem.getQuantity();
			int prodQty = product.getQuantity();
	    	int newQty = quantity + prodQty;
	    	product.setQuantity(newQty);
			productService.addProduct(product);
	    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
	    	if(prod != null){
				prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
				lcpProductService.addProduct(prod);
	    	}
		}
		}
		try {
		orderService.cancelOrder(order);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try {
				return responseJsonObject.put("result", "failure").toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	try {
		if(order.getMessage().getUser().getEmailVerified()==1)
		SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Cancelled", "<p>Dear User <br /><br />Your order with "+organization.getName()+" has been cancelled.\n\nWe are looking forward to your next order. </p>");
		responseJsonObject.put("result", "success");
	} catch (JSONException e) {
		e.printStackTrace();
	}	
	List <String> androidTargets = getTargetDevices(organization);
	if(androidTargets.size() > 0) {
		GcmRequest gcmRequest = new GcmRequest();
	
		HashMap<String, Integer> dashData = null;
		try {
			dashData = dashBoardLocal(organizationabbr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		gcmRequest.broadcast(androidTargets,organizationabbr,dashData);
		}
	return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/changestate/rejected/{orderId}",method = RequestMethod.GET )
	public String changeToRejectedState(@PathVariable int orderId) {
		JSONObject responseJsonObject = new JSONObject();
		Order order = orderService.getOrder(orderId);
		order.setStatus("rejected");
		Organization organization = order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		List <OrderItem> items = order.getOrderItems();
		Iterator <OrderItem> iterator = items.iterator();
		if(organization.getStockManagement()) {
		while(iterator.hasNext()) {
			OrderItem orderItem = iterator.next();
			Product product = orderItem.getProduct();
			int quantity = orderItem.getQuantity();
			int prodQty = product.getQuantity();
	    	int newQty = quantity + prodQty;
	    	product.setQuantity(newQty);
			productService.addProduct(product);
	    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
	    	if(prod != null){
				prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
				lcpProductService.addProduct(prod);
	    	}
		}
		}
		try {
		orderService.rejectOrder(order);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try {
				return responseJsonObject.put("result", "failure").toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	try {
		responseJsonObject.put("result", "success");
	} catch (JSONException e) {
		e.printStackTrace();
	}	
	
	List <String> androidTargets = getTargetDevices(organization);
	if(androidTargets.size() > 0) {
	GcmRequest gcmRequest = new GcmRequest();
	HashMap<String, Integer> dashData = null;
	try {
		dashData = dashBoardLocal(organizationabbr);
	} catch (ParseException e) {
		e.printStackTrace();
	}
	gcmRequest.broadcast(androidTargets,organizationabbr,dashData);
	}
	return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/orders/details/{orderId}",method = RequestMethod.GET )
	public String getOrderDetails(@PathVariable int orderId) {
		JSONObject responseJsonObject = new JSONObject();
		JSONArray items = new JSONArray();
		Order order = orderService.getOrder(orderId);
		List<OrderItem> orderItems = order.getOrderItems();
		Iterator<OrderItem> iterator = orderItems.iterator();
		while(iterator.hasNext()) {
			OrderItem orderItem = iterator.next(); 
			JSONObject item = new JSONObject();
			try {
				item.put("productname", orderItem.getProduct().getName());
				item.put("quantity", Float.toString(orderItem.getQuantity()));
				item.put("rate", Float.toString(orderItem.getUnitRate()));
				items.put(item);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("items", items);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/presetquantities",method = RequestMethod.GET )
	public String getPresetQuantity(@RequestParam (value="orgabbr") String orgabbr) {
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray array = new JSONArray();
		List <PresetQuantity> presetQty = presetQuantityService.getPresetQuantityList(organization);
		Iterator <PresetQuantity> iterator = presetQty.iterator();
		while(iterator.hasNext()) {
			JSONObject qty = new JSONObject();
			PresetQuantity presetQuantity = iterator.next();
			try {
				qty.put("producttype",presetQuantity.getProductType().getName());
				qty.put("qty", presetQuantity.getQuantity());
				array.put(qty);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			jsonResponseObject.put("presetquantity", array);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
		
	}
	
	//Not worrying about voice now.. Must fix later. Time is short. 
	@Transactional
	@RequestMapping(value = "/savedorders",method = RequestMethod.GET )
	public String getSavedOrders(@RequestParam (value = "abbr")String abbr, @RequestParam(value = "phonenumber") String phonenumber) {
		Organization organization = organizationService.getOrganizationByAbbreviation(abbr);
		User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();

		System.out.println("In here 1");
		JSONObject responseJsonObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		List <Message> messages = messageService.getMessagesForUser(user, "order", "binary");
		Iterator <Message> iterator = messages.iterator();
		while (iterator.hasNext()) {
			System.out.println("Found one");
			Message message = iterator.next();
			Order order = message.getOrder();
			if (order.getStatus().equals("saved") && (order.getOrganization() == organization)) {
				JSONObject object = new JSONObject();
				try {
					object.put("stockManagement",organization.getStockManagement());
					object.put("orderId", order.getOrderId());
					object.put("status",order.getStatus());
					
					DateFormat df = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS");
					df.setTimeZone(TimeZone.getTimeZone("IST"));
					String formattedDate8601 = df.format(message.getTime());
					//Cheap hack
					object.put("registeredTime", formattedDate8601+"+0000");

					List <OrderItem> items = order.getOrderItems();
					Iterator <OrderItem> orderIterator = items.iterator();
					JSONArray itemArray = new JSONArray();
					while (orderIterator.hasNext()) {
						JSONObject orderDetails = new JSONObject();
						OrderItem orderItem = orderIterator.next();
						orderDetails.put("productId", String.valueOf(orderItem.getProduct().getProductId()));
						orderDetails.put("quantity", String.valueOf(orderItem.getQuantity()));
						orderDetails.put("productname", orderItem.getProduct().getName());
						orderDetails.put("stockquantity",String.valueOf(orderItem.getProduct().getQuantity()));
						orderDetails.put("unitrate", String.valueOf(orderItem.getUnitRate()));
						itemArray.put(orderDetails);
					}
					object.put("orderItems", itemArray);
					System.out.println("Reached here");
					orderArray.put(object);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			responseJsonObject.put("orders", orderArray);
			System.out.println("Reached here 1");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/processedorders",method = RequestMethod.GET )
	public String getProcessedOrders(@RequestParam (value = "abbr")String abbr, @RequestParam(value = "phonenumber") String phonenumber) {
		Organization organization = organizationService.getOrganizationByAbbreviation(abbr);
		User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();

		System.out.println("In here 2");
		JSONObject responseJsonObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		List <Message> messages = messageService.getMessagesForUser(user, "order", "binary");
		Iterator <Message> iterator = messages.iterator();
		while (iterator.hasNext()) {
			System.out.println("Found one");
			Message message = iterator.next();
			Order order = message.getOrder();
			if (order.getStatus().equals("processed") && (order.getOrganization() == organization)) {
				JSONObject object = new JSONObject();
				try {
					object.put("stockManagement",organization.getStockManagement());
					object.put("orderId", order.getOrderId());
					object.put("status",order.getStatus());
					object.put("delivery", order.getDelivery());
					DateFormat df = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS");
					df.setTimeZone(TimeZone.getTimeZone("IST"));
					String formattedDate8601 = df.format(message.getTime());
					//Cheap hack - yeah
					object.put("registeredTime", formattedDate8601+"+0000");		
					List <OrderItem> items = order.getOrderItems();
					Iterator <OrderItem> orderIterator = items.iterator();
					JSONArray itemArray = new JSONArray();
					while (orderIterator.hasNext()) {
						System.out.println("Found one");
						JSONObject orderDetails = new JSONObject();
						OrderItem orderItem = orderIterator.next();
						orderDetails.put("productId", String.valueOf(orderItem.getProduct().getProductId()));
						orderDetails.put("quantity", String.valueOf(orderItem.getQuantity()));
						orderDetails.put("productname", orderItem.getProduct().getName());
						orderDetails.put("stockquantity",String.valueOf(orderItem.getProduct().getQuantity()));
						orderDetails.put("unitrate", String.valueOf(orderItem.getUnitRate()));
						itemArray.put(orderDetails);
					}
					object.put("orderItems", itemArray);
					orderArray.put(object);
					System.out.println("Reached here");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			responseJsonObject.put("orders", orderArray);
			System.out.println("Reached here 2");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	@RequestMapping(value = "/deliveredorders",method = RequestMethod.GET )
	public String getDeliveredOrders(@RequestParam (value = "abbr")String abbr, @RequestParam(value = "phonenumber") String phonenumber) {
		Organization organization = organizationService.getOrganizationByAbbreviation(abbr);
		User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();

		System.out.println("In here 3");
		JSONObject responseJsonObject = new JSONObject();
		JSONArray orderArray = new JSONArray();
		List <Message> messages = messageService.getMessagesForUser(user, "order", "binary");
		Iterator <Message> iterator = messages.iterator();
		while (iterator.hasNext()) {
			System.out.println("Found one");
			Message message = iterator.next();
			Order order = message.getOrder();
			if (order.getStatus().equals("delivered") && (order.getOrganization() == organization)) {
				JSONObject object = new JSONObject();
				try {
					object.put("stockManagement",organization.getStockManagement());
					object.put("orderId", order.getOrderId());
					object.put("status",order.getStatus());
					object.put("delivery", order.getDelivery());
					object.put("payment", String.valueOf(order.getIsPaid()));
					DateFormat df = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS");
					df.setTimeZone(TimeZone.getTimeZone("IST"));
					String formattedDate8601 = df.format(message.getTime());
					//Cheap hack
					object.put("registeredTime", formattedDate8601+"+0000");
					JSONArray items = new JSONArray();
					List<OrdersStore> ordersStoreList = ordersStoreService.getStoredItems(order);
					Iterator <OrdersStore> ordersStoreIter = ordersStoreList.iterator();
					List<OrderItem> orderItems = order.getOrderItems();
					Iterator<OrderItem> iter = orderItems.iterator();
					while (ordersStoreIter.hasNext()) {
						OrdersStore ordersStore =  ordersStoreIter.next();
						JSONObject item = new JSONObject();
						try {
							item.put("productname", ordersStore.getProductName());
							item.put("quantity", Integer.toString(ordersStore.getQuantity()));
							item.put("unitrate", Float.toString(ordersStore.getUnitRate()));
							items.put(item);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						//items.put(item);	
					}
					object.put("orderItems", items);
					System.out.println("Reached here");
					orderArray.put(object);
				}
			catch(JSONException e) {
				e.printStackTrace();
			}
			}
		}
			try {
				responseJsonObject.put("orders", orderArray);
				System.out.println("Reached here 3");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		return responseJsonObject.toString();
	
	}
	
	@Transactional
	@RequestMapping(value = "/order/{org}/{orderId}/delete",method = RequestMethod.GET )
	public String delOrderItem(@PathVariable int orderId, @PathVariable String org)
	{
		Boolean stock = organizationService.getOrganizationByAbbreviation(org).getStockManagement();
		JSONObject response = new JSONObject();
		//List<OrderItem> orderitem= orderItemRepository.findByOrder_OrderId(orderId);
		//Order copyOrder;
		Order order = orderService.getOrder(orderId);
		/*Message mes = messageService.getMessageFromOrder(order);
		messageService.removeMessage(mes);
		//orderService.removeOrder(order);
		//orderRepository.delete(order);
		int count=0;
		while(count<orderitem.size())
		{
		orderItemRepository.delete(orderitem.get(count++));
		}
		//List<Order> orderList = orderService.getOrderByOrganizationSavedSorted(organization);
		try {
			jsonResponseObject.put("Status", "Success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	
		//New
		

		if (order.getStatus().equals("processed")) {
			try {	
			response.put("status", "error");
				response.put("error", "Order has already been processed by you");
				return response.toString();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("delivered")) {
			try {	
			response.put("status", "error");
				response.put("error", "Order has already been delivered to the users address");
				return response.toString();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("deleted")) {
			try {	
			response.put("status", "error");
				response.put("error", "Order has already been deleted by you");
				return response.toString();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("cancelled")) {
			try {	
			response.put("status", "error");
				response.put("error", "Order has already been deleted by the User");
				return response.toString();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			response.put("status", "Success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//TODO - Check whether this is acceptable or not.
		order.setStatus("cancelled");
		Message message = order.getMessage();
		//message.setComments(comments);
		messageService.addMessage(message);
		String orderString = "";
		
		List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
		Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
		float netAmount=0;
		orderString ="<ol>";
		while (itemsIterator.hasNext()) {
			OrderItem orderItem = itemsIterator.next();
			OrdersStore ordersStore = new OrdersStore();
			ordersStore.setOrder(order);
			ordersStore.setProductName(orderItem.getProduct().getName());
			ordersStore.setQuantity(orderItem.getQuantity());
			ordersStore.setUnitRate(orderItem.getUnitRate());
			ordersStoreService.addOrdersStore(ordersStore);
			orderString += "<li>";
			orderString +=orderItem.getProduct().getName()+" --- "+String.valueOf(orderItem.getQuantity())+" units\n";
			orderString += "</li>";
			netAmount += (orderItem.getQuantity() * orderItem.getUnitRate());
		}
		orderString += "</ol>";
		List<OrderItem> orderItems = orderService.getOrder(orderId).getOrderItems();
		Iterator <OrderItem> iterator = orderItems.iterator();
		if(stock){
			while (iterator.hasNext()) {
				OrderItem orderItem = iterator.next();
				Product product = orderItem.getProduct();
		    	int newQty = product.getQuantity() + orderItem.getQuantity();
		    	product.setQuantity(newQty);
				productService.addProduct(product);
		    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
		    	if(prod != null){
					prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
					lcpProductService.addProduct(prod);
		    	}
				System.out.println("Updating product on deletion - "+product.getName());
				orderItem.setProduct(null);
			}
		}
		else{
			while (iterator.hasNext()) {
				OrderItem orderItem = iterator.next();
				orderItem.setProduct(null);
			}
		}
		List <String> androidConsumerTargets = getTargetConsumerDevices(userPhoneNumberService.getUserPrimaryPhoneNumber(order.getMessage().getUser()).getPhoneNumber());
		if (androidConsumerTargets.size() > 0) {
			System.out.println("Broadcasts sent");
			GcmRequest gcmRequest = new GcmRequest();
			gcmRequest.consumerBroadcastWithMessage("Order Deletion", "Order ID: "+ orderId +" has been deleted by the admin" ,androidConsumerTargets, "10");
				}
		
		if(order.getMessage().getUser().getEmailVerified()==1)
		SendMail.sendMail(order.getMessage().getUser().getEmail(), "Lokacart: Order Cancellation Acknowledgement", "<p>Dear User<br /><br />Your order with order ID "+order.getOrderId()+" has been deleted by "+ organizationService.getOrganizationByAbbreviation(org).getName() +".<br />The items ordered were: </p><p style='text-align:center'>"+orderString+"</p><p><br />The total amount is: Rs."+String.format("%.2f", netAmount)+". <br /><br />We hope to serve you again!</p>");
		orderRepository.save(order);
		
		return response.toString();
		
		//return jsonResponseObject.toString();
	
	}
	
	
	@Transactional
	@RequestMapping(value = "/orders/deletePG",method = RequestMethod.POST )
	public @ResponseBody String deleteOrderItemPG(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		//PaytmData paytmData = null;
		int orderId = 0;
		String org = null;
		try{
			jsonObject = new JSONObject(requestBody);
			org = jsonObject.getString("org");
			orderId = jsonObject.getInt("orderId");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		Boolean stock = organizationService.getOrganizationByAbbreviation(org).getStockManagement();
		JSONObject response = new JSONObject();
		Order order = orderService.getOrder(orderId);
		try {
			response.put("status", "Success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		order.setStatus("deleted");
		Message message = order.getMessage();
		//message.setComments(comments);
		messageService.addMessage(message);
		
		List <OrderItem> orderItemsNew = orderService.getOrder(orderId).getOrderItems();
		Iterator <OrderItem> itemsIterator = orderItemsNew.iterator();
		float netAmount = 0;
		while (itemsIterator.hasNext()) {
			OrderItem orderItem = itemsIterator.next();
			OrdersStore ordersStore = new OrdersStore();
			ordersStore.setOrder(order);
			ordersStore.setProductName(orderItem.getProduct().getName());
			ordersStore.setQuantity(orderItem.getQuantity());
			ordersStore.setUnitRate(orderItem.getUnitRate());
			ordersStoreService.addOrdersStore(ordersStore);
			netAmount += (orderItem.getQuantity() * orderItem.getUnitRate());
		}
		List<OrderItem> orderItems = orderService.getOrder(orderId).getOrderItems();
		Iterator <OrderItem> iterator = orderItems.iterator();
		if(stock){
			while (iterator.hasNext()) {
				OrderItem orderItem = iterator.next();
				Product product = orderItem.getProduct();
		    	int newQty = product.getQuantity() + orderItem.getQuantity();
		    	product.setQuantity(newQty);
				productService.addProduct(product);
		    	LcpProduct prod = lcpProductService.getProductById(product.getProductId());
		    	if(prod != null){
					prod.setQuantity((int)(newQty*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())*product.getUnitQty())));
					lcpProductService.addProduct(prod);
		    	}
				orderItem.setProduct(null);
			}
		}
		else{
			while (iterator.hasNext()) {
				OrderItem orderItem = iterator.next();
				orderItem.setProduct(null);
			}
		}
		//paytmData = paytmDatarepository.findByOrganization(organizationService.getOrganizationByAbbreviation(org));
		//paytmData.setDeficit(paytmData.getDeficit() - netAmount);
		orderRepository.save(order);
		return response.toString();
	}
	
	
	// Not used
	@Transactional
	@RequestMapping(value = "/order/delete",method = RequestMethod.GET )
	public String deleteOrderItem(@RequestParam(value="orderId") int orderId)
	{
		JSONObject jsonResponseObject = new JSONObject();
		List<OrderItem> orderitem= orderItemRepository.findByOrder_OrderId(orderId);
		Order copyOrder;
		Order order = orderService.getOrder(orderId);
		Message mes = messageService.getMessageFromOrder(order);
		messageService.removeMessage(mes);
		//orderService.removeOrder(order);
		//orderRepository.delete(order);
		int count=0;
		while(count<orderitem.size())
		{
		orderItemRepository.delete(orderitem.get(count++));
		}
		//List<Order> orderList = orderService.getOrderByOrganizationSavedSorted(organization);
		try {
			jsonResponseObject.put("Status", "Success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/order/deletemsg",method = RequestMethod.GET )
	public String deleteOrder(@RequestParam(value="orderId") int orderId)
	{
		JSONObject jsonResponseObject = new JSONObject();
		
		Order order = orderService.getOrder(orderId);
		
		orderService.removeOrder(order);
		
		return jsonResponseObject.toString();
	}
	
	
	
	//delivery dates add
	@Transactional
	@RequestMapping(value = "/deliverydate",method = RequestMethod.POST )
	public @ResponseBody String delivereddate(@RequestBody String requestBody) {

		System.out.println("Called processed");
		JSONObject responseJsonObject = new JSONObject();
		//JSONObject response= new JSONObject();
		String date= null;
		JSONObject jsonObject = null;
		Order order=null;
		int orderid=0;
		try {
			jsonObject = new JSONObject(requestBody);
			date= jsonObject.getString("date");
			orderid= Integer.parseInt(jsonObject.getString("id"));
		} catch (JSONException e) {
			//Uncaught but not untamed :-)			
			//return "Error";
		}
		order=orderService.getOrder(orderid);
		//Order order = orderService.getOrder(orderId);
		if (order.getStatus().equals("processed")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already processed");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("delivered")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already delivered");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("cancelled")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already cancelled");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (order.getStatus().equals("deleted")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "Order has already been deleted");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		int id = order.getOrderId();
		order.setStatus("processed");
		Organization organization = order.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		String phonenumber = order.getMessage().getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
		System.out.println("Phonenumber: "+phonenumber);
		
		order.setDelivery(date);
	
		
		try {
		orderService.processOrder(order);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try {
				return responseJsonObject.put("result", "failure").toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	
		BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
		SendBill.sendMail(order, organization, billLayoutSetting);
		System.out.println("mail sent");
		//responseJsonObject.put("result", "success");
		List <String> androidTargets = getTargetDevices(organization);
		if(androidTargets.size() > 0) {
			GcmRequest gcmRequest = new GcmRequest();
			HashMap<String, Integer> dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(androidTargets,organizationabbr,dashData);		}
		
		List <String> androidConsumerTargets = getTargetConsumerDevices(phonenumber);
		if (androidConsumerTargets.size() > 0) {
			System.out.println("Broadcasts sent");
			GcmRequest gcmRequest = new GcmRequest();
			if(organizationabbr.equals("HHB"))
				gcmRequest.consumerBroadcastWithMessage("Order Processed", "Order "+order.getOrderId()+" has been processed and will be delivered tommorrow", androidConsumerTargets, "4");
			else
				gcmRequest.consumerBroadcast(androidConsumerTargets, String.valueOf(id));
		}
		try {
			responseJsonObject.put("result", "success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	
	}
	

	
	
	//delivery dates add
	@Transactional
	@RequestMapping(value = "/deliverydateLcp",method = RequestMethod.POST )
	public @ResponseBody String delivereddatelcp(@RequestBody String requestBody) {

		System.out.println("Called processed");
		JSONObject responseJsonObject = new JSONObject();
		//JSONObject response= new JSONObject();
		String date= null;
		JSONObject jsonObject = null;
		LcpOrderItem orderItem = null;
		int orderid=0;
		try {
			jsonObject = new JSONObject(requestBody);
			date= jsonObject.getString("date");
			orderid= Integer.parseInt(jsonObject.getString("id"));
		} catch (JSONException e) {
			//Uncaught but not untamed :-)			
			//return "Error";
		}
		orderItem = lcpOrderItemRepository.getOne(orderid);
		//Order order = orderService.getOrder(orderId);
		if (orderItem.getStatus().equals("processed")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already processed");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (orderItem.getStatus().equals("delivered")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already delivered");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (orderItem.getStatus().equals("cancelled")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "order already cancelled");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else if (orderItem.getStatus().equals("deleted")) {
			try {
			responseJsonObject.put("result", "failure");
			responseJsonObject.put("error", "Order has already been deleted");
			return responseJsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		//int id = orderItem.getOrderId();
		Organization organization = orderItem.getOrganization();
		String organizationabbr = organization.getAbbreviation();
		/*String phonenumber = orderItem.getMessage().getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
		System.out.println("Phonenumber: "+phonenumber);
		*/
		try {

			orderItem.setStatus("processed");
			orderItem.setDelivery(date);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try {
				return responseJsonObject.put("result", "failure").toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	
		/*BillLayoutSettings billLayoutSetting = billLayoutSettingsRepository.findByOrganization(organization);
		SendBill.sendMail(orderItem, organization, billLayoutSetting);
		System.out.println("mail sent");
		//responseJsonObject.put("result", "success");
		List <String> androidTargets = getTargetDevices(organization);
		if(androidTargets.size() > 0) {
			GcmRequest gcmRequest = new GcmRequest();
			HashMap<String, Integer> dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(androidTargets,organizationabbr,dashData);		}
		
		List <String> androidConsumerTargets = getTargetConsumerDevices(phonenumber);
		if (androidConsumerTargets.size() > 0) {
			System.out.println("Broadcasts sent");
			GcmRequest gcmRequest = new GcmRequest();
			if(organizationabbr.equals("HHB"))
				gcmRequest.consumerBroadcastWithMessage("Order Processed", "Order "+orderItem.getOrderId()+" has been processed and will be delivered tommorrow", androidConsumerTargets, "4");
			else
				gcmRequest.consumerBroadcast(androidConsumerTargets, String.valueOf(id));
		}*/
		try {
			responseJsonObject.put("result", "success");
			FcmRequest.send_FCM_Broadcast(getTargetConsumerDevicesLcp(orderItem.getOrder().getRegistration().getPhone()), "Order Processed", "Order ID "+orderItem.getOrderItemId()+" has been processed by the admin", 2);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	
	}
		
	
	
	//Helper to resolve quantity issue.
	@Transactional
	@RequestMapping(value = "/fixqty",method = RequestMethod.GET )
	public @ResponseBody String fixQuantity() {
		List<OrderItem> items = orderItemRepository.findAll();
		Iterator <OrderItem> iterator = items.iterator();
		JSONObject responseJsonObject = new JSONObject();
		while (iterator.hasNext()) {
			OrderItem orderItem = iterator.next();
		//	orderItemRepository.delete(orderItem);
			int qty =(int) Math.ceil(orderItem.getQuantity());
			orderItem.setQuantity(qty);
			System.out.println(orderItem.getQuantity());
			orderItemRepository.save(orderItem);
		}
		try {
			responseJsonObject.put("response", "success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
}