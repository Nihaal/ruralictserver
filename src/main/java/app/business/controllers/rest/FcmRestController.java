package app.business.controllers.rest;

import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.FcmTokensService;
import app.entities.FcmTokens;
import app.entities.GcmTokens;

@RestController
@RequestMapping("/app")
public class FcmRestController{
	
	@Autowired
	FcmTokensService fcmTokenService;
	
	@RequestMapping(value = "/fcmregistration", method=RequestMethod.POST)
	public String registerToken(@RequestBody String requestBody) throws JSONException{
		JSONObject jsonResponseObject = new JSONObject(requestBody);
		JSONObject tokenDetails = null;
		String token = null, number = null;
		try{
			tokenDetails = new JSONObject(requestBody);
			token = tokenDetails.getString("token");
			number = tokenDetails.getString("number");
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		
		try{
			List<FcmTokens> tokens = fcmTokenService.getListByPhoneNumber(number);
			//FcmTokens tokenCheck = fcmTokenService.getByToken(token);
			Iterator<FcmTokens> iter = tokens.iterator();
			while(iter.hasNext()){
				fcmTokenService.removeToken(iter.next());
			}
		}
		catch(Exception e){
			System.out.println("new token");
		}
		FcmTokens fcmToken = new FcmTokens(token, number);
		try{
			fcmTokenService.addToken(fcmToken);
		}
		catch(Exception e){
			try{
				jsonResponseObject.put("response", "fail");
			}
			catch(JSONException e1){
				e1.printStackTrace();
			}
		}
		try{
			jsonResponseObject.put("response", "success");
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@RequestMapping(value="/fcmderegistertoken", method=RequestMethod.POST)
	public String deregesterToken(@RequestBody String requestBody){
		JSONObject jsonResponseObject = new JSONObject();
		JSONObject tokenDetails = null;
		String token;
		System.out.println("deregester called");
		try{
			tokenDetails = new JSONObject(requestBody);
			token = tokenDetails.getString("token");
			System.out.println(token);
			FcmTokens fcmToken = fcmTokenService.getByToken(token);
			fcmTokenService.removeToken(fcmToken);
		
		}
		catch(Exception e) {
			System.out.println("Token remove failed");
			try {
				jsonResponseObject.put("response", "fail");
				return jsonResponseObject.toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		try {
			jsonResponseObject.put("response", "success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@RequestMapping(value="/fcmderegistermanual", method=RequestMethod.POST)
	public String deregisterToken(@RequestBody String requestBody){
		JSONObject jsonResponseObject = new JSONObject();
		JSONObject tokenDetails = null;
		String number;
		System.out.println("deregester called");
		try{
			tokenDetails = new JSONObject(requestBody);
			number = tokenDetails.getString("number");
			System.out.println(number);
			List<FcmTokens> tokens = fcmTokenService.getListByPhoneNumber(number);
			Iterator<FcmTokens> iter = tokens.iterator();
			while(iter.hasNext()){
				fcmTokenService.removeToken(iter.next());
			}
		
		}
		catch(Exception e) {
			System.out.println("Token remove failed");
			try {
				jsonResponseObject.put("response", "fail");
				return jsonResponseObject.toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		try {
			jsonResponseObject.put("response", "success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
}



