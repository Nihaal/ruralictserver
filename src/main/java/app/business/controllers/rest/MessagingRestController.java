package app.business.controllers.rest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.services.GcmTokensService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserPhoneNumberService;
import app.data.repositories.FcmTokensRepository;
import app.data.repositories.LcpOrderItemRepository;
import app.entities.FcmTokens;
import app.entities.GcmTokens;
import app.entities.LcpOrderItem;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.SmsApiKeys;
import app.util.FcmRequest;
import app.util.GcmRequest;

@RestController
@RequestMapping("/api")
public class MessagingRestController {

	@Autowired 
	UserPhoneNumberService userPhoneNumberService;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Autowired
	SmsApiKeysService smsApiKeysService;

	@Autowired
	LcpOrderItemRepository lcpOrderItemRepository;

	@Autowired
	GcmTokensService gcmTokensService;

	@Autowired
	FcmTokensRepository fcmTokensRepository;
	
	@Autowired
	OrganizationService organizationService;
	
	public List <String> getTargetConsumerDevices(Organization organization) {
		List <String>androidTargets = new ArrayList<String>();
		try{
			List <OrganizationMembership> membershipList = organizationMembershipService.getOrganizationMembershipList(organization);
			Iterator<OrganizationMembership> membershipIter = membershipList.iterator();
			while (membershipIter.hasNext()) {
				OrganizationMembership organizationMembership = membershipIter.next();
				String phonenumber = organizationMembership.getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
				List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(phonenumber);
				Iterator <GcmTokens> iter = gcmTokens.iterator();
				while(iter.hasNext()) {
					androidTargets.add(iter.next().getToken());
				}	
			}
		}
		catch(Exception e) {
			System.out.println("error in token retreival");
		}
		return androidTargets;
	}
	
	
	public List <String> getTargetConsumerDevicesLcp() {
		List <String>androidTargets = new ArrayList<String>();
		try{
			List<FcmTokens> fcmTokens = fcmTokensRepository.findAll();
			Iterator <FcmTokens> iter = fcmTokens.iterator();
			while(iter.hasNext()) {
				androidTargets.add(iter.next().getToken());
			}
			}
			catch(Exception e){
				System.out.println("error in token retreival");
			}
		return androidTargets;
	}
	
	
	
	public List <String> getTargetConsumerDevicesSMS(Organization organization) {
		List <String>androidTargets = new ArrayList<String>();
		try{
			List <OrganizationMembership> membershipList = organizationMembershipService.getOrganizationMembershipList(organization);
			Iterator<OrganizationMembership> membershipIter = membershipList.iterator();
			while (membershipIter.hasNext()) {
				OrganizationMembership organizationMembership = membershipIter.next();
				if(organizationMembership.getIsAdmin() == false){
					String phonenumber = organizationMembership.getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
					androidTargets.add(phonenumber);
				}	
			}
		}
		catch(Exception e) {
			System.out.println("error in token retreival");
		}
		return androidTargets;
	}
	
	
	public List <String> getTargetConsumerDevicesSMSLcp(Organization organization) {
		List <String>androidTargets = new ArrayList<String>();
		try{
			List<LcpOrderItem> orderItems = lcpOrderItemRepository.findByOrganization(organization);
			Iterator<LcpOrderItem> iter = orderItems.iterator();
			HashSet<String> phone = new HashSet<String>();
			while (iter.hasNext()) {
				LcpOrderItem orderItem = iter.next();
				String phonenumber = orderItem.getOrder().getRegistration().getPhone();
				phone.add("91"+phonenumber);					
			}
			Iterator<String> it = phone.iterator();
			while(it.hasNext()){
				androidTargets.add(it.next());
			}
		}
		catch(Exception e) {
			System.out.println("error in token retreival");
		}
		return androidTargets;
	}
	
	
	public List <String> getTargetConsumerDevicesSMSBoth(Organization organization) {
		List <String>androidTargets = new ArrayList<String>();
		try{
			List<LcpOrderItem> orderItems = lcpOrderItemRepository.findByOrganization(organization);
			Iterator<LcpOrderItem> iter = orderItems.iterator();
			HashSet<String> phone = new HashSet<String>();
			while (iter.hasNext()) {
				LcpOrderItem orderItem = iter.next();
				String phonenumber = orderItem.getOrder().getRegistration().getPhone();
				phone.add("91"+phonenumber);					
			}
			List <OrganizationMembership> membershipList = organizationMembershipService.getOrganizationMembershipList(organization);
			Iterator<OrganizationMembership> membershipIter = membershipList.iterator();
			while (membershipIter.hasNext()) {
				OrganizationMembership organizationMembership = membershipIter.next();
				if(organizationMembership.getIsAdmin() == false){
					String phonenumber = organizationMembership.getUser().getUserPhoneNumbers().get(0).getPhoneNumber();
					phone.add(phonenumber);
				}
			}
			Iterator<String> it = phone.iterator();
			while(it.hasNext()){
				androidTargets.add(it.next());
			}
		}
		catch(Exception e) {
			System.out.println("error in token retreival");
		}
		return androidTargets;
	}
	
	
	@Transactional
	@RequestMapping(value = "/sendbroadcast",method = RequestMethod.POST)
	public @ResponseBody String sendPromotionalBroadcast(@RequestBody String requestBody) {
		JSONObject jsonObject = null;;
		JSONObject responseJsonObject = new JSONObject();
		Organization organization =  null;
		String message = null;
		int code = 0;
		try {
			jsonObject = new JSONObject(requestBody);
			String orgabbr = jsonObject.getString("orgabbr");
			message = jsonObject.getString("message");
			code = jsonObject.getInt("code");
			organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		}
		catch(JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "JSON format error");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		if(code == 0){
			List <String> androidTargets = getTargetConsumerDevices(organization);
			if (androidTargets.size() > 0) {
				GcmRequest gcmRequest = new GcmRequest();
				System.out.println("Message: "+message);
				gcmRequest.consumerPromoBroadcast(message,organization.getName(), androidTargets, "5");
			}
		}
		else if(code == 1){
			List<String> androidTarget = getTargetConsumerDevicesLcp();
			if (androidTarget.size() > 0) {
				System.out.println("Message: "+message);
				FcmRequest.send_FCM_Broadcast_Topic(androidTarget, "Notification from "+organization.getName() , message);
			}
		}
		else if(code == 2){
			List <String> androidTargets = getTargetConsumerDevices(organization);
			if (androidTargets.size() > 0) {
				GcmRequest gcmRequest = new GcmRequest();
				System.out.println("Message: "+message);
				gcmRequest.consumerPromoBroadcast(message,organization.getName(), androidTargets, "5");
			}
			List<String> androidTarget = getTargetConsumerDevicesLcp();
			if (androidTarget.size() > 0) {
				System.out.println("Message: "+message);
				FcmRequest.send_FCM_Broadcast_Topic(androidTarget, "Notification from "+organization.getName() , message);
			}
		}
		try {
			responseJsonObject.put("response", "Message successfully broadcasted");
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/sendbroadcastlcp",method = RequestMethod.POST)
	public @ResponseBody String sendPromotionalBroadcastLcp(@RequestBody String requestBody) {
		JSONObject jsonObject = null;;
		JSONObject responseJsonObject = new JSONObject();
		Organization organization =  null;
		String message = null;
		try {
			jsonObject = new JSONObject(requestBody);
			String orgabbr = jsonObject.getString("orgabbr");
			message = jsonObject.getString("message");
			organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		}
		catch(JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "JSON format error");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		List<String> androidTargets = getTargetConsumerDevicesLcp();
		if (androidTargets.size() > 0) {
			System.out.println("Message: "+message);
			FcmRequest.send_FCM_Broadcast_Multiple(androidTargets, "Notification from "+organization.getName() , message, 4);
		}
		try {
			responseJsonObject.put("response", "Message successfully broadcasted");
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value = "/sendsmsbroadcast",method = RequestMethod.POST)
	public @ResponseBody String sendSMSBroadcast(@RequestBody String requestBody) {
		JSONObject jsonObject = null;;
		JSONObject responseJsonObject = new JSONObject();
		Organization organization =  null;
		int code = 0;
		String message = null;
		try {
			jsonObject = new JSONObject(requestBody);
			String orgabbr = jsonObject.getString("orgabbr");
			message = jsonObject.getString("message");
			code = jsonObject.getInt("code");
			organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		}
		catch(JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "JSON format error");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		List <String>androidTargets = null;
		if(code == 0)
			androidTargets = getTargetConsumerDevicesSMS(organization);
		else if(code == 1)
			androidTargets = getTargetConsumerDevicesSMSLcp(organization);
		else if(code == 2)
			androidTargets = getTargetConsumerDevicesSMSBoth(organization);
		System.out.println(""+androidTargets.size());
		
		SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(organization);
		String authId = smsApiKeys.getAuthId();
		String authToken = smsApiKeys.getAuthToken();
		String sourceNumber = smsApiKeys.getSourceNumber();
		RestAPI api = new RestAPI(authId, authToken, "v1");
		String numbersFormat="";
		int i=0;
		while(i<androidTargets.size()){
			numbersFormat=numbersFormat+androidTargets.get(i++).toString()+"<";
		}
		
		numbersFormat=numbersFormat+"919941375176";
		System.out.println(numbersFormat);
		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("src", sourceNumber);
		parameters.put("dst",numbersFormat );
		parameters.put("text",message+" from "+organization.getAbbreviation().toString());
		parameters.put("method"	, "GET");
		
		try {
			MessageResponse msgResponse = api.sendMessage(parameters);
			System.out.println(msgResponse);
			System.out.println("Api ID : " + msgResponse.apiId);
			System.out.println("Message : " + msgResponse.message);
			if (msgResponse.serverCode == 202) {
				//System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

			} else {
				System.out.println(msgResponse.error);
			}
		} catch (PlivoException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		
		
		
		
		
		try {
			responseJsonObject.put("response", "Message successfully broadcasted");
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
}
