package app.business.controllers.rest;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.LcpProductService;
import app.business.services.OrderSummaryService;
import app.business.services.OrderSummaryService.OrderSummary;
import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.data.repositories.LcpProductRepository;
import app.data.repositories.LcpProductTypeRepository;
import app.data.repositories.SiUnitRepository;
import app.entities.LcpProduct;
import app.entities.LcpProductType;
import app.entities.Organization;
import app.entities.Product;
import app.entities.ProductType;
import app.util.SpreadsheetParser;
import app.util.Utils;


@RestController
@RequestMapping("/api")

public class AddProductRestController {

	
	@Autowired
	SiUnitRepository siUnitRepository;
	
	@Autowired 
	OrganizationService organizationService;

	@Autowired
	LcpProductTypeRepository lcpProductTypeRepository;
	
	@Autowired
	LcpProductRepository lcpProductRepository;
	
	@Autowired
	LcpProductService lcpProductService;
	
	@Autowired 
	OrderSummaryService orderSummaryService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	ProductTypeService productTypeService;
	
	
	@RequestMapping(value ="/product/add", method = RequestMethod.POST )
	public String addProduct(@RequestBody String requestBody){
		
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String organizationabbr = null, description = null, name = null;
		Organization organization; List<Product> id= null;
		Product product = new Product();
		try {
			jsonObject = new JSONObject(requestBody);
			organizationabbr=jsonObject.getString("orgabbr");
			jsonObject.getString("name");
			product.setName(jsonObject.getString("name"));
			organization = organizationService.getOrganizationByAbbreviation(organizationabbr);
			ProductType productType = productTypeService.getProductTypeByNameAndOrg(jsonObject.getString("productType"), organization);
			product.setUnitRate(Float.parseFloat(jsonObject.getString("rate")));
			product.setProductType(productType);
			product.setStatus(productType.getStatus());
			//perUQty
			product.setQuantity(Integer.parseInt(jsonObject.getString("qty")));
			try{
				description = jsonObject.getString("description");
				product.setDescription(description);
			}
			catch(Exception e) {
				//nothing to do here
			}
		}
		catch(JSONException e)
		{
			e.printStackTrace();
		}
		try{
			productService.addProduct(product);
		}
		catch(Exception e){
			try {
				responseJsonObject.put("upload", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		try {
			
			responseJsonObject.put("upload", "success");
			 id = productService.getProductByName(jsonObject.getString("name"));
			 //You know, what you did! -JJ
			 
			responseJsonObject.put("id", Integer.toString(id.get(0).getProductId()));
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	@RequestMapping(value ="/product/addnew", method = RequestMethod.POST )
	public String addProductNew(@RequestBody String requestBody){
		
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String organizationabbr = null, description = null, name = null;
		Organization organization = null; 
		Product prod = null;
		Product product = new Product();
		String si_unit = null;
		String fullName = null;
		int last_id = productService.getAllProductListSortedByIdDesc().get(0).getProductId();
		int last_id_lcp = lcpProductRepository.findAllByOrderByProductIdDesc().get(0).getProductId();
		int id = Math.max(last_id, last_id_lcp)+1;
		try {
			jsonObject = new JSONObject(requestBody);
			si_unit = jsonObject.getString("siUnit");
			organizationabbr = jsonObject.getString("orgabbr");
			product.setProductId(id);
			product.setJustName(jsonObject.getString("name"));
			product.setSiUnit(si_unit);
			product.setUnitQty(jsonObject.getInt("uQty"));
			fullName = jsonObject.getInt("uQty")+" "+jsonObject.getString("siUnit")+" of "+jsonObject.getString("name");
			product.setName(fullName);
			if(productService.getProductByNameAndOrg(fullName, organizationService.getOrganizationByAbbreviation(organizationabbr)) != null){
				responseJsonObject.put("upload", "exists");
				return responseJsonObject.toString();
			}
			organization = organizationService.getOrganizationByAbbreviation(organizationabbr);
			ProductType productType = productTypeService.getProductTypeByNameAndOrg(jsonObject.getString("productType"), organization);
			product.setUnitRate(Float.parseFloat(jsonObject.getString("rate")));
			product.setProductType(productType);
			product.setStatus(productType.getStatus());
			//perUQty
			product.setQuantity(Integer.parseInt(jsonObject.getString("qty")));
			try{
				description = jsonObject.getString("description");
				product.setDescription(description);
			}
			catch(Exception e) {
				//nothing to do here
			}
		}
		catch(JSONException e)
		{
			e.printStackTrace();
		}
		try{
			productService.addProduct(product);
		}
		catch(Exception e){
			try {
				responseJsonObject.put("upload", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		try {
			
			responseJsonObject.put("upload", "success");
			try{
				System.out.println(product.getProductId());
			}
			catch(Exception e){
				System.out.println("Exception getting product by Id");
			}
			prod = productService.getProductByNameAndOrg(fullName, organization);
			//id = productService.getProductByName(jsonObject.getString("name"));
			//You know, what you did! -JJ
			responseJsonObject.put("si_group_id", siUnitRepository.findBySiUnit(si_unit).getLcGroup());
			responseJsonObject.put("id", Integer.toString(prod.getProductId()));
			//responseJsonObject.put("id", Integer.toString(id.get(0).getProductId()));
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value ="/product/edit", method = RequestMethod.POST)
	public String editProduct(@RequestBody String requestBody) {		
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String organizationabbr = null;
		String productName = null, newName = null, newDesc = null;
		Product product = null;
		int id=0;
		Organization organization = null;
		int newQuantity = 0;
		float newRate = 0;
		try {
			jsonObject = new JSONObject(requestBody);
			organizationabbr = jsonObject.getString("orgabbr");
			organization = organizationService.getOrganizationByAbbreviation(organizationabbr);
			productName = jsonObject.getString("name");
			newName = jsonObject.getString("newname");
			newQuantity = Integer.parseInt(jsonObject.getString("qty"));
			newRate = Float.parseFloat(jsonObject.getString("rate"));
			try{
				newDesc = jsonObject.getString("newdesc");
			}
			catch(JSONException e) {
				e.printStackTrace();
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		product = productService.getProductByNameAndOrg(productName, organization);
		product.setName(newName);
		product.setQuantity(newQuantity);
		product.setUnitRate(newRate);
		if(newDesc != null) {
			product.setDescription(newDesc);
		}
		try{
			productService.addProduct(product);
		}
		catch(Exception e) {
			try {
				responseJsonObject.put("edit", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			return responseJsonObject.toString();
		}
		try {
			responseJsonObject.put("edit","success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value ="/product/editNew", method = RequestMethod.POST)
	
	public String newEditProductAPI(@RequestBody String requestBody) {
		
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String newName = null, newDesc = null;
		Product product = null;
		int id = 0;
		int newQuantity = 0;
		float newRate = 0;
		try {
			jsonObject = new JSONObject(requestBody);
			id = Integer.parseInt(jsonObject.getString("id"));
			newName = jsonObject.getString("newname");
			newQuantity = Integer.parseInt(jsonObject.getString("qty"));
			newRate = Float.parseFloat(jsonObject.getString("rate"));
			try{
				newDesc = jsonObject.getString("newdesc");
			}
			catch(JSONException e) {
				e.printStackTrace();
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		product = productService.getProductById(id);
		product.setName(newName);
		product.setQuantity(newQuantity);
		product.setUnitRate(newRate);
		if(newDesc!=null) {
			product.setDescription(newDesc);
		}
		try{
			productService.addProduct(product);
		}
		catch(Exception e) {
			try {
				responseJsonObject.put("edit", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			return responseJsonObject.toString();
		}
		try {
			responseJsonObject.put("edit","success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
		

	@Transactional
	@RequestMapping(value ="/product/newEdit", method = RequestMethod.POST)	
	public String editProductAPI(@RequestBody String requestBody) {		
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String organizationabbr = null;
		String newSiUnit = null, newName = null, newDesc = null;
		String lcpName = null, lcpSiUnit = null;
		int lcpMoq = 0, lcpLogistics = 0, lcpCheck = 0, lcpStatus = 0;
		ProductType lcpProdType = null;
		Product product = null;
		LcpProduct prod = null;
		int id = 0, newUnitQty = 0;
		Organization organization = null;
		int newQuantity = 0;
		float newRate = 0, lcpRate = 0;
		try {
			jsonObject = new JSONObject(requestBody);
			id = Integer.parseInt(jsonObject.getString("id"));
			organizationabbr = jsonObject.getString("orgabbr");
			organization = organizationService.getOrganizationByAbbreviation(organizationabbr);
			newSiUnit = jsonObject.getString("siUnit");
			newName = jsonObject.getString("newname");
			newUnitQty = jsonObject.getInt("unitQty");
			newQuantity = Integer.parseInt(jsonObject.getString("qty"));
			newRate = Float.parseFloat(jsonObject.getString("rate"));
			try{
				newDesc = jsonObject.getString("newdesc");
			}
			catch(JSONException e) {
				e.printStackTrace();
			}
			lcpCheck = jsonObject.getInt("lcp");
			if(lcpCheck == 1){
				lcpStatus = jsonObject.getInt("lcpStatus");
				if(lcpStatus == 1){
					lcpProdType = productTypeService.findProductType(jsonObject.getInt("lcpProdId"));
					lcpLogistics = jsonObject.getInt("lcpLogistics");
					lcpMoq = jsonObject.getInt("lcpMoq");
					lcpName = newName;
					lcpRate = Float.parseFloat(jsonObject.getString("lcpRate"));
					lcpSiUnit = jsonObject.getString("lcpSiUnit");
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		product = productService.getProductById(id);
		String name = newUnitQty+" "+newSiUnit+" of "+newName;
		Product newProd = productService.getProductByNameAndOrg(name, product.getProductType().getOrganization());
		if(newProd != null){
			if(newProd.getProductId() != product.getProductId()){
				try{
					responseJsonObject.put("edit", "exists");
					return responseJsonObject.toString();
				}
				catch(JSONException e) {}
			}
		}
		product.setName(name);
		product.setJustName(newName);
		product.setQuantity(newQuantity);
		product.setUnitRate(newRate);
		product.setSiUnit(newSiUnit);
		product.setUnitQty(newUnitQty);
		if(newDesc!=null) {
			product.setDescription(newDesc);
		}
		if(lcpCheck == 1){
			if(lcpProductService.getProductById(id) != null){
				prod = lcpProductService.getProductById(id);
			}
			else{
				prod = new LcpProduct();
				prod.setProductId(id);
			}

			prod.setStatus(lcpStatus);
			if(lcpStatus == 1){
				prod.setProductType(lcpProdType);
				prod.setLogAvailability(lcpLogistics);
				prod.setMoq(lcpMoq);
				prod.setName(lcpName);
				prod.setSiUnit(lcpSiUnit);
				prod.setUnitRate(lcpRate);
				prod.setOrganization(organization);
				prod.setUnit(1);
				prod.setQuantity((int)(newQuantity*(Utils.conversionFact(newSiUnit, lcpSiUnit)*newUnitQty)));
				if(newDesc!=null) {
					prod.setDescription(newDesc);
				}
			}
		}
		try{
			productService.addProduct(product);
			if(lcpCheck == 1)
				lcpProductService.addProduct(prod);
		}
		catch(Exception e) {
			try {
				responseJsonObject.put("edit", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			return responseJsonObject.toString();
		}
		try {
			responseJsonObject.put("edit","success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	

	@Transactional
	@RequestMapping(value ="/product/editlcp", method = RequestMethod.POST)	
	public String editProductLcpAPI(@RequestBody String requestBody) {		
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String newName = null, newDesc = null, siUnit = null;
		LcpProduct product = null;
		Product prod = null;
		int id = 0, newLogistics = 0, newMoq = 0;
		int newQuantity = 0;
		float newRate = 0, lcUnitRate = 0;
		int lcId = 0, lcUnitQty = 0, lcStatus = 0, lcCheck = 0;
		ProductType pType = null;
		String lcProduct = null, lcName = null, lcSiUnit = null;
		try {
			jsonObject = new JSONObject(requestBody);
			id = Integer.parseInt(jsonObject.getString("id"));
			newName = jsonObject.getString("name");
			newMoq = jsonObject.getInt("moq");
			newLogistics = jsonObject.getInt("logistics");
			newQuantity = Integer.parseInt(jsonObject.getString("qty"));
			newRate = Float.parseFloat(jsonObject.getString("rate"));
			siUnit = jsonObject.getString("siUnit");
			try{
				newDesc = jsonObject.getString("desc");
			}
			catch(JSONException e) {
				e.printStackTrace();
			}
			lcCheck = jsonObject.getInt("lc");
			if(lcCheck == 1){
				lcStatus = jsonObject.getInt("lcStatus");
				if(lcStatus == 1){
					lcId = id;
					lcName = newName;
					lcUnitQty = jsonObject.getInt("lcUnitQty");
					lcSiUnit = jsonObject.getString("lcSiUnit");
					lcProduct = lcUnitQty+" "+lcSiUnit+" of "+lcName;
					lcUnitRate = Float.parseFloat(jsonObject.getString("lcUnitRate"));
					pType = productTypeService.findProductType(jsonObject.getInt("lcProdId"));
					Product newProd = productService.getProductByNameAndOrg(lcProduct, lcpProductService.getProductById(id).getOrganization());
					if(newProd != null){
						if(newProd.getProductId() != id){
							responseJsonObject.put("edit", "exists");
							return responseJsonObject.toString();
						}
					}
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		product = lcpProductService.getProductById(id);
		product.setName(newName);
		product.setQuantity(newQuantity);
		product.setUnitRate(newRate);
		product.setLogAvailability(newLogistics);
		product.setMoq(newMoq);
		if(newDesc != null) {
			product.setDescription(newDesc);
		}
		if(lcCheck == 1){
			if(productService.getProductById(id) != null){
				prod = productService.getProductById(id);
			}
			else{
				prod = new Product();
				prod.setProductId(id);
			}
			prod.setStatus(lcStatus);
			if(lcStatus == 1){
				prod.setName(lcProduct);
				prod.setJustName(lcName);
				prod.setUnitQty(lcUnitQty);
				prod.setSiUnit(lcSiUnit);
				prod.setUnitRate(lcUnitRate);
				prod.setProductType(pType);
				prod.setQuantity((int)(newQuantity*(Utils.conversionFact(siUnit, lcSiUnit)/lcUnitQty)));
				if(newDesc!=null) {
					prod.setDescription(newDesc);
				}
			}
		}
		
		try{
			lcpProductService.addProduct(product);
			if(lcCheck == 1)
				productService.addProduct(prod);
		}
		catch(Exception e) {
			try {
				responseJsonObject.put("edit", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			return responseJsonObject.toString();
		}
		try {
			responseJsonObject.put("edit","success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	
	@RequestMapping(value ="/product/delete", method = RequestMethod.POST)
	public String deleteProduct(@RequestBody String requestBody) {
		JSONObject jsonResponseObject = new JSONObject();
		String abbr = null, name = null;
		try{
			JSONObject object = new JSONObject(requestBody);
			abbr = object.getString("orgabbr");
			name = object.getString("name");
			Organization organization = organizationService.getOrganizationByAbbreviation(abbr);
			Product product = productService.getProductByNameAndOrg(name, organization);
			productService.removeProduct(product);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			try {
				jsonResponseObject.put("result", "Failed to delete");
				return jsonResponseObject.toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		try {
			jsonResponseObject.put("result", "Delete successful");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}

	@Transactional
	@RequestMapping(value="/{org}/generatesheet", method=RequestMethod.GET)
	@ResponseBody
	public  FileSystemResource generateSheet(@PathVariable String org) {
		System.out.println("Controller hit");

		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		List<ProductType> productTypes = productService.getProductTypeList(organization);
		List<Product> products = productService.getProductList(productTypes);
		Iterator <Product> iterator = products.iterator();
		List <String> prodNames = new ArrayList<String>();
		List <Integer> prodId = new ArrayList<Integer>();
		List <String> prodType = new ArrayList<String>();
		List <Float> unitRate = new ArrayList<Float>();
		List <Integer> quantity = new ArrayList<Integer>();

		while(iterator.hasNext()) {
			Product product = iterator.next();
			prodNames.add(product.getName());
			prodId.add(product.getProductId());
			prodType.add(String.valueOf(product.getProductType().getName()));
			unitRate.add(product.getUnitRate());
			quantity.add(product.getQuantity());
		}
		File file = SpreadsheetParser.generateProductSheet(prodId, prodNames, prodType, unitRate, quantity, organization.getName());
		return new FileSystemResource(file); 
	}
	
	@Transactional
	@RequestMapping(value="/{orgId}/{date_t}/generateSummary", method=RequestMethod.GET)
	@ResponseBody
	public  FileSystemResource generateSummary(@PathVariable int orgId, @PathVariable String date_t) {
		System.out.println("Controller hit");
		String date_f = "2015-01-01";
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate = null;
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        Date toDate = null;
		try {
			fromDate = df.parse(date_f);
			toDate = dt.parse(date_t);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(fromDate);
		List<OrderSummary> orders = orderSummaryService.getOrderSummaryListForOrganization(organizationService.getOrganizationById(orgId), fromDate, toDate);
		Iterator <OrderSummary> iterator = orders.iterator();
		List <String> prodNames = new ArrayList<String>();
		List <Float> quantity = new ArrayList<Float>();
		List <Float> unitRate = new ArrayList<Float>();

		while(iterator.hasNext()) {
			OrderSummary order = iterator.next();
			prodNames.add(order.getProductName());
			unitRate.add(order.getunitRate());
			quantity.add(order.getQuantity());
		}
		File file = SpreadsheetParser.generateSummary(prodNames, quantity, unitRate, organizationService.getOrganizationById(orgId).getName());
		return new FileSystemResource(file); 
	}
	
	@Transactional
	@RequestMapping(value="/{org}/parsesheet", method=RequestMethod.GET)
	public void parseSheet(@PathVariable String org) {
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
	//	HashMap<Integer, String []> data = SpreadsheetParser.parseProductSheet(organization.getAbbreviation());
		HashMap<Integer, String []> data  = null;
		System.out.println("Data Size: "+data.size());
		for (int i =0;i<data.size();i++) {
			String []contents = data.get(i);
			int id = Integer.parseInt(contents[0]);
			String name = contents[1];
			String type = contents[2];
			float rate = Float.parseFloat(contents[3]);
			int quantity = Integer.parseInt(contents[4]);
			if (id ==0) {
				//new product
				System.out.println("New product");
				Product product = new Product();
				ProductType productType = productTypeService.getByOrganizationAndName(organization, type);
				product.setName(name);
				product.setUnitRate(rate);
				product.setProductType(productType);
				product.setQuantity(quantity);
				try {
					productService.addProduct(product);
				}
				catch(Exception e) {
					System.out.println("cannot add");
				}
				
			}
			else if ( id != 0) {
				//Edit product
				System.out.println("in edit prodcut");
				Product product = productService.getProductById(id);
				int flag = 0;
				if (!name.equals(product.getName())) {
					product.setName(name);
					flag =1;
				}
				if (rate != product.getUnitRate()){
					product.setUnitRate(rate);
					flag=1;
				}
				if (flag == 1 ) {
					try {
						productService.addProduct(product);
					}
					catch(Exception e) {
						System.out.println("cannot update");
					}
				}
			}			
		}
	}
	

	@RequestMapping(value ="/product/addlcp", method = RequestMethod.POST )
	public String addProductLcp(@RequestBody String requestBody){
		
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String organizationabbr = "lcart", si_unit = null;
		Organization organization;
		LcpProduct product = new LcpProduct();
		int last_id = productService.getAllProductListSortedByIdDesc().get(0).getProductId();
		int last_id_lcp = lcpProductRepository.findAllByOrderByProductIdDesc().get(0).getProductId();
		int id = Math.max(last_id, last_id_lcp)+1;
		try {
			jsonObject = new JSONObject(requestBody);
			product.setProductId(id);
			product.setName(jsonObject.getString("name"));
			organization = organizationService.getOrganizationByAbbreviation(jsonObject.getString("orgabbr"));
			//ProductType productType = productTypeService.getReturnProductTypeById(jsonObject.getInt("prodTypeId"));
			ProductType productType = productTypeService.getByOrganizationAndName(organizationService.getOrganizationByAbbreviation(organizationabbr), jsonObject.getString("productType"));
			System.out.println(jsonObject.getString("productType"));
			System.out.println(productType.getName());
			product.setUnitRate(Float.parseFloat(jsonObject.getString("rate")));
			product.setMoq(jsonObject.getInt("moq"));
			product.setUnit(1);
			si_unit = jsonObject.getString("siUnit");
			product.setSiUnit(si_unit);
			product.setLogAvailability(jsonObject.getInt("logistic"));
			product.setProductType(productType);
			product.setOrganization(organization);
			product.setStatus(1);
			product.setQuantity(Integer.parseInt(jsonObject.getString("qty")));
		}
		catch(JSONException e)
		{
			e.printStackTrace();
		}
		try{
			lcpProductService.addProduct(product);
		}
		catch(Exception e){
			try {
				responseJsonObject.put("upload", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		try {			
			responseJsonObject.put("upload", "success");
			LcpProduct lcp_prod = lcpProductRepository.findOne(id);
			responseJsonObject.put("si_group_id", siUnitRepository.findBySiUnit(si_unit).getLcpGroup());
			responseJsonObject.put("id", Integer.toString(lcp_prod.getProductId()));			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
}
