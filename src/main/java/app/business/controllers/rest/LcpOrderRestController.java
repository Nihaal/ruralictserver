package app.business.controllers.rest;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.services.BillLayoutSettingsService;
import app.business.services.GcmTokensService;
import app.business.services.LcpOrderItemService;
import app.business.services.LcpOrderService;
import app.business.services.OrganizationService;
import app.business.services.PresetQuantityService;
import app.business.services.ProductService;
import app.business.services.LcpProductService;
import app.business.services.OrderItemService;
import app.business.services.OrderService;
import app.business.services.OrdersStoreService;
import app.business.services.OrganizationMembershipService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.business.services.message.BinaryMessageService;
import app.business.services.message.MessageService;
import app.data.repositories.BillLayoutSettingsRepository;
import app.data.repositories.BinaryMessageRepository;
import app.data.repositories.GroupRepository;
import app.data.repositories.LcpOrderItemRepository;
import app.data.repositories.LcpOrderRepository;
import app.data.repositories.OrderItemRepository;
import app.data.repositories.OrderRepository;
import app.data.repositories.OrdersStoreRepository;
import app.data.repositories.OrganizationRepository;
import app.data.repositories.RegistrationRepository;
import app.entities.BillLayoutSettings;
import app.entities.GcmTokens;
import app.entities.Group;
import app.entities.LcpOrder;
import app.entities.LcpOrderItem;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.Product;
import app.entities.Registration;
import app.entities.LcpProduct;
import app.entities.Order;
import app.entities.User;
import app.entities.message.Message;
import app.util.GcmRequest;
import app.util.Utils;

@RestController
@RequestMapping("/app")
public class LcpOrderRestController {

	@Autowired
	LcpProductService productService;
	
	@Autowired
	ProductService prodService;
	
	@Autowired
	LcpOrderRepository orderRepository;

	@Autowired
	OrganizationRepository organizationRepository;
	
	@Autowired
	OrganizationService organizationService;
	
	@Autowired
	LcpOrderItemRepository orderItemRepository;
	
	@Autowired
	BinaryMessageRepository binaryMessageRepository;
	
	@Autowired
	GroupRepository groupRepository;
	
	@Autowired
	RegistrationRepository registrationRepository;

	@Autowired
	UserService userService;
	
	@Autowired
	LcpOrderService orderService;
	
	@Autowired
	BillLayoutSettingsRepository billLayoutSettingsRepository;
	
	@Autowired 
	UserPhoneNumberService userPhoneNumberService;
	
	@Autowired
	LcpOrderItemService orderItemService;
	
	@Autowired
	LcpOrderItemRepository lcpOrderItemRepository;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Autowired
	GcmTokensService gcmTokensService;
	
	public HashMap<String, Integer> dashBoardLocal(String orgabbr) throws ParseException {

		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		List<LcpProduct> prodList = productService.getLcpProductListAdmin(organization);
		List<LcpOrderItem> orderList = lcpOrderItemRepository.findByOrganization(organization);	
		int saved = 0, paid = 0, unpaid = 0, processed = 0, cancelled = 0, delivered = 0;
		Iterator<LcpOrderItem> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			LcpOrderItem orderItem = iterator.next();
			if(orderItem.getStatus().equals("placed"))
				saved++;
			else if(orderItem.getStatus().equals("processed"))
				processed++;
			else if(orderItem.getStatus().equals("cancelled"))
				cancelled++;
			else if(orderItem.getStatus().equals("delivered")){
				if(orderItem.getIsPaid() == true)
					paid++;
				else if(orderItem.getIsPaid() == false)
					unpaid++;
				delivered++;
			}
		}
		HashMap<String, Integer> dashmap = new HashMap<String, Integer>();
		dashmap.put("saved", saved);
		dashmap.put("paid", paid);
		dashmap.put("unpaid", unpaid);
		dashmap.put("processed", processed);
		dashmap.put("cancelled", cancelled);
		dashmap.put("products", prodList.size());
		dashmap.put("delivered", delivered);
		return dashmap;
	}
	
	
	@Transactional
	@RequestMapping(value = "/newaddorders", method = RequestMethod.POST)
	public @ResponseBody String newAddOrders(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		List<HashMap<String, String>> Listmap=new ArrayList<HashMap<String, String>>();
		Organization organization = null;
		String organizationabbr = null;
		int userId;
		try{
			JSONArray orderProducts = null;
			jsonObject = new JSONObject(requestBody);
			orderProducts = jsonObject.getJSONArray("orderItems");
			int total = orderProducts.length();
			String phone = jsonObject.getString("phone");
			String message = jsonObject.getString("comments");
			Registration reg = registrationRepository.findByPhone(phone);
			float amount = (float) jsonObject.getDouble("amount");
			Calendar cal= Calendar.getInstance();
			cal.clear(Calendar.HOUR_OF_DAY);
			cal.clear(Calendar.AM_PM);
			cal.clear(Calendar.MINUTE);
			cal.clear(Calendar.SECOND);
			cal.clear(Calendar.MILLISECOND);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");      
		    try {
				String dateWithoutTime = sdf.format(new java.util.Date());
				String date = dateWithoutTime.toString();
		  	LcpOrder order = new LcpOrder(total, reg, amount, message, date);
		    
			order = orderRepository.save(order);
			for(int i = 0; i < total; i++){
				JSONObject row = orderProducts.getJSONObject(i);
				int productId = row.getInt("prod_id");
				LcpProduct product = productService.getProductById(productId);				
				int quantity = row.getInt("quantity");
				float unitRate = (float) row.getDouble("unit_rate");
				int orgId = product.getOrganization().getOrganizationId();
				Organization org = organizationRepository.findOne(orgId);
				HashMap<String, String> map=new HashMap<String, String>();
				if(org.getStockManagement() == true){
					int temp = product.getQuantity();
					if(quantity <= temp)
					{
						int q = temp - quantity;
						product.setQuantity(q);
						Product prod = prodService.getProductById(product.getProductId());
						if(prod != null){
							prod.setQuantity((int)(q*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())/prod.getUnitQty())));
						}
					}
					else
					{
						try{
							map.put("error", "product quantity not available");
							map.put("id", Integer.toString(product.getProductId()));
							map.put("available", Integer.toString(product.getQuantity()));
						}
						catch(Exception e){
							e.printStackTrace();
						}
						Listmap.add(map);
						continue;
					}
				}
				LcpOrderItem orderItem = new LcpOrderItem(order, product, quantity, unitRate, org);
				orderItem = orderItemRepository.save(orderItem);
				
			}
		    } catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		    
		catch(JSONException e){
			e.printStackTrace();
		}
		try {
			response.put("status", "success");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(Listmap.size()!=0){
			try {
				response.put("error", Listmap);
				response.put("status", "failure");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return response.toString();
	}

	@Transactional
	@RequestMapping(value="/cancellcporders", method = RequestMethod.POST)
	public @ResponseBody String cancelOrders(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		int itemId = 0;
		try{
			jsonObject = new JSONObject(requestBody);
			itemId = jsonObject.getInt("itemId");
		}
		catch(JSONException je){
			je.printStackTrace();
		}
		System.out.println(itemId);
		LcpOrderItem orderItem = orderItemRepository.findOne(itemId);
		System.out.println("here5");
		/*if(orderItem.getOrder().getTotal() == 1){
			System.out.println("here1");
			LcpOrder ordr = orderRepository.findOne(orderItem.getOrder().getOrderId());
			orderService.changeToCancel(ordr);
		}*/
		try {
			if(orderItem.getStatus().equals("processed")){
				response.put("status", "failure");
				response.put("reason", "Order was already processed.");
				return response.toString();
			}
			else if(orderItem.getStatus().equals("delivered")){
				response.put("status", "failure");
				response.put("reason", "Order was already delivered.");
				return response.toString();
			}
			else if(orderItem.getStatus().equals("cancelled")){
				response.put("status", "failure");
				response.put("reason", "Order was already cancelled.");
				return response.toString();
			}
			else if(orderItem.getStatus().equals("placed")){
				orderItemService.changeToCancel(orderItem);
				Organization org = orderItem.getOrganization();
				List <String> androidTargets = getTargetDevices(org);
				if(androidTargets.size()>0) {
					GcmRequest gcmRequest = new GcmRequest();
					HashMap<String,Integer> dashData = null;
					try {
						dashData = dashBoardLocal(org.getAbbreviation());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					gcmRequest.broadcast(orderItem.getOrder().getRegistration().getFirstname()+" has cancelled a B2B order", "Order cancellation", androidTargets,0);
					System.out.println(org.getAbbreviation());
					gcmRequest.broadcastLcp(androidTargets, org.getAbbreviation(), dashData);
				}
				response.put("status", "success");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response.toString();
	}
	
	public List <String> getTargetDevices (Organization organization)  {
		
		List<OrganizationMembership> organizationMembership = organizationMembershipService.getOrganizationMembershipListByIsAdmin(organization, true);
		List<String> phoneNumbers = new ArrayList<String>();
		Iterator <OrganizationMembership> membershipIterator = organizationMembership.iterator();
		while (membershipIterator.hasNext()) {
			OrganizationMembership membership = membershipIterator.next();
			User user = membership.getUser();
			phoneNumbers.add(userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber());
		}
		Iterator <String> iterator = phoneNumbers.iterator();
		List <String>androidTargets = new ArrayList<String>();
		while(iterator.hasNext()) {
			String number = iterator.next();
			try{
			List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(number);
			Iterator <GcmTokens> iter = gcmTokens.iterator();
			while(iter.hasNext()) {
			
			androidTargets.add(iter.next().getToken());
			}
			}
			catch(Exception e){
				System.out.println("no token for number: "+number);
			}
		}
		return androidTargets;
	}
	
	@Transactional
	@RequestMapping(value = "/addorders", method = RequestMethod.POST)
	public @ResponseBody String addOrders(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		List<HashMap<String, String>> Listmap=new ArrayList<HashMap<String, String>>();
		Organization organization = null;
		String organizationabbr = null;
		String address = null;
		int userId;
		try{
			JSONArray orderProducts = null;
			jsonObject = new JSONObject(requestBody);
			orderProducts = jsonObject.getJSONArray("orderItems");
			int total = orderProducts.length();
			String phone = jsonObject.getString("phone");
			String message = jsonObject.getString("comments");
			Registration reg = registrationRepository.findByPhone(phone);
			try{
				address = jsonObject.getString("address");
			} catch(Exception e){
				address = reg.getAddress();
			} 
			float amount = (float) jsonObject.getDouble("amount");
			Calendar cal= Calendar.getInstance();
			cal.clear(Calendar.HOUR_OF_DAY);
			cal.clear(Calendar.AM_PM);
			cal.clear(Calendar.MINUTE);
			cal.clear(Calendar.SECOND);
			cal.clear(Calendar.MILLISECOND);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");      
		    try {
				String dateWithoutTime = sdf.format(new java.util.Date());
				String date = dateWithoutTime.toString();
		  	LcpOrder order = new LcpOrder(total, reg, amount, message, address, date);
		    
			order = orderRepository.save(order);
			for(int i = 0; i < total; i++){
				JSONObject row = orderProducts.getJSONObject(i);
				int productId = row.getInt("prod_id");
				LcpProduct product = productService.getProductById(productId);				
				int quantity = row.getInt("quantity");
				float unitRate = (float) row.getDouble("unit_rate");
				int orgId = product.getOrganization().getOrganizationId();
				Organization org = organizationRepository.findOne(orgId);
				organizationabbr = org.getAbbreviation();
				HashMap<String, String> map=new HashMap<String, String>();
				if(org.getStockManagement() == true){
					int temp = product.getQuantity();
					if(quantity <= temp)
					{
						int q = temp - quantity;
						product.setQuantity(q);
						Product prod = prodService.getProductById(product.getProductId());
						if(prod != null){
							prod.setQuantity((int)(q*(Utils.conversionFact(product.getSiUnit(), prod.getSiUnit())/prod.getUnitQty())));
						}
					}
					else
					{
						try{
							map.put("error", "product quantity not available");
							map.put("name",product.getName());
							map.put("unit", product.getSiUnit());
							map.put("id", Integer.toString(product.getProductId()));
							map.put("available", Integer.toString(product.getQuantity()));
						}
						catch(Exception e){
							e.printStackTrace();
						}
						Listmap.add(map);
						continue;
					}
				}
				LcpOrderItem orderItem = new LcpOrderItem(order, product, quantity, unitRate, org);
				orderItem = orderItemRepository.save(orderItem);
				List <String> androidTargets = getTargetDevices(org);
				if(androidTargets.size()>0) {
					GcmRequest gcmRequest = new GcmRequest();
					HashMap<String,Integer> dashData = null;
					try {
						dashData = dashBoardLocal(organizationabbr);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					gcmRequest.broadcast(reg.getFirstname()+" has placed a B2B order", "New order", androidTargets,0);
					System.out.println(organizationabbr);
					if(product.getQuantity() < 10)
						gcmRequest.broadcast(product.getName()+" is running low on stock", "Low stock", androidTargets, 1);
					gcmRequest.broadcastLcp(androidTargets,organizationabbr,dashData);
				}
			}
		    } catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		    
		catch(JSONException e){
			e.printStackTrace();
		}
		try {
			response.put("status", "success");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(Listmap.size()!=0){
			try {
				response.put("error", Listmap);
				response.put("status", "failure");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return response.toString();
	}
	
	@Transactional
	@RequestMapping(value="/carttocheckout", method = RequestMethod.POST)
	public @ResponseBody String cartToCheckout(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		int cartquant ;
        float unitrate;
		List<HashMap<String, String>> Listmap=new ArrayList<HashMap<String, String>>();
		
		
		
		try{
			JSONArray orderProducts = null;
			jsonObject = new JSONObject(requestBody);
			orderProducts = jsonObject.getJSONArray("orderItems");
			for(int i = 0; i < orderProducts.length(); i++){
				JSONObject row = orderProducts.getJSONObject(i);
				int productId = row.getInt("prod_id");
				cartquant = row.getInt("cartquantity");
	            unitrate = (float)row.getDouble("unitRate");
				LcpProduct product = productService.getProductById(productId);				
				int orgId = product.getOrganization().getOrganizationId();
				Organization org = organizationRepository.findOne(orgId);
				HashMap<String, String> map=new HashMap<String, String>();
				//map.put("unitRate", Float.toString(product.getUnitRate()));
				map.put("id", Integer.toString(product.getProductId()));
				if(product.getOrganization().getStockManagement()){	
					if(product.getQuantity() >= cartquant)
						map.put("quantity", "true");
					else{
						map.put("quantity", "false");
						map.put("unit", product.getSiUnit());
						map.put("avilablequantity", Integer.toString(product.getQuantity()));
					}
				}
				else
					map.put("quantity", "true");
				if(product.getUnitRate() == unitrate)
					map.put("unitRate", "true");
				else
					map.put("unitRate", "false");
					map.put("currentRate", Float.toString(product.getUnitRate()));
				map.put("name", product.getName());
				Listmap.add(map);
			}
		}
		
		catch(JSONException e){
			e.printStackTrace();
		}
	   
		try {
			response.put("prices", Listmap);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return response.toString();
	}
	
	@Transactional
	@RequestMapping(value="/placedtoprocessed", method = RequestMethod.POST)
	public @ResponseBody String placedToProcess(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		int itemId = 0;
		try{
			jsonObject = new JSONObject(requestBody);
			itemId = jsonObject.getInt("itemId");
		}
		catch(JSONException je){
			je.printStackTrace();
		}
		System.out.println(itemId);
		LcpOrderItem orderItem = orderItemRepository.findOne(itemId);
		System.out.println("here5");
		if(orderItem.getOrder().getTotal() == 1){
			System.out.println("here1");
			LcpOrder ordr = orderRepository.findOne(orderItem.getOrder().getOrderId());
			orderService.changeToProcessed(ordr);
		}
		orderItemService.changeToProcess(orderItem);
		try {
			response.put("status", "success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response.toString();
	}
	
	@Transactional
	@RequestMapping(value="/vieworder", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, List<HashMap<String, String>>> viewOrders(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		HashMap<String, List<HashMap<String, String>>> orderMap = new HashMap<String, List<HashMap<String, String>>>();
		String phoneNumber = null;
		List<HashMap<String, String>> Listmap=new ArrayList<HashMap<String, String>>();
		HashMap<String, HashMap<String, String>> ordermap=new HashMap<String, HashMap<String, String>>();
		try{
			jsonObject = new JSONObject(requestBody);
			phoneNumber = jsonObject.getString("number");
			Registration reg = registrationRepository.findByPhone(phoneNumber);
			List<LcpOrder> orderlist = orderService.getOrdersByUsers(reg);
			Iterator <LcpOrder> iterator = orderlist.iterator();
			while(iterator.hasNext()){
				LcpOrder order = iterator.next();
				List<LcpOrderItem> orderItem = orderItemService.getOrderItemsByOrderId(order);
				for(LcpOrderItem item : orderItem){
					HashMap<String, String> map=new HashMap<String, String>();
					map.put("orderItemId", Integer.toString(item.getOrderItemId()));
					map.put("name", item.getProduct().getName());
					if(item.getProduct().getImageUrl() == null)
						map.put("imageUrl", "");
					else
						map.put("imageUrl", item.getProduct().getImageUrl());
					map.put("quantity", Integer.toString(item.getQuantity()));
					map.put("delAddress", item.getOrder().getRegistration().getDelAddress());
					map.put("rate", Float.toString(item.getUnitRate()));
					map.put("status", item.getStatus());
					map.put("isPaid", Boolean.toString(item.getIsPaid()));
					if((item.getOrder().getDelivery()) != null)
						map.put("delivery_date", item.getDelivery());
					else
						map.put("delivery_date", "");
					map.put("organization name", item.getProduct().getOrganization().getName());
					map.put("unit", item.getProduct().getSiUnit());
					Listmap.add(map);
				}
			}
			orderMap.put("orders", Listmap);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		return orderMap;
	}
}