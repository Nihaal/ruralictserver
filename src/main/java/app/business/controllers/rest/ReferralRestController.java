package app.business.controllers.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.coyote.http11.InputFilter;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.JsonObject;
import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.services.GroupMembershipService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.ReferralService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.data.repositories.OrganizationMembershipRepository;
import app.data.repositories.UserPhoneNumberRepository;
import app.data.repositories.UserRepository;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.Product;
import app.entities.ProductType;
import app.entities.Referral;
import app.entities.SmsApiKeys;
import app.entities.User;
import app.entities.UserPhoneNumber;
import app.util.SpreadsheetParser;
import app.util.Utils;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@RestController
@RequestMapping("/api")
public class ReferralRestController {

	@Autowired
	UserPhoneNumberService userPhoneNumberService;

	@Autowired
	UserService userService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserPhoneNumberRepository userPhoneNumberRepository;

	@Autowired
	ReferralService referralService;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	SmsApiKeysService smsApiKeysService;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Autowired
	OrganizationMembershipRepository organizationMemberRepository;
	
	@Autowired
	GroupMembershipService groupMembershipService;
	
	
	@RequestMapping(value = "/loginWebApp",method = RequestMethod.POST )
	public String login(@RequestBody String requestBody) {
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject=null;
		String phonenumber = null;
		String password=null;
		try {
			jsonObject = new JSONObject(requestBody);
			password=jsonObject.getString("password");
			phonenumber=jsonObject.getString("phonenumber");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		User user = userRepository.findByuserPhoneNumbers_phoneNumber(phonenumber);
		if(user!=null) {
			if(password==null) {
				try {
					responseJsonObject.put("Status", "Error");
					responseJsonObject.put("Error", "Password is null.");
					responseJsonObject.put("Authentication", "failure");
					responseJsonObject.put("registered", "true");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			if(user.getSha256Password()==null) {
				try {
					responseJsonObject.put("Status", "Error");
					responseJsonObject.put("Error", "User Password is null. Set the password");
					responseJsonObject.put("Authentication", "failure");
					responseJsonObject.put("registered", "true");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			if(passwordEncoder.matches(password, user.getSha256Password())) {
				List<OrganizationMembership> organizationMemberships = user.getOrganizationMemberships();
				List<Organization> organizationList=organizationService.getAllOrganizationList();
				if(organizationList.size()==0) {
					try {
						responseJsonObject.put("Authentication", "success");
						responseJsonObject.put("email", user.getEmail());
						responseJsonObject.put("Error", "No organization has accepted the user");
						responseJsonObject.put("organizations", "null");
					}
					catch (Exception e)	{
						e.printStackTrace();
					}
					return responseJsonObject.toString();
				}
				JSONArray orgArray= new JSONArray();
				for(Organization organization: organizationList) {
					try {
						if(!organization.getName().equals("Testing") && !organization.getName().equals("TestOrg1") && !organization.getName().equals("Testorg3")) {					
							JSONObject org= new JSONObject();
							org.put("name", organization.getName());
							org.put("abbr", organization.getAbbreviation());
							if(organizationMembershipService.getUserOrganizationMembership(user, organization)==null) {
								org.put("status", "Rejected");
								orgArray.put(org);
								continue;
							}
							if(organizationMembershipService.getOrganizationMembershipStatus(user, organization)==1)
								org.put("status", "Accepted");
							else if(organizationMembershipService.getOrganizationMembershipStatus(user, organization)==0)
								org.put("status", "Pending");
							else
								org.put("status", "Rejected");
							orgArray.put(org);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				try {			
					responseJsonObject.put("Authentication", "success");
					responseJsonObject.put("email", user.getEmail());
					responseJsonObject.put("organizations", orgArray);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			else {
				try {
					responseJsonObject.put("Authentication", "failure");
					responseJsonObject.put("registered", "true");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
		}
		else {
			try {
				responseJsonObject.put("Authentication", "failure");
				responseJsonObject.put("registered", "false");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
	}

	@RequestMapping(value = "/registerWebApp",method = RequestMethod.POST ,produces="application/json")
	@Transactional
	public HashMap<String,String> registration(@RequestBody String requestBody)
	{
		HashMap<String,String> response= new HashMap<String, String>();
		JSONObject jsonObject=null;
		String phonenumber = null;
		String address =null;
		String password=null;
		String name=null;
		String email=null;
		String pincode = null, lastname =null;
		JSONArray orgListJsonArray = null;
		User user = new User();
		UserPhoneNumber userPhoneNumber= new UserPhoneNumber();
		List<UserPhoneNumber> userPhoneNumbers= new ArrayList<UserPhoneNumber>();
		try {
			jsonObject = new JSONObject(requestBody);
			phonenumber=jsonObject.getString("phonenumber");
			password=jsonObject.getString("password");
			try{
				name=jsonObject.getString("name");
			}
			catch(Exception e) {
				System.out.println("No name");
			}
			try{
				email=jsonObject.getString("email");
			}
			catch(Exception e) {		
				System.out.println("No email");		
			}		
			try{		
				lastname = jsonObject.getString("lastname");		
			}		
			catch(Exception e) {		
				System.out.println("No lastname");		
			}
			try{
				address=jsonObject.getString("address");
			}
			catch(Exception e) {
				System.out.println("No address");
			}
			try{
				pincode = jsonObject.getString("pincode");
			}
			catch(Exception e) {
				System.out.println("No pincode");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		User usercheck = userRepository.findByuserPhoneNumbers_phoneNumber(phonenumber);
		if(usercheck!=null)
		{
			response.put("Status", "Failure");
			response.put("Error", "Number Exists");
			return response;
		}
		List<User> userCheckList = userRepository.findByEmail(email);
		if(userCheckList.size()>0)
		{
			response.put("Status", "Failure");
			response.put("Error", "Email Exists");
			return response;
		}
		user.setAddress(address);
		user.setCallLocale("en");
		user.setEmail(email);
		user.setSha256Password(passwordEncoder.encode(password));
		user.setName(name);
		if (pincode != null)
			user.setPincode(pincode);
		if(lastname != null)
			user.setLastname(lastname);
		user=userRepository.save(user);
		user.setTextbroadcastlimit(0);
		user.setVoicebroadcastlimit(0);	
		user=userRepository.save(user);
		System.out.println("User phone number is being saved");
		phonenumber="91"+phonenumber;
		userPhoneNumber.setPhoneNumber(phonenumber);
		userPhoneNumber.setPrimary(true);
		userPhoneNumber.setUser(user);
		userPhoneNumber=userPhoneNumberRepository.save(userPhoneNumber);
		userPhoneNumbers.add(userPhoneNumber);
		user.setUserPhoneNumbers(userPhoneNumbers);
		System.out.println("User phone number is  saved");
		userRepository.save(user);
		response.put("Status","Success");
		return response;
	}
	
	@Transactional
	@RequestMapping (value="/uploadsheet/{abbr}", method = RequestMethod.POST, produces = "text/plain")
	public @ResponseBody String handleSheetUpload(HttpServletRequest request, @PathVariable String abbr) {
		System.out.println("inside");
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mRequest.getFileNames();
		//only one iteration i.e itr.next() as it has only one file
		MultipartFile mFile = mRequest.getFile(itr.next());
		String fileName = mFile.getOriginalFilename();
		System.out.println("File is "+fileName);
		File temp = Utils.saveFile("temp.xls", Utils.getSpreadsheetDir(), mFile);
		File serverFile = new File(Utils.getSpreadsheetDir() + "/" + fileName);
		//System.out.println(Utils.getSpreadsheetDir() +File.separator+ fileName);
		Random randomint = new Random();
		int flag=1;
		do{
			try 
			{
				Files.copy(temp.toPath(), serverFile.toPath());
				flag=1;
				System.out.println("Copied.");
			}	
			catch (FileAlreadyExistsException e)
			{
				System.out.println("File already exist. Renaming file and trying again.");
				fileName = fileName.substring(0,fileName.length()-4);
				fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".xls";
				serverFile = new File(Utils.getSpreadsheetDir() + "/" + fileName);
				flag=0;
			}
			catch (IOException e) {
				e.printStackTrace();
				flag=1;
			}
		}while(flag==0);
		
		//System.out.println(url);
		//String utr = "C:\\Add Member.xls";
		//return url;
	    
	
		
		/*MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mRequest.getFileNames();
		MultipartFile mFile = mRequest.getFile(itr.next());
		String fileName = mFile.getOriginalFilename();
		File temp = Utils.saveFile("temp.xls", Utils.getSpreadsheetDir(), mFile);
		File serverFile = new File(Utils.getSpreadsheetDir() +File.separator+ fileName);
		if (serverFile.exists())
			serverFile.delete();
		int flag =1;
		do{

			try 
			{
				Files.copy(temp.toPath(), serverFile.toPath());
				flag=1;
			}	
			catch(Exception e){
				e.printStackTrace();
			}
		}while(flag==0);
		String url = Utils.getSpreadsheetDirURL() + fileName;
		System.out.println(url);*/
		String op = "";
		JSONObject obj = null;
		int count = 0;
		final String regex = "[6-9]+\\d{9}";
		//final String regexEmail = "[a-zA-Z0-9]+[@][a-z]+[.][a-z]";
		try{
			FileInputStream fis = new FileInputStream(serverFile);
			HSSFWorkbook wb = new HSSFWorkbook(fis);
			HSSFSheet sheet = wb.getSheetAt(0);
			Iterator<Row> rowIt = sheet.iterator();
			Row row = rowIt.next();
			while(rowIt.hasNext()){
				row = rowIt.next();
				//if(row.getCell(2)!=null){
				String number = row.getCell(0).toString();
				//String email = null;
				number = (number.charAt(0)+number.substring(2, 11)).split("E")[0];
				if(number.length()!=10)
					for(int i=0; i<=10-number.length(); i++)
						number += '0';
				System.out.println(number); 
				if(number.matches(regex)){
					/*try{
						email = row.getCell(4).toString();
					}
					catch(NullPointerException e){
						email = new String("91"+number+"@gmail.com");
					}
					if(email.isEmpty() || !email.matches(regexEmail))
						email = new String("91"+number+"@gmail.com");*/
					String req = "{\"phonenumber\":\"91"+number+"\",\"role\":\"Member\",\"abbr\":\""+abbr+"\"}";
					System.out.println(req);
					obj = newRefer(req);
					String resp = obj.getString("response");
					if(resp.equals("failure")){
						op += (number+", ");// ("+obj.getString("error")+"),");
					}
					count++;
					/*}
					else{
						break;
					}*/
				}
				else
					continue;
			}
			wb.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}		
		serverFile.delete();
		if(count == 0)
			op = "nil";
		return op;
		//return url;	
	}

	
	//
	
	
	public JSONObject newRefer(String requestBody) {
		int noEmail=0;

		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null, branchResponse = null, shortenerResponse = null;
		String API_KEY = "AIzaSyBU0JpWx49ZIJHw5lF-lBJeAnL5ZpXktL0";
		String phonenumber = null, email = null, orgabbr = null, refreeEmail = null, password = null,
				generatedUrl = null, address = null, name = null, lastname = null;
		String shortendUrl = null;
		Random random = new Random();
		List<UserPhoneNumber> userPhoneNumbers = new ArrayList<UserPhoneNumber>();
		Organization destOrganization = null;
		try {
			jsonObject = new JSONObject(requestBody);
			System.out.println("JSON Object refer: " + jsonObject);
			phonenumber = jsonObject.getString("phonenumber");
			System.out.println(phonenumber);
			
			orgabbr = jsonObject.getString("abbr");
			System.out.println(orgabbr);
			try {
				refreeEmail = jsonObject.getString("refemail");
			} catch (Exception e) {
				// fetch
				Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
				System.out.println(organization.getName());
				List<OrganizationMembership> list = organizationMembershipService
						.getOrganizationMembershipListByIsAdmin(organization, true);
				refreeEmail = list.get(0).getUser().getEmail();
			}
			try {
				email = jsonObject.getString("email");
				System.out.println(email);
				if(email.equals("")){
					noEmail=1;
					email=phonenumber+"@gmail.com";					
				}
			} catch (Exception e11) {
				noEmail=1;
				email=phonenumber+"@gmail.com";
				// Do nothing
			}

			try {
				address = jsonObject.getString("address");
			} catch (Exception e1) {
				// Do nothing
			}
			try {
				name = jsonObject.getString("name");
			} catch (Exception e2) {
				// Do nothing
			}
			try {
				lastname = jsonObject.getString("lastname");
			} catch (Exception e3) {
				// Do nothing
			}
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				System.out.println("JSON exception caught");
				responseJsonObject.put("response", "failure");
				responseJsonObject.put("error", "Format error");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		System.out.println("JSON passed");

		System.out.println(userPhoneNumberService.findPreExistingPhoneNumber(phonenumber)+"   "+userService.getUserFromEmail(email));
		destOrganization = organizationService.getOrganizationByAbbreviation(orgabbr);
		if ((userPhoneNumberService.findPreExistingPhoneNumber(phonenumber) == true)) {
			// New user scenario
			if(userService.getUserFromEmail(email) != null){
				try {
					responseJsonObject.put("response", "failure");
					responseJsonObject.put("error", "User with same email already exists");
					return responseJsonObject;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("inside");
			User user = new User();
			user.setEmail(email);
			userService.addUser(user);
			System.out.println("user with only email added");

			UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
			userPhoneNumber.setPhoneNumber(phonenumber);
			userPhoneNumber.setPrimary(true);
			userPhoneNumber.setUser(user);
			userPhoneNumberService.addUserPhoneNumber(userPhoneNumber);
			userPhoneNumbers.add(userPhoneNumber);
			user.setUserPhoneNumbers(userPhoneNumbers);
			java.util.Date date = new java.util.Date();
			Timestamp currentTimestamp = new Timestamp(date.getTime());
			user.setTime(currentTimestamp);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			user.setWebLocale("en");
			user.setCallLocale("en");
			if (address != null)
				user.setAddress(address);
			if (name != null)
				user.setName(name);
			if (lastname != null)
				user.setLastname(lastname);
			String fname;
			if(noEmail!=1)
			{
				int i = email.indexOf("@");
				fname = email.substring(0, i);
			 
			}
			else
			{
				fname=" ";
			}
			password = fname+random.nextInt(1000);
			password = Integer.toString(random.nextInt(10));
			for (int count = 0; count < 9; ++count)
				password = password + random.nextInt(10);
			user.setPassToken(password);
			user.setSha256Password(passwordEncoder.encode(password));
			userService.addUser(user);
			System.out.println("user with only password added");

			User refUser = userService.getUserFromEmail(refreeEmail);
			System.out.println("User name: " + refUser.getName());
			Referral referral = new Referral();
			System.out.println("Should start if-else");
			if(userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail()!=null)
			{email = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail();
			System.out.println("I found this" + userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail());
			}
			System.out.println("see this" + email);
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser); // This is the referee
			String referralCode = String.valueOf(random.nextInt(999999));
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);
			String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String android_uri_scheme = "lokacart://";
			String android_package_name = "com.mobile.ict.cart";
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
			// String reqBody =
			// "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\"phonenumber\":\""+phonenumber+"\",\"email\":\""+email+"\",\"password\":\""+password+"\",\"referralCode\":\""+referralCode+"\"}\"";
			String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
					+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
					+ referralCode + "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\"" + user.getPassToken()
					+ "\\\"}\"\n    \n}";

			/*
			 * String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+
			 * "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+
			 * "\\\", \\\"email\\\":\\\""+email+
			 * "\\\", \\\"referralCode\\\":\\\""+referralCode+
			 * "\\\", \\\"password\\\":\\\""+ password+
			 * "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\""
			 * +user.getPassToken()+"\\\",\\\"organization\\\":\\\""+
			 * destOrganization.getName()+ "\\\", \\\"abbr\\\":\\\""
			 * +destOrganization.getAbbreviation()+"\\\"}\"\n    \n}";
			 */
			System.out.println("body: " + reqBody);

			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: " + generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			/* //bestB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://best-erp.com/ruralict/app/code/" + referralCode
					+ "\"\n\n}";
			*/ //bestE
			
			//ictB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://ruralict.cse.iitb.ac.in/ruralict/app/code/" + referralCode
					+ "\"\n\n}";
			//ictE
			
			OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: " + shortendUrl);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + destOrganization.getName()
					+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

			try {
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			int flag = 0;
			User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
			// Existing user
			int details = 0;
			if (user.getEmail() == null || user.getLastname() == null || user.getName() == null
					|| user.getAddress() == null || user.getName() == null || user.getPincode() == null) {
				details = 1;
			}
			List<OrganizationMembership> list = user.getOrganizationMemberships();
			Iterator<OrganizationMembership> iterator = list.iterator();
			while (iterator.hasNext()) {
				OrganizationMembership organizationMembership = iterator.next();
				if (organizationMembership.getIsAdmin() == true)
					flag = 1;
			}
			if (flag == 1) {
				try {
					responseJsonObject.put("response", "failure");
					responseJsonObject.put("error", "Admin accounts cannot be used on the consumer app");
					return responseJsonObject;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (user.getPassToken() == null) {
				// Resetting password
				password = Integer.toString(random.nextInt(10));
				for (int count = 0; count < 9; ++count)
					password = password + random.nextInt(10);
				user.setPassToken(password);
				user.setSha256Password(passwordEncoder.encode(password));
				userService.addUser(user);
				System.out.println("pass token: " + password);
			}
			List<OrganizationMembership> memberList = user.getOrganizationMemberships();
			if (memberList.isEmpty() == false) {
				Iterator<OrganizationMembership> memberIterator = memberList.iterator();
				while (memberIterator.hasNext()) {
					OrganizationMembership organizationMembership = memberIterator.next();
					if (destOrganization == organizationMembership.getOrganization()) {
						try {
							responseJsonObject.put("response", "failure");
							responseJsonObject.put("error", "Already a member");
						} catch (JSONException e) {
							e.printStackTrace();
						}
						return responseJsonObject;
					}
				}
			}
			User refUser = userService.getUserFromEmail(refreeEmail);
			Referral referral = new Referral();
			System.out.println("Should start if-else");
			if(userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail()!=null)
			{email = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail();
			System.out.println("I found this" + userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail());
			}
			System.out.println("see this" + email);
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser);
			String referralCode = String.valueOf(random.nextInt(999999));
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
			String android_uri_scheme = "lokacart://";
			String android_package_name = "com.mobile.ict.cart";
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);
			String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
					+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
					+ referralCode + "\\\", \\\"details\\\":\\\"" + details + "\\\", \\\"token\\\":\\\""
					+ user.getPassToken() + "\\\"}\"\n    \n}";

			System.out.println("body: " + reqBody);

			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
			/*
			 * MediaType mediaType = MediaType.parse("application/json");
			 * okhttp3.RequestBody body =
			 * okhttp3.RequestBody.create(mediaType,
			 * "{\n\"branch_key\":\"key_live_feebgAAhbH9Tv85H5wLQhpdaefiZv5Dv\",\n\"data\":\"{\\\"name\\\": \\\"Alex\\\"}\"\n    \n}"
			 * ); Request request = new Request.Builder()
			 * .url("https://api.branch.io/v1/url") .post(body)
			 * .addHeader("content-type", "application/json")
			 * .addHeader("cache-control", "no-cache")
			 * .addHeader("postman-token",
			 * "7a4da102-dc81-31b9-3028-aedee4a30025") .build();
			 */
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: " + generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}

			/* //bestB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://best-erp.com/ruralict/app/code/"
					+ referralCode + "\"\n\n}";
			*/ //bestE
			
			//ictB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://ruralict.cse.iitb.ac.in/ruralict/app/code/"
					+ referralCode + "\"\n\n}";
			//ictE
			
			OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: " + shortendUrl);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}

			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + destOrganization.getName()
					+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			System.out.println("http://best-erp.com/ruralict/app/code/" + referralCode);
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

			try {
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return responseJsonObject;
		}
		//int noEmail=0;
		/*JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null, branchResponse = null, shortenerResponse=null;
		String phonenumber = null, email = null, orgabbr = null, refreeEmail = null, password=null, generatedUrl = null, address = null, name = null, lastname = null;
		String shortendUrl = null;
		String API_KEY = "AIzaSyBU0JpWx49ZIJHw5lF-lBJeAnL5ZpXktL0";
		Random random = new Random();
		List<UserPhoneNumber> userPhoneNumbers = new ArrayList<UserPhoneNumber>();
		Organization destOrganization = null;
		System.out.println(requestBody);
		try {
			System.out.println("JSON Object refer: "+jsonObject);
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			System.out.println(phonenumber);
			orgabbr = jsonObject.getString("abbr");
			try{
			refreeEmail = jsonObject.getString("refemail");
			}
			catch(Exception e) {
				//fetch 
				Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
				List <OrganizationMembership> list = organizationMembershipService.getOrganizationMembershipListByIsAdmin(organization, true);
				refreeEmail = list.get(0).getUser().getEmail();
			}
			
			try{
				address = jsonObject.getString("address");
			}
			catch(Exception e1)
			{
				//Do nothing
			}

			try {
				email = jsonObject.getString("email");
			} catch (Exception e11) {
				email = phonenumber+"@gmail.com";
			}

			try {
				name = jsonObject.getString("name");
			}
			catch(Exception e2) {
				//Do nothing
			}
			try {
				lastname = jsonObject.getString("lastname");
			}
			catch (Exception e3) {
				//Do nothing
			}
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "failure");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if(email.equals("null")){
			email = phonenumber+"@gmail.com";
		}
		System.out.println("JSON passed");
		destOrganization = organizationService.getOrganizationByAbbreviation(orgabbr);
		if (userPhoneNumberService.findPreExistingPhoneNumber(phonenumber) == true) {
			//New user scenario
			System.out.println("inside");
			User user = new User();
			System.out.println(email);
			user.setEmail(email);
			userService.addUser(user);
			System.out.println("user with only email added");
			UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
			userPhoneNumber.setPhoneNumber(phonenumber);
			userPhoneNumber.setPrimary(true);
			userPhoneNumber.setUser(user);
			userPhoneNumberService.addUserPhoneNumber(userPhoneNumber);
			System.out.println("Here");
			userPhoneNumbers.add(userPhoneNumber);
			user.setUserPhoneNumbers(userPhoneNumbers);
			java.util.Date date= new java.util.Date();
			Timestamp currentTimestamp= new Timestamp(date.getTime());
			user.setTime(currentTimestamp);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			user.setWebLocale("en");
			user.setCallLocale("en");
			if(address != null)
				user.setAddress(address);
			if(name != null)
				user.setName(name);
			if(lastname != null)
				user.setLastname(lastname);
			//int i = email.indexOf("@");
			//String fname=email.substring(0, i);
			//password = fname+random.nextInt(1000);
			password = Integer.toString(random.nextInt(10));
			for (int count = 0; count < 9; ++count)
				password = password + random.nextInt(10);
			user.setPassToken(password);
			user.setSha256Password(passwordEncoder.encode(password));
			userService.addUser(user);
			System.out.println("user with only password added");

			User refUser = userService.getUserFromEmail(refreeEmail);
			System.out.println("User name: "+refUser.getName());
			Referral referral = new Referral();
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser); //This is the referee
			String referralCode = String.valueOf(random.nextInt(999999));
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);
			String BRANCH_KEY= "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String android_uri_scheme = "lokacart://";
		    String android_package_name =  "com.mobile.ict.cart";
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
		//	String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\"phonenumber\":\""+phonenumber+"\",\"email\":\""+email+"\",\"password\":\""+password+"\",\"referralCode\":\""+referralCode+"\"}\"";
			String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+"\\\", \\\"email\\\":\\\""+email+"\\\", \\\"referralCode\\\":\\\""+referralCode+"\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\""+user.getPassToken()+"\\\"}\"\n    \n}";
			
			/*String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+
					"\\\", \\\"email\\\":\\\""+email+"\\\", \\\"referralCode\\\":\\\""+referralCode+"\\\", \\\"password\\\":\\\""+
					password+"\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\""+user.getPassToken()+"\\\",\\\"organization\\\":\\\""+destOrganization.getName()+
					"\\\", \\\"abbr\\\":\\\""+destOrganization.getAbbreviation()+"\\\"}\"\n    \n}"; 
			System.out.println("body: "+reqBody);
			
			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder()
			  .url("https://api.branch.io/v1/url")
			  .post(body)
			  .addHeader("content-type", "application/json")
			  .addHeader("cache-control", "no-cache")
			  .addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118")
			  .build();
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: "+generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			String reqBodyUrl = "{\n    \"longUrl\": \"http://best-erp.com/ruralict/app/code/"+referralCode+ "\"\n\n}";
			OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					  .url("https://www.googleapis.com/urlshortener/v1/url?key="+API_KEY)
					  .post(bodyNew)
					  .addHeader("content-type", "application/json")
					  .addHeader("cache-control", "no-cache")
					  .addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b")
					  .build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: "+shortendUrl);
				
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text",
					"Hello from Lokacart! \nYou have been referred to " + destOrganization.getName() + " by "
							+ refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

			try {
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		else{
			System.out.println("here");
			int flag = 0;
			//User user = userService.getUserFromEmail(email);
			UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phonenumber);
			if(!userPhoneNumber.getUser().getName().equals(name)){
				try {
					responseJsonObject.put("response", "failure");
					responseJsonObject.put("error", "Name and Phone mismatch.");
					System.out.println("Existing Name and Phone No. mismatch.");
					return responseJsonObject;
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			else{
				User user = userPhoneNumber.getUser();
				//Existing user
				System.out.println("Existing user");
				int details = 0;
				if (user.getEmail() == null || user.getLastname() == null || user.getName() == null || user.getAddress() == null || user.getName() == null || user.getPincode() == null) {
					details = 1;
				}
				if(user.getEmail() == null || user.getEmail().split("@")[0].equals(phonenumber))
					email = user.getEmail();
				List<OrganizationMembership> list = user.getOrganizationMemberships();
				Iterator <OrganizationMembership> iterator = list.iterator();
				while (iterator.hasNext()) {
					OrganizationMembership organizationMembership = iterator.next();
					if (organizationMembership.getIsAdmin() == true)
						flag = 1;
					else if(organizationMembership.getOrganization() == organizationService.getOrganizationByAbbreviation(orgabbr))
						flag = 2;
				}
				if (flag == 1 ) {
					try {
						responseJsonObject.put("response", "failure");
						responseJsonObject.put("error", "Admin accounts not allowed.");
						System.out.println("Admin");
						return responseJsonObject;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(flag == 2){
					try{
						responseJsonObject.put("response", "failure");
						responseJsonObject.put("error", "Already a member.");
						System.out.println("Admin");
						return responseJsonObject;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (user.getPassToken() == null) {
					//Resetting password
					password = Integer.toString(random.nextInt(10));
					for (int count = 0; count < 9; ++count)
						password = password + random.nextInt(10);
					user.setPassToken(password);
					user.setSha256Password(passwordEncoder.encode(password));
					userService.addUser(user);
					System.out.println("pass token: "+password);
				}
				List <OrganizationMembership> memberList = user.getOrganizationMemberships();
				if(memberList.isEmpty() == false){
				Iterator <OrganizationMembership> memberIterator = memberList.iterator();
				while(memberIterator.hasNext()){
					OrganizationMembership organizationMembership = memberIterator.next();
					if (destOrganization == organizationMembership.getOrganization()){
						try {
							responseJsonObject.put("response", "failure");
						} catch (JSONException e) {
							e.printStackTrace();
						}
						return responseJsonObject;
					}
				}
				}
				User refUser = userService.getUserFromEmail(refreeEmail);
				Referral referral = new Referral();
				referral.setEmail(email);
				referral.setPhonenumber(phonenumber);
				referral.setOrganization(destOrganization);
				referral.setUser(refUser);
				System.out.println("reached here");
				String referralCode = String.valueOf(random.nextInt(999999));
				String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
				String android_uri_scheme = "lokacart://";
			    String android_package_name =  "com.mobile.ict.cart";
				Referral tempReferral = referralService.getByReferralCode(referralCode);
				while (tempReferral != null) {
					referralCode = String.valueOf(random.nextInt(999999));
					tempReferral = referralService.getByReferralCode(referralCode);
				}
				referral.setReferralCode(referralCode);
				String BRANCH_KEY= "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
				String android_deeplink = "lokacart://";
				String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+"\\\", \\\"email\\\":\\\""+email+"\\\", \\\"referralCode\\\":\\\""+referralCode+"\\\", \\\"details\\\":\\\""+details+"\\\", \\\"token\\\":\\\""+user.getPassToken()+"\\\"}\"\n    \n}";
				
				System.out.println("body: "+reqBody);							
				
				OkHttpClient client = new OkHttpClient();
				MediaType mediaType = MediaType.parse("application/json");
				okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
				Request request = new Request.Builder()
				  .url("https://api.branch.io/v1/url")
				  .post(body)
				  .addHeader("content-type", "application/json")
				  .addHeader("cache-control", "no-cache")
				  .addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118")
				  .build(); 
			/*
			 * 	MediaType mediaType = MediaType.parse("application/json");
				okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, "{\n\"branch_key\":\"key_live_feebgAAhbH9Tv85H5wLQhpdaefiZv5Dv\",\n\"data\":\"{\\\"name\\\": \\\"Alex\\\"}\"\n    \n}");
				Request request = new Request.Builder()
				  .url("https://api.branch.io/v1/url")
				  .post(body)
				  .addHeader("content-type", "application/json")
				  .addHeader("cache-control", "no-cache")
				  .addHeader("postman-token", "7a4da102-dc81-31b9-3028-aedee4a30025")
				  .build(); 
				try {
					Response response = client.newCall(request).execute();
					System.out.println(response.toString());
				    String jsonData = response.body().string();
					branchResponse = new JSONObject(jsonData);
					generatedUrl = branchResponse.getString("url");
					System.out.println("URL: "+generatedUrl);
					referral.setLink(generatedUrl);
					referralService.addReferral(referral);
	
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
				String reqBodyUrl = "{\n    \"longUrl\": \"http://best-erp.com/ruralict/app/code/"+referralCode+ "\"\n\n}";
				OkHttpClient clientNew = new OkHttpClient();
				MediaType mediaTypeNew = MediaType.parse("application/json");
				okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
				Request requestNew = new Request.Builder()
						  .url("https://www.googleapis.com/urlshortener/v1/url?key="+API_KEY)
						  .post(bodyNew)
						  .addHeader("content-type", "application/json")
						  .addHeader("cache-control", "no-cache")
						  .addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b")
						  .build();
				try {
					Response response = clientNew.newCall(requestNew).execute();
					System.out.println(response.toString());
					String jsonData = response.body().string();
					shortenerResponse = new JSONObject(jsonData);
					shortendUrl = shortenerResponse.getString("id");
					System.out.println("Shortened URL: "+shortendUrl);
					
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
								
				SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
				String authId = smsApiKeys.getAuthId();
				String authToken = smsApiKeys.getAuthToken();
				String sourceNumber = smsApiKeys.getSourceNumber();
				RestAPI api = new RestAPI(authId, authToken, "v1");

				LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
				parameters.put("src", sourceNumber);
				parameters.put("dst", phonenumber);
				parameters.put("text",
						"Hello from Lokacart! \nYou have been referred to " + destOrganization.getName() + " by "
								+ refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
				parameters.put("method", "GET");
				System.out.println("http://best-erp.com/ruralict/app/code/"+referralCode);
				try {
					MessageResponse msgResponse = api.sendMessage(parameters);
					System.out.println(msgResponse);
					System.out.println("Api ID : " + msgResponse.apiId);
					System.out.println("Message : " + msgResponse.message);
					if (msgResponse.serverCode == 202) {
						System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

					} else {
						System.out.println(msgResponse.error);
					}
				} catch (PlivoException e) {
					System.out.println(e.getLocalizedMessage());
				}

				try {
					responseJsonObject.put("response", "success");
				} catch (JSONException e) {
					e.printStackTrace();
				}
					
			}
			/*else {
				//send error
				try {
					responseJsonObject.put("response", "failure");
					responseJsonObject.put("error", "User with different details already exists.");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		return responseJsonObject;
	}*/

	
	
	//For individual
	
	@Transactional
	@RequestMapping(value = "/referCode", method = RequestMethod.POST)

	public String newReferralSys(@RequestBody String requestBody) {
		int noEmail=1;

		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null, branchResponse = null, shortenerResponse = null;
		String API_KEY = "AIzaSyBU0JpWx49ZIJHw5lF-lBJeAnL5ZpXktL0";
		String phonenumber = null, email = null, refCode = null, refreeEmail = null, password = null,
				generatedUrl = null, address = null, name = null, lastname = null;
		String shortendUrl = null;
		Random random = new Random();
		List<UserPhoneNumber> userPhoneNumbers = new ArrayList<UserPhoneNumber>();
		Organization destOrganization = null;
		try {
			jsonObject = new JSONObject(requestBody);
			System.out.println("JSON Object refer: " + jsonObject);
			phonenumber = jsonObject.getString("phonenumber");
			System.out.println(phonenumber);
			
			refCode = jsonObject.getString("refCode");
			try {
				Organization organization = organizationService.getOrganizationByRefCode(refCode);
				System.out.println(organization.getName());
				List<OrganizationMembership> list = organizationMembershipService
						.getOrganizationMembershipListByIsAdmin(organization, true);
				refreeEmail = list.get(0).getUser().getEmail();
			} catch (Exception e) {
				// fetch
				responseJsonObject.put("response", "No such code");
				return responseJsonObject.toString();
				
			}
			email = phonenumber+"@gmail.com";		
			/*
			try {
				email = jsonObject.getString("email");
				System.out.println(email);
				if(email.equals("")){
					noEmail=1;
					email=phonenumber+"@gmail.com";					
				}
			} catch (Exception e11) {
				
				// Do nothing
			}
			try {
				address = jsonObject.getString("address");
			} catch (Exception e1) {
				// Do nothing
			}
			try {
				name = jsonObject.getString("name");
			} catch (Exception e2) {
				// Do nothing
			}
			try {
				lastname = jsonObject.getString("lastname");
			} catch (Exception e3) {
				// Do nothing
			}*/
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				System.out.println("JSON exception caught");
				responseJsonObject.put("response", "Format error");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		System.out.println("JSON passed");

		System.out.println(userPhoneNumberService.findPreExistingPhoneNumber(phonenumber)+"   "+userService.getUserFromEmail(email));
		destOrganization = organizationService.getOrganizationByRefCode(refCode);
		if ((userPhoneNumberService.findPreExistingPhoneNumber(phonenumber) == true)) {
			// New user scenario
			if(userService.getUserFromEmail(email) != null){
				try {
					// TODO - Need to check this condition
					responseJsonObject.put("response", "Already a member");
					return responseJsonObject.toString();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("inside");
			User user = new User();
			user.setEmail(email);
			userService.addUser(user);
			System.out.println("user with only email added");

			UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
			userPhoneNumber.setPhoneNumber(phonenumber);
			userPhoneNumber.setPrimary(true);
			userPhoneNumber.setUser(user);
			userPhoneNumberService.addUserPhoneNumber(userPhoneNumber);
			userPhoneNumbers.add(userPhoneNumber);
			user.setUserPhoneNumbers(userPhoneNumbers);
			java.util.Date date = new java.util.Date();
			Timestamp currentTimestamp = new Timestamp(date.getTime());
			user.setTime(currentTimestamp);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			user.setWebLocale("en");
			user.setCallLocale("en");
			if (address != null)
				user.setAddress(address);
			if (name != null)
				user.setName(name);
			if (lastname != null)
				user.setLastname(lastname);
			String fname;
			if(noEmail!=1)
			{
				int i = email.indexOf("@");
				fname = email.substring(0, i);
			 
			}
			else
			{
				fname=" ";
			}
			password = fname+random.nextInt(1000);
			password = Integer.toString(random.nextInt(10));
			for (int count = 0; count < 9; ++count)
				password = password + random.nextInt(10);
			user.setPassToken(password);
			user.setSha256Password(passwordEncoder.encode(password));
			userService.addUser(user);
			System.out.println("user with only password added");

			User refUser = userService.getUserFromEmail(refreeEmail);
			System.out.println("User name: " + refUser.getName());
			Referral referral = new Referral();
			System.out.println("Should start if-else");
			if(userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail()!=null)
			{email = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail();
			System.out.println("I found this" + userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail());
			}
			System.out.println("see this" + email);
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser); // This is the referee
			String referralCode = String.valueOf(random.nextInt(999999));
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);
			String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String android_uri_scheme = "lokacart://";
			String android_package_name = "com.mobile.ict.cart";
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
			// String reqBody =
			// "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\"phonenumber\":\""+phonenumber+"\",\"email\":\""+email+"\",\"password\":\""+password+"\",\"referralCode\":\""+referralCode+"\"}\"";
			String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
					+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
					+ referralCode + "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\"" + user.getPassToken()
					+ "\\\"}\"\n    \n}";

			/*
			 * String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+
			 * "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+
			 * "\\\", \\\"email\\\":\\\""+email+
			 * "\\\", \\\"referralCode\\\":\\\""+referralCode+
			 * "\\\", \\\"password\\\":\\\""+ password+
			 * "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\""
			 * +user.getPassToken()+"\\\",\\\"organization\\\":\\\""+
			 * destOrganization.getName()+ "\\\", \\\"abbr\\\":\\\""
			 * +destOrganization.getAbbreviation()+"\\\"}\"\n    \n}";
			 */
			System.out.println("body: " + reqBody);

			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: " + generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			/* //bestB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://best-erp.com/ruralict/app/code/" + referralCode
					+ "\"\n\n}";
			*/ //bestE
			
			//ictB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://ruralict.cse.iitb.ac.in/ruralict/app/code/" + referralCode
					+ "\"\n\n}";
			//ictE			
			
			OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: " + shortendUrl);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + destOrganization.getName()
					+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

			try {
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			int flag = 0;
			User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
			// Existing user
			int details = 0;
			if (user.getEmail() == null || user.getLastname() == null || user.getName() == null
					|| user.getAddress() == null || user.getName() == null || user.getPincode() == null) {
				details = 1;
			}
			List<OrganizationMembership> list = user.getOrganizationMemberships();
			Iterator<OrganizationMembership> iterator = list.iterator();
			while (iterator.hasNext()) {
				OrganizationMembership organizationMembership = iterator.next();
				if (organizationMembership.getIsAdmin() == true)
					flag = 1;
			}
			if (flag == 1) {
				try {
					responseJsonObject.put("response", "admin");
					return responseJsonObject.toString();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (user.getPassToken() == null) {
				// Resetting password
				password = Integer.toString(random.nextInt(10));
				for (int count = 0; count < 9; ++count)
					password = password + random.nextInt(10);
				user.setPassToken(password);
				user.setSha256Password(passwordEncoder.encode(password));
				userService.addUser(user);
				System.out.println("pass token: " + password);
			}
			List<OrganizationMembership> memberList = user.getOrganizationMemberships();
			if (memberList.isEmpty() == false) {
				Iterator<OrganizationMembership> memberIterator = memberList.iterator();
				while (memberIterator.hasNext()) {
					OrganizationMembership organizationMembership = memberIterator.next();
					if (destOrganization == organizationMembership.getOrganization()) {
						try {
							responseJsonObject.put("response", "Already a member");
						} catch (JSONException e) {
							e.printStackTrace();
						}
						return responseJsonObject.toString();
					}
				}
			}
			User refUser = userService.getUserFromEmail(refreeEmail);
			Referral referral = new Referral();
			System.out.println("Should start if-else");
			if(userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail()!=null)
			{email = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail();
			System.out.println("I found this" + userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail());
			}
			System.out.println("see this" + email);
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser);
			String referralCode = String.valueOf(random.nextInt(999999));
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
			String android_uri_scheme = "lokacart://";
			String android_package_name = "com.mobile.ict.cart";
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);
			String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
					+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
					+ referralCode + "\\\", \\\"details\\\":\\\"" + details + "\\\", \\\"token\\\":\\\""
					+ user.getPassToken() + "\\\"}\"\n    \n}";

			System.out.println("body: " + reqBody);

			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
			/*
			 * MediaType mediaType = MediaType.parse("application/json");
			 * okhttp3.RequestBody body =
			 * okhttp3.RequestBody.create(mediaType,
			 * "{\n\"branch_key\":\"key_live_feebgAAhbH9Tv85H5wLQhpdaefiZv5Dv\",\n\"data\":\"{\\\"name\\\": \\\"Alex\\\"}\"\n    \n}"
			 * ); Request request = new Request.Builder()
			 * .url("https://api.branch.io/v1/url") .post(body)
			 * .addHeader("content-type", "application/json")
			 * .addHeader("cache-control", "no-cache")
			 * .addHeader("postman-token",
			 * "7a4da102-dc81-31b9-3028-aedee4a30025") .build();
			 */
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: " + generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			/* //bestB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://best-erp.com/ruralict/app/code/"
					+ referralCode + "\"\n\n}";
			*/ //bestE
			
			//ictB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://ruralict.cse.iitb.ac.in/ruralict/app/code/"
					+ referralCode + "\"\n\n}";
			//ictE
			
			OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: " + shortendUrl);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}

			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + destOrganization.getName()
					+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			System.out.println("http://best-erp.com/ruralict/app/code/" + referralCode);
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

			try {
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return responseJsonObject.toString();
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/refer", method = RequestMethod.POST, produces = "application/json")

	public @ResponseBody String newReferral(@RequestBody String requestBody) {
		int noEmail=0;

		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null, branchResponse = null, shortenerResponse = null;
		String API_KEY = "AIzaSyBU0JpWx49ZIJHw5lF-lBJeAnL5ZpXktL0";
		String phonenumber = null, email = null, orgabbr = null, refreeEmail = null, password = null,
				generatedUrl = null, address = null, name = null, lastname = null;
		String shortendUrl = null;
		Random random = new Random();
		List<UserPhoneNumber> userPhoneNumbers = new ArrayList<UserPhoneNumber>();
		Organization destOrganization = null;
		try {
			jsonObject = new JSONObject(requestBody);
			System.out.println("JSON Object refer: " + jsonObject);
			phonenumber = jsonObject.getString("phonenumber");
			System.out.println(phonenumber);
			
			orgabbr = jsonObject.getString("abbr");
			System.out.println(orgabbr);
			try {
				refreeEmail = jsonObject.getString("refemail");
			} catch (Exception e) {
				// fetch
				Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
				System.out.println(organization.getName());
				List<OrganizationMembership> list = organizationMembershipService
						.getOrganizationMembershipListByIsAdmin(organization, true);
				refreeEmail = list.get(0).getUser().getEmail();
			}
			try {
				email = jsonObject.getString("email");
				System.out.println(email);
				if(email.equals("")){
					noEmail=1;
					email=phonenumber+"@gmail.com";					
				}
			} catch (Exception e11) {
				noEmail=1;
				email=phonenumber+"@gmail.com";
				// Do nothing
			}

			try {
				address = jsonObject.getString("address");
			} catch (Exception e1) {
				// Do nothing
			}
			try {
				name = jsonObject.getString("name");
			} catch (Exception e2) {
				// Do nothing
			}
			try {
				lastname = jsonObject.getString("lastname");
			} catch (Exception e3) {
				// Do nothing
			}
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				System.out.println("JSON exception caught");
				responseJsonObject.put("response", "failure");
				responseJsonObject.put("error", "Format error");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		System.out.println("JSON passed");

		System.out.println(userPhoneNumberService.findPreExistingPhoneNumber(phonenumber)+"   "+userService.getUserFromEmail(email));
		destOrganization = organizationService.getOrganizationByAbbreviation(orgabbr);
		if ((userPhoneNumberService.findPreExistingPhoneNumber(phonenumber) == true)) {
			// New user scenario
			if(userService.getUserFromEmail(email) != null){
				try {
					responseJsonObject.put("response", "failure");
					responseJsonObject.put("error", "User with same email already exists");
					return responseJsonObject.toString();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("inside");
			User user = new User();
			user.setEmail(email);
			userService.addUser(user);
			System.out.println("user with only email added");

			UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
			userPhoneNumber.setPhoneNumber(phonenumber);
			userPhoneNumber.setPrimary(true);
			userPhoneNumber.setUser(user);
			userPhoneNumberService.addUserPhoneNumber(userPhoneNumber);
			userPhoneNumbers.add(userPhoneNumber);
			user.setUserPhoneNumbers(userPhoneNumbers);
			java.util.Date date = new java.util.Date();
			Timestamp currentTimestamp = new Timestamp(date.getTime());
			user.setTime(currentTimestamp);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			user.setWebLocale("en");
			user.setCallLocale("en");
			if (address != null)
				user.setAddress(address);
			if (name != null)
				user.setName(name);
			if (lastname != null)
				user.setLastname(lastname);
			String fname;
			if(noEmail!=1)
			{
				int i = email.indexOf("@");
				fname = email.substring(0, i);
			 
			}
			else
			{
				fname=" ";
			}
			password = fname+random.nextInt(1000);
			password = Integer.toString(random.nextInt(10));
			for (int count = 0; count < 9; ++count)
				password = password + random.nextInt(10);
			user.setPassToken(password);
			user.setSha256Password(passwordEncoder.encode(password));
			userService.addUser(user);
			System.out.println("user with only password added");

			User refUser = userService.getUserFromEmail(refreeEmail);
			System.out.println("User name: " + refUser.getName());
			Referral referral = new Referral();
			System.out.println("Should start if-else");
			if(userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail()!=null)
			{email = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail();
			System.out.println("I found this" + userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail());
			}
			System.out.println("see this" + email);
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser); // This is the referee
			String referralCode = String.valueOf(random.nextInt(999999));
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);
			String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String android_uri_scheme = "lokacart://";
			String android_package_name = "com.mobile.ict.cart";
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
			// String reqBody =
			// "{\n\"branch_key\":\""+BRANCH_KEY+"\",\n\"data\":\"{\"phonenumber\":\""+phonenumber+"\",\"email\":\""+email+"\",\"password\":\""+password+"\",\"referralCode\":\""+referralCode+"\"}\"";
			String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
					+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
					+ referralCode + "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\"" + user.getPassToken()
					+ "\\\"}\"\n    \n}";

			/*
			 * String reqBody = "{\n\"branch_key\":\""+BRANCH_KEY+
			 * "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""+phonenumber+
			 * "\\\", \\\"email\\\":\\\""+email+
			 * "\\\", \\\"referralCode\\\":\\\""+referralCode+
			 * "\\\", \\\"password\\\":\\\""+ password+
			 * "\\\", \\\"details\\\":\\\"1\\\", \\\"token\\\":\\\""
			 * +user.getPassToken()+"\\\",\\\"organization\\\":\\\""+
			 * destOrganization.getName()+ "\\\", \\\"abbr\\\":\\\""
			 * +destOrganization.getAbbreviation()+"\\\"}\"\n    \n}";
			 */
			System.out.println("body: " + reqBody);

			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: " + generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			/* //bestB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://best-erp.com/ruralict/app/code/" + referralCode
					+ "\"\n\n}";
			*/ //bestE
			
			//ictB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://ruralict.cse.iitb.ac.in/ruralict/app/code/" + referralCode
					+ "\"\n\n}";
			//ictE
			
			OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: " + shortendUrl);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + destOrganization.getName()
					+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

			try {
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			int flag = 0;
			User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
			// Existing user
			int details = 0;
			if (user.getEmail() == null || user.getLastname() == null || user.getName() == null
					|| user.getAddress() == null || user.getName() == null || user.getPincode() == null) {
				details = 1;
			}
			List<OrganizationMembership> list = user.getOrganizationMemberships();
			Iterator<OrganizationMembership> iterator = list.iterator();
			while (iterator.hasNext()) {
				OrganizationMembership organizationMembership = iterator.next();
				if (organizationMembership.getIsAdmin() == true)
					flag = 1;
			}
			if (flag == 1) {
				try {
					responseJsonObject.put("response", "failure");
					responseJsonObject.put("error", "Admin accounts cannot be used on the consumer app");
					return responseJsonObject.toString();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (user.getPassToken() == null) {
				// Resetting password
				password = Integer.toString(random.nextInt(10));
				for (int count = 0; count < 9; ++count)
					password = password + random.nextInt(10);
				user.setPassToken(password);
				user.setSha256Password(passwordEncoder.encode(password));
				userService.addUser(user);
				System.out.println("pass token: " + password);
			}
			List<OrganizationMembership> memberList = user.getOrganizationMemberships();
			if (memberList.isEmpty() == false) {
				Iterator<OrganizationMembership> memberIterator = memberList.iterator();
				while (memberIterator.hasNext()) {
					OrganizationMembership organizationMembership = memberIterator.next();
					if (destOrganization == organizationMembership.getOrganization()) {
						try {
							responseJsonObject.put("response", "failure");
							responseJsonObject.put("error", "Already a member");
						} catch (JSONException e) {
							e.printStackTrace();
						}
						return responseJsonObject.toString();
					}
				}
			}
			User refUser = userService.getUserFromEmail(refreeEmail);
			Referral referral = new Referral();
			System.out.println("Should start if-else");
			if(userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail()!=null)
			{email = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail();
			System.out.println("I found this" + userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail());
			}
			System.out.println("see this" + email);
			referral.setEmail(email);
			referral.setPhonenumber(phonenumber);
			referral.setOrganization(destOrganization);
			referral.setUser(refUser);
			String referralCode = String.valueOf(random.nextInt(999999));
			String android_url = "https://play.google.com/store/apps/details?id=com.mobile.ict.cart";
			String android_uri_scheme = "lokacart://";
			String android_package_name = "com.mobile.ict.cart";
			Referral tempReferral = referralService.getByReferralCode(referralCode);
			while (tempReferral != null) {
				referralCode = String.valueOf(random.nextInt(999999));
				tempReferral = referralService.getByReferralCode(referralCode);
			}
			referral.setReferralCode(referralCode);
			String BRANCH_KEY = "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			String android_deeplink = "lokacart://";
			String reqBody = "{\n\"branch_key\":\"" + BRANCH_KEY + "\",\n\"data\":\"{\\\"phonenumber\\\":\\\""
					+ phonenumber + "\\\", \\\"email\\\":\\\"" + email + "\\\", \\\"referralCode\\\":\\\""
					+ referralCode + "\\\", \\\"details\\\":\\\"" + details + "\\\", \\\"token\\\":\\\""
					+ user.getPassToken() + "\\\"}\"\n    \n}";

			System.out.println("body: " + reqBody);

			OkHttpClient client = new OkHttpClient();
			MediaType mediaType = MediaType.parse("application/json");
			okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, reqBody);
			Request request = new Request.Builder().url("https://api.branch.io/v1/url").post(body)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "c366fc2b-523b-bcba-0a8b-bda6e43a2118").build();
			/*
			 * MediaType mediaType = MediaType.parse("application/json");
			 * okhttp3.RequestBody body =
			 * okhttp3.RequestBody.create(mediaType,
			 * "{\n\"branch_key\":\"key_live_feebgAAhbH9Tv85H5wLQhpdaefiZv5Dv\",\n\"data\":\"{\\\"name\\\": \\\"Alex\\\"}\"\n    \n}"
			 * ); Request request = new Request.Builder()
			 * .url("https://api.branch.io/v1/url") .post(body)
			 * .addHeader("content-type", "application/json")
			 * .addHeader("cache-control", "no-cache")
			 * .addHeader("postman-token",
			 * "7a4da102-dc81-31b9-3028-aedee4a30025") .build();
			 */
			try {
				Response response = client.newCall(request).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				branchResponse = new JSONObject(jsonData);
				generatedUrl = branchResponse.getString("url");
				System.out.println("URL: " + generatedUrl);
				referral.setLink(generatedUrl);
				referralService.addReferral(referral);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			/* //bestB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://best-erp.com/ruralict/app/code/" + referralCode
					+ "\"\n\n}";
			*/ //bestE
			
			//ictB
			String reqBodyUrl = "{\n    \"longUrl\": \"http://ruralict.cse.iitb.ac.in/ruralict/app/code/" + referralCode
					+ "\"\n\n}";
			//ictE
			OkHttpClient clientNew = new OkHttpClient();
			MediaType mediaTypeNew = MediaType.parse("application/json");
			okhttp3.RequestBody bodyNew = okhttp3.RequestBody.create(mediaTypeNew, reqBodyUrl);
			Request requestNew = new Request.Builder()
					.url("https://www.googleapis.com/urlshortener/v1/url?key=" + API_KEY).post(bodyNew)
					.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
					.addHeader("postman-token", "71e06008-92c4-c2a3-5c6a-e718f5bc3e5b").build();
			try {
				Response response = clientNew.newCall(requestNew).execute();
				System.out.println(response.toString());
				String jsonData = response.body().string();
				shortenerResponse = new JSONObject(jsonData);
				shortendUrl = shortenerResponse.getString("id");
				System.out.println("Shortened URL: " + shortendUrl);

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}

			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(destOrganization);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");

			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", phonenumber);
			parameters.put("text", "Hello from Lokacart! \nYou have been referred to " + destOrganization.getName()
					+ " by " + refUser.getName() + "\nClick here to accept and use: " + shortendUrl);
			parameters.put("method", "GET");
			System.out.println("http://best-erp.com/ruralict/app/code/" + referralCode);
			try {
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}

			try {
				responseJsonObject.put("response", "success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return responseJsonObject.toString();
	}
	

}
