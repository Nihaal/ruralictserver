package app.business.controllers.rest;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.LcpOrderService;
import app.business.services.LcpProductService;
import app.business.services.OrderItemService;
import app.business.services.OrderService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.business.services.UserService;
import app.business.services.UserViewService;
import app.business.services.message.MessageService;
import app.data.repositories.LcpOrderItemRepository;
import app.data.repositories.OrderRepository;
import app.entities.Group;
import app.entities.LcpOrder;
import app.entities.LcpOrderItem;
import app.entities.LcpProduct;
import app.entities.Order;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.Product;
import app.entities.ProductType;
import app.entities.User;
import app.entities.message.Message;

@RestController
@RequestMapping("/api")
public class DashboardRestController {


	@Autowired
	OrderRepository orderRepository;
	@Autowired
	OrderItemService orderItemService;
	@Autowired
	OrganizationService organizationService;
	@Autowired
	MessageService messageService;
	@Autowired
	OrderService orderService;
	@Autowired
	UserViewService userViewService;
	@Autowired
	UserService userService;
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	
	@Autowired 
	ProductService productService;
	
	@Autowired
	ProductTypeService productTypeService;
	
	@Autowired
	LcpProductService lcpproductService;
	
	@Autowired
	LcpOrderItemRepository lcpOrderItemRepository;

	@Autowired
	LcpOrderService lcpOrderService;
	
	@Transactional
	@RequestMapping(value = "/dashboard",method = RequestMethod.GET )
	public @ResponseBody String dashBoard(@RequestParam(value="orgabbr") String orgabbr) throws ParseException {
		System.out.println(orgabbr+"in dahsboard function");
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray productDetailsArray = new JSONArray();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		Group g= organizationService.getParentGroup(organization);
		List<ProductType> productTypeList = productTypeService.getAllByOrganisation(organization);
		Iterator <ProductType> iterator = productTypeList.iterator();
		while(iterator.hasNext()) {
			JSONArray jsonArray = new JSONArray();
			ProductType productType = iterator.next();
			JSONObject jsonObject = new JSONObject();
			List <Product> productList = productService.getProductListByType(productType);
			Iterator <Product> productIter = productList.iterator();
			while (productIter.hasNext()) {
				Product product = productIter.next();
				JSONObject productObject = new JSONObject();
				try {
				productObject.put("product_name", product.getName());
				productObject.put("price", product.getUnitRate());
				productObject.put("quantity", Integer.toString(product.getQuantity()));
				productObject.put("audioUrl", product.getAudioUrl());
				productObject.put("imageUrl", product.getImageUrl());
				if(product.getDescription() == null)
					productObject.put("description", "");
				else
					productObject.put("description", product.getDescription());	
				
				jsonArray.put(productObject);
				}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}
			try {
				jsonObject.put(productType.getName(), jsonArray);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			productDetailsArray.put(jsonObject);
		}
		List<Order> paidOrders = orderRepository.findByOrganizationAndStatusAndText(organization, "delivered", "Paid");
		List<Order> unpaidOrders = orderRepository.findByOrganizationAndStatusAndText(organization, "delivered", "Unpaid");

		List<Order> deliveredOrders = orderService.getOrderByOrganizatonDeliveredSorted(organization);
		List<Message> messageapppro=messageService.getMessageListByOrderStatus(g, "binary", "processed");
		List<Message> messageappnew=messageService.getMessageListByOrderStatus(g, "binary", "saved");
		List<Message> messageappcan=messageService.getMessageListByOrderStatus(g, "binary", "cancelled");
		List<OrganizationMembership> membershipListpending = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 0);
		List<OrganizationMembership> membershipListapproved = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 1);
		//HashMap<String, Integer> dashmap = new HashMap<String, Integer>();
		try{
		jsonResponseObject.put("saved", messageappnew.size());
		jsonResponseObject.put("paid", paidOrders.size());
		jsonResponseObject.put("unpaid", unpaidOrders.size());
		jsonResponseObject.put("processed", messageapppro.size());
		jsonResponseObject.put("cancelled", messageappcan.size());
		jsonResponseObject.put("products", productDetailsArray);
		jsonResponseObject.put("delivered", deliveredOrders.size());
		
		jsonResponseObject.put("totalUsers", membershipListapproved.size());
		jsonResponseObject.put("pendingUsers", membershipListpending.size());
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		int todayUsers=0;
		for(OrganizationMembership membership : membershipListpending)
		{

			User user = membership.getUser();
		
			try
			{
				Timestamp time = user.getTime();
				
				Calendar cal= Calendar.getInstance();
				cal.clear(Calendar.HOUR_OF_DAY);
				cal.clear(Calendar.AM_PM);
				cal.clear(Calendar.MINUTE);
				cal.clear(Calendar.SECOND);
				cal.clear(Calendar.MILLISECOND);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			    java.util.Date dateWithoutTime = sdf.parse(sdf.format(new java.util.Date()));
				if(time.after(dateWithoutTime))
				{
					todayUsers=todayUsers+1;
				}
			}
			catch(NullPointerException | ParseException e)
			{
				System.out.println("User name not having his timestamp recorded is: " + user.getName() + " having userID: " + user.getUserId());
			}
		}
		try {
			jsonResponseObject.put("newUsersToday",todayUsers);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		System.out.println(jsonResponseObject.toString());
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/dashboardLcp",method = RequestMethod.GET )
	public @ResponseBody String dashBoardLcp(@RequestParam(value="orgabbr") String orgabbr) throws ParseException {
		System.out.println(orgabbr+"in dahsboard function");
		JSONObject jsonResponseObject = new JSONObject();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		List<LcpProduct> prodList = lcpproductService.getLcpProductListAdmin(organization);
		List<LcpOrderItem> orderList = lcpOrderItemRepository.findByOrganization(organization);	
		int saved = 0, paid = 0, unpaid = 0, processed = 0, cancelled = 0, delivered = 0;
		Iterator<LcpOrderItem> iterator = orderList.iterator();
		while(iterator.hasNext())
		{
			LcpOrderItem orderItem = iterator.next();
			if(orderItem.getStatus().equals("placed"))
				saved++;
			else if(orderItem.getStatus().equals("processed"))
				processed++;
			else if(orderItem.getStatus().equals("cancelled"))
				cancelled++;
			else if(orderItem.getStatus().equals("delivered")){
				if(orderItem.getIsPaid() == true)
					paid++;
				else if(orderItem.getIsPaid() == false)
					unpaid++;
				delivered++;
			}
		}
		try{
			jsonResponseObject.put("saved", saved);
			jsonResponseObject.put("paid", paid);
			jsonResponseObject.put("unpaid", unpaid);
			jsonResponseObject.put("processed", processed);
			jsonResponseObject.put("cancelled", cancelled);
			jsonResponseObject.put("products", prodList.size());
			jsonResponseObject.put("delivered", delivered);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
}