package app.business.controllers.rest;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.GroupMembershipService;
import app.business.services.GroupService;
import app.business.services.OrderService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.business.services.UserViewService;
import app.business.services.VoiceService;
import app.data.repositories.BillLayoutSettingsRepository;
import app.data.repositories.GroupRepository;
import app.data.repositories.OrganizationMembershipRepository;
import app.data.repositories.OrganizationRepository;
import app.data.repositories.SmsApiKeysRepository;
import app.data.repositories.UserPhoneNumberRepository;
import app.data.repositories.UserRepository;
import app.data.repositories.WelcomeMessageRepository;
import app.entities.BillLayoutSettings;
import app.entities.Group;
import app.entities.GroupMembership;
import app.entities.Order;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.SmsApiKeys;
import app.entities.User;
import app.entities.UserPhoneNumber;
import app.entities.WelcomeMessage;
import app.util.SendMail;
import app.util.UserManage;

@RestController
@RequestMapping("/api/{org}/manageUsers")
public class ManageUsersRestController {
	@Autowired
	UserViewService userViewService;

	@Autowired
	UserService userService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	OrganizationMembershipService organizationMembershipService;

	@Autowired
	GroupService groupService;

	@Autowired
	GroupMembershipService groupMembershipService;

	@Autowired
	UserPhoneNumberService userPhoneNumberService;	
	
	@Autowired
	OrganizationRepository organizationRepository;
	
	@Autowired
	WelcomeMessageRepository messageRepository;	
	
	@Autowired
	VoiceService voiService;
	
	@Autowired
	GroupRepository groupRepository;
	
	@Autowired
	BillLayoutSettingsRepository billLayoutSettingRepo;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserPhoneNumberRepository userPhoneNumberRepository; 
	
	@Autowired
	OrganizationMembershipRepository organizationMemberRepository;
	
	@Autowired
	SmsApiKeysRepository smsRepository;
	

	@Transactional
	@RequestMapping(value="/organizationAdd", method=RequestMethod.POST)
	//@PreAuthorize("hasRole('ADMIN'+#org)")
	public String organizationPage(@RequestBody String requestBody) throws Exception  {
		JSONObject jsonObject = new JSONObject(requestBody);
		User user = new User();
		UserPhoneNumber userPhoneNumber = new UserPhoneNumber();
		List<UserPhoneNumber> userPhoneNumbers = new ArrayList<UserPhoneNumber>();
		String phonenumber = null;
		String orgName = null;
		String plivoAuthID = null;
		String plivoAuthToken = null;
		String address = null;
		String password = null;
		String name = null;
		String email = null;
		String abbr = null;
		String placeAddress = null;
		String ivrNumber = null;
		List <UserPhoneNumber> PhoneNumbers = userPhoneNumberRepository.findAll();
		JSONObject response = new JSONObject();
		try {

			phonenumber = jsonObject.getString("phonenumber");
			abbr = jsonObject.getString("abbreviation").toUpperCase();
			plivoAuthID = jsonObject.getString("plivoAuthID");
			plivoAuthToken = jsonObject.getString("plivoAuthToken");
			placeAddress = jsonObject.getString("orgAddress");	
			orgName = jsonObject.getString("organizationName");
			address = jsonObject.getString("userAddress");
			password = jsonObject.getString("password");
			name = jsonObject.getString("username");
			email = jsonObject.getString("email");
			ivrNumber = jsonObject.getString("ivrNumber");
			
			if(orgName.length()>255){
				response.put("status", "error");
				response.put("reason", "Organization name should be less than 255 characters"); 
				return response.toString();
			}
			if(address.length()>201){
				response.put("status", "error");
				response.put("reason", "Organization address should be less than 201 characters"); 
				return response.toString();
			}
			if(userService.getUserFromEmail(email) != null){
				response.put("status", "error");
				response.put("reason", "Email number already exists."); 
				return response.toString();
			}
			if(organizationService.getOrganizationByIVRS(ivrNumber) != null){
				response.put("status", "error");
				response.put("reason", "IVRS number already exists."); 
				return response.toString();
			}
			if(!userPhoneNumberService.findPreExistingPhoneNumber("91"+phonenumber)){
				response.put("status", "error");
				response.put("reason", "Phone number already exists."); 
				return response.toString();
			}
			if(organizationService.getOrganizationByAbbreviation(abbr) != null){
				response.put("status", "error");
				response.put("reason", "Abbreviation for Organization already exists."); 
				return response.toString();
			}
			
			if(abbr.length()<1 || email.length()<1 || ivrNumber.length()<1 || name.length()<1 || password.length()<1 || phonenumber.length()<1 || orgName.length()<1 || placeAddress.length()<1 || address.length()<1){
				response.put("status", "error");
				System.out.println("In here");
				response.put("reason", "Please fill in all the required details."); 
				return response.toString();
			}
		} catch (Exception e) {
			response.put("status", "error");
			System.out.println("In here");
			response.put("reason", "Please fill in all the required details."); 
			return response.toString();
		}
		organizationRepository.save(new Organization(orgName,abbr,abbr,placeAddress,phonenumber,organizationService.getOrganizationById(1),"12","12",null,null));
		messageRepository.save(new WelcomeMessage(organizationService.getOrganizationByAbbreviation(abbr), "en", voiService.getVoice(1)));
		messageRepository.save(new WelcomeMessage(organizationService.getOrganizationByAbbreviation(abbr), "hi", voiService.getVoice(2)));
		messageRepository.save(new WelcomeMessage(organizationService.getOrganizationByAbbreviation(abbr), "mr", voiService.getVoice(3)));
		groupRepository.save(new Group(organizationService.getOrganizationByAbbreviation(abbr),"Parent Group",null));		
		billLayoutSettingRepo.save(new BillLayoutSettings(organizationService.getOrganizationByAbbreviation(abbr),organizationService.getOrganizationByAbbreviation(abbr).getName(),"Thank You!"));
		user.setAddress(address);
		user.setCallLocale("en");
		user.setEmail(email);
		user.setSha256Password(passwordEncoder.encode(password));
		user.setName(name);
		user.setTextbroadcastlimit(0);
		user.setVoicebroadcastlimit(0);
		user = userRepository.save(user);
		System.out.println("User phone number is being saved");
		phonenumber = "91" + phonenumber;
		userPhoneNumber.setPhoneNumber(phonenumber);
		userPhoneNumber.setPrimary(true);
		userPhoneNumber.setUser(user);
		userPhoneNumber = userPhoneNumberRepository.save(userPhoneNumber);
		userPhoneNumbers.add(userPhoneNumber);
		user.setUserPhoneNumbers(userPhoneNumbers);
		System.out.println("User phone number is  saved");
		userRepository.save(user);
		System.out.println("User phone no is: " + phonenumber);
		User user1 = userRepository.findByuserPhoneNumbers_phoneNumber(phonenumber);
		Organization organization = new Organization();
		java.util.Date date = new java.util.Date();
		Timestamp currentTimestamp = new Timestamp(date.getTime());
		user1.setTime(currentTimestamp);
		List<OrganizationMembership> organizationMemberships = new ArrayList<OrganizationMembership>();
		organization = organizationService.getOrganizationByAbbreviation(abbr);
		OrganizationMembership organizationMembership = new OrganizationMembership();
		organizationMembership.setOrganization(organization);
		organizationMembership.setUser(user1);
		organizationMembership.setIsAdmin(true);
		organizationMembership.setIsPublisher(false);
		organizationMembership.setStatus(1);
		organizationMembership = organizationMemberRepository.save(organizationMembership);
		organizationMemberships.add(organizationMembership);
		user1.setOrganizationMemberships(organizationMemberships);
		userRepository.save(user1);
		groupMembershipService.addParentGroupMembership(organization, user1);
		if(plivoAuthID.length()>1 && plivoAuthToken.length()>1){
			String result = addSmsApis(abbr, plivoAuthID, plivoAuthToken, phonenumber);
			if(result.equals("failure")){
				response.put("status", "error");
				response.put("reason", "Error adding Plivo account."); 
				return response.toString();
			}
		}
		response.put("status", "Success");
		//response.put("reason", "Please fill in all the required details.");
		return response.toString(); 
		
		
	}
	
	@RequestMapping(value="/addPlivoAccount", method=RequestMethod.POST)
	public String plivoAddition(@RequestBody String requestBody){
		JSONObject request = null;
		JSONObject response = new JSONObject();
		String abbr = null, plivoAuthID = null, plivoAuthToken = null, phonenumber = null;
		try{
			request = new JSONObject(requestBody);
			abbr = request.getString("abbr");
			plivoAuthID = request.getString("plivoAuthID");
			plivoAuthToken = request.getString("plivoAuthToken");
			phonenumber = request.getString("phonenumber");
			if(abbr.length()<1 || plivoAuthID.length()<1 || plivoAuthToken.length()<1 || phonenumber.length()<1){
				response.put("status", "error");
				response.put("reason", "Some fields are invalid.");
				return response.toString();
			}
			else{
				String resp = addSmsApis(abbr, plivoAuthID, plivoAuthToken, phonenumber);
				if(resp.equals("success")){
					response.put("status", "success");					
				}
				else if(resp.equals("failure")){
					response.put("status", "error");
					response.put("reason", "Please try again.");
				}
			}
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		return response.toString();
	}
	
	public String addSmsApis(String abbr, String plivoAuthID, String plivoAuthToken, String phonenumber){
		Organization org = organizationService.getOrganizationByAbbreviation(abbr);
		try{
			smsRepository.save(new SmsApiKeys(plivoAuthID, plivoAuthToken, phonenumber, org));
		}
		catch(Exception e){
			return "failure";
		}
		return "success";
	}
	
	@Transactional
	@RequestMapping(value = "/userList", method = RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	public String getUserListJson(@PathVariable String org) {
		//List<UserManage> userrows = new ArrayList<UserManage>();
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		Organization organization = organizationService.getOrganizationByAbbreviation(org);

		List<OrganizationMembership> membershipList = organizationMembershipService.getOrganizationMembershipListByStatusSorted(organization, 1);

		for(OrganizationMembership membership : membershipList)
		{
			System.out.println("New user!!!");
			User user = membership.getUser();

			try
			{
				// Get required attributes for each user
				int manageUserID = user.getUserId();
				String name = user.getName();
				String email = user.getEmail();
				String phone = userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber();
				String role  = userService.getUserRole(user, organization);
				String address = user.getAddress();
				Timestamp time= user.getTime();
				String lastname = "",pincode ="";
				try {
					lastname = user.getLastname();
					pincode = user.getPincode();
				}
				catch(Exception e) {
					
				}
				JSONObject object = new JSONObject();
				try {
				object.put("userID", manageUserID);
				object.put("name", name);
				object.put("email", email);
				object.put("phone", phone);
				object.put("role", role);
				object.put("address", address);
				object.put("time", time);
				try{
					if(!lastname.equals("0"))
					object.put("lastname", lastname);
					if(!pincode.equals("0"))
					object.put("pincode", pincode);
					}
					catch(Exception e ){
					}
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				// Create the UserManage Object and add it to the list
			//	UserManage userrow = new UserManage(manageUserID, name, email, phone, role, address, time);
				System.out.println(name);
				if(name.length() != 0)
					if(name != null)
						jsonArray.put(object);
					
			}
			catch(NullPointerException e)
			{
				System.out.println("User name not having his phone number is: " + user.getName() + " having userID: " + user.getUserId());
			}
		}
		try {
			jsonResponseObject.put("users", jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();		
	}
	
	@Transactional
	@RequestMapping(value="/userApprovalList", method=RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	public String getUserApprovalListJson(@PathVariable String org) {
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		List<OrganizationMembership> membershipList = organizationMembershipService.getOrganizationMembershipListByStatusSorted(organization, 0);
		for(OrganizationMembership membership : membershipList)
		{

			User user = membership.getUser();			
			try
			{
				// Get required attributes for each user
				int manageUserID = user.getUserId();
				String name = user.getName();
				String email = user.getEmail();
				String phone = userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber();
				String role  = userService.getUserRole(user, organization);
				String address = user.getAddress();
				Timestamp time = user.getTime();
				String lastname = null, pincode=null;
				try{
				 lastname = user.getLastname();
				 pincode = user.getPincode();
				}
				catch(Exception e ) {
					
				}
				// Create the UserManage Object and add it to the list
				JSONObject object = new JSONObject();
				try {
					object.put("userID", manageUserID);
					object.put("name", name);
					object.put("email", email);
					object.put("phone", phone);
					object.put("role", role);
					object.put("address", address);
					object.put("time", time);
					if (lastname != null && !lastname.equals("0"))
					object.put("lastname", lastname);
					if(pincode != null && !pincode.equals("0"))
					object.put("pincode", pincode);
				
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				jsonArray.put(object);
			}
			catch(NullPointerException e)
			{
				System.out.println("User name not having his phone number is: " + user.getName() + " having userID: " + user.getUserId());
			}
		}
		try {
			jsonResponseObject.put("users",jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	@Transactional
	// Method to the get all the User list in the 'Manage Users' tab
	@RequestMapping(value="/getUsersList", method=RequestMethod.GET, produces = "application/json")

	public List<UserManage> getUsersList(@PathVariable String org) {

		List<UserManage> userrows = new ArrayList<UserManage>();

		Organization organization = organizationService.getOrganizationByAbbreviation(org);

		List<OrganizationMembership> membershipList = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 1);

		for(OrganizationMembership membership : membershipList)
		{

			User user = membership.getUser();

			try
			{
				// Get required attributes for each user
				int stat = 0;
				int manageUserID = user.getUserId();
				String name = user.getName();
				String lastname = user.getLastname();
				String email = user.getEmail();
				String phone = userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber();
				String role  = userService.getUserRole(user, organization);
				System.out.println(role);
				String address = user.getAddress();
				Timestamp time= user.getTime();
				System.out.println(name);
				// Create the UserManage Object and add it to the list
				if(!name.equals("") && !role.equals("Admin")){
					UserManage userrow = new UserManage(stat, manageUserID, name, email, phone, role, address, time, lastname);
					userrows.add(userrow);
				}
			}
			catch(NullPointerException e)
			{
				System.out.println("User name not having his phone number is: " + user.getName() + " having userID: " + user.getUserId());
			}
		}
		return userrows;
	}
	
	
	
	@Transactional
	// Method to the get all the User list in the 'Manage Users' tab
	@RequestMapping(value="/getUserList", method=RequestMethod.GET, produces = "application/json")

	public List<UserManage> getUserList(@PathVariable String org) {

		List<UserManage> userrows = new ArrayList<UserManage>();

		Organization organization = organizationService.getOrganizationByAbbreviation(org);

		List<OrganizationMembership> membershipList = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 1);

		for(OrganizationMembership membership : membershipList)
		{

			User user = membership.getUser();

			try
			{
				// Get required attributes for each user
				int stat = 0;
				int manageUserID = user.getUserId();
				String name = user.getName();
				String lastname = user.getLastname();
				String email = user.getEmail();
				String phone = userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber();
				String role  = userService.getUserRole(user, organization);
				String address = user.getAddress();
				Timestamp time= user.getTime();
				System.out.println(name);
				// Create the UserManage Object and add it to the list
				if(!name.equals("")){
					UserManage userrow = new UserManage(stat, manageUserID, name, email, phone, role, address, time, lastname);
					userrows.add(userrow);
				}
			}
			catch(NullPointerException e)
			{
				System.out.println("User name not having his phone number is: " + user.getName() + " having userID: " + user.getUserId());
			}
		}
		return userrows;
	}
	
	@Transactional
	@RequestMapping(value="/getUserApprovalList", method=RequestMethod.GET, produces = "application/json")

	public List<UserManage> getUserApprovalList(@PathVariable String org) {
		List<UserManage> userrows = new ArrayList<UserManage>();
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		List<OrganizationMembership> membershipList = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 0);
		for(OrganizationMembership membership : membershipList)
		{

			User user = membership.getUser();
			
			try
			{
				// Get required attributes for each user
				int manageUserID = user.getUserId();
				String name = user.getName();
				String email = user.getEmail();
				String phone = userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber();
				String role  = userService.getUserRole(user, organization);
				String address = user.getAddress();
				Timestamp time = user.getTime();
				// Create the UserManage Object and add it to the list
				UserManage userrow = new UserManage(manageUserID, name, email, phone, role, address, time);
				userrows.add(userrow);
			}
			catch(NullPointerException e)
			{
				System.out.println("User name not having his phone number is: " + user.getName() + " having userID: " + user.getUserId());
			}
		}
		return userrows;
	}
	
	
	@RequestMapping(value = "/adminTerminates", method = RequestMethod.POST)
	@Transactional
	public String terminateMembershipAsAnAdmin(@PathVariable String org, @RequestBody String requestBody) {
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		User user = null;
		int userId = 0;
		int flag = 0;
		String phno = null;
		try {
			object = new JSONObject(requestBody);
			userId = Integer.parseInt(object.getString("userid"));
			System.out.println(userId);
			user = userService.getUser(userId);
			phno = object.getString("phno");
			System.out.println(phno);

		} catch (Exception e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

		}

		for (GroupMembership groupMembership : user.getGroupMemberships()) {
			for(Order order : orderService.getOrderByGroupProcessed(groupMembership.getGroup())){
				if(order.getMessage().getUser().getUserId() == userId && (order.getStatus().equals("processed") || order.getStatus().equals("saved") ||	(order.getStatus().equals("delivered") && order.getIsPaid() == true)))
				{
					flag = 1;
					System.out.println("Found one");
					break;
				}
			}
			if(flag == 1)
				break;
		}

		if(flag == 0){
			for (GroupMembership groupMembership : user.getGroupMemberships()) {
				if (groupMembership.getGroup().getOrganization().getName().equals(organization.getName()))
					groupMembershipService.removeGroupMembership(groupMembership);
			}
	
			OrganizationMembership organizationMembership = organizationMembershipService
					.getUserOrganizationMembership(user, organization);
			organizationMembershipService.removeOrganizationMembership(organizationMembership);
			try {
				responseJsonObject.put("response", "Success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else{
			try {
				responseJsonObject.put("response", "Error");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return responseJsonObject.toString();
	}
	
	
	@RequestMapping (value="/editProfile", method =RequestMethod.POST)
	@Transactional
	public @ResponseBody String editProfile(@RequestBody String requestBody) {
		String firstname = null, address = null, lastname = null, pincode=  null, phonenumber = null, email = null;
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		
		try{
			object = new JSONObject(requestBody);
			firstname = object.getString("firstname");
			lastname = object.getString("lastname");
			address = object.getString("address");
			pincode = object.getString("pincode");
		
			phonenumber  =object.getString("phonenumber");
		}
		catch(JSONException e){
			e.printStackTrace();
			try{
			responseJsonObject.put("response", "Failure");
			}
			catch(Exception e2){
				e2.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phonenumber);
		User user = userPhoneNumber.getUser();
		user.setName(firstname);
		user.setLastname(lastname); 
		user.setAddress(address);
		user.setPincode(pincode);
		try {
			email = object.getString("email");
			String oldemail = object.getString("oldemail");
			if(email.equals(oldemail))
			{
				//if the phone number and email are not in the same record
				if(!userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().equals( userService.getUserFromEmail(email)))
				{
					try {
						responseJsonObject.put("response", "The Mail ID is already associated with another account");
					} catch (JSONException e1) {
						e1.printStackTrace();
					}

					return responseJsonObject.toString();
					
				}
				else
				{
				user.setEmail(email);
				try {
					responseJsonObject.put("emailVerify", userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmailVerified());
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				}
				
			}
			else
			{
				
				System.out.println("------------Check it entered ------------");
				if(!userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().equals(userService.getUserFromEmail(oldemail)))
				{
					try {
						responseJsonObject.put("response", "The Mail ID is already associated with another account already");
					} catch (JSONException e1) {
						e1.printStackTrace();
					}

					return responseJsonObject.toString();
					
				}
				else
				{
					if(userService.getUserFromEmail(email)==null)
					{
					
					//System.out.println("------------Get here na ------------");

					
					user.setEmailVerified(0);
					
					user.setEmail(email);
					try {
						responseJsonObject.put("emailVerify", 0);
						
					} catch (JSONException e) {
						e.printStackTrace();
					}
					/* //bestB
					SendMail.sendMail(email, "Verify Mail ID from Lokacart" , "Kindly click on this <a href='http://best-erp.com/ruralict/app/emailVerify/"+user.getUserId()+"'>link</a> "
							+ "to verify your mail ID, "
							+ "through which you will be receiving the notifications from Lokacart.<br>");
					*/ //bestE

					//ictB
					SendMail.sendMail(email, "Verify Mail ID from Lokacart" , "Kindly click on this <a href='http://ruralict.cse.iitb.ac.in/ruralict/app/emailVerify/"+user.getUserId()+"'>link</a> "
							+ "to verify your mail ID, "
							+ "through which you will be receiving the notifications from Lokacart.<br>");
					//ictE
				}
					else {
						
						try {
							responseJsonObject.put("response", "The Mail ID is already associated with another account already");
						} catch (JSONException e1) {
							e1.printStackTrace();
						}
						return responseJsonObject.toString();
						
						
					}
				}
				
				
			}
						
				
			/*if(userService.getUserFromEmail(email)==null)
			{
				System.out.println("first if");
			user.setEmail(email);
			if(email!=oldemail)
			{
				
			}
			}
			else if(userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail().equals(email))
			{
				//check he is present for changing other details.
				System.out.println("other details");
				user.setEmail(email);
			}
			else if(userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail().equals(oldemail))
			{
				if((email!=oldemail)&&(userService.getUserFromEmail(email)==null))
				{//check authentication of the user
				System.out.println("email itself");
				user.setEmailVerified(0);
				user.setEmail(email);
				}
			}
			else if((userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser().getEmail()!=email)&& (userService.getUserFromEmail(oldemail)!=null))
			{
				System.out.println("edited email but changing now");
				user.setEmail(email);	
			}
			else
			{
				try {
					responseJsonObject.put("response", "The Mail ID is already associated with another account already");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}

				return responseJsonObject.toString();
			}*/
			
				
		} catch (Exception e) {

		}
		
		userService.addUser(user);
		System.out.println(email);
		//System.out.println("added -- no errors");
		/*try {
			responseJsonObject.put("emailVerify", user.getEmailVerified());
			responseJsonObject.put("response", "Success");
		} catch (JSONException e) {
			e.printStackTrace();
		}*/
		try {
			
			responseJsonObject.put("response", "Success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		return responseJsonObject.toString();
	}
	
	
	
	
	
	
	@RequestMapping(value="/editUser", method = RequestMethod.POST)
	@Transactional
	public String editUser(@PathVariable String org, @RequestBody String requestBody) {

		
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		// Get the input parameters from AngularJS
		String name = null, email = null, address = null, role =null, pincode = null, phonenumber =null, lastname = null;
		boolean isAdmin = false, isPublisher = false;
		int userId=0, isPubInt =0, isAdminInt =0, flag=0;
		try{
		object = new JSONObject(requestBody);
		userId = Integer.parseInt(object.getString("userid"));
		name = object.getString("name");
		email = object.getString("email");
		address = object.getString("address");
		pincode  = object.getString("pincode");
		phonenumber = object.getString("phone");
		try {
		lastname = object.getString("lastname");
		}
		catch(Exception e) {
			
		}
		//role = object.getString("role");
		try{
		isPubInt = Integer.parseInt(object.getString("isPublisher"));
		flag=1;
		if (isPubInt == 1) {
			isPublisher = true;
		}
	
		isAdminInt = Integer.parseInt(object.getString("isAdmin"));
		if (isAdminInt == 1) {
			isAdmin = true;
		}
		}
		
		catch(JSONException e){
			System.out.println("Publisher details not found");
		}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		// Add the new User to database
		User user = null;
		OrganizationMembership membership = null;
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		UserPhoneNumber userPhoneNumber = null;
		try{
		user = userService.getUser(userId);
		if (user == null)
		{
			responseJsonObject.put("response", "User not found");
			return responseJsonObject.toString();
		}
		UserPhoneNumber tempUserPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phonenumber);
		if (tempUserPhoneNumber != null) {
			if (tempUserPhoneNumber.getUser() != user){
				responseJsonObject.put("response", "Phone number exists");
				return responseJsonObject.toString();
			}
		}
		User tempUser = userService.getUserFromEmail(email);
		if ((user != tempUser) && tempUser != null) {
			responseJsonObject.put("response", "Email ID exists");
			return responseJsonObject.toString();
		}
		
	/*	Iterator <UserPhoneNumber>iterator = list.iterator();
		while(iterator.hasNext()){
			if(iterator.next().getPhoneNumber().equals(phonenumber)) {
				
				
				
			}
		}*/
		membership = organizationMembershipService.getUserOrganizationMembership(user, organization);
		userPhoneNumber = userPhoneNumberService.getUserPrimaryPhoneNumber(user);
		}
		catch(Exception e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "User not found");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		
		// Update the attributes of the user
		user.setName(name);
		user.setEmail(email);
		user.setAddress(address);
		user.setPincode(pincode);
		if (lastname != null)
		user.setLastname(lastname);
		if (flag ==1){
		membership.setIsAdmin(isAdmin);
		membership.setIsPublisher(isPublisher);
		organizationMembershipService.addOrganizationMembership(membership);
		}
		userService.addUser(user);
		try {
			responseJsonObject.put("response","success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	@RequestMapping(value="/addUser", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String addUser(@PathVariable String org, @RequestBody String requestBody) {

		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		// Get the input parameters from AngularJS
		int isPubInt =0, isAdminInt=0;
		boolean isPublisher=false, isAdmin= false;
	//	String password = null;

		String name = null, email = null, phone = null, role = null, address = null, fname = null,pincode = null, lastname=null;
		try{
			
		object = new JSONObject(requestBody);
		name = object.getString("name");
		email = object.getString("email");
		phone = object.getString("phone");
		//role  = object.getString("role");
		address = object.getString("address");
		pincode = object.getString("pincode");
		try {
			lastname = object.getString("lastname");
		}
		catch(Exception e) {
			System.out.println("no lastname");
		}
		
		isPubInt = Integer.parseInt(object.getString("isPublisher"));
		if (isPubInt == 1) {
			isPublisher = true;
		}
	
		isAdminInt = Integer.parseInt(object.getString("isAdmin"));
		if (isAdminInt == 1) {
			isAdmin = true;
		}
		fname=name;
		if(name.contains(" "))
		{
			int i=name.indexOf(" ");
			fname=name.substring(0, i);
		}
		
		}
		catch (Exception e){
			e.printStackTrace();
			try {
				responseJsonObject.put("response","Failed");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		Random randomint = new Random();
		
		// Variables to store the boolean values of the roles
/*		boolean isAdmin = false;
		boolean isPublisher = false;*/

		// Find if the number is already present in the database
		// If present report it to the frontend
		
		
		if(!userPhoneNumberService.findPreExistingPhoneNumber(phone))
		{
			/*try {
				responseJsonObject.put("response", "Phone Number already exists");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();*/
			UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phone);
			User tempUser = userService.getUserFromEmail(email);
			List<OrganizationMembership> list = userService.getAdminMembershipList(tempUser);
			if((list == null) || list.isEmpty()) {
				try {
					responseJsonObject.put("response","Cannot use Admin account in Consumer app");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			if (userPhoneNumber.getUser() == tempUser && ((list == null) || list.isEmpty())){
				//add membership instead
				OrganizationMembership membership = new OrganizationMembership(organization, tempUser, false, false, 1);
				organizationMembershipService.addOrganizationMembership(membership);
				groupMembershipService.addParentGroupMembership(organization, tempUser);	
				try {
					responseJsonObject.put("response","success");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}
			else {
				//conflict
				try {
					responseJsonObject.put("response", "Email/Phone Number already exists");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return responseJsonObject.toString();
			}	
		}

		// Add the new User to database
		/*User tempUser = userService.getUserFromEmail(email);
		if (tempUser != null) {
			try {
				responseJsonObject.put("response", "Email already exists");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}*/
		
	//	String password= fname+randomint.nextInt(1000);
		String password = null;
		User user = null;
		if (lastname != null) {
			user = new User(name, address, "en", "en", email, pincode,lastname);
		}
		else {
 		user = new User(name, address, "en", "en", email, pincode);
		}
 		java.util.Date date= new java.util.Date();
		Timestamp currentTimestamp= new Timestamp(date.getTime());
		user.setTime(currentTimestamp);
		user.setTextbroadcastlimit(0);
		user.setVoicebroadcastlimit(0);
		if(isAdmin) {
			password= fname+randomint.nextInt(1000);
			user.setSha256Password(passwordEncoder.encode(password));
		}
		else {
			password = Integer.toString(randomint.nextInt(10));
			for (int count = 0; count < 9; ++count)
				password = password + randomint.nextInt(10);
			user.setPassToken(password);
			user.setSha256Password(passwordEncoder.encode(password));
		}
		if(isAdmin)
		{
			user.setTextbroadcastlimit(-1);
			user.setVoicebroadcastlimit(-1);
		}
		else if(isPublisher)
		{
			user.setTextbroadcastlimit(-1);
			user.setVoicebroadcastlimit(-1);
			
		}
		else
		{
			isAdmin=false;
			isPublisher=false;;
		}
		try
		{
		userService.addUser(user);
		}
		catch(Exception e) {
			try {
				responseJsonObject.put("response", "Email ID already exists");
			} catch (JSONException e1) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		System.out.println("user timestamp is: "+user.getTime());

		UserPhoneNumber primaryPhoneNumber = new UserPhoneNumber(user, phone, true);
		userPhoneNumberService.addUserPhoneNumber(primaryPhoneNumber);

		// Add the Organization Membership for the user in the Database
		
		OrganizationMembership membership = new OrganizationMembership(organization, user, isAdmin, isPublisher, 1);
		organizationMembershipService.addOrganizationMembership(membership);
		// By Default Add the new user to parent group
		groupMembershipService.addParentGroupMembership(organization, user);

		// Create the UserManage Object
		int manageUserID = user.getUserId();

		//UserManage userrow = new UserManage(manageUserID, name, email, phone, role, address, currentTimestamp);
		System.out.println("password is: "+password);
		if (isAdmin)
			SendMail.sendMail(email, "Lokacart: User credentials", "Dear User,\nThe admin of "+organization.getName()+" has added you as a trusted member in the organization.\nNow you can place your order by logging in to our lokacart app using the credentials given below-\nUsername : "+email+"\nPassword : "+password+"\n\nIf you wish to change your password, you can simply click on forget your password button on the app login screen and follow the instructions.\n\nThankyou,\nBest Regards,\nLokacart Team");
		else {
			String BRANCH_KEY= "key_live_nam1TdTbBvxAcKO49YOKDnlhvzlFSU8z";
			
		}
		// Finally return it as a JSON response body
		try {
			responseJsonObject.put("response","success");
			responseJsonObject.put("userId",user.getUserId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	@RequestMapping(value="/addRole", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String addRole(@PathVariable String org, @RequestBody String requestBody) {

		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		OrganizationMembership membership = null;
		int userId=0;
		User user =null;
		String addRole = null;
		try {
		object = new JSONObject(requestBody);
		userId = Integer.parseInt(object.getString("userid"));
		addRole = object.getString("addRole");
		user = userService.getUser(userId);
		membership = organizationMembershipService.getUserOrganizationMembership(user, organization);
		}
		
		catch(Exception e){
			e.printStackTrace();
			try{
			responseJsonObject.put("response","Member not found");
			}
			catch(Exception e1)
			{
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		if(addRole.equals("Admin"))
		{
			membership.setIsAdmin(true);
			user.setTextbroadcastlimit(-1);
			user.setVoicebroadcastlimit(-1);
			userService.addUser(user);
		}
		else if(addRole.equals("Publisher"))
		{
			membership.setIsPublisher(true);
			user.setTextbroadcastlimit(-1);
			user.setVoicebroadcastlimit(-1);
			userService.addUser(user);
		}
		else if(addRole.equals("Member"))
		{
			membership.setIsAdmin(false);
			membership.setIsPublisher(false);
		}

		// Finally make changes in the database
		try{
		organizationMembershipService.addOrganizationMembership(membership);
		}
		catch(Exception e){
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "failed");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		try {
			responseJsonObject.put("response", "Success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
		}
	
	
	@RequestMapping(value="/removeRole", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String removeUserRole(@PathVariable String org, @RequestBody String requestBody) {

		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		//int manageUserId = Integer.parseInt(userDetails.get("userid"));
		//String removeRole = userDetails.get("removeRole");
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		User user = null;
		int userId=0;
		String removeRole = null;
		OrganizationMembership membership=null;
		try{
		object = new JSONObject(requestBody);
		userId = Integer.parseInt(object.getString("userid"));
		removeRole = object.getString("removeRole");
		user = userService.getUser(userId);
		membership = organizationMembershipService.getUserOrganizationMembership(user, organization);
		}
		catch(Exception e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			
		}
		
		if(removeRole.equals("Admin"))
		{
			membership.setIsAdmin(false);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			userService.addUser(user);
		}
		else if(removeRole.equals("Publisher"))
		{
			membership.setIsPublisher(false);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			userService.addUser(user);
		}

		// Finally make changes in the database
		organizationMembershipService.addOrganizationMembership(membership);
		try {
			responseJsonObject.put("response", "Success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	

	@Transactional
	@RequestMapping(value="/countUserList", method=RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	public List<Integer> countUserList(@PathVariable String org) {

		List<Integer> count = new ArrayList<Integer>();
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		List<OrganizationMembership> membershipListpending = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 0);
		List<OrganizationMembership> membershipListapproved = organizationMembershipService.getOrganizationMembershipListByStatus(organization, 1);
		int totUsers=membershipListpending.size()+membershipListapproved.size();
		int todayUsers=0;
		int pendingUsers=membershipListpending.size();
		for(OrganizationMembership membership : membershipListpending)
		{

			User user = membership.getUser();
		
			try
			{
				// Get required attributes for each user
				int manageUserID = user.getUserId();
				String name = user.getName();
				String email = user.getEmail();
				String phone = userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber();
				String role  = userService.getUserRole(user, organization);
				String address = user.getAddress();
				Timestamp time = user.getTime();
				
				// Create the UserManage Object and add it to the list
				UserManage userrow = new UserManage(manageUserID, name, email, phone, role, address, time);
				Calendar cal= Calendar.getInstance();
				cal.clear(Calendar.HOUR_OF_DAY);
				cal.clear(Calendar.AM_PM);
				cal.clear(Calendar.MINUTE);
				cal.clear(Calendar.SECOND);
				cal.clear(Calendar.MILLISECOND);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			    java.util.Date dateWithoutTime = sdf.parse(sdf.format(new java.util.Date()));
				if(time.after(dateWithoutTime))
				{
					todayUsers=todayUsers+1;
				}
			}
			catch(NullPointerException | ParseException e)
			{
				System.out.println("User name not having his phone number is: " + user.getName() + " having userID: " + user.getUserId());
			}
		}
		count.add(totUsers);
		count.add(pendingUsers);
		count.add(todayUsers);
		return count;
	}
	
	// Method to add a new user according to the details entered in the Modal Dialog Box
	@RequestMapping(value="/addNewUser", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public UserManage addNewUser(@PathVariable String org, @RequestBody Map<String,String> newUserDetails) {

		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		int joinCount=1;
		// Get the input parameters from AngularJS
		String name = newUserDetails.get("name");
		String email = newUserDetails.get("email");
		String phone = newUserDetails.get("phone");
		String role  = newUserDetails.get("role");
		String address = newUserDetails.get("address");
		String fname=name;
		if(name.contains(" "))
		{
			int i=name.indexOf(" ");
			fname=name.substring(0, i);
		}
		Random randomint = new Random();
		String password= fname+randomint.nextInt(1000);

		// Variables to store the boolean values of the roles
		boolean isAdmin = false;
		boolean isPublisher = false;

		// Find if the number is already present in the database
		// If present report it to the frontend
		if(!userPhoneNumberService.findPreExistingPhoneNumber(phone) )
		{
			User user = userPhoneNumberService.getUserPhoneNumber(phone).getUser();
			if (!user.getEmail().equals(email))
				return null;
			else if (organizationMembershipService.getUserOrganizationMembership(user, organization) != null)
				return null;
			else
			{
				OrganizationMembership latestMembership = organizationMembershipService.getLatestOrganizationMembership(user);
				if (latestMembership != null) {
					joinCount = latestMembership.getJoinCount() + 1;
				}
				OrganizationMembership membership = new OrganizationMembership(organization, user, isAdmin, isPublisher, 1);
				membership.setJoinCount(joinCount);
				organizationMembershipService.addOrganizationMembership(membership);	
				groupMembershipService.addParentGroupMembership(organization, user);
				int manageUserID = user.getUserId();
				java.util.Date date= new java.util.Date();
				Timestamp currentTimestamp= new Timestamp(date.getTime());
				UserManage userrow = new UserManage(manageUserID, name, email, phone, role, address, currentTimestamp);
				SendMail.sendMail(email, "Lokacart: New Organization Membership", "Dear User,\nThe admin of "+organization.getName()+" has added you as a trusted member in the organization.\nNow you can place your order by logging in to our lokacart app using your registered account.\n\nThankyou,\nBest Regards,\nLokacart Team");
	
				return userrow;
			}
		
		}

		// Add the new User to database
		User user = new User(name, address, "en", "en", email);
		java.util.Date date= new java.util.Date();
		Timestamp currentTimestamp= new Timestamp(date.getTime());
		user.setTime(currentTimestamp);
		user.setTextbroadcastlimit(0);
		user.setVoicebroadcastlimit(0);
		user.setSha256Password(passwordEncoder.encode(password));
		userService.addUser(user);
		System.out.println("user timestamp is: "+user.getTime());

		UserPhoneNumber primaryPhoneNumber = new UserPhoneNumber(user, phone, true);
		userPhoneNumberService.addUserPhoneNumber(primaryPhoneNumber);
		/* TODO when modified to allow addition of member if already exisiting, set joinCount accordingly */
		// Add the Organization Membership for the user in the Database
		/*OrganizationMembership latestMembership = organizationMembershipService.getLatestOrganizationMembership(user);
		if (latestMembership != null) {
			joinCount = latestMembership.getJoinCount() + 1;
		}*/
		OrganizationMembership membership = new OrganizationMembership(organization, user, isAdmin, isPublisher, 1);
		membership.setJoinCount(joinCount);
		organizationMembershipService.addOrganizationMembership(membership);

		// By Default Add the new user to parent group
		groupMembershipService.addParentGroupMembership(organization, user);

		// Create the UserManage Object
		int manageUserID = user.getUserId();

		UserManage userrow = new UserManage(manageUserID, name, email, phone, role, address, currentTimestamp);
		System.out.println("password is: "+password);
		SendMail.sendMail(email, "Lokacart: User credentials", "Dear User,\nThe admin of "+organization.getName()+" has added you as a trusted member in the organization.\nNow you can place your order by logging in to our lokacart app using the credentials given below-\nUsername : "+email+"\nPassword : "+password+"\n\nIf you wish to change your password, you can simply click on forget your password button on the app login screen and follow the instructions.\n\nThankyou,\nBest Regards,\nLokacart Team");
		
		// Finally return it as a JSON response body
		return userrow;
	}

	// It is assumed that the user does not have the role of admin or publisher
	@RequestMapping(value="/addUserRole", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public void addRole(@PathVariable String org, @RequestBody Map<String,String> userDetails) {

		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		int manageUserId = Integer.parseInt(userDetails.get("userid"));
		String addRole = userDetails.get("addRole");

		User user = userService.getUser(manageUserId);

		OrganizationMembership membership = organizationMembershipService.getUserOrganizationMembership(user, organization);

		if(addRole.equals("Admin"))
		{
			membership.setIsAdmin(true);
			user.setTextbroadcastlimit(-1);
			user.setVoicebroadcastlimit(-1);
			userService.addUser(user);
		}
		else if(addRole.equals("Publisher"))
		{
			membership.setIsPublisher(true);
			user.setTextbroadcastlimit(-1);
			user.setVoicebroadcastlimit(-1);
			userService.addUser(user);
		}
		else if(addRole.equals("Member"))
		{
			membership.setIsAdmin(false);
			membership.setIsPublisher(false);
		}

		// Finally make changes in the database
		organizationMembershipService.addOrganizationMembership(membership);
	}

	@RequestMapping(value="/removeUserRole", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public void removeRole(@PathVariable String org, @RequestBody Map<String,String> userDetails) {

		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		int manageUserId = Integer.parseInt(userDetails.get("userid"));
		String removeRole = userDetails.get("removeRole");

		User user = userService.getUser(manageUserId);
		OrganizationMembership membership = organizationMembershipService.getUserOrganizationMembership(user, organization);

		if(removeRole.equals("Admin"))
		{
			membership.setIsAdmin(false);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			userService.addUser(user);
		}
		else if(removeRole.equals("Publisher"))
		{
			membership.setIsPublisher(false);
			user.setTextbroadcastlimit(0);
			user.setVoicebroadcastlimit(0);
			userService.addUser(user);
		}

		// Finally make changes in the database
		organizationMembershipService.addOrganizationMembership(membership);
	}

	// Method to add a new user according to the details entered in the Modal Dialog Box
	@RequestMapping(value="/editUserWithPhoneNumber", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String editUserWithPhoneNumber(@PathVariable String org, @RequestBody Map<String,String> currentUserDetails) {

		// Get the input parameters from AngularJS
		int manageUserId = Integer.parseInt(currentUserDetails.get("userid"));
		String name = currentUserDetails.get("name");
		String email = currentUserDetails.get("email");
		String phone = currentUserDetails.get("phone");
		String address = currentUserDetails.get("address");
		
		// Find if the number is already present in the database 
		// If present report it to the Frontend
		if(!userPhoneNumberService.findPreExistingPhoneNumber(phone))
		{
			return "-1";
		}
		
		// Add the new User to database
		User user = userService.getUser(manageUserId);

		// Update the attributes of the user
		user.setName(name);
		user.setEmail(email);
		user.setAddress(address);
		userService.addUser(user);
		
		// Update the primary phone number of the user
		UserPhoneNumber userPrimaryPhoneNumber = userPhoneNumberService.getUserPrimaryPhoneNumber(user);
		userPrimaryPhoneNumber.setPhoneNumber(phone);
		userPhoneNumberService.addUserPhoneNumber(userPrimaryPhoneNumber);
		
		return phone;
	}
	
	// Method to edit a exisitng user only if his phone number is not altered
	@RequestMapping(value="/editUserOnly", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public void editUserOnly(@PathVariable String org, @RequestBody Map<String,String> currentUserDetails) {

		// Get the input parameters from AngularJS
		int manageUserId = Integer.parseInt(currentUserDetails.get("userid"));
		String name = currentUserDetails.get("name");
		String lastname = currentUserDetails.get("lastname");
		String email = currentUserDetails.get("email");
		String address = currentUserDetails.get("address");
		
		// Add the new User to database
		User user = userService.getUser(manageUserId);

		// Update the attributes of the user
		user.setName(name);
		user.setLastname(lastname);
		user.setEmail(email);
		user.setAddress(address);
		userService.addUser(user);
	}

	// Method to get user details in a Modal Dialog Box
	@RequestMapping(value="/getUserDetails", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public Map<String, Vector<String> > getUserDetails(@PathVariable String org, @RequestBody int manageUserId) {

		Organization organization = organizationService.getOrganizationByAbbreviation(org);

		Vector<String> userGroupNames = new Vector<String>(10,2);
		Vector<String> userPhoneNumbers = new Vector<String>(10,2);

		// Add the new User to database
		User user = userService.getUser(manageUserId);

		List<Group> userGroups = groupMembershipService.getGroupListByUserAndOrganization(user, organization);

		for(Group userGroup : userGroups)
		{
			userGroupNames.add(userGroup.getName());
		}

		// Add the Primary Phone number for the user in the database
		UserPhoneNumber primaryPhoneNumber = userPhoneNumberService.getUserPrimaryPhoneNumber(user);
		userPhoneNumbers.add(primaryPhoneNumber.getPhoneNumber() + " (Primary)" );

		List<UserPhoneNumber> userSecondaryPhoneNumbers = userPhoneNumberService.getUserSecondaryPhoneNumbers(user);

		if(userSecondaryPhoneNumbers != null)
		{
			for(UserPhoneNumber userSecondaryPhoneNumber : userSecondaryPhoneNumbers)
			{
				userPhoneNumbers.add(userSecondaryPhoneNumber.getPhoneNumber());
			}
		}

		Map<String, Vector<String> > jsonbody = new HashMap<String, Vector<String> >();
		jsonbody.put("groups", userGroupNames);
		jsonbody.put("phoneNumbers", userPhoneNumbers);

		return jsonbody;
	}

}

