package app.business.controllers.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class FcmTestingController {
	public final static String AUTH_KEY_FCM = "AAAAr9WqZIM:APA91bFwWZ3tC7-bx-EnbGemA8gDnI19dlNM5MGKVpRLnE0-QsNCO_rmdgP0VSz7PKPQY9znGSFiW517yoJuycqFDI4JWLGVfc6L-PH9qEeH0M1dWOkzjR4ErVGaT90li2qxs2oOlXu7";
	public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
	

	public static String sendPushNotification(String token, String title, String body)
	        throws IOException {
	    URL url = new URL(API_URL_FCM);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	
	    conn.setUseCaches(false);
	    conn.setDoInput(true);
	    conn.setDoOutput(true);
	
	    conn.setRequestMethod("POST");
	    conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM);
	    conn.setRequestProperty("Content-Type", "application/json");
	    JSONObject json = new JSONObject();
	    String response = "";
    try{	
	    json.put("to", token);
	    JSONObject info = new JSONObject();
	    info.put("title", title); // Notification title
	    info.put("body", body); // Notification body
	    info.put("sound", "Default");
	    json.put("notification", info);
	}
	catch(JSONException je){
		je.printStackTrace();
	}
	    try {
	        OutputStreamWriter wr = new OutputStreamWriter(
	                conn.getOutputStream());
	        wr.write(json.toString());
	        wr.flush();
	
	        BufferedReader br = new BufferedReader(new InputStreamReader(
	                (conn.getInputStream())));
	
	        String output;
	        System.out.println("Output from Server .... \n");
	        while ((output = br.readLine()) != null) {
	            System.out.println(output);
	        }
	        response = "success";
	    } catch (Exception e) {
	        e.printStackTrace();
	        response = "failure";
	    }
	    System.out.println("FCM Notification is sent successfully");
	
	    return response;
	}
	

}