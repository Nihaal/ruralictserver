package app.business.controllers.rest;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.LcpFeedbackService;
import app.business.services.RegistrationService;
import app.data.repositories.RegistrationRepository;
import app.entities.LcpFeedback;
import app.entities.Registration;
import app.util.SendContent;

@RestController
@RequestMapping("/app")
public class LcpFeedbackRestController {
	
	
	@Autowired
	LcpFeedbackService feedbackService;
	
	@Autowired
	RegistrationRepository registrationRepo;
	
	@Transactional
	@RequestMapping(value = "/lcpfeedback",method = RequestMethod.POST )
	public @ResponseBody String submitFeedback(@RequestBody String requestBody, HttpServletResponse response) {
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		String orgabbr=null, content=null, phonenumber=null;
		try {
			object = new JSONObject(requestBody);
			content = object.getString("content");
			phonenumber = object.getString("phonenumber");
		} catch (JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "Error in uploading");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		Registration user = registrationRepo.findByPhone(phonenumber);
		LcpFeedback feedback = new LcpFeedback(content, user.getId());
		feedbackService.addFeedback(feedback);
		
		/* //bestB
		String email = "lokacart@itaakash.com";  
		*/ //bestE
		
		//ictB
		String email = "lokacart@cse.iitb.ac.in";
		//ictE
		
		String subject = "User Feedback";
		String body = "<html><head></head><body><b>"+user.getFirstname()+"</b> has submitted feedback.<br /><br /><br />"+
				"<b>Timestamp: </b>" + String.valueOf(new Timestamp((new Date()).getTime())) + "<br />" +"<b>Email ID: </b>"+user.getEmail()+ "<br />" + "<b>Phonenumber: </b>"+user.getPhone()+
				"<br /><b>Feedback: </b><br /><br /><p>"+content+"</p><br /><br /><br /></body></html>";
		SendContent.sendMail(email, subject, body);
		try {
			responseJsonObject.put("response","success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
		return responseJsonObject.toString();
	}

}
