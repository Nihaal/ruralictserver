package app.business.controllers.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import app.business.services.LcpProductService;
import app.business.services.LcpProductTypeService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.business.services.UserService;
import app.data.repositories.LcpProductRepository;
import app.data.repositories.OrganizationRepository;
import app.data.repositories.SiUnitRepository;
import app.entities.Order;
import app.entities.Organization;
import app.entities.Product;
import app.entities.ProductType;
import app.entities.SiUnit;
import app.entities.User;
import app.entities.LcpProduct;
import app.entities.LcpProductType;
import app.util.OrderManage;
import app.util.ProductsManage;

@RestController
@RequestMapping("/api/products/search/byType")
public class ProductTypeRestController {
	
	@Autowired
	OrganizationService organizationService;
	@Autowired
	ProductTypeService productTypeService;
	@Autowired
	UserService userService;
	@Autowired
	OrganizationRepository organizationRepository;
	@Autowired
	ProductService productService;
	@Autowired
	LcpProductService lcpproductService;
	@Autowired
	LcpProductTypeService lcpProdTypeService;
	@Autowired
	SiUnitRepository siUnitRepository;
	@Autowired
	LcpProductRepository lcpProductRepository;
	@Autowired
	OrganizationMembershipService organizationMembershipService;
	
	@Transactional
	@RequestMapping(value = "/map",method = RequestMethod.GET )
	public HashMap<String, List<HashMap<String, String>>> productListByType(@RequestParam(value="orgabbr") String orgabbr){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		/*
		if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
			try {
				responseJsonObject.put("status", "Failure");	
				responseJsonObject.put("error", "You are no longer a member");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();
			JSONArray jsonArray = new JSONArray();	
			
			for(Product product: productList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) && (product.getStatus() == 1)) {
					System.out.println(ptype.getName());
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
					jsonObject.put("imageUrl", product.getImageUrl());
					jsonObject.put("audioUrl", product.getAudioUrl());	
					jsonObject.put("stockManagement", stockManagement.toString());
					jsonObject.put("description", product.getDescription());
					jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				
			}
			}
			try {
				JSONObject temp = new JSONObject();
				temp.put(ptype.getName(), jsonArray);
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("products", details);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
			return responseJsonObject.toString();   */
			
			
		 
		
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();
			System.out.println(ptype.getName());
			List<HashMap<String, String>> Listmap=new ArrayList<HashMap<String, String>>();
			HashMap<String, HashMap<String, String>> productmap=new HashMap<String, HashMap<String, String>>();
			for(Product product: productList)
			{
				HashMap<String, String> map=new HashMap<String, String>();
				if(ptype.getName().equals(product.getProductType().getName()) /*&& (product.getStatus() == 1)*/)
				{
					map.put("id", Integer.toString(product.getProductId()));
					map.put("name", product.getName());
					map.put("quantity", Integer.toString((product.getQuantity())));
					map.put("unitRate", Float.toString(product.getUnitRate()));
					map.put("imageUrl", product.getImageUrl());
					map.put("audioUrl", product.getAudioUrl());
					map.put("status", Integer.toString(product.getStatus()));
					map.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						map.put("description", "");
					else
						map.put("description", product.getDescription());
					Listmap.add(map);
				}
			}
			productTypeMap.put(ptype.getName(), Listmap);
		}
		return productTypeMap;
	
	}
	
	@Transactional
	@RequestMapping(value = "/newmap",method = RequestMethod.GET )
	public HashMap<String, List<HashMap<String, String>>> productListByTypeNew(@RequestParam(value="orgabbr") String orgabbr){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		Boolean stockManagement = organization.getStockManagement();
		for (ProductType ptype: productTypeList)
		{
			System.out.println(ptype.getName());
			List<HashMap<String, String>> Listmap=new ArrayList<HashMap<String, String>>();
			for(Product product: productList)
			{
				HashMap<String, String> map=new HashMap<String, String>();
				if(ptype.getName().equals(product.getProductType().getName()))
				{
					map.put("id", Integer.toString(product.getProductId()));
					map.put("name", product.getName());
					if(product.getJustName() != null)
						map.put("prodName", product.getJustName());
					else
						map.put("prodName", "null");
					if(product.getSiUnit() != null)
						map.put("siUnit", product.getSiUnit());
					else
						map.put("siUnit", "null");
					if(product.getUnitQty() != 0)
						map.put("unitQty", Integer.toString(product.getUnitQty()));
					else
						map.put("unitQty", Integer.toString(0));						
					map.put("quantity", Integer.toString((product.getQuantity())));
					map.put("unitRate", Float.toString(product.getUnitRate()));
					if(product.getImageUrl() == null || product.getImageUrl().length()<=1)
						map.put("imageUrl", null);
					else
						map.put("imageUrl", product.getImageUrl());
					if(product.getAudioUrl() == null || product.getAudioUrl().length()<=1)
						map.put("audioUrl", null);
					else
						map.put("audioUrl", product.getAudioUrl());
					map.put("status", Integer.toString(product.getStatus()));
					map.put("stockManagement", stockManagement.toString());
					try{
						map.put("unitGrp", Integer.toString(siUnitRepository.findBySiUnit(product.getSiUnit()).getLcGroup()));
					}
					catch(Exception e){
						map.put("unitGrp", Integer.toString(0));
					}
					if(product.getDescription() == null)
						map.put("description", "");
					else
						map.put("description", product.getDescription());
					if(lcpproductService.getProductById(product.getProductId()) != null){
						LcpProduct lcpProduct = lcpproductService.getProductById(product.getProductId());
						map.put("lcpAvailability", Integer.toString(1));
						map.put("lcpUnit", lcpProduct.getSiUnit());
						map.put("lcpMoq", Integer.toString(lcpProduct.getMoq()));
						map.put("lcpPType", lcpProduct.getProductType().getName());
						map.put("lcpRate", Float.toString(lcpProduct.getUnitRate()));
						map.put("lcpStatus", Integer.toString(lcpProduct.getStatus()));
						map.put("lcpLogistic", Integer.toString(lcpProduct.getLogAvailability()));
					}
					else{
						map.put("lcpAvailability", Integer.toString(0));
						map.put("lcpUnit", "null");
						map.put("lcpMoq", "0");
						map.put("lcpPType", "null");
						map.put("lcpRate", "0.0");
						map.put("lcpStatus", "0");
						map.put("lcpLogistic", "0");
					}
					Listmap.add(map);
				}
			}
			productTypeMap.put(ptype.getName(), Listmap);
		}
		return productTypeMap;	
	}
	
	@Transactional
	@RequestMapping(value = "/maplcp",method = RequestMethod.GET )
	public HashMap<String, List<HashMap<String, String>>> productListByTypeLcp(@RequestParam(value="orgabbr") String orgabbr){
		List<ProductType> productTypeList = productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation("lcart"));
		List<LcpProduct> productList = lcpproductService.getLcpProductListAdmin(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		/*Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();*/
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();
			System.out.println(ptype.getName());
			List<HashMap<String, String>> Listmap = new ArrayList<HashMap<String, String>>();
			//HashMap<String, HashMap<String, String>> productmap = new HashMap<String, HashMap<String, String>>();
			for(LcpProduct product: productList)
			{
				HashMap<String, String> map = new HashMap<String, String>();
				if(ptype.getName().equals(product.getProductType().getName()) /*&& (product.getStatus() == 1)*/)
				{
					/*map.put("id", Integer.toString(product.getProductId()));
					map.put("name", product.getName());
					map.put("quantity", Integer.toString((product.getQuantity())));
					map.put("unitRate", Float.toString(product.getUnitRate()));
					map.put("imageUrl", product.getImageUrl());
					map.put("audioUrl", product.getAudioUrl());
					map.put("status", Integer.toString(product.getStatus()));
					map.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						map.put("description", "");
					else
						map.put("description", product.getDescription());
					if(lcpproductService.getProductById(product.getProductId()) != null){
						if(productService.getProductById(product.getProductId()) != null){
							Product lcProduct = productService.getProductById(product.getProductId());
							map.put("lcAvailability", Integer.toString(1));
							map.put("lcUnit", lcProduct.getSiUnit());
							map.put("lcUQty", Integer.toString(lcProduct.getUnitQty()));
							map.put("lcRate", Float.toString(lcProduct.getUnitRate()));
						}
						else{
							map.put("lcAvailability", Integer.toString(0));
							map.put("lcUnit", "null");
							map.put("lcUQty", "null");
							map.put("lcRate", "null");
						}
					}*/
					map.put("id", Integer.toString(product.getProductId()));
					map.put("name", product.getName());
					map.put("quantity", Integer.toString((product.getQuantity())));
					map.put("unitRate", Float.toString(product.getUnitRate()));
					map.put("description", product.getDescription());
					map.put("minqty", Integer.toString(product.getMoq()));
					map.put("si", product.getSiUnit());
					map.put("logistics", (product.getLogAvailability() == 0 ? "No" : "Yes"));
					map.put("deliveryCharge", Integer.toString(organizationService.getOrganizationByAbbreviation(orgabbr).getDelCharge()));	
					try{
						map.put("unitGroup", Integer.toString(siUnitRepository.findBySiUnit(product.getSiUnit()).getLcpGroup()));
					} catch(NullPointerException e){}
					if(product.getImageUrl() == null || product.getImageUrl().length()<=1)
						map.put("imageUrl", null);
					else
						map.put("imageUrl", product.getImageUrl());
					map.put("status", Integer.toString(product.getStatus()));
					map.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null || product.getDescription().length() <= 1)
						map.put("description", "");
					else
						map.put("description", product.getDescription());
					if(productService.getProductById(product.getProductId()) != null){
						Product lcProduct = productService.getProductById(product.getProductId());
						map.put("lcAvailability", Integer.toString(1));
						if(lcProduct.getSiUnit() == null)
							map.put("lcUnit", "null");							
						else
							map.put("lcUnit", lcProduct.getSiUnit());
						map.put("lcUQty", Integer.toString(lcProduct.getUnitQty()));
						map.put("lcRate", Float.toString(lcProduct.getUnitRate()));
						map.put("lcStatus", Integer.toString(lcProduct.getStatus()));
						map.put("lcPType", lcProduct.getProductType().getName());
					}
					else{
						map.put("lcAvailability", Integer.toString(0));
						map.put("lcUnit", "null");
						map.put("lcUQty", "0");
						map.put("lcRate", "0.0");
						map.put("lcStatus","0");
						map.put("lcPType", "null");
					}
					Listmap.add(map);
				}
			}
			productTypeMap.put(ptype.getName(), Listmap);
		}
		return productTypeMap;
	
	}
	
	@Transactional
	@RequestMapping(value = "/temp",method = RequestMethod.POST )
	public String productsTypeLcp(@RequestParam(value="orgabbr") String orgabbr, @RequestBody String requestBody, HttpServletResponse  response){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		//List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		//List<LcpProduct> productList = lcpproductService.getLcpProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation("lcart");
		
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		JSONObject jobj = new JSONObject();
		String type = null;
//		if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
//			try {
//				responseJsonObject.put("status", "Failure");	
//				responseJsonObject.put("error", "You are no longer a member");
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//			return responseJsonObject.toString();
//		}
		try{
			jobj = new JSONObject(requestBody);
			type = jobj.getString("type");
		}
		catch(JSONException e){
			e.printStackTrace();
		}
			//List<Object[]> iter= new ArrayList<Object[]>();
			JSONArray jsonArray = new JSONArray();	
			
			for(Product product: productList) {
				if(product.getProductType().getName().equals("Dairy Products")){
				ProductType pt = productTypeService.getReturnProductTypeById(147);
				System.out.println("Hey");
				
				LcpProduct lcpproduct = new LcpProduct(organization, product.getName(), pt, product.getUnitRate(), product.getQuantity(), product.getDescription(), product.getImageUrl(), product.getStatus());
				lcpProductRepository.save(lcpproduct);
				}
				JSONObject jsonObject = new JSONObject();
				if(type.equals(product.getProductType().getName())) {
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
					if(product.getImageUrl() == null)
						jsonObject.put("imageUrl", "");
					else
						jsonObject.put("imageUrl", product.getImageUrl());
					//jsonObject.put("moq", product.getMoq());
					//jsonObject.put("logistics", product.getLogAvailability());
					//jsonObject.put("organization", product.getOrganization().getName());
					//jsonObject.put("unit", product.getSiUnit());
					if(product.getDescription() == null)
						jsonObject.put("description", "");
					else
						jsonObject.put("description", product.getDescription());
					jsonArray.put(jsonObject);
					
					
					} catch (JSONException e) {
						e.printStackTrace();
				}
				
			}
		}
		try {
			responseJsonObject.put("products", jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
			return responseJsonObject.toString();  
	}
	
	@Transactional
	@RequestMapping(value = "{id}/getDesc",method = RequestMethod.GET )
	public String getDesc(@PathVariable int id) {
		JSONObject responseJsonObject = new JSONObject();
		Product prod = productService.getProductById(id);
		String desc = prod.getDescription();
		try {
			responseJsonObject.put("desc", desc);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	

	@Transactional
	@RequestMapping(value = "/lcptypemapnew",method = RequestMethod.GET )
	public String productTypeListLcp(@RequestParam(value="orgabbr") String orgabbr, HttpServletResponse  response){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		//List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		//List<LcpProduct> productList = lcpproductService.getLcpProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<ProductType> productList = lcpproductService.getProTypeList(organizationService.getOrganizationByAbbreviation(orgabbr));
 		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		
		/*if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
			try {
				responseJsonObject.put("status", "Failure");	
				responseJsonObject.put("error", "You are no longer a member");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}*/
		
		//for (ProductType ptype: productTypeList)
		for (ProductType ptype: productList)
			
			{
			//List<Object[]> iter= new ArrayList<Object[]>();
			JSONArray jsonArray = new JSONArray();	
			try {
				JSONObject temp = new JSONObject();
				//temp.put(ptype.getName(), jsonArray);
				temp.put("productTypeId", ptype.getProductTypeId());
				temp.put("status", ptype.getStatus());
				temp.put("typename", ptype.getName());
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("types", details);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
			return responseJsonObject.toString();  
	}
 	//New mapnew version after Aug 3
 	
	@Transactional
	@RequestMapping(value = "/lcptypewiseproducts",method = RequestMethod.POST )
	public String productsByTypeLcp(@RequestParam(value="orgabbr") String orgabbr, @RequestBody String requestBody, HttpServletResponse  response){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		//List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<LcpProduct> productList = lcpproductService.getLcpProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		JSONObject jobj = new JSONObject();
		String type = null;
		if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
			try {
				responseJsonObject.put("status", "Failure");	
				responseJsonObject.put("error", "You are no longer a member");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		try{
			jobj = new JSONObject(requestBody);
			type = jobj.getString("type");
		}
		catch(JSONException e){
			e.printStackTrace();
		}
			//List<Object[]> iter= new ArrayList<Object[]>();
			JSONArray jsonArray = new JSONArray();	
			
			for(LcpProduct product: productList) {
				JSONObject jsonObject = new JSONObject();
				if(type.equals(product.getProductType().getName()) && (product.getStatus() == 1) && (product.getQuantity() >= product.getMoq())) {
					//System.out.println(ptype.getName());
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
					if(product.getImageUrl() == null)
						jsonObject.put("imageUrl", "");
					else
						jsonObject.put("imageUrl", product.getImageUrl());
					jsonObject.put("moq", product.getMoq());
					jsonObject.put("logistics", (product.getLogAvailability() == 0 ? "No" : "Yes"));
					jsonObject.put("deliveryCharge", organizationService.getOrganizationByAbbreviation(orgabbr).getDelCharge());	
					jsonObject.put("organization", product.getOrganization().getName());
					jsonObject.put("unit", product.getSiUnit());
					//jsonObject.put("audioUrl", product.getAudioUrlWav());	
					//jsonObject.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						jsonObject.put("description", "");
					else
						jsonObject.put("description", product.getDescription());
					jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				
			}
			}
		try {
			responseJsonObject.put("products", jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
			return responseJsonObject.toString();  
	}
	
	
	//Lokacartplus 
	@Transactional
	@RequestMapping(value = "/lcpmapnew",method = RequestMethod.GET )
	public String productListByTypeLcp(@RequestParam(value="orgabbr") String orgabbr, HttpServletResponse  response){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		//List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<LcpProduct> productList = lcpproductService.getLcpProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		
		if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
			try {
				responseJsonObject.put("status", "Failure");	
				responseJsonObject.put("error", "You are no longer a member");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();
			JSONArray jsonArray = new JSONArray();	
			
			for(LcpProduct product: productList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) && (product.getStatus() == 1) && (product.getMoq() <= product.getQuantity())) {
					//System.out.println(ptype.getName());
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", product.getUnitRate());
					jsonObject.put("imageUrl", product.getImageUrl());
					jsonObject.put("moq", product.getMoq());
					jsonObject.put("logistics", (product.getLogAvailability() == 0 ? "No" : "Yes"));
					jsonObject.put("deliveryCharge", organizationService.getOrganizationByAbbreviation(orgabbr).getDelCharge());	
					jsonObject.put("organization", product.getOrganization().getName());
					jsonObject.put("unit", product.getSiUnit());
					//jsonObject.put("audioUrl", product.getAudioUrlWav());	
					//jsonObject.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						jsonObject.put("description", "");
					else
						jsonObject.put("description", product.getDescription());
					jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				
			}
			}
			try {
				JSONObject temp = new JSONObject();
				temp.put(ptype.getName(), jsonArray);
				temp.put("productTypeId", ptype.getProductTypeId());
				temp.put("status", ptype.getStatus());
				temp.put("typename", ptype.getName());
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("products", details);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
			return responseJsonObject.toString();  
	}
	
	
	@Transactional
	@RequestMapping(value = "/lcpmaphahaha",method = RequestMethod.GET )
	public String productListForLcp(@RequestParam(value="orgabbr") String orgabbr, HttpServletResponse  response){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		//List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<LcpProduct> productList = lcpproductService.getLcpProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		JSONArray jsonArray = new JSONArray();	
		
		if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
			try {
				responseJsonObject.put("status", "Failure");	
				responseJsonObject.put("error", "You are no longer a member");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();		
			for(LcpProduct product: productList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) && (product.getStatus() == 1) && (product.getMoq() <= product.getQuantity())) {
					//System.out.println(ptype.getName());
					try {
						jsonObject.put("id", Integer.toString(product.getProductId()));
						jsonObject.put("name", product.getName());
						jsonObject.put("quantity", Integer.toString((product.getQuantity())));
						jsonObject.put("unitRate", product.getUnitRate());
						jsonObject.put("imageUrl", product.getImageUrl());
						jsonObject.put("moq", product.getMoq());
						jsonObject.put("logistics", (product.getLogAvailability() == 0 ? "No" : "Yes"));
						jsonObject.put("deliveryCharge", organizationService.getOrganizationByAbbreviation(orgabbr).getDelCharge());	
						jsonObject.put("organization", product.getOrganization().getName());
						jsonObject.put("unit", product.getSiUnit());
						jsonObject.put("status", product.getStatus());
						jsonObject.put("pType", product.getProductType().getName());
						jsonObject.put("pTypeId", product.getProductType().getProductTypeId());
						//jsonObject.put("audioUrl", product.getAudioUrlWav());	
						//jsonObject.put("stockManagement", stockManagement.toString());
						if(product.getDescription() == null)
							jsonObject.put("description", "");
						else
							jsonObject.put("description", product.getDescription());
						jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}				
				}
			}
		}
		try {
			responseJsonObject.put("products", jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
			return responseJsonObject.toString();  
	}
	
	
	@Transactional
	@RequestMapping(value = "{org}/getList",method = RequestMethod.GET )
	public List<ProductsManage> getProductsList(@PathVariable String org) {

		List<ProductsManage> prodrows = new ArrayList<ProductsManage>();

		Organization organization = organizationService.getOrganizationByAbbreviation(org);

		List<Product> prodList = productService.getProductList(organization);

		for(Product product : prodList)
		{		
			try
			{
				// Get required attributes for each user
				String name = product.getName();
				String audioUrlWav = product.getAudioUrlWav();
				String imageUrl = product.getImageUrl();
				int productId = product.getProductId();
				String audioUrl = product.getAudioUrl();
				int status = product.getStatus();
				float unitRate = product.getUnitRate();
				int quantity = product.getQuantity();
				String productType = product.getProductType().getName();
				ProductsManage prodrow = new ProductsManage(name, audioUrlWav, imageUrl, productId, audioUrl, status, unitRate, quantity, productType);
				prodrows.add(prodrow);
			}
			catch(NullPointerException e)
			{
				//System.out.println("Order ID: " + order.getOrderId() + " has no name.");
			}
		}
		return prodrows;
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/mapnew",method = RequestMethod.GET )
	public String productListByTypeNew(@RequestParam(value="orgabbr") String orgabbr, HttpServletResponse  response){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		
		if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
			try {
				responseJsonObject.put("status", "Failure");	
				responseJsonObject.put("error", "You are no longer a member");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();
			JSONArray jsonArray = new JSONArray();	
			
			for(Product product: productList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) && (product.getStatus() == 1)) {
					System.out.println(ptype.getName());
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
					jsonObject.put("imageUrl", product.getImageUrl());
					jsonObject.put("audioUrl", product.getAudioUrlWav());	
					jsonObject.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						jsonObject.put("description", "");
					else
						jsonObject.put("description", product.getDescription());
					jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				
			}
			}
			try {
				JSONObject temp = new JSONObject();
				temp.put(ptype.getName(), jsonArray);
				temp.put("productId", ptype.getProductTypeId());
				temp.put("status", ptype.getStatus());
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			//paytm detail added
			responseJsonObject.put("paytm", (organization.isPaytm() ? 1 : 0));
			responseJsonObject.put("products", details);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setHeader("Access-Control-Allow-Origin", "*");
			return responseJsonObject.toString();  
	}
	
	
	
	//New mapnew version after Aug 3


	@Transactional
	@RequestMapping(value = "/getproductsmap",method = RequestMethod.GET )
	public String productListByTypeAug(@RequestParam(value="orgabbr") String orgabbr){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		
		if (organizationMembershipService.getUserOrganizationMembership(currentUser, organization) == null){
			try {
				responseJsonObject.put("status", "Failure");	
				responseJsonObject.put("error", "You are no longer a member");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();
			JSONArray jsonArray = new JSONArray();	
			
			for(Product product: productList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) /*&& (product.getStatus() == 1)*/) {
					System.out.println(ptype.getName());
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
					jsonObject.put("imageUrl", product.getImageUrl());
					jsonObject.put("audioUrl", product.getAudioUrl());	
					jsonObject.put("status", product.getStatus());
					jsonObject.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						jsonObject.put("description", "");
					else
						jsonObject.put("description", product.getDescription());
					jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				
			}
			}
			try {
				JSONObject temp = new JSONObject();
				temp.put(ptype.getName(), jsonArray);
				temp.put("productId", ptype.getProductTypeId());
				temp.put("status", ptype.getStatus());
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("products", details);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();  
	}

	
	@Transactional
	@RequestMapping(value = "/getproductsmapnew",method = RequestMethod.GET )
	public String productListByTypes(@RequestParam(value="orgabbr") String orgabbr){
		List<ProductType> productTypeList=productTypeService.getAllByOrganisationSortedBySequence(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<Product> productList = productService.getProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		User currentUser = userService.getCurrentUser();
		String orgabbrLCP = "lcart";
		JSONObject responseJsonObject = new JSONObject();
		JSONArray pTypeArray = new JSONArray();
		JSONArray details = new JSONArray();
		JSONArray siArray = new JSONArray();
		JSONArray unitArrayLC = new JSONArray();
		JSONArray unitArrayLCP = new JSONArray();
		JSONObject siObject = new JSONObject();
		List<SiUnit> unitListLC = siUnitRepository.findAllByOrderByLcGroupDesc();
		int i = unitListLC.get(0).getLcGroup();
		List<SiUnit> unitListLCP = siUnitRepository.findAllByOrderByLcpGroupDesc();
		int j = unitListLCP.get(0).getLcGroup();
		List<ProductType> prodTypeListLCP = productTypeService.getAllByOrganisation(organizationService.getOrganizationByAbbreviation(orgabbrLCP));
		
		try{
			for(SiUnit siUnit : unitListLC){
				JSONObject unitObj = new JSONObject();
				if(siUnit.getLcGroup() == i){
					unitObj.put("name", siUnit.getSiUnit());
					siArray.put(unitObj);
				}
				else{
					siObject.put("units", siArray);
					siObject.put("group", i);
					unitArrayLC.put(siObject);
					siArray = new JSONArray();
					siObject = new JSONObject();
					unitObj.put("name", siUnit.getSiUnit());
					siArray.put(unitObj);
					i = siUnit.getLcGroup();
				}
			}
			if(i != 0){
				siObject.put("units", siArray);
				siObject.put("group", i);
				unitArrayLC.put(siObject);
			}
			
			siArray = new JSONArray();
			siObject = new JSONObject();
			
			for(SiUnit siUnit : unitListLCP){
				JSONObject unitObj = new JSONObject();
				if(siUnit.getLcpGroup() == j){
					unitObj.put("name", siUnit.getSiUnit());
					siArray.put(unitObj);
				}
				else{
					siObject.put("units", siArray);
					siObject.put("group", j);
					unitArrayLCP.put(siObject);
					siArray = new JSONArray();
					siObject = new JSONObject();
					unitObj.put("name", siUnit.getSiUnit());
					siArray.put(unitObj);
					j = siUnit.getLcpGroup();
				}
			}
			if(j != 0){
				siObject.put("units", siArray);
				siObject.put("group", j);
				unitArrayLC.put(siObject);
			}
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		
		if(prodTypeListLCP != null){
			for (ProductType ptype: prodTypeListLCP)
			{
				JSONObject pTypeObject = new JSONObject();
				try{
					pTypeObject.put("id", ptype.getProductTypeId());
					pTypeObject.put("name", ptype.getName());
					pTypeArray.put(pTypeObject);
				}
				catch(JSONException e){
					e.printStackTrace();
				}
			}
		}
		
		
		for (ProductType ptype: productTypeList)
		{
			//List<Object[]> iter= new ArrayList<Object[]>();
			JSONArray jsonArray = new JSONArray();	
			
			for(Product product: productList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) /*&& (product.getStatus() == 1)*/) {
					System.out.println(ptype.getName());
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					if(product.getJustName() != null)
						jsonObject.put("prodName", product.getJustName());
					else
						jsonObject.put("prodName", "null");
					if(product.getSiUnit() != null){
						jsonObject.put("siUnit", product.getSiUnit());
					}
					else{
						jsonObject.put("siUnit", "null");
					}
					if(product.getUnitQty() != 0){
						jsonObject.put("unitQty", Integer.toString(product.getUnitQty()));
					}
					else{
						jsonObject.put("unitQty", Integer.toString(0));
					}
					jsonObject.put("unitQty", Integer.toString(product.getUnitQty()));
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
					if(product.getImageUrl() == null)
						jsonObject.put("imageUrl", product.getImageUrl());
					else
						jsonObject.put("imageUrl", JSONObject.NULL);
					if(product.getAudioUrl() == null)
						jsonObject.put("audioUrl", product.getAudioUrl());	
					else
						jsonObject.put("audioUrl", JSONObject.NULL);
					jsonObject.put("status", Integer.toString(product.getStatus()));
					try{
						jsonObject.put("unitGroup", Integer.toString(siUnitRepository.findBySiUnit(product.getSiUnit()).getLcGroup()));
					} 
					catch(NullPointerException e)
					{
						jsonObject.put("unitGroup", Integer.toString(0));						
					}
					jsonObject.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						jsonObject.put("description", "");
					else
						jsonObject.put("description", product.getDescription());
					if(lcpproductService.getProductById(product.getProductId()) != null){
						LcpProduct lcpProduct = lcpproductService.getProductById(product.getProductId());
						jsonObject.put("lcpAvailability", Integer.toString(1));
						jsonObject.put("lcpUnit", lcpProduct.getSiUnit());
						jsonObject.put("lcpMoq", Integer.toString(lcpProduct.getMoq()));
						jsonObject.put("lcpPType", lcpProduct.getProductType().getName());
						jsonObject.put("lcpRate", Float.toString(lcpProduct.getUnitRate()));
						jsonObject.put("lcpStatus", Integer.toString(lcpProduct.getStatus()));
						jsonObject.put("lcpLogistic", Integer.toString(lcpProduct.getLogAvailability()));
					}
					else{
						jsonObject.put("lcpAvailability", "0");
						jsonObject.put("lcpUnit", "0");
						jsonObject.put("lcpMoq", "0");
						jsonObject.put("lcpPType", "null");
						jsonObject.put("lcpRate", "0.0");
						jsonObject.put("lcpStatus", "0");
						jsonObject.put("lcpLogistic", "0");
					}
					jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
			try {
				JSONObject temp = new JSONObject();
				temp.put(ptype.getName(), jsonArray);
				temp.put("productId", ptype.getProductTypeId());
				temp.put("status", ptype.getStatus());
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("products", details);
			responseJsonObject.put("pTypes", pTypeArray);
			responseJsonObject.put("siUnitLC", unitArrayLC);
			responseJsonObject.put("siUnitLCP", unitArrayLCP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();  
	}

	
	@Transactional
	@RequestMapping(value = "/getproductsmaplcp",method = RequestMethod.GET )
	public String productListByTypeLcpNew(@RequestParam(value="orgabbr") String orgabbr){
		String orgabbrLCP = "lcart";
		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		List<ProductType> prodTypeList = lcpproductService.getProductTypeList(organizationService.getOrganizationByAbbreviation(orgabbrLCP));
		//List<ProductType> prodTypeList = productTypeService.getAllByOrganisation(organizationService.getOrganizationByAbbreviation(orgabbrLCP));
		List<ProductType> prodTypeListLC = productTypeService.getAllByOrganisation(organization);
		List<LcpProduct> prodList = lcpproductService.getLcpProductListAdmin(organization);
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		/*int last_id = productService.getAllProductListSortedByIdDesc().get(0).getProductId();
		System.out.println(last_id);*/
		JSONArray pTypeArray = new JSONArray();
		JSONArray siArray = new JSONArray();
		JSONArray unitArrayLC = new JSONArray();
		JSONArray unitArrayLCP = new JSONArray();
		JSONObject siObject = new JSONObject();
		List<SiUnit> unitListLC = siUnitRepository.findAllByOrderByLcGroupDesc();
		int i = unitListLC.get(0).getLcGroup();
		List<SiUnit> unitListLCP = siUnitRepository.findAllByOrderByLcpGroupDesc();
		int j = unitListLCP.get(0).getLcGroup();
		try{
			for(SiUnit siUnit : unitListLC){
				JSONObject unitObj = new JSONObject();
				if(siUnit.getLcGroup() == i){
					unitObj.put("name", siUnit.getSiUnit());
					siArray.put(unitObj);
				}
				else{
					siObject.put("units", siArray);
					siObject.put("group", i);
					unitArrayLC.put(siObject);
					siArray = new JSONArray();
					siObject = new JSONObject();
					unitObj.put("name", siUnit.getSiUnit());
					siArray.put(unitObj);
					i = siUnit.getLcGroup();
				}
			}
			if(i != 0){
				siObject.put("units", siArray);
				siObject.put("group", i);
				unitArrayLC.put(siObject);
			}
			
			siArray = new JSONArray();
			siObject = new JSONObject();
			
			for(SiUnit siUnit : unitListLCP){
				JSONObject unitObj = new JSONObject();
				if(siUnit.getLcpGroup() == j){
					unitObj.put("name", siUnit.getSiUnit());
					siArray.put(unitObj);
				}
				else{
					siObject.put("units", siArray);
					siObject.put("group", j);
					unitArrayLCP.put(siObject);
					siArray = new JSONArray();
					siObject = new JSONObject();
					unitObj.put("name", siUnit.getSiUnit());
					siArray.put(unitObj);
					j = siUnit.getLcpGroup();
				}
			}
			if(j != 0){
				siObject.put("units", siArray);
				siObject.put("group", j);
				unitArrayLC.put(siObject);
			}
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		
		if(prodTypeListLC != null){
			for (ProductType ptype: prodTypeListLC)
			{
				JSONObject pTypeObject = new JSONObject();
				try{
					pTypeObject.put("id", ptype.getProductTypeId());
					pTypeObject.put("name", ptype.getName());
					pTypeArray.put(pTypeObject);
				}
				catch(JSONException e){
					e.printStackTrace();
				}
			}
		}
		
		for (ProductType ptype: prodTypeList)
		{				
			JSONArray jsonArray = new JSONArray();			
			for(LcpProduct product: prodList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) /*&& (product.getStatus() == 1)*/) {
					try {
						jsonObject.put("id", Integer.toString(product.getProductId()));
						jsonObject.put("name", product.getName());
						jsonObject.put("quantity", Integer.toString((product.getQuantity())));
						jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
						jsonObject.put("description", product.getDescription());
						jsonObject.put("minqty", Integer.toString(product.getMoq()));
						jsonObject.put("si", product.getSiUnit());
						jsonObject.put("logistics", (product.getLogAvailability() == 0 ? "No" : "Yes"));
						jsonObject.put("deliveryCharge", organizationService.getOrganizationByAbbreviation(orgabbr).getDelCharge());	
						try{
							jsonObject.put("unitGroup", Integer.toString(siUnitRepository.findBySiUnit(product.getSiUnit()).getLcpGroup()));
						} catch(NullPointerException e){}
						if(product.getImageUrl() == null || product.getImageUrl().length()<=1)
							jsonObject.put("imageUrl", "NA");
						else
							jsonObject.put("imageUrl", product.getImageUrl());
						jsonObject.put("status", Integer.toString(product.getStatus()));
						jsonObject.put("stockManagement", stockManagement.toString());
						if(product.getDescription() == null)
							jsonObject.put("description", "NA");
						else
							jsonObject.put("description", product.getDescription());
						if(productService.getProductById(product.getProductId()) != null){
							Product lcProduct = productService.getProductById(product.getProductId());
							jsonObject.put("lcAvailability", "1");
							jsonObject.put("lcUnit", lcProduct.getSiUnit());
							jsonObject.put("lcUQty", Integer.toString(lcProduct.getUnitQty()));
							jsonObject.put("lcRate", Float.toString(lcProduct.getUnitRate()));
							jsonObject.put("lcPType", lcProduct.getProductType().getName());
							jsonObject.put("lcStatus", Integer.toString(lcProduct.getStatus()));
						}
						else{
							jsonObject.put("lcAvailability", "0");
							jsonObject.put("lcUnit", "null");
							jsonObject.put("lcUQty", "0");
							jsonObject.put("lcRate", "0.0");
							jsonObject.put("lcPType", "null");
							jsonObject.put("lcStatus", "0");
						}
						jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}				
				}
			}
			try {
				JSONObject temp = new JSONObject();
				temp.put(ptype.getName(), jsonArray);
				temp.put("productId", ptype.getProductTypeId());
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("products", details);
			responseJsonObject.put("pTypes", pTypeArray);
			responseJsonObject.put("siUnitLC", unitArrayLC);
			responseJsonObject.put("siUnitLCP", unitArrayLCP);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	
	/*@Transactional
	@RequestMapping(value = "/getproductsmaplcp",method = RequestMethod.GET )
	public String productListByTypeLcp(){
		String orgabbr = "lcart";
		List<ProductType> prodTypeList = productTypeService.getAllByOrganisation(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<LcpProduct> prodList = lcpproductService.getLcpProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		JSONObject responseJsonObject = new JSONObject();
		JSONArray details = new JSONArray();
		int last_id = productService.getAllProductListSortedByIdDesc().get(0).getProductId();
		System.out.println(last_id);
		for (ProductType ptype: prodTypeList)
		{
			JSONArray jsonArray = new JSONArray();			
			for(LcpProduct product: prodList) {
				JSONObject jsonObject = new JSONObject();
				if(ptype.getName().equals(product.getProductType().getName()) && (product.getStatus() == 1)) {
					try {
					jsonObject.put("id", Integer.toString(product.getProductId()));
					jsonObject.put("name", product.getName());
					jsonObject.put("quantity", Integer.toString((product.getQuantity())));
					jsonObject.put("unitRate", Float.toString(product.getUnitRate()));
					jsonObject.put("description", product.getDescription());
					jsonObject.put("minqty", product.getMoq());
					jsonObject.put("si", product.getSiUnit());
					jsonObject.put("logistics", product.getLogAvailability());	
					jsonObject.put("deliveryCharge", organizationService.getOrganizationByAbbreviation(orgabbr).getDelCharge());		
					if(product.getImageUrl() == null || product.getImageUrl().length()<=1)
						jsonObject.put("imageUrl", "NA");
					else
						jsonObject.put("imageUrl", product.getImageUrl());
					jsonObject.put("status", product.getStatus());
					jsonObject.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						jsonObject.put("description", "NA");
					else
						jsonObject.put("description", product.getDescription());
					jsonArray.put(jsonObject);
					} catch (JSONException e) {
						e.printStackTrace();
					}				
				}
			}
			try {
				JSONObject temp = new JSONObject();
				temp.put(ptype.getName(), jsonArray);
				temp.put("productId", ptype.getProductTypeId());
				details.put(temp);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("products", details);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();  
	}*/
	
	
	@Transactional
	@RequestMapping(value = "/{id}/setDesc", method = RequestMethod.POST)
	public String setDesc(@RequestBody String requestBody, @PathVariable int id)
	{
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		Product product = productService.getProductById(id);
		try {
			jsonObject = new JSONObject(requestBody);
			String desc = jsonObject.getString("desc");
			product.setDescription(desc);
			System.out.println(desc +"  "+ product.getDescription());
			responseJsonObject.put("status", "success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/byname",method = RequestMethod.POST ,produces="application/json" )
	public String updatedPrice(@RequestBody String requestBody)
	{
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject=null;
		JSONArray productListJsonArray = null;
		JSONArray proArray= new JSONArray();
		int org_id = 0;
		int flag=0;
		try {
			jsonObject = new JSONObject(requestBody);
			org_id = jsonObject.getInt("org_id");
			productListJsonArray = jsonObject.getJSONArray("product_list");
			responseJsonObject.put("status", "updated");
			Organization organization= organizationRepository.findOne(org_id); //Finding organization of the product name
			Boolean stockManagement = organization.getStockManagement();
			responseJsonObject.put("stockManagement",stockManagement.toString());
			for (int i=0;i<productListJsonArray.length();i++)
			{
				 try {
					JSONObject productObj = productListJsonArray.getJSONObject(i);
					String product_name = productObj.getString("product_name");
					float price = Float.parseFloat((productObj.getString("price")));
					if(organization==null)
					{
						responseJsonObject.put("Status", "Failure");
						responseJsonObject.put("Error", "Organization with Id "+org_id+" does not exists");
						return responseJsonObject.toString();
					}
					else
					{
						Product product = productService.getProductByNameAndOrg(product_name, organization); 
						JSONObject up_product= new JSONObject();
						
							flag=1;
							up_product.put("product_name", product.getName());
							up_product.put("price", product.getUnitRate());
							up_product.put("quantity", Integer.toString(product.getQuantity()));
							up_product.put("audioUrl", product.getAudioUrl());
							up_product.put("imageUrl", product.getImageUrl());
							if(product.getDescription() == null)
								up_product.put("description", "");
							else
								up_product.put("description", product.getDescription());
							proArray.put(up_product);
							responseJsonObject.put("status", "updated");
							responseJsonObject.put("products", proArray);
						
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			if(flag==0)
				responseJsonObject.put("status", "no change");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
// Aug 2 Anu
	@Transactional
	@RequestMapping(value = "/toggleproducttype/{prodTypeID}", method = RequestMethod.GET)
	public @ResponseBody String getProductTypes(@PathVariable int prodTypeID) {
		System.out.println(prodTypeID+"!!!!!!");
		
		ProductType prodtype=productTypeService.findProductType(prodTypeID);
		System.out.println(prodTypeID+"!!!!!!");
		int status=prodtype.getStatus();
		System.out.println(status+"!!!");
		if(status==0)
			{status=1;}
			else
			{status=0;}
				
		List<Product> productoftype=productService.getProductListByType(prodtype);
		System.out.println(productoftype );
		
		
		
		Iterator <Product> iterator=productoftype.iterator();
		while(iterator.hasNext()){
			Product p=iterator.next();
			p.setStatus(status);
		}
		System.out.println(prodtype.getStatus()+"!!"+prodtype.getName()+" "+prodtype.getOrganization());
		prodtype.setStatus(status);
		System.out.println(prodtype.getStatus()+"!!");
		
		
	
		JSONObject responseJsonObject = new JSONObject();
		try{
		responseJsonObject.put("response","Sucess");
		}catch(Exception e)
		{
		}
		return responseJsonObject.toString();
	}

	
	@Transactional
	@RequestMapping(value = "/lcpmap",method = RequestMethod.GET )
	public HashMap<String, List<HashMap<String, String>>> lcpProductListByType(){
		String orgabbr = "lcart";
		List<ProductType> productTypeList=productTypeService.getAllByOrganisation(organizationService.getOrganizationByAbbreviation(orgabbr));
		List<LcpProduct> productList = lcpproductService.getLcpProductList(organizationService.getOrganizationByAbbreviation(orgabbr));
		HashMap<String, List<HashMap<String, String>>> productTypeMap = new HashMap<String, List<HashMap<String, String>>>();
		Boolean stockManagement = organizationService.getOrganizationByAbbreviation(orgabbr).getStockManagement();
		for(ProductType ptype: productTypeList)
		{
			System.out.println(ptype.getName());
			List<HashMap<String, String>> Listmap=new ArrayList<HashMap<String, String>>();
			for(LcpProduct product: productList)
			{
				HashMap<String, String> map=new HashMap<String, String>();
				if(ptype.getName().equals(product.getProductType().getName()))
				{
					map.put("id", Integer.toString(product.getProductId()));
					map.put("name", product.getName());
					map.put("quantity", Integer.toString(product.getQuantity()));
					map.put("unitRate", Float.toString(product.getUnitRate()));
					map.put("imageUrl", product.getImageUrl());
					map.put("moq", Integer.toString(product.getMoq()));
					map.put("si", product.getSiUnit());
					map.put("status", Integer.toString(product.getStatus()));
					map.put("stockManagement", stockManagement.toString());
					if(product.getDescription() == null)
						map.put("description", "");
					else
						map.put("description", product.getDescription());
					Listmap.add(map);
				}
			}
			productTypeMap.put(ptype.getName(), Listmap);
		}
		return productTypeMap;	
	}
	
	
}
