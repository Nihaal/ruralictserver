package app.business.controllers.rest;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.GcmTokensService;
import app.business.services.GroupMembershipService;
import app.business.services.OrderService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.ReferralService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.business.services.message.MessageService;
import app.data.repositories.OrderRepository;
import app.data.repositories.OrganizationMembershipRepository;
import app.entities.GcmTokens;
import app.entities.Group;
import app.entities.GroupMembership;
import app.entities.Order;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.Referral;
import app.entities.User;
import app.entities.UserPhoneNumber;
import app.entities.message.Message;
import app.util.GcmRequest;
import app.util.SendMail;

@RestController
@RequestMapping("/app")
public class MemberManagementRestController {

	@Autowired
	OrganizationMembershipService organizationMembershipService;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	UserService userService;
	
	@Autowired
	OrderService orderService;

	@Autowired
	UserPhoneNumberService userPhoneNumberService;

	@Autowired
	GcmTokensService gcmTokensService;

	@Autowired
	MessageService messageService;

	@Autowired
	GroupMembershipService groupMembershipService;

	@Autowired
	OrganizationMembershipRepository organizationMembershipRepository;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	ReferralService referralService;

	public HashMap<String, Integer> dashBoardLocal(String orgabbr) throws ParseException {

		Organization organization = organizationService.getOrganizationByAbbreviation(orgabbr);
		Group g = organizationService.getParentGroup(organization);
		List<Message> messageapppro = messageService.getMessageListByOrderStatus(g, "binary", "processed");
		List<Message> messageappnew = messageService.getMessageListByOrderStatus(g, "binary", "saved");
		List<Message> messageappcan = messageService.getMessageListByOrderStatus(g, "binary", "cancelled");
		
		List<Order> paidOrders = orderRepository.findByOrganizationAndStatusAndText(organization, "delivered", "Paid");
		List<Order> unpaidOrders = orderRepository.findByOrganizationAndStatusAndText(organization, "delivered", "Unpaid");

		List<Order> deliveredOrders = orderService.getOrderByOrganizatonDeliveredSorted(organization);
		HashMap<String, Integer> dashmap = new HashMap<String, Integer>();
		dashmap.put("saved", messageappnew.size());
		dashmap.put("processed", messageapppro.size());
		dashmap.put("cancelled", messageappcan.size());

		dashmap.put("paid", paidOrders.size());
		dashmap.put("unpaid", unpaidOrders.size());
	
		dashmap.put("delivered", deliveredOrders.size());
		
		

		List<OrganizationMembership> membershipListpending = organizationMembershipService
				.getOrganizationMembershipListByStatus(organization, 0);
		List<OrganizationMembership> membershipListapproved = organizationMembershipService
				.getOrganizationMembershipListByStatus(organization, 1);
		dashmap.put("totalUsers", membershipListapproved.size());
		dashmap.put("pendingUsers", membershipListpending.size());
		int todayUsers = 0;
		for (OrganizationMembership membership : membershipListpending) {

			User user = membership.getUser();

			try {
				Timestamp time = user.getTime();

				Calendar cal = Calendar.getInstance();
				cal.clear(Calendar.HOUR_OF_DAY);
				cal.clear(Calendar.AM_PM);
				cal.clear(Calendar.MINUTE);
				cal.clear(Calendar.SECOND);
				cal.clear(Calendar.MILLISECOND);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date dateWithoutTime = sdf.parse(sdf.format(new java.util.Date()));
				if (time.after(dateWithoutTime)) {
					todayUsers = todayUsers + 1;
				}
			} catch (NullPointerException | ParseException e) {
				System.out.println("User name not having his timestamp recorded is: " + user.getName()
						+ " having userID: " + user.getUserId());
			}
		}
		dashmap.put("newUsersToday", todayUsers);
		return dashmap;
	}

	public List<String> getTargetDevices(Organization organization) {
		List<OrganizationMembership> organizationMembership = organizationMembershipService
				.getOrganizationMembershipListByIsAdmin(organization, true);
		List<String> phoneNumbers = new ArrayList<String>();
		Iterator<OrganizationMembership> membershipIterator = organizationMembership.iterator();
		while (membershipIterator.hasNext()) {
			OrganizationMembership membership = membershipIterator.next();
			User user = membership.getUser();
			phoneNumbers.add(userPhoneNumberService.getUserPrimaryPhoneNumber(user).getPhoneNumber());
		}
		Iterator<String> iterator = phoneNumbers.iterator();
		List<String> androidTargets = new ArrayList<String>();
		while (iterator.hasNext()) {
			String number = iterator.next();
			try {
				List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(number);
				Iterator<GcmTokens> iter = gcmTokens.iterator();
				while (iter.hasNext()) {

					androidTargets.add(iter.next().getToken());
				}
			} catch (Exception e) {
				System.out.println("no token for number: " + number);
			}
		}
		return androidTargets;
	}

	@Transactional
	@RequestMapping(value = "/approve", method = RequestMethod.POST)
	public String approveMember(@RequestBody String requestBody) {
		JSONObject responseJsonObject = new JSONObject();
		String organizationabbr = null;
		Organization organization = null;
		int userId = 0;
		try {
			JSONObject object = new JSONObject(requestBody);
			organizationabbr = object.getString("orgabbr");
			userId = object.getInt("userId");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			organization = organizationService.getOrganizationByAbbreviation(organizationabbr);
			User user = userService.getUser(userId);
			OrganizationMembership organizationMembership = organizationMembershipService
					.getUserOrganizationMembership(user, organization);
			organizationMembership.setStatus(1);
			organizationMembershipService.addOrganizationMembership(organizationMembership);
		} catch (Exception e) {
			try {
				responseJsonObject.put("response", "Failed");
				return responseJsonObject.toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("response", "Member Approved");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		List<String> androidTargets = getTargetDevices(organization);
		if (androidTargets.size() > 0) {
			GcmRequest gcmRequest = new GcmRequest();
			HashMap<String, Integer> dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(androidTargets, organizationabbr, dashData);
		}
		return responseJsonObject.toString();
	}

	@Transactional
	@RequestMapping(value = "/approveAll", method = RequestMethod.POST)
	public String approveAllMembers(@RequestBody String requestBody) {

		JSONObject responseJsonObject = new JSONObject();
		String organizationabbr = null;
		Organization organization = null;
		try {
			JSONObject object = new JSONObject(requestBody);
			organizationabbr = object.getString("orgabbr");
			organization = organizationService.getOrganizationByAbbreviation(organizationabbr);
			JSONArray jsonArray = object.getJSONArray("userIds");
			for (int i = 0; i < jsonArray.length(); i++) {
				int userId = jsonArray.getInt(i);
				User user = userService.getUser(userId);
				OrganizationMembership organizationMembership = organizationMembershipService
						.getUserOrganizationMembership(user, organization);
				organizationMembership.setStatus(1);
				organizationMembershipService.addOrganizationMembership(organizationMembership);

			}
		} catch (Exception e) {
			try {
				e.printStackTrace();
				responseJsonObject.put("response", "Failed");
				return responseJsonObject.toString();
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		try {
			responseJsonObject.put("response", "Members Approved");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		List<String> androidTargets = getTargetDevices(organization);
		if (androidTargets.size() > 0) {
			GcmRequest gcmRequest = new GcmRequest();
			HashMap<String, Integer> dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(androidTargets, organizationabbr, dashData);
		}
		return responseJsonObject.toString();
	}

	@Transactional
	@RequestMapping(value = "/reject", method = RequestMethod.POST)
	public @ResponseBody String rejectMember(@RequestBody String requestBody) {
		JSONObject responseJsonObject = new JSONObject();
		String organizationabbr = null;
		Organization organization = null;
		int userId = 0;
		try {
			JSONObject object = new JSONObject(requestBody);
			organizationabbr = object.getString("orgabbr");
			userId = object.getInt("userId");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			organization = organizationService.getOrganizationByAbbreviation(organizationabbr);
			User user = userService.getUser(userId);

			for (GroupMembership groupMembership : user.getGroupMemberships()) {

				if (groupMembership.getGroup().getOrganization().getName().equals(organization.getName())) {

					groupMembershipService.removeGroupMembership(groupMembership);

				}
			}

			OrganizationMembership organizationMembership = organizationMembershipService
					.getUserOrganizationMembership(user, organization);
			organizationMembershipService.removeOrganizationMembership(organizationMembership);
			try {
				responseJsonObject.put("response", "Member Removed");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			try {
				e.printStackTrace();
				responseJsonObject.put("response", "Failed");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		List<String> androidTargets = getTargetDevices(organization);
		if (androidTargets.size() > 0) {
			GcmRequest gcmRequest = new GcmRequest();
			HashMap<String, Integer> dashData = null;
			try {
				dashData = dashBoardLocal(organizationabbr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gcmRequest.broadcast(androidTargets, organizationabbr, dashData);
		}
		return responseJsonObject.toString();
	}

	@Transactional
	@RequestMapping(value = "/helper", method = RequestMethod.GET)
	public void process() {
		// List <OrganizationMembership> memberships =
		// organizationMembershipService.getAllOrganizationMembershipList();
		System.out.println("controller hit");
		List<User> users = userService.getAllUserList();
		System.out.println("Size of user list " + users.size());
		Iterator<User> userIter = users.iterator();
		while (userIter.hasNext()) {
			User user = userIter.next();
			System.out.println("going to work on user id: " + user.getUserId());
			List<OrganizationMembership> memberships = organizationMembershipService
					.getOrganizationMembershipListByUser(user);
			System.out.println("size of user memberships: " + memberships.size());
			if (memberships != null && !memberships.isEmpty()) {
				Iterator<OrganizationMembership> iter = memberships.iterator();
				int count = 1;
				while (iter.hasNext()) {
					OrganizationMembership organizationMembership = iter.next();
					System.out.println("count: " + count + " membership id: "
							+ organizationMembership.getOrganizationMembershipId());
					organizationMembership.setJoinCount(count);

					organizationMembershipService.addOrganizationMembership(organizationMembership);
					++count;
				}
			}
			System.out.println("Update for User: " + user.getUserId() + " is complete.");
		}
	}
	
	public List <String> getTargetConsumerDevices(String phonenumber) {
		List <String>androidTargets = new ArrayList<String>();

		try{
			List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(phonenumber);
			Iterator <GcmTokens> iter = gcmTokens.iterator();
			while(iter.hasNext()) {
				androidTargets.add(iter.next().getToken());
			}
			}
			catch(Exception e){
				System.out.println("no token for number: "+phonenumber);
			}
		return androidTargets;
	}
	
	@Transactional
	@RequestMapping(value = "/emailVerify/{userId}", method = RequestMethod.GET)
	public @ResponseBody String emailVerify(@PathVariable int userId) {
		
		User user = userService.getUser(userId);
		System.out.println(userId);
		user.setEmailVerified(1);
		System.out.println(userId);
		userService.addUser(user);
		List <String> androidConsumerTargets = getTargetConsumerDevices(userPhoneNumberService.getUserPrimaryPhoneNumber(userService.getUser(userId)).getPhoneNumber());
		if (androidConsumerTargets.size() > 0) {
			System.out.println("Broadcasts sent");
			GcmRequest gcmRequest = new GcmRequest();
			
				gcmRequest.consumerBroadcastWithMessage("Email", "Email verified", androidConsumerTargets, "8");
		}
		/* //bestB
		return "<html><head></head><body><img src='http://best-erp.com/extras/images/loadForEmail.jpg' width=100%></body></html";
		*/ //bestE
		
		//ictB
		return "<html><head></head><body><img src='http://ruralict.cse.iitb.in/Downloads/images/loadForEmail.jpg' width=100%></body></html";
		//ictE
	}

	@Transactional
	@RequestMapping(value = "/emailVerifyLink/{phoneNumber}", method = RequestMethod.GET)
	public @ResponseBody String emailVerifyLink(@PathVariable String phoneNumber) {
		JSONObject responseJsonObject = new JSONObject();
		phoneNumber="91"+phoneNumber;
		User user = userPhoneNumberService.getUserPhoneNumber(phoneNumber).getUser();
		if(user.getEmailVerified()==0)
		{
			//ictB
			SendMail.sendMail(user.getEmail(), "Verify Mail ID from Lokacart" , "Kindly click on this <a href='http://ruralict.cse.iitb.ac.in/ruralict/app/emailVerify/"+user.getUserId()+"'>link</a> "
					+ "to verify your mail ID, "
					+ "through which you will be receiving the notifications from Lokacart.<br>");
			//ictE
			
			/* //bestB
			SendMail.sendMail(user.getEmail(), "Verify Mail ID from Lokacart" , "Kindly click on this <a href='http://best-erp.com/ruralict/app/emailVerify/"+user.getUserId()+"'>link</a> "
					+ "to verify your mail ID, "
					+ "through which you will be receiving the notifications from Lokacart.<br>");
			*/ //bestE
			try {
				responseJsonObject.put("status", "sent");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		else
		{
			try {
				responseJsonObject.put("status", "Email Verified Already");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
				
	}
	
	@Transactional
	@RequestMapping(value = "/CheckEmailVerify/{phoneNumber}", method = RequestMethod.GET)
	public @ResponseBody String CheckEmailVerifyLink(@PathVariable String phoneNumber) {
		JSONObject responseJsonObject = new JSONObject();
		phoneNumber="91"+phoneNumber;
		User user = userPhoneNumberService.getUserPhoneNumber(phoneNumber).getUser();
		if(user.getEmailVerified()==0)
		{
		
		try {
			responseJsonObject.put("verified", 0);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseJsonObject.toString();
		}
		else
		{
			try {
				responseJsonObject.put("verified", 1);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
				
	}
	

	@Transactional
	@RequestMapping(value = "/delete",method = RequestMethod.POST )
	public @ResponseBody String deleteMembership (@RequestBody String requestBody) {
		JSONObject responseJsonObject = new JSONObject();
		String organizationabbr = null;
		Organization organization = null;
	//	int userId = 0;
		String phonenumber = null;
		try{
			JSONObject object = new JSONObject(requestBody);
			organizationabbr = object.getString("abbr");
			phonenumber = object.getString("phonenumber");
		}
		catch(JSONException e) {
			e.printStackTrace();
		}
		try {
		organization = organizationService.getOrganizationByAbbreviation(organizationabbr);
		User user = userPhoneNumberService.getUserPhoneNumber(phonenumber).getUser();
		//remove group memberships
		UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phonenumber);
	//	List <Order> list = orderRepository.findByMessage_formatAndStatusAndOrganization_abbreviationAndMessage_user_userPhoneNumbers_phoneNumber("binary", "processed", "Test2",userPhoneNumber.getPhoneNumber());
		List <Order> list = new ArrayList<Order>();
		List <Message> messages = messageService.getMessagesForUser(user, "order", "binary");
		Iterator <Message> iterator = messages.iterator();
		while (iterator.hasNext()) {
			Message message = iterator.next();
			Order order = message.getOrder();
			String paidOrNot = "";
			paidOrNot = order.getText();
			if ((order.getOrganization() == organization)){
				if(order.getStatus().equals("saved") || order.getStatus().equals("processed"))
				{
					list.add(order);
				
				}
				if(order.getStatus().equals("delivered"))
				{
					if(!order.getIsPaid())
					list.add(order);
				
				}
			}
		}
		if(!list.isEmpty()) {
			System.out.println(list.size());
			responseJsonObject.put("response", "Cannot be deleted");
			return responseJsonObject.toString();
		}
		for(GroupMembership groupMembership: user.getGroupMemberships()) {
			if(groupMembership.getGroup().getOrganization().getName().equals(organization.getName()))  
				groupMembershipService.removeGroupMembership(groupMembership); 	
		}
		//remove membership
		OrganizationMembership organizationMembership= organizationMembershipService.getUserOrganizationMembership(user, organization);
		organizationMembershipService.removeOrganizationMembership(organizationMembership);
		//proceed to deactivate referral
		List <Referral> referrals = referralService.getRemoveReferralsList(phonenumber, organization);
		Iterator <Referral> referralIter = referrals.iterator();
		while (referralIter.hasNext()) {
			referralService.removeReferral(referralIter.next());
			System.out.println("Referral removed");
		}
		}
		catch (Exception e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "error");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
	try {
		responseJsonObject.put("response", "Membership removed");
	} catch (JSONException e) {
		e.printStackTrace();
	}
	return responseJsonObject.toString();	
}
}
