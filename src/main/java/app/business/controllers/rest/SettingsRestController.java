package app.business.controllers.rest;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import app.business.services.BillLayoutSettingsService;
import app.business.services.OrganizationService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.data.repositories.OrgEnquiryRepository;
import app.entities.BillLayoutSettings;
import app.entities.OrgEnquiry;
import app.entities.Organization;
import app.entities.User;
import app.entities.UserPhoneNumber;
import app.util.Utils;

@Controller
@RequestMapping("/api/{org}")
public class SettingsRestController {

	@Autowired
	OrganizationService organizationService;
	
	@Autowired
	OrgEnquiryRepository orgEnquiryRepository;
	
	@Autowired
	BillLayoutSettingsService billLayoutSettingsService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserPhoneNumberService userPhoneNumberService;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	
	@RequestMapping(value="/billsettingsupdate", method=RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public @ResponseBody String changeBillSettings(@PathVariable String org, @RequestBody String requestBody) {
		JSONObject jsonResponseObject = new JSONObject();
		String newName = null, newAddress = null, newContact = null, newHeader = null, newFooter = null;
		try{
			JSONObject object = new JSONObject(requestBody);
			newName = object.getString("newname");
			newAddress = object.getString("newaddress");
			newContact = object.getString("newcontact");
			newHeader = object.getString("newheader");
			newFooter = object.getString("newfooter");
			Organization organization = organizationService.getOrganizationByAbbreviation(org);
			BillLayoutSettings billLayoutSettings = billLayoutSettingsService.getBillLayoutSettingsByOrganization(organization);
			organization.setName(newName);
			organization.setAddress(newAddress);
			organization.setContact(newContact);
			billLayoutSettings.setHeaderContent(newHeader);
			billLayoutSettings.setFooterContent(newFooter);
			organizationService.addOrganization(organization);
			billLayoutSettingsService.addBillLayoutSettings(billLayoutSettings);
		}
		catch(Exception e) {
			try {
				e.printStackTrace();
				jsonResponseObject.put("response", "Failed to update");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
		
		try {
			jsonResponseObject.put("response", "Successfully updated");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@Transactional
	@RequestMapping(value="/billsettingsview", method=RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")

	public @ResponseBody String viewBillSettings(@PathVariable String org) {
		JSONObject jsonResponseObject = new JSONObject();
		try{
			
			Organization organization = organizationService.getOrganizationByAbbreviation(org);
			BillLayoutSettings billLayoutSettings = billLayoutSettingsService.getBillLayoutSettingsByOrganization(organization);
			jsonResponseObject.put("name", organization.getName());
			jsonResponseObject.put("address", organization.getAddress());
			jsonResponseObject.put("contact", organization.getContact());
			jsonResponseObject.put("header", billLayoutSettings.getHeaderContent());
			jsonResponseObject.put("footer", billLayoutSettings.getFooterContent());
		}
		catch(Exception e){
			e.printStackTrace();
			try {
				jsonResponseObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
		try {
			jsonResponseObject.put("response", "success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
		
	}
	@Transactional
	@RequestMapping(value="/profilesettingsview", method=RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	
	
	public @ResponseBody String viewProfileSettings(@PathVariable String org, @RequestParam String number) {
		JSONObject jsonResponseObject = new JSONObject();
		try{
			UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(number);
			User user = userPhoneNumber.getUser();
			jsonResponseObject.put("name", user.getName());
			jsonResponseObject.put("email",user.getEmail());
			jsonResponseObject.put("phonenumber",userPhoneNumber.getPhoneNumber());
			jsonResponseObject.put("address", user.getAddress());
		}
		catch (Exception e){
			e.printStackTrace();
			try {
				jsonResponseObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
		
		try {
			jsonResponseObject.put("response", "success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return jsonResponseObject.toString();
	}
	

	@Transactional
	@RequestMapping(value="/newProfileSettings", method=RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	public @ResponseBody String getProfileSettings(@PathVariable String org, @RequestParam String number) {
		JSONObject jsonResponseObject = new JSONObject();
		OrgEnquiry orgEnquiry = null;
		try{
			Organization organization = organizationService.getOrganizationByAbbreviation(org);
			UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(number);
			User user = userPhoneNumber.getUser();
			orgEnquiry = orgEnquiryRepository.findByEmail(user.getEmail());
			jsonResponseObject.put("username", user.getName());
			if(user.getEmail().equals("91"+number+"@gmail.com"))
				jsonResponseObject.put("email","");
			else
				jsonResponseObject.put("email", user.getEmail());
			jsonResponseObject.put("contact", userPhoneNumber.getPhoneNumber());
			jsonResponseObject.put("address", user.getAddress());
			jsonResponseObject.put("orgName", organization.getName());
			jsonResponseObject.put("lokaPref", organization.getAppPref());
			if(organization.getRefCode() == null){
				jsonResponseObject.put("refCode", "null");
			}
			else{
				jsonResponseObject.put("refCode", organization.getRefCode());
			}
			if(orgEnquiry == null){
				jsonResponseObject.put("licNo", "NA");
				jsonResponseObject.put("orgLic", "NA");
				jsonResponseObject.put("panNo", "NA");
			}
			else{
				if(orgEnquiry.getLicNo() == null)
					jsonResponseObject.put("licNo", "null");
				else
					jsonResponseObject.put("licNo", orgEnquiry.getLicNo());
				if(orgEnquiry.getOrgLicUrl() == null)
					jsonResponseObject.put("orgLic", "null");
				else
					jsonResponseObject.put("orgLic", orgEnquiry.getOrgLicUrl());
				if(orgEnquiry.getPanNo() == null)
					jsonResponseObject.put("panNo", "null");
				else
					jsonResponseObject.put("panNo", orgEnquiry.getPanNo());
				
			}
		}
		catch (Exception e){
			e.printStackTrace();
			try {
				jsonResponseObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
		
		try {
			jsonResponseObject.put("response", "success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return jsonResponseObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value="/getThreshold", method=RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	
	
	public @ResponseBody String getThreshold(@PathVariable String org) {
		JSONObject jsonResponseObject = new JSONObject();
		int threshold = organizationService.getOrganizationByAbbreviation(org).getOrderThreshold();
		try {
			jsonResponseObject.put("response", threshold);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	

	@Transactional
	@RequestMapping(value="/profileUpdate", method=RequestMethod.POST)
	public @ResponseBody String changeProfileSettingsNew(@PathVariable String org, HttpServletRequest requestBody) {
		JSONObject jsonResponseObject = new JSONObject();
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) requestBody;
		String phonenumber = null, email = null, name = null, password = null, address = null, orgLicUrl = null, orgName = null, licNo = null, lokaPref = null, panNo = null;
		Boolean organicLic = true;
		try{
			licNo = mRequest.getParameter("licNo");
			lokaPref = mRequest.getParameter("lokaPref");
			orgName = mRequest.getParameter("orgName");
			phonenumber = mRequest.getParameter("contact");
			email = mRequest.getParameter("email");
			name = mRequest.getParameter("username");
			address = mRequest.getParameter("address");
			panNo = mRequest.getParameter("panNo");
			UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phonenumber);
			User user = userPhoneNumber.getUser();
			if(!user.getEmail().equals(email)){
				if(userService.getUserFromEmail(email) != null){
					jsonResponseObject.put("response", "failure");
					jsonResponseObject.put("reason", "Email already exists.");
					return jsonResponseObject.toString();
				}
			}
			Organization organization = organizationService.getOrganizationByAbbreviation(org);
			OrgEnquiry orgEnquiry = orgEnquiryRepository.findByEmail(user.getEmail());
			try{
				password = mRequest.getParameter("password");
				user.setSha256Password(passwordEncoder.encode(password));
			}
			catch(Exception e){
				//do nothing, as no password update requested
			}
			try{
				Iterator<String> itr = mRequest.getFileNames();					
				MultipartFile mFile = mRequest.getFile(itr.next());
				String fileName = mFile.getOriginalFilename();
				Random randomint = new Random();		
				File temp = Utils.saveFile("temp.jpg", Utils.getImageDir(), mFile);
				File serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
				int flag=1;
				do{
					try 
					{
						Files.copy(temp.toPath(), serverFile.toPath());
						flag=1;
					}							
					catch (FileAlreadyExistsException e)
					{
						System.out.println("File already exist. Renaming file and trying again.");
						fileName = fileName.substring(0,fileName.length()-4);
						fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".jpg";
						serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
						flag=0;
					}
					catch (IOException e) {
						e.printStackTrace();
						flag=1;
					}
				} while(flag==0);					
				orgLicUrl = Utils.getImageDirURL() + fileName;
				if(orgLicUrl.length() > 1)
					organicLic = true;
				if (temp.exists())
					temp.delete();
			}
			catch(Exception e){
				orgLicUrl = orgEnquiry.getOrgLicUrl();
				organicLic = orgEnquiry.isOrganicLic();
			}
			user.setName(name);
			user.setEmail(email);
			user.setAddress(address);
			//userService.addUser(user);
			organization.setAddress(address);
			organization.setName(orgName);
			organization.setAppPref(Integer.parseInt(lokaPref));
			try{
				organization.setRefCode(mRequest.getParameter("refCode"));
			} catch(Exception e) {}
			//organizationService.addOrganization(organization);
			if(!licNo.equals("NA")){
				orgEnquiry.setOrganicLic(organicLic);
				orgEnquiry.setLicNo(licNo);
				orgEnquiry.setOrgLicUrl(orgLicUrl);
				orgEnquiry.setEmail(email);
				orgEnquiry.setPanNo(panNo);
			}
			//orgEnquiryRepository.save(orgEnquiry);
			
			///check if new number or email exists
		}
		catch(Exception e){
			e.printStackTrace();
			try {
				jsonResponseObject.put("response", "failure");
				jsonResponseObject.put("reason", "Couldn't update. Please try again.");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
	
		try {
			jsonResponseObject.put("response", "Successfully updated");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	
	@RequestMapping(value="/profilesettingsupdate", method=RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	
	public @ResponseBody String changeProfileSettings(@PathVariable String org, @RequestBody String requestBody) {
		JSONObject jsonResponseObject = new JSONObject();
		String phonenumber = null, email =null, name = null, password = null, address = null, newNumber = null;
		try{
			JSONObject object = new JSONObject(requestBody);
			phonenumber = object.getString("phonenumber");
			try {
			newNumber = object.getString("newnumber");
			}
			catch(Exception e) {
				//caught.. :P
			}
			email = object.getString("email");
			name = object.getString("name");
			address = object.getString("address");
			UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPhoneNumber(phonenumber);
			User user = userPhoneNumber.getUser();
			try{
				password = object.getString("password");
				user.setSha256Password(passwordEncoder.encode(password));
			}
			catch(Exception e){
				//do nothing, as no password update requested
			}
			user.setName(name);
			user.setEmail(email);
			user.setAddress(address);
			if (newNumber != null)
			{
				userPhoneNumber.setPhoneNumber(newNumber);
				userPhoneNumberService.addUserPhoneNumber(userPhoneNumber);
			}
			userService.addUser(user);
		}
		catch(Exception e){
			e.printStackTrace();
			try {
				jsonResponseObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
	
		try {
			jsonResponseObject.put("response", "Successfully updated");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@RequestMapping(value="/appsettingsview", method=RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public @ResponseBody String viewAppSettings(@PathVariable String org, @RequestParam String number) {
		JSONObject jsonResponseObject = new JSONObject();
		Boolean stockManagement = null;
		Boolean autoApprove = null;
		String orgDesc = null;
		String address=null;
		String phone =  null;
		int orderThreshold = 0;
		try{
			Organization organization = organizationService.getOrganizationByAbbreviation(org);
			stockManagement = organization.getStockManagement();
			autoApprove = organization.getAutoApprove();
			address=organization.getAddress();
			phone=organization.getContacts();
			orderThreshold = organization.getOrderThreshold();
			orgDesc = organization.getOrganizationDesc();
			jsonResponseObject.put("phone", organization.getContacts());
			jsonResponseObject.put("address", organization.getAddress());
			jsonResponseObject.put("stockManagement", stockManagement);
			jsonResponseObject.put("autoApprove", autoApprove);
			jsonResponseObject.put("response", "success");
			jsonResponseObject.put("delCharges", organization.getDelCharge());
			if (orderThreshold == 0)
				jsonResponseObject.put("orderThreshold",String.valueOf(orderThreshold));
			else
				jsonResponseObject.put("orderThreshold",String.valueOf(orderThreshold));
			
			if (orgDesc == null)
				jsonResponseObject.put("description", "");
			else
				jsonResponseObject.put("description", organization.getOrganizationDesc());
			
			if ( address== null)
				jsonResponseObject.put("address","");
			else
				jsonResponseObject.put("address", address);
			if (phone == null)
				jsonResponseObject.put("phone","");
			else
				jsonResponseObject.put("phone", phone);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	return jsonResponseObject.toString();
	}
	
	@RequestMapping(value="/thresholdupdate", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody String changeStockSettings(@PathVariable String org, @RequestBody String requestBody) {
		JSONObject jsonResponseObject = new JSONObject();
		int orderThreshold = 0;
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		try{
			JSONObject object = new JSONObject(requestBody);
			orderThreshold = Integer.parseInt(object.getString("orderThreshold"));
			organization.setOrderThreshold(orderThreshold);
			organizationService.addOrganization(organization);
		}
		catch(JSONException e) {
			try {
				jsonResponseObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
		try {
			jsonResponseObject.put("response", "Successfully updated");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	

	@RequestMapping(value="/delchargeupdate", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody String changeDeliverySettings(@PathVariable String org, @RequestBody String requestBody) {
		JSONObject jsonResponseObject = new JSONObject();
		int deliveryCharges = 0;
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		try{
			JSONObject object = new JSONObject(requestBody);
			deliveryCharges = Integer.parseInt(object.getString("deliveryCharges"));
			organization.setDelCharge(deliveryCharges);
			organizationService.addOrganization(organization);
		}
		catch(JSONException e) {
			try {
				jsonResponseObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
		try {
			jsonResponseObject.put("response", "Successfully updated");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	
	@RequestMapping (value = "/orginfo", method = RequestMethod.GET, produces = "application/json")
	@Transactional
	public @ResponseBody String getOrgInfo(@PathVariable String org) {
		
		JSONObject jsonResponseObject = new JSONObject();
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		try {
			if (organization.getLogoUrl() == null)
				jsonResponseObject.put("logoUrl","");
			else
				jsonResponseObject.put("logoUrl",organization.getLogoUrl());
			
			if (organization.getOrganizationDesc() == null)
				jsonResponseObject.put("description","");
			else
				jsonResponseObject.put("description", organization.getOrganizationDesc());
			
			if (organization.getAddress() == null)
				jsonResponseObject.put("address","");
			else
				jsonResponseObject.put("address", organization.getAddress());
			if (organization.getContacts() == null)
				jsonResponseObject.put("phone","");
			else
				jsonResponseObject.put("phone", organization.getContacts());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@RequestMapping(value="/appsettingsupdate", method=RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public @ResponseBody String changeAppSettings(@PathVariable String org, @RequestBody String requestBody) {
		JSONObject jsonResponseObject = new JSONObject();
		Boolean stockManagement, autoApprove;
		int orderThreshold = 0;
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		try{
			JSONObject object = new JSONObject(requestBody);
			stockManagement = object.getBoolean("stockManagement");
			autoApprove = object.getBoolean("autoApprove");
			try{
				orderThreshold = Integer.parseInt(object.getString("orderThreshold"));
				organization.setOrderThreshold(orderThreshold);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			organization.setStockManagement(stockManagement);
			organization.setAutoApprove(autoApprove);
			
		}
		catch(Exception e) {
			e.printStackTrace();
			try {
				jsonResponseObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return jsonResponseObject.toString();
		}
		
		try {
			jsonResponseObject.put("response", "Successfully updated");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	return jsonResponseObject.toString();
	}
	
	
	@RequestMapping (value = "/orgdesc", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody String uploadOrgDesc(@PathVariable String org, @RequestBody String requestBody) {
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String description = null;
		String phone = null;
		String address = null;
		try{
			jsonObject = new JSONObject(requestBody);
			description = jsonObject.getString("description");
			phone = jsonObject.getString("phone");
			address = jsonObject.getString("address");
			System.out.println(description);
			organization.setContacts(phone);
			organization.setAddress(address);
			organization.setOrganizationDesc(description);
			organizationService.addOrganization(organization);
		}
		catch(JSONException e) {
			e.printStackTrace();
		}
		catch (Exception e1) {
			e1.printStackTrace();
			try {
				responseJsonObject.put("response", "Failed");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();

		}
		try {
			responseJsonObject.put("response", "Success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	
	
	@RequestMapping(value="/logoupload", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody String uploadLogo(@PathVariable String org, HttpServletRequest request) {
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		JSONObject responseJsonObject = new JSONObject();
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;
		Iterator<String> itr = mRequest.getFileNames();
		
		//only one iteration i.e itr.next() as it has only one file
		MultipartFile mFile = mRequest.getFile(itr.next());
		String fileName = mFile.getOriginalFilename();
		Random randomint = new Random();

		File temp = Utils.saveFile("temp.jpg", Utils.getImageDir(), mFile);
		File serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
		int flag=1;
		do{
			try 
		{
			Files.copy(temp.toPath(), serverFile.toPath());
			flag=1;
		}	
		catch (FileAlreadyExistsException e)
		{
			System.out.println("File already exist. Renaming file and trying again.");
			fileName = fileName.substring(0,fileName.length()-4);
			fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".jpg";
			serverFile = new File(Utils.getImageDir() +File.separator+ fileName);
			flag=0;
		}
		catch (IOException e) {
			e.printStackTrace();
			flag=1;
		}
	}while(flag==0);
	String url = Utils.getImageDirURL() + fileName;
	String currentUrl = organization.getLogoUrl();
	if(currentUrl != null){
		//Clean up
		int x = currentUrl.lastIndexOf(File.separator);
		String location = currentUrl.substring(x+1, currentUrl.length());
		location = Utils.getImageDir()+File.separator+location;
		System.out.println("location: "+location);
		File file = new File(location);
		if(file.exists())
			file.delete();
	}
	System.out.println("file name image: "+fileName);
	organization.setLogoUrl(url);
	organizationService.addOrganization(organization);
	if (temp.exists())
		temp.delete();
	try {
		responseJsonObject.put("response", "Image upload successful");
	} catch (JSONException e) {
		e.printStackTrace();
	}
	return responseJsonObject.toString();
	
}
}




