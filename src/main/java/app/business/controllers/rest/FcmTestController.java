package app.business.controllers.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/app")
public class FcmTestController {
	public final static String AUTH_KEY_FCM = "AAAAr9WqZIM:APA91bFwWZ3tC7-bx-EnbGemA8gDnI19dlNM5MGKVpRLnE0-QsNCO_rmdgP0VSz7PKPQY9znGSFiW517yoJuycqFDI4JWLGVfc6L-PH9qEeH0M1dWOkzjR4ErVGaT90li2qxs2oOlXu7";
	public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
	

	@RequestMapping(value = "/fcminfcm", method = RequestMethod.POST)
	public static String sendPushNotification(@RequestBody String requestBody)
	        throws IOException {
		JSONObject jObj = null;
		String title = null;
		try {
			jObj = new JSONObject(requestBody);
			title = jObj.getString("title");
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
	    URL url = new URL(API_URL_FCM);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	
	    conn.setUseCaches(false);
	    conn.setDoInput(true);
	    conn.setDoOutput(true);
	
	    conn.setRequestMethod("POST");
	    conn.setRequestProperty("Authorization", "key=" + AUTH_KEY_FCM);
	    conn.setRequestProperty("Content-Type", "application/json");
	    JSONObject json = new JSONObject();
	    JSONObject response = new JSONObject();
    try{	
	    json.put("to", "eb56Iz4YymI:APA91bFg0DGxOVMIxtmRYtiHiiLHYd9GYfpP4RmzhDQS9YBrnQN6zz1Gr42KJhEGQilL-tz971gMt86Y8VVO66t-5T4z-oBVxZfwyhYSRbBHbmaWvB6dT59HGgZ7LEP7snhdz_29Ruck");
	    JSONObject info = new JSONObject();
	    info.put("title", "Lokacart Plus"); // Notification title
	    info.put("body", "IIT Bombay Lerem Ipsum Lerem Ipsum Lerem Ipsum Lerem Ipsum Lerem Ipsum"); // Notification
	                                                            // body
	    json.put("notification", info);
	}
	catch(JSONException je){
		je.printStackTrace();
	}
	    try {
	        OutputStreamWriter wr = new OutputStreamWriter(
	                conn.getOutputStream());
	        wr.write(json.toString());
	        wr.flush();
	
	        BufferedReader br = new BufferedReader(new InputStreamReader(
	                (conn.getInputStream())));
	
	        String output;
	        System.out.println("Output from Server .... \n");
	        while ((output = br.readLine()) != null) {
	            System.out.println(output);
	        }
	        response.put("status", "success");
	    } catch (Exception e) {
	        e.printStackTrace();
	        try {
				response.put("status", "failure");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    }
	    System.out.println("FCM Notification is sent successfully");
	
	    return response.toString();
	}
	

}