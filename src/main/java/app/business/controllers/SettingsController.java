package app.business.controllers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.services.BillLayoutSettingsService;
import app.business.services.GcmTokensService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserPhoneNumberService;
import app.business.services.UserService;
import app.business.services.VoiceService;
import app.business.services.WelcomeMessageService;
import app.entities.BillLayoutSettings;
import app.entities.GcmTokens;
import app.entities.Organization;
import app.entities.SmsApiKeys;
import app.entities.User;
import app.entities.UserPhoneNumber;
import app.entities.Voice;
import app.entities.WelcomeMessage;
import app.util.GcmRequest;
import app.util.Utils;

@Controller
@RequestMapping("/web/{org}")
public class SettingsController {

	@Autowired
	OrganizationService organizationService;

	@Autowired
	WelcomeMessageService welcomeMessageService;

	@Autowired
	OrganizationMembershipService organizationMembershipService;

	@Autowired
	UserService userService;
	
	@Autowired
	UserPhoneNumberService userPhoneNumberService;

	@Autowired
	VoiceService voiceService;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	BillLayoutSettingsService billLayoutSettingsService;
	
	@Autowired
	GcmTokensService gcmTokensService;
	
	@Autowired
	SmsApiKeysService smsApiKeysService;

	@RequestMapping(value="/settingsPage")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	public String settingsPage(@PathVariable String org, Model model) {
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		BillLayoutSettings billLayout =billLayoutSettingsService.getBillLayoutSettings(billLayoutSettingsService.getBillLayoutSettingsByOrganization(organization).getBillLayoutSettingsId());
		List<Organization> organizations = organizationService.getAllOrganizationList();
		User user = userService.getCurrentUser();
		UserPhoneNumber userPhoneNumber = userPhoneNumberService.getUserPrimaryPhoneNumber(user);
		model.addAttribute("billLayout" , billLayout);
		model.addAttribute("user",user);
		model.addAttribute("organization", organization);
		model.addAttribute("organizations", organizations);
		model.addAttribute("userPhoneNumber",userPhoneNumber);
		return "settings";
	}
	

@RequestMapping(value="/sendWebSmsBroadcast", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	@ResponseBody
	public void sendWebSmsBroadcast(@PathVariable String org, @RequestBody String profileSettingDetails) {
		 System.out.println("sendWebSmsBroadcast reached!");
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		String numbersFormat="";	
		// Get the input parameters from AngularJS
		JSONObject jsonObject = null;;
		JSONObject responseJsonObject = new JSONObject();
		//Organization organization =  null;
		String message = null;
		try {
			jsonObject = new JSONObject(profileSettingDetails);
			String orgabbr = jsonObject.getString("orgabbr");
			message = jsonObject.getString("message");
			//organization = organizationService.getOrganizationByAbbreviation(orgabbr);
			numbersFormat=jsonObject.getString("phnos");
		}
		catch(JSONException e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "JSON format error");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		
		
		SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(organization);
		String authId = smsApiKeys.getAuthId();
		String authToken = smsApiKeys.getAuthToken();
		String sourceNumber = smsApiKeys.getSourceNumber();
		RestAPI api = new RestAPI(authId, authToken, "v1");
		
		
		numbersFormat=numbersFormat+"919757157339";
		System.out.println(numbersFormat);
		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("src", sourceNumber);
		parameters.put("dst",numbersFormat );
		parameters.put("text",message+"-from "+organization.getAbbreviation().toString());
		parameters.put("method"	, "GET");
		
		try {
			MessageResponse msgResponse = api.sendMessage(parameters);
			System.out.println(msgResponse);
			System.out.println("Api ID : " + msgResponse.apiId);
			System.out.println("Message : " + msgResponse.message);
			if (msgResponse.serverCode == 202) {
				//System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

			} else {
				System.out.println(msgResponse.error);
			}
		} catch (PlivoException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		
		try {
			responseJsonObject.put("response", "Message successfully broadcasted");
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//return responseJsonObject.toString();
	}





		@Transactional
		@RequestMapping(value = "/sendpushnotificationbroadcast",method = RequestMethod.POST)
		public @ResponseBody String sendPromotionalBroadcast(@PathVariable String org, @RequestBody String requestBody) {
		 
		    System.out.println("sendPushNotificationBroadcast reached!");
			Organization organization = organizationService.getOrganizationByAbbreviation(org);
			String numbersFormat="";	
			JSONObject jsonObject = null;
			JSONObject responseJsonObject = new JSONObject();
			String message = null;
			try {
				jsonObject = new JSONObject(requestBody);
				String orgabbr = jsonObject.getString("orgabbr");
				message = jsonObject.getString("message");
				organization = organizationService.getOrganizationByAbbreviation(orgabbr);
				System.out.println("organization="+organization);
				numbersFormat=jsonObject.getString("phnos");//in the form no1<no2<no3
				
			}
			catch(JSONException e) {
				e.printStackTrace();
				try {
					responseJsonObject.put("response", "JSON format error");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			}
			//numbersFormat=numbersFormat+"919757157339";
			List <String> androidTargets = Arrays.asList(numbersFormat.split("<"));
			System.out.println(androidTargets+"@@@@");
			
			
			List <String>androidTargets1 = new ArrayList<String>();
			for(int i=0;i<androidTargets.size();i++) {
				try{
				System.out.println("Android Targets "+androidTargets.get(i));
				List<GcmTokens> gcmTokens = gcmTokensService.getListByPhoneNumber(androidTargets.get(i));
				System.out.println(i+" "+gcmTokens);
				Iterator <GcmTokens> iter = gcmTokens.iterator();
				System.out.println(gcmTokens+" "+i+"@@@@@@@@@@");
				while(iter.hasNext()) {
				androidTargets1.add(iter.next().getToken());
				}	
				}catch(Exception e)
				{
					System.out.println("no tokens for phone number");
				}
				
			}
			if (androidTargets1.size() > 0) {
				System.out.println("Inside GCMREQUEST");
				GcmRequest gcmRequest = new GcmRequest();
				System.out.println("Message: "+message);
				gcmRequest.consumerPromoBroadcast(message,organization.getName(), androidTargets1, "5");
			}
			try {
				responseJsonObject.put("response", "Message successfully broadcasted");
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
	
	@RequestMapping(value="/billLayout" , method=RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	@ResponseBody
	public void billLayout(@PathVariable String org, @RequestBody Map<String,String> profileSettingDetails){
		
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		BillLayoutSettings billLayout = billLayoutSettingsService.getBillLayoutSettingsByOrganization(organization);
		String name = profileSettingDetails.get("name");
		String address = profileSettingDetails.get("address");
		String contact = profileSettingDetails.get("contact");
		String header = profileSettingDetails.get("header");
		String footer = profileSettingDetails.get("footer");
		organization = organizationService.updateOrganization(organization, address, name, contact);
		billLayout = billLayoutSettingsService.updateBillLayoutSettingsFooterContent(billLayout, footer);
		billLayout = billLayoutSettingsService.updateBillLayoutSettingsHeaderContent(billLayout, header);
		
	}

	@RequestMapping(value="/getwelcomeMessageUrl", method=RequestMethod.POST)
	@Transactional
	public @ResponseBody List<String> getVoiceObject(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{

		// Get Parameters passed from AngularJS using FormData
		int organizationid = Integer.parseInt(request.getParameter("orgid"));
		Organization organization = organizationService.getOrganizationById(organizationid);

		WelcomeMessage englishMessage = welcomeMessageService.getByOrganizationAndLocale(organization,"en");
		WelcomeMessage marathiMessage = welcomeMessageService.getByOrganizationAndLocale(organization, "mr");
		WelcomeMessage hindiMessage = welcomeMessageService.getByOrganizationAndLocale(organization, "hi");

		List<String> voices = new ArrayList<String>();
		voices.add(englishMessage.getVoice().getUrl());
		voices.add(marathiMessage.getVoice().getUrl());
		voices.add(hindiMessage.getVoice().getUrl());
		
		return voices;
	}

	@RequestMapping(value="/updateUserNP", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	@ResponseBody
	public void updateUserNP(@PathVariable String org, @RequestBody Map<String,String> profileSettingDetails) {

		Organization organization = organizationService.getOrganizationByAbbreviation(org);

		// Get the input parameters from AngularJS
		String name = profileSettingDetails.get("name");
		String email = profileSettingDetails.get("email");
		String phone = profileSettingDetails.get("phone");
		String address = profileSettingDetails.get("city");
		String role ="admin";

		User user = userService.getCurrentUser();
		UserPhoneNumber previousPrimaryPhoneNumber = userPhoneNumberService.getUserPrimaryPhoneNumber(user);

		// Add the new User to database
		user.setName(name);
		user.setAddress(address);
		user.setEmail(email);


		// Find if the number is already present in the database 
		// If present report it to the Frontend
		if(!userPhoneNumberService.findPreExistingPhoneNumber(phone))
		{
			userService.addUser(user);
		}

		else {

			// Then add the new Primary number to the database
			previousPrimaryPhoneNumber.setPhoneNumber(phone);
			userPhoneNumberService.addUserPhoneNumber(previousPrimaryPhoneNumber);
		}
	}
	
	
	@RequestMapping(value="/updateUser", method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	@ResponseBody
	public void updateUser(@PathVariable String org, @RequestBody Map<String,String> profileSettingDetails) {

		Organization organization = organizationService.getOrganizationByAbbreviation(org);

		// Get the input parameters from AngularJS
		String name = profileSettingDetails.get("name");
		String email = profileSettingDetails.get("email");
		String phone = profileSettingDetails.get("phone");
		String address = profileSettingDetails.get("city");
		String password = profileSettingDetails.get("password");
		String role ="admin";

		User user = userService.getCurrentUser();
		UserPhoneNumber previousPrimaryPhoneNumber = userPhoneNumberService.getUserPrimaryPhoneNumber(user);

		// Add the new User to database
		user.setName(name);
		user.setAddress(address);
		user.setEmail(email);
		user.setSha256Password(passwordEncoder.encode(password));


		// Find if the number is already present in the database 
		// If present report it to the Frontend
		if(!userPhoneNumberService.findPreExistingPhoneNumber(phone))
		{
			userService.addUser(user);
		}

		else {

			// Then add the new Primary number to the database
			previousPrimaryPhoneNumber.setPhoneNumber(phone);
			userPhoneNumberService.addUserPhoneNumber(previousPrimaryPhoneNumber);
		}
	}

	@RequestMapping(value="/resetwelcomeMessageUrl")
	@Transactional
	public @ResponseBody List<String> resetWelcomeMessageUrl(@PathVariable String org) {

		Organization organization = organizationService.getOrganizationByAbbreviation(org);

		WelcomeMessage englishMessage = welcomeMessageService.getByOrganizationAndLocale(organization,"en");
		WelcomeMessage marathiMessage = welcomeMessageService.getByOrganizationAndLocale(organization, "mr");
		WelcomeMessage hindiMessage = welcomeMessageService.getByOrganizationAndLocale(organization, "hi");

		welcomeMessageService.setWelcomeMessageVoice(englishMessage, 1);
		welcomeMessageService.setWelcomeMessageVoice(hindiMessage, 2);
		welcomeMessageService.setWelcomeMessageVoice(marathiMessage, 3);

		String englishMessageUrl = englishMessage.getVoice().getUrl();
		String hindiMessageUrl = hindiMessage.getVoice().getUrl();
		String marathiMessageUrl = marathiMessage.getVoice().getUrl();

		List<String> defaultVoiceUrl = new ArrayList<String>();
		defaultVoiceUrl.add(englishMessageUrl);
		defaultVoiceUrl.add(marathiMessageUrl);
		defaultVoiceUrl.add(hindiMessageUrl);

		return defaultVoiceUrl;

	}

	@RequestMapping(value="/upload/welcomeMessage", method=RequestMethod.POST, produces = "text/plain")
	@Transactional
	public @ResponseBody String handleFileUpload(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{

		// Convert for getting the files
		MultipartHttpServletRequest mRequest;
		mRequest = (MultipartHttpServletRequest) request;

		String[] supportedAudioFiletypes = new String[]{"audio/wav"};

		// Get Parameters passed from AngularJS using FormData
		int organizationid = Integer.parseInt(request.getParameter("orgid"));
		Organization organization = organizationService.getOrganizationById(organizationid);

		// Get the uploaded file and its name
		try
		{
			Iterator<String> itr = mRequest.getFileNames();
			MultipartFile uploadedAudioFile = mRequest.getFile(itr.next());

			// Check if the file is Empty or not
			if(uploadedAudioFile.isEmpty())
			{
				return "-1";
			}

			// Check if the file is of supported audio formats
			if(!Arrays.asList(supportedAudioFiletypes).contains(uploadedAudioFile.getContentType()))
			{
				// Take some action in the frontend Part
				// like an alert box saying please upload wav file
				return "-3";
			}

			//Check if the File Size is greater than 5MB
			// As if we upload a audio file much greater in size, kuckoo takes a long time to load
			if(uploadedAudioFile.getSize() > 5000000)
			{
				return "-2";
			}

			// Get the File Name
			String fileName = uploadedAudioFile.getOriginalFilename();

			String locale = request.getParameter("locale");


			String serverFolder = "/home/ruralivrs/Ruralict/apache-tomcat-7.0.42/webapps/Downloads/voices/welcomeMessage";

			// Save as Temporary File and Convert to Kuckoo Format
			File temp = new File(serverFolder + File.separator + "temp.wav" );

			// Remove spaces as kuckoo doesn't allow filename with spaces
			fileName = fileName.replaceAll("\\s+","");

			// Change Extension of the file to wav
			fileName = fileName.substring(0,fileName.length()-3);
			fileName = fileName + "wav";

			// Get the current Working Directory and the full Filepath
			
			/* //bestB
			String databaseFolder = "http://best-erp.com/extras/voices/welcomeMessage/";
			*/ //bestE
			
			//ictB
			String databaseFolder = "http://ruralict.cse.iitb.ac.in/Downloads/voices/welcomeMessage/";
			//ictE
			
			String databaseFileUrl = databaseFolder + fileName;


			// Check if the file is already present or not and rename it accordingly
			Voice previousFileSameName  = voiceService.getVoicebyUrl(databaseFileUrl);

			if(previousFileSameName != null)
			{
				// Insert some-random number to automatically rename the file
				fileName = fileName.substring(0,fileName.length()-4);
				Random randomint = new Random();
				fileName = fileName + "_" + Integer.toString(randomint.nextInt()) + ".wav";
				databaseFileUrl = databaseFolder + fileName;

			}

			// Create a new file and convert the file to appropriate Kuckoo Format
			File serverFile = new File(serverFolder + File.separator + fileName);
			uploadedAudioFile.transferTo(temp);
			serverFile = Utils.convertToKookooFormat(temp, serverFile);

			// Create a new Voice Object
			Voice voice = new Voice(databaseFileUrl, true);

			// Add the voice object in the database
			voiceService.addVoice(voice);

			// Update the Welcome Message
			WelcomeMessage welcomeMessage = welcomeMessageService.getByOrganizationAndLocale(organization, locale);
			welcomeMessage.setVoice(voice);
			welcomeMessageService.addWelcomeMessage(welcomeMessage);

			System.out.println("Welcome Message was successfully Uploaded and Updated.");
			return voice.getUrl();
		}
		catch(NoSuchElementException e)
		{
			return "-1";
		}
		catch(IOException e)
		{
		   return  "-4";	
		}
		
	}
	
	/*
	 * This part would be implemented in beta version
	@RequestMapping(value="/updateOrganizationConfiguration", method=RequestMethod.POST)
	@Transactional
	public @ResponseBody int updateOrganizationConfiguration(@RequestBody List<Map<String,String>> organizationsConfiguration){

		for(Map<String,String> organizationCon : organizationsConfiguration){
			int organizationId = Integer.parseInt(organizationCon.get("organizationId")); 
			boolean hasOnlyInbox = (organizationCon.get("hasOnlyInbox") == "true");
			boolean hasFeedback = (organizationCon.get("hasFeedback") == "true");
			boolean hasResponse = (organizationCon.get("hasResponse") == "true");
			boolean hasTextMessageResponse = organizationCon.get("hasTextMessageResponse") == "true";
			boolean hasBill = organizationCon.get("hasBill") == "true";
			Organization organization = organizationService.getOrganizationById(organizationId);
			organization.setHasOnlyInbox(hasOnlyInbox);
			organization.setHasFeedback(hasFeedback);
			organization.setHasResponse(hasResponse);
			organization.setHasTextMessageResponse(hasTextMessageResponse);
			organization.setHasBill(hasBill);
			//addOrganization has save function which updates the organization as well
			organizationService.addOrganization(organization);
		}
		
		
		return 1;
	}*/
}
