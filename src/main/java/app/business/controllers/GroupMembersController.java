package app.business.controllers;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.business.services.GroupMembershipService;
import app.business.services.GroupService;
import app.business.services.OrganizationMembershipService;
import app.business.services.OrganizationService;
import app.business.services.UserService;
import app.business.services.UserViewService;
import app.business.services.UserViewService.UserView;
import app.entities.Group;
import app.entities.GroupMembership;
import app.entities.Organization;
import app.entities.OrganizationMembership;
import app.entities.User;

@Controller
@RequestMapping("/web/{org}")
public class GroupMembersController {

	@Autowired
	UserViewService userViewService;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	GroupService groupService;

	@Autowired
	OrganizationMembershipService organizationMembershipService;

	@Autowired
	GroupMembershipService groupMembershipService;

	@Autowired
	UserService userService;

	@RequestMapping(value = "/memberList/{groupId}")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	public String settingsPage(@PathVariable String org, @PathVariable int groupId, Model model) {

		List<UserView> userViewList = userViewService.getUserViewListByGroup(groupId, org);
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		List<Group> groupList = groupService.getGroupListByOrganization(organization);
		for (UserView user : userViewList) {
			List<GroupMembership> groupMembershipList = user.getUser().getGroupMemberships();
			List<GroupMembership> removeGroupMemberShipList = new ArrayList<GroupMembership>();
			for (int i = 0; i < groupMembershipList.size(); i++) {
				if (groupMembershipList.get(i).getGroup().getOrganization().getName() != organization.getName()) {
					removeGroupMemberShipList.add(groupMembershipList.get(i));
				}
			}
			groupMembershipList.removeAll(removeGroupMemberShipList);
		}
		Group parentGroup = organizationService.getParentGroup(organization);
		model.addAttribute("organization", organization);
		model.addAttribute("userViews", userViewList);
		model.addAttribute("parentGroup", parentGroup);
		model.addAttribute("groupId", groupId);
		model.addAttribute("g", groupList);
		return "groupWiseMember";
	}

	@RequestMapping(value = "/adminTerminates", method = RequestMethod.POST)
	@Transactional
	public String terminateMembershipAsAnAdmin(@PathVariable String org, @RequestBody String requestBody) {
		Organization organization = organizationService.getOrganizationByAbbreviation(org);
		JSONObject object = null;
		JSONObject responseJsonObject = new JSONObject();
		User user = null;
		int userId = 0;
		String phno = null;
		try {
			object = new JSONObject(requestBody);
			userId = Integer.parseInt(object.getString("userid"));
			System.out.println(userId);
			user = userService.getUser(userId);
			phno = object.getString("phno");
			System.out.println(phno);

		} catch (Exception e) {
			e.printStackTrace();
			try {
				responseJsonObject.put("response", "failure");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

		}

		for (GroupMembership groupMembership : user.getGroupMemberships()) {
			if (groupMembership.getGroup().getOrganization().getName().equals(organization.getName()))
				groupMembershipService.removeGroupMembership(groupMembership);
		}

		OrganizationMembership organizationMembership = organizationMembershipService
				.getUserOrganizationMembership(user, organization);
		organizationMembershipService.removeOrganizationMembership(organizationMembership);
		try {
			responseJsonObject.put("response", "Success");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();

	}
}