package app.business.controllers;

import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import app.business.services.GroupService;
import app.business.services.OrganizationService;
import app.business.services.PresetQuantityService;
import app.business.services.ProductService;
import app.business.services.message.BinaryMessageService;
import app.business.services.message.VoiceMessageService;
import app.entities.Group;
import app.entities.PresetQuantity;
import app.entities.Product;
import app.entities.message.Message;


@Controller
@RequestMapping("/web/{org}")
public class AppMessageListController {

	@Autowired
	GroupService groupService;
	
	@Autowired
	VoiceMessageService voiceMessageService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	OrganizationService organizationService;

	@Autowired
	PresetQuantityService presetQuantityService;
	
	@Autowired
	BinaryMessageService binaryMessageService;
	
	@RequestMapping(value="/appMessage/processed")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String voiceProcessedMessage(@PathVariable String org, Model model) {
		/*Group group = organizationService.getParentGroup(organizationService.getOrganizationByAbbreviation(org));
		List<Message> appProcessedMessageList=binaryMessageService.getProcessedBinaryMessageList(group);		
		model.addAttribute("message",appProcessedMessageList);*/
		return "appProcessedMessage";
	}
	
	@RequestMapping(value="/appMessage/delivered")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String voiceDeliveredMessage(@PathVariable String org, Model model) {
		/*System.out.println("inside... :P");
		System.out.println(org);
		Group group = organizationService.getParentGroup(organizationService.getOrganizationByAbbreviation(org));
		List<Message> appDeliveredMessageList=binaryMessageService.getDeliveredBinaryMessageList(group);		
		Iterator <Message> iterator = appDeliveredMessageList.iterator();
		while(iterator.hasNext()) {
			System.out.println("--> "+iterator.next().getMessageId());
		}
		model.addAttribute("message",appDeliveredMessageList);*/
		return "appDeliveredMessage";
	}
	
	@RequestMapping(value="/appMessage/saved")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String voiceSavedMessage(@PathVariable String org, Model model) {
		/*Group group = organizationService.getParentGroup(organizationService.getOrganizationByAbbreviation(org));
		List<Message> appSavedMessageList= binaryMessageService.getSavedBinaryMessageList(group);
		List<Product> productList= productService.getProductList(organizationService.getOrganizationByAbbreviation(org));
		List<PresetQuantity> presetQuantityList= presetQuantityService.getPresetQuantityList(organizationService.getOrganizationByAbbreviation(org));
		model.addAttribute("products", productList);
		model.addAttribute("presetQuantity", presetQuantityList);
		model.addAttribute("message",appSavedMessageList);*/
		return "appSavedMessage";
	}
	
	@RequestMapping(value="/appMessage/cancelled")
	@PreAuthorize("hasRole('ADMIN'+#org)")
	@Transactional
	public String voiceCancelledMessage(@PathVariable String org, Model model) {
		Group group = organizationService.getParentGroup(organizationService.getOrganizationByAbbreviation(org));
		List<Message> appCancelledMessageList = binaryMessageService.getCancelledBinaryMessageList(group);
		model.addAttribute("message",appCancelledMessageList);
		return "appCancelledMessage";
	}
}

