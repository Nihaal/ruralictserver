package app.business.controllers;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.LcpProductService;
import app.business.services.LcpProductTypeService;
import app.business.services.OrganizationService;
import app.business.services.ProductService;
import app.business.services.ProductTypeService;
import app.data.repositories.LcpProductTypeRepository;
import app.data.repositories.ProductRepository;
import app.entities.BillLayoutSettings;
import app.entities.LcpProduct;
import app.entities.LcpProductType;
import app.entities.Order;
import app.entities.Organization;
import app.entities.Product;
import app.entities.ProductType;
import app.util.GcmRequest;
import app.util.OrderManage;
import app.util.ProductManage;
import app.util.SendBill;
import app.util.SendMail;
@Controller
@RestController
@RequestMapping("/app")
public class DataCopyController{
       @Autowired
       ProductTypeService productTypeService;

       @Autowired
       LcpProductTypeService lcpproductTypeService;
       
       @Autowired
       OrganizationService organizationService;
       
       @Autowired
       ProductService productService;
       
       @Autowired
       LcpProductService lcpproductService;
       
       @Autowired
       LcpProductTypeRepository lcpprodtyperepo;
       
       @Transactional
       @RequestMapping(value = "/copytype", method = RequestMethod.GET)
       public void copyTypes(){
    	   	  String q = null;
              List<ProductType> prod = productTypeService.getAllProductTypeList();
              Iterator<ProductType> iterator = prod.iterator();
              JSONObject responseJsonObject = new JSONObject();
              JSONArray prodArray = new JSONArray();
              //System.out.println("hereey");
              while(iterator.hasNext()){
                     ProductType p = iterator.next();
                     JSONObject obj = new JSONObject();
                     //System.out.println("heeey");
                     try{
                    	   //System.out.println("heey");
                           obj.put("id", p.getProductTypeId());
                           obj.put("name", p.getName());
                           obj.put("description", p.getDescription());
                           obj.put("sequence", p.getSequence());
                           obj.put("status", p.getStatus());
                           obj.put("imgurl", p.getImageUrl());
                           //System.out.println("hey");
                     }
                     catch(JSONException e){
                           e.printStackTrace();
                     }
                     prodArray.put(obj);
              }
              try{
                     responseJsonObject.put("producttypes",prodArray);
                     q = responseJsonObject.toString();
                     System.out.println(responseJsonObject.toString());
              }
              catch(JSONException e){
                     e.printStackTrace();
              }
              //lcpproducttype
              JSONObject json = null;
              JSONObject jObj = null;
              JSONArray jArray = null;
              String name = null;
              String description = null;
              String imageUrl = null;
              int sequence = 0;
              int status = 0;
              try{
                     jObj = new JSONObject(q);
                     jArray = jObj.getJSONArray("producttypes");
                     for(int i = 0; i < prodArray.length(); i++){
                           json = jArray.getJSONObject(i);
                           name = json.getString("name");
                           //description = json.getString("description");
                           sequence = json.getInt("sequence");
                           status = json.getInt("status");
                           //imageUrl = json.getString("imgurl");
                           LcpProductType proType = new LcpProductType(name,description, imageUrl, sequence, status);
                           lcpprodtyperepo.save(proType);
                     }
              }
              catch(JSONException e){
                     e.printStackTrace();
              }
       }

       @Transactional
       @RequestMapping(value = "/copyproducts", method = RequestMethod.GET)
       public void copyProducts(){
    	   	  String q = null;
              List<ProductType> prodType = productTypeService.getAllProductTypeList();
              ProductType p1 = prodType.get(0);
              List<Product> prod = productService.getProductList(prodType);
              Iterator<Product> iterator = prod.iterator();              
              JSONObject responseJsonObject = new JSONObject();
              JSONArray prodArray = new JSONArray();
              //System.out.println("hereey");
              while(iterator.hasNext()){
                     Product p = iterator.next();
                     JSONObject obj = new JSONObject();
                     //System.out.println("heeey");
                     try{
                    	   //System.out.println("heey");
                           obj.put("id", p.getProductId());
                           obj.put("name", p.getName());
                           obj.put("description", p.getDescription());
                           obj.put("imgurl", p.getImageUrl());
                           obj.put("status", p.getStatus());
                           obj.put("unitrate", p.getUnitRate());
                           obj.put("quantity", p.getQuantity());
                           obj.put("type", p.getProductType().getProductTypeId());
                           //System.out.println("hey");
                     }
                     catch(JSONException e){
                           e.printStackTrace();
                     }
                     prodArray.put(obj);
              }
              try{
                     responseJsonObject.put("products",prodArray);
                     q = responseJsonObject.toString();
                     System.out.println(responseJsonObject.toString());
              }
              catch(JSONException e){
                     e.printStackTrace();
              }
              //lcpproducttype
              JSONObject json = null;
              JSONObject jObj = null;
              JSONArray jArray = null;
              String name = null;
              String description = null;
              String imageUrl = null;
              int sequence = 0;
              int quantity = 0;
              int status = 0;
              int id = 0;
              float unitrate = 0;
              Organization org = null;              
              int proType;
             // Class<ProductType> proType;
              try{
                     jObj = new JSONObject(q);
                     jArray = jObj.getJSONArray("products");
                     for(int i = 0; i < prodArray.length(); i++){  
                    	 json = jArray.getJSONObject(i);
                           name = json.getString("name");
                           proType = json.getInt("type");
                           try{
                           description = json.getString("description");
                           }
                           catch(JSONException e){
                        	   description = null;
                           }
                           try{
                           imageUrl = json.getString("imgurl");
                           }
                           catch(JSONException e){
                        	   imageUrl = null;
                           }
                           status = json.getInt("status");
                           quantity = json.getInt("quantity");
                           id = json.getInt("id");
                           org = organizationService.getOrganizationByAbbreviation(json.getString("orgabbr"));
                           unitrate = BigDecimal.valueOf(json.getDouble("unitrate")).floatValue();
                           //proType = json.getClass("type");
                           /*LcpProduct lcppro = new LcpProduct(name, org, p1, unitrate, quantity, description, imageUrl, status);
                           lcpproductService.addProduct(lcppro);*/
                     }
              }
              catch(JSONException e){
                     e.printStackTrace();
              }
       }
}
