package app.business.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import app.business.services.OrganizationService;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;

@RestController
@RequestMapping("/app")
public class PaytmScraper {

	@Autowired
	OrganizationService organizationService;
	
	@RequestMapping(value="/scrape/{merchant}", method = RequestMethod.GET)
	public String demoScraper(@PathVariable String merchant){
		WebClient webClient = new WebClient(BrowserVersion.BEST_SUPPORTED);
		try{
			HtmlPage htmlPage = webClient.getPage("https://pay.paytm.com/"+organizationService.getOrganizationByAbbreviation(merchant).getPortalUrl());
			HtmlInput input = htmlPage.getElementByName("txnAmount");
			input.setAttribute("value", "123");
			HtmlInput submit = htmlPage.getElementByName("proceed payment");
			HtmlPage htmlPage2 = submit.click();
			System.out.println(htmlPage2.getBaseURL());
			input = htmlPage.getElementByName("");
			input.setAttribute("", "");
		}
		catch(Exception e){
			return "failure";
		}
		finally{
			webClient.close();
		}
		return "success";
	}
}
