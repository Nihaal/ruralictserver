package app.business.controllers;

import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.business.services.OrganizationService;
import app.business.services.UserViewService;
import app.business.services.UserViewService.UserView;
import app.data.repositories.LoginDemoRepository;
import app.entities.Loginuserdemo;
import app.entities.Organization;

@Controller
@RestController
@RequestMapping("/api")

public class LoginController {
	@Autowired
	LoginDemoRepository loginDemoRepository;
	
	@Autowired
	OrganizationService organizationService;
	
	@Transactional
	@RequestMapping(value = "/logindemo", method = RequestMethod.POST)
	public String loginMethod(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		String email = null;
		JSONObject responseJsonObject = new JSONObject();
		try{
			jsonObject = new JSONObject(requestBody);
			email = jsonObject.getString("emailid");
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		Organization organization = organizationService.getOrganizationByAbbreviation("Test2");
		Loginuserdemo demo = new Loginuserdemo();
		try{
			//System.out.println(email);
			demo = getUserbyId(email);
			//System.out.println(demo.getEmailid());
			String p = demo.getPassword();
			int ph = demo.getPhone();
			String q = organization.getLogoUrl();
			System.out.println("Field is "+q);
			
			//String q = null;
			/*try{
				responseJsonObject.put("desc", q);
				System.out.println("Field is try "+q);
			}
			catch(JSONException e){
				System.out.println("Field is Null");
			}*/
			responseJsonObject.put("desc", q);
			responseJsonObject.put("phone",ph);			
			responseJsonObject.put("success", "true");
			responseJsonObject.put("password", p);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Field is Null");
			
		}
		return responseJsonObject.toString();
	}
	
	public Loginuserdemo getUserbyId(String emailid){
		return loginDemoRepository.findByEmailid(emailid);
	}
}
	
	
