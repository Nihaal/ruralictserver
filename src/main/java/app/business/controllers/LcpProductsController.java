package app.business.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

//import app.business.services.OrganizationService;
import app.business.services.LcpProductService;
//import app.business.services.LcpProductTypeService;
//import app.entities.Organization;
import app.entities.LcpProduct;
//import app.entities.Product;
import app.entities.ProductType;
//import app.entities.ProductType;
import app.util.SpreadsheetParser;
import app.util.Utils;

@Controller
@RestController
@RequestMapping("/app")

public class LcpProductsController{
	@Autowired
	LcpProductService lcpProductService;
	
	@Transactional
	@RequestMapping(value = "/lcpproduct", method=RequestMethod.GET)
	public String generateList(){
		JSONObject jsonResponseObject = new JSONObject();
		JSONArray productDetailsArray = new JSONArray();
		JSONArray jsonArray = new JSONArray();
		//ProductType productType = iterator.next();
		JSONObject jsonObject = new JSONObject();
		List <LcpProduct> lcpproductList = lcpProductService.getAllProductList();
		Iterator <LcpProduct> productIter = lcpproductList.iterator();
		while (productIter.hasNext()) {
			LcpProduct lcpproduct = productIter.next();
			JSONObject productObject = new JSONObject();
			try {
			productObject.put("product_name", lcpproduct.getName());
			productObject.put("price", lcpproduct.getUnitRate());
			productObject.put("quantity", Integer.toString(lcpproduct.getQuantity()));
			if(lcpproduct.getImageUrl() == null)
				productObject.put("imageUrl", "");
			else
				productObject.put("imageUrl", lcpproduct.getImageUrl());
			if(lcpproduct.getDescription() == null)
				productObject.put("description", "");
			else
				productObject.put("description", lcpproduct.getDescription());	
			
			productDetailsArray.put(productObject);
			}
		catch (JSONException e) {
			e.printStackTrace();
		}
			//productDetailsArray.put(jsonObject);
		}
		try{
			jsonResponseObject.put("products", productDetailsArray);
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		return jsonResponseObject.toString();
	}
	
	@Transactional
    @RequestMapping(value = "/lcpsearch", method=RequestMethod.POST)
    public String searchResults(@RequestBody String requestBody){
           JSONObject jsonObject = null;
           JSONArray productDetailsArray = new JSONArray();
           JSONObject responseJsonObject = new JSONObject();
           String queryString = null;
           int flag = 0;
           try{
                  jsonObject = new JSONObject(requestBody);
                  queryString = jsonObject.getString("query");
                  flag = jsonObject.getInt("flag");
                  List<LcpProduct> products = null;
                  if(flag == 0)
                	  products = lcpProductService.getSearchedProducts(queryString);
                  else if(flag == 1)
                	  products = lcpProductService.getSearchedProductsSortedByPrice(queryString);
                  else if(flag == 2)
                	  products = lcpProductService.getSearchedProductsSortedByPriceDesc(queryString);
            	  Iterator <LcpProduct> productIter = products.iterator();
            	  while(productIter.hasNext()){
            		  LcpProduct lcpproduct = productIter.next();
            		  JSONObject productObject = new JSONObject();		
            		  try {
            			  	productObject.put("id", lcpproduct.getProductId());
            				productObject.put("name", lcpproduct.getName());
            				productObject.put("quantity", Integer.toString(lcpproduct.getQuantity()));
            				//productObject.put("audioUrl", lcpproduct.getAudioUrl());
            				productObject.put("unitRate",lcpproduct.getUnitRate() );
            				productObject.put("imageUrl", lcpproduct.getImageUrl());
            				productObject.put("moq", lcpproduct.getMoq());
	          				productObject.put("deliveryCharge", lcpproduct.getOrganization().getDelCharge());		 
            				productObject.put("logistics", (lcpproduct.getLogAvailability() == 0 ? "No" : "Yes"));
            				productObject.put("organization", lcpproduct.getOrganization().getName());
            				productObject.put("orgId", lcpproduct.getOrganization().getOrganizationId());
            				productObject.put("unit", lcpproduct.getSiUnit());
            				if(lcpproduct.getImageUrl() == null)
            					productObject.put("imageUrl", "");
            				else
            					productObject.put("imageUrl", lcpproduct.getImageUrl());
            				if(lcpproduct.getDescription() == null)
            					productObject.put("description", "");
            				else
            					productObject.put("description", lcpproduct.getDescription());	
            				productObject.put("type", lcpproduct.getProductType().getName());
            				
            				productDetailsArray.put(productObject);
            				}
            			catch (JSONException e) {
            				e.printStackTrace();
            			}
            				//productDetailsArray.put(jsonObject);
            			}
            			try{
            				responseJsonObject.put("products", productDetailsArray);
            			}
            			catch(JSONException e){
            				e.printStackTrace();
            			}
            	  }
           catch(JSONException e){
        	   e.printStackTrace();
           }
           
           return responseJsonObject.toString();
    }
	
	@Transactional
    @RequestMapping(value = "/lcpsearchbetweenquantity", method=RequestMethod.POST)
    public String searchBetweenQuantity(@RequestBody String requestBody){
           JSONObject jsonObject = null;
           JSONArray productDetailsArray = new JSONArray();
           JSONObject responseJsonObject = new JSONObject();
           String queryString = null;
           int from, to, flag;
           try{
                  jsonObject = new JSONObject(requestBody);
                  queryString = jsonObject.getString("query");
                  from = jsonObject.getInt("from");
                  to = jsonObject.getInt("to");
                  flag = jsonObject.getInt("flag");
                  List<LcpProduct> products = null;
                  if(flag == 0)
                	  products = lcpProductService.getSearchedProducts(queryString, from, to);
                  else if(flag == 1)
                	  products = lcpProductService.getSearchedProductsAsc(queryString, from, to);
                  else if(flag == 2)
                	  products = lcpProductService.getSearchedProductsDesc(queryString, from, to);
                  Iterator <LcpProduct> productIter = products.iterator();
            	  while(productIter.hasNext()){
            		  LcpProduct lcpproduct = productIter.next();
            		  JSONObject productObject = new JSONObject();		
            		  try {
            			  	productObject.put("id", lcpproduct.getProductId());
            				productObject.put("name", lcpproduct.getName());
            				productObject.put("quantity", Integer.toString(lcpproduct.getQuantity()));
            				//productObject.put("audioUrl", lcpproduct.getAudioUrl());
            				productObject.put("unitRate",lcpproduct.getUnitRate() );
            				productObject.put("imageUrl", lcpproduct.getImageUrl());
            				productObject.put("moq", lcpproduct.getMoq());
	          				productObject.put("deliveryCharge", lcpproduct.getOrganization().getDelCharge());		 
            				productObject.put("logistics", (lcpproduct.getLogAvailability() == 0 ? "No" : "Yes"));
            				productObject.put("organization", lcpproduct.getOrganization().getName());
            				productObject.put("orgId", lcpproduct.getOrganization().getOrganizationId());
            				productObject.put("unit", lcpproduct.getSiUnit());
            				if(lcpproduct.getImageUrl() == null)
            					productObject.put("imageUrl", "");
            				else
            					productObject.put("imageUrl", lcpproduct.getImageUrl());
            				if(lcpproduct.getDescription() == null)
            					productObject.put("description", "");
            				else
            					productObject.put("description", lcpproduct.getDescription());	
            				productObject.put("type", lcpproduct.getProductType().getName());
            				
            				productDetailsArray.put(productObject);
            				}
            			catch (JSONException e) {
            				e.printStackTrace();
            			}
            				//productDetailsArray.put(jsonObject);
            			}
            			try{
            				responseJsonObject.put("products", productDetailsArray);
            			}
            			catch(JSONException e){
            				e.printStackTrace();
            			}
            	  }
           catch(JSONException e){
        	   e.printStackTrace();
           }
           
           return responseJsonObject.toString();
    }
	
	@Transactional
    @RequestMapping(value = "/lcppricefilter", method=RequestMethod.POST)
    public String priceFilter(@RequestBody String requestBody){
           JSONObject jsonObject = null;
           JSONArray productDetailsArray = new JSONArray();
           JSONObject responseJsonObject = new JSONObject();
           float queryString1 = 0;
           float queryString2 = 0;
           try{
                  jsonObject = new JSONObject(requestBody);
                  queryString1 = (float)jsonObject.getDouble("from");
                  queryString2 = (float)jsonObject.getDouble("to");
                  List<LcpProduct> products = null;
            	  products = lcpProductService.getFilteredPrice(queryString1, queryString2);
            	  Iterator <LcpProduct> productIter = products.iterator();
            	  while(productIter.hasNext()){
            		  LcpProduct lcpproduct = productIter.next();
            		  JSONObject productObject = new JSONObject();		
            		  try {
            			  productObject.put("id", lcpproduct.getProductId());
            			  productObject.put("name", lcpproduct.getName());
          				productObject.put("price", lcpproduct.getUnitRate());
          				productObject.put("quantity", Integer.toString(lcpproduct.getQuantity()));
          				//productObject.put("audioUrl", lcpproduct.getAudioUrl());
          				productObject.put("unitRate",lcpproduct.getUnitRate() );
          				productObject.put("imageUrl", lcpproduct.getImageUrl());
          				productObject.put("moq", lcpproduct.getMoq());
          				productObject.put("deliveryCharge", lcpproduct.getOrganization().getDelCharge());		 
          				productObject.put("logistics", lcpproduct.getLogAvailability());
          				productObject.put("organization", lcpproduct.getOrganization().getName());
        				productObject.put("orgId", lcpproduct.getOrganization().getOrganizationId());
          				productObject.put("unit", lcpproduct.getSiUnit());
          				if(lcpproduct.getImageUrl() == null)
        					productObject.put("imageUrl", "");
        				else
        					productObject.put("imageUrl", lcpproduct.getImageUrl());
            			if(lcpproduct.getDescription() == null)
            				productObject.put("description", "");
            			else
            				productObject.put("description", lcpproduct.getDescription());	
            				
            			productDetailsArray.put(productObject);
            		}
            		catch (JSONException e) {
            			e.printStackTrace();
            		}
            				//productDetailsArray.put(jsonObject);
            	}
            			try{
            				responseJsonObject.put("products", productDetailsArray);
            			}
            			catch(JSONException e){
            				e.printStackTrace();
            			}
            	  }
           catch(JSONException e){
        	   e.printStackTrace();
           }
           
           return responseJsonObject.toString();
    }
	
	@Transactional
    @RequestMapping(value = "/lcpproductnameandquantity", method=RequestMethod.POST)
    public String nameWithQuantityFilter(@RequestBody String requestBody){
           JSONObject jsonObject = null;
           JSONArray productDetailsArray = new JSONArray();
           JSONObject responseJsonObject = new JSONObject();
           String name = null;
           int queryString1 = 0;
           int queryString2 = 0;
           int min, max ;
           LcpProduct promin,promax, globmin, globmax;
           try{
              jsonObject = new JSONObject(requestBody);
              name = jsonObject.getString("name");
              queryString1 = jsonObject.getInt("from");
              queryString2 = jsonObject.getInt("to");
             
              List<LcpProduct> products = null;
        	  products = lcpProductService.getProductWithQuantity(name, queryString1, queryString2);
        	  promin = lcpProductService.getMinQuantity(name);
        	  promax = lcpProductService.getMaxQuantity(name);
        	  globmin = null;
        	  globmax = null;
        	  if(!products.isEmpty()){
        	  globmin = lcpProductService.getGlobalMaxQuantity(products.get(0).getProductType());
        	  globmax = lcpProductService.getGlobalMinQuantity(products.get(0).getProductType());
        	  }
        	  Iterator <LcpProduct> productIter = products.iterator();
        	  while(productIter.hasNext()){
        		  LcpProduct lcpproduct = productIter.next();
        		  JSONObject productObject = new JSONObject();		
        		  try {
        			  productObject.put("id", lcpproduct.getProductId());  
        			productObject.put("name", lcpproduct.getName());
					productObject.put("price", lcpproduct.getUnitRate());
					productObject.put("quantity", Integer.toString(lcpproduct.getQuantity()));
					productObject.put("unitRate",lcpproduct.getUnitRate() );
					productObject.put("imageUrl", lcpproduct.getImageUrl());
					productObject.put("moq", lcpproduct.getMoq());
      				productObject.put("deliveryCharge", lcpproduct.getOrganization().getDelCharge());	
					productObject.put("logistics", (lcpproduct.getLogAvailability() == 0 ? "No" : "Yes"));
					productObject.put("organization", lcpproduct.getOrganization().getName());
    				productObject.put("orgId", lcpproduct.getOrganization().getOrganizationId());
					productObject.put("unit", lcpproduct.getSiUnit());
					if(lcpproduct.getImageUrl() == null)
    					productObject.put("imageUrl", "");
    				else
    					productObject.put("imageUrl", lcpproduct.getImageUrl());
					if(lcpproduct.getDescription() == null)
						productObject.put("description", "");
					else
						productObject.put("description", lcpproduct.getDescription());	
					
					productDetailsArray.put(productObject);
        				}
        		  catch (JSONException e) {
        				e.printStackTrace();
        		  }
        	  }
        	  try{
        		  responseJsonObject.put("products", productDetailsArray);
        		  responseJsonObject.put("min", promin.getQuantity());
        		  responseJsonObject.put("max", promax.getQuantity());
        		  if(globmin != null)
        			  responseJsonObject.put("typemin", globmin.getQuantity());
        		  else 
        			  responseJsonObject.put("typemin", "");
        		  if(globmax != null)
        			  responseJsonObject.put("typemax", globmax.getQuantity());
        		  else
        			  responseJsonObject.put("typemax", "");
            	  
        	  }
        	  catch(JSONException e){
        		  e.printStackTrace();
        	  }
           }
           catch(JSONException e){
        	   e.printStackTrace();
           }
           return responseJsonObject.toString();
    }
	
	@Transactional
    @RequestMapping(value = "/lcppriceorderfilter", method=RequestMethod.POST)
    public String priceOrderFilter(@RequestBody String requestBody){
           JSONObject jsonObject = null;
           JSONArray productDetailsArray = new JSONArray();
           JSONObject responseJsonObject = new JSONObject();
           try{
                  jsonObject = new JSONObject(requestBody);
                  List<LcpProduct> products = null;
            	  products = lcpProductService.getOrderedFilteredPrice();
            	  Iterator <LcpProduct> productIter = products.iterator();
            	  while(productIter.hasNext()){
            		  LcpProduct lcpproduct = productIter.next();
            		  JSONObject productObject = new JSONObject();		
            		  try {
            			  productObject.put("id", lcpproduct.getProductId());
            			  productObject.put("name", lcpproduct.getName());
          				productObject.put("price", lcpproduct.getUnitRate());
          				productObject.put("quantity", Integer.toString(lcpproduct.getQuantity()));
          				//productObject.put("audioUrl", lcpproduct.getAudioUrl());
          				productObject.put("unitRate",lcpproduct.getUnitRate() );
          				productObject.put("imageUrl", lcpproduct.getImageUrl());
          				productObject.put("moq", lcpproduct.getMoq());
          				productObject.put("deliveryCharge", lcpproduct.getOrganization().getDelCharge());		 
          				productObject.put("logistics", (lcpproduct.getLogAvailability() == 0 ? "No" : "Yes"));
          				productObject.put("organization", lcpproduct.getOrganization().getName());
        				productObject.put("orgId", lcpproduct.getOrganization().getOrganizationId());
          				productObject.put("unit", lcpproduct.getSiUnit());
          				if(lcpproduct.getImageUrl() == null)
        					productObject.put("imageUrl", "");
        				else
        					productObject.put("imageUrl", lcpproduct.getImageUrl());
            			if(lcpproduct.getDescription() == null)
            				productObject.put("description", "");
            			else
            				productObject.put("description", lcpproduct.getDescription());	
            				
            			productDetailsArray.put(productObject);
            				}
            			catch (JSONException e) {
            				e.printStackTrace();
            			}
            				//productDetailsArray.put(jsonObject);
            			}
            			try{
            				responseJsonObject.put("products", productDetailsArray);
            			}
            			catch(JSONException e){
            				e.printStackTrace();
            			}
            	  }
           catch(JSONException e){
        	   e.printStackTrace();
           }
           
           return responseJsonObject.toString();
    }
	
	@Transactional
    @RequestMapping(value = "/lcppriceorderbytypefilter", method=RequestMethod.POST)
    public String typeWisePriceOrderFilter(@RequestBody String requestBody){
           JSONObject jsonObject = null;
           String type = null;
           int flag = 0;
           JSONArray productDetailsArray = new JSONArray();
           JSONObject responseJsonObject = new JSONObject();
           try{
                  jsonObject = new JSONObject(requestBody);
                  type = jsonObject.getString("type");
                  flag = jsonObject.getInt("flag");
                  List<LcpProduct> products = null;
                  if(flag == 0)
                	  products = lcpProductService.getOrderedFilteredPrice();
                  else if(flag == 1)
                	  products = lcpProductService.getOrderedFilteredPriceDesc();
                  Iterator <LcpProduct> productIter = products.iterator();
            	  while(productIter.hasNext()){
            		  LcpProduct lcpproduct = productIter.next();
            		  JSONObject productObject = new JSONObject();
            		  if(type.equals(lcpproduct.getProductType().getName())){
	            		  try {
	            			  productObject.put("id", lcpproduct.getProductId());
	            			productObject.put("name", lcpproduct.getName());
	          				productObject.put("quantity", Integer.toString(lcpproduct.getQuantity()));
	          				productObject.put("unitRate",lcpproduct.getUnitRate() );
	          				productObject.put("imageUrl", lcpproduct.getImageUrl());
	          				productObject.put("moq", lcpproduct.getMoq());
	          				productObject.put("deliveryCharge", lcpproduct.getOrganization().getDelCharge());	 
	          				productObject.put("logistics", (lcpproduct.getLogAvailability() == 0 ? "No" : "Yes"));
	          				productObject.put("organization", lcpproduct.getOrganization().getName());
            				productObject.put("orgId", lcpproduct.getOrganization().getOrganizationId());
	          				productObject.put("unit", lcpproduct.getSiUnit());
	          				if(lcpproduct.getImageUrl() == null)
            					productObject.put("imageUrl", "");
            				else
            					productObject.put("imageUrl", lcpproduct.getImageUrl());
	            			if(lcpproduct.getDescription() == null)
	            				productObject.put("description", "");
	            			else
	            				productObject.put("description", lcpproduct.getDescription());	
	            				
	            				productDetailsArray.put(productObject);
	            		  }
	            		  catch (JSONException e) {
	            			  e.printStackTrace();
	            		  }
            	  }	//productDetailsArray.put(jsonObject);
            			}
            			try{
            				responseJsonObject.put("products", productDetailsArray);
            			}
            			catch(JSONException e){
            				e.printStackTrace();
            			}
            	  }
           catch(JSONException e){
        	   e.printStackTrace();
           }
           
           return responseJsonObject.toString();
    }

	}
