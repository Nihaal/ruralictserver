package app.business.controllers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.plivo.helper.api.client.RestAPI;
import com.plivo.helper.api.response.message.MessageResponse;
import com.plivo.helper.exception.PlivoException;

import app.business.controllers.rest.FcmTestingController;
import app.business.services.RegistrationService;
import app.business.services.SmsApiKeysService;
import app.business.services.UserService;
import app.business.services.UserViewService;
import app.business.services.UserViewService.UserView;
import app.data.repositories.RegistrationRepository;
import app.data.repositories.SmsApiKeysRepository;
import app.data.repositories.UserPhoneNumberRepository;
import app.entities.Organization;
import app.entities.Registration;
import app.entities.SmsApiKeys;
import app.entities.User;
import app.entities.UserPhoneNumber;

@Controller
@RestController
@RequestMapping("/app")
public class RegistrationController {
	@Autowired
	RegistrationService registrationservice;
	 
	@Autowired
	UserService userservice;
		
	@Autowired
	SmsApiKeysRepository smsApiKeysRepository;
	
	@Autowired
	SmsApiKeysService smsApiKeysService;
	
	@Autowired
	RegistrationRepository registrationrepo;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	UserPhoneNumberRepository userPhoneNumberRepository;
	
	@Transactional
	@RequestMapping(value="/intpwd", method = RequestMethod.POST)
	public void loginCheck1(@RequestBody String requestBody) throws NoSuchAlgorithmException{
		JSONObject jsonObject = null;
		String password = null;
		String email = null;
		try{
			jsonObject = new JSONObject(requestBody);
			email = jsonObject.getString("email");
			password = jsonObject.getString("password");
			User user =  userservice.getUserFromEmail(email);
			System.out.println(user.getEmail());
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(
			password.getBytes(StandardCharsets.UTF_8));
			String sha256hex = new String(Hex.encode(hash));
			System.out.println(sha256hex);
			System.out.println(passwordEncoder.encode(password));
			user.setSha256Password(passwordEncoder.encode(password));
			if(passwordEncoder.matches("abee00c1e9d5df37f592b0b0f2515cfb769ba18b24351c2f803f650332e20ef36f75292f8f2bd386", sha256hex))
				System.out.println("true");
		}
		catch(JSONException e){
			e.printStackTrace();
		}
	}
	

	@Transactional
	@RequestMapping(value = "/changelcppassword", method = RequestMethod.POST)
	public String changePassword(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject response = new JSONObject();
		String phonenumber = null;
		String originalString = null;
		try{
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			originalString = jsonObject.getString("password");
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(
			  originalString.getBytes(StandardCharsets.UTF_8));
			String sha256hex = new String(Hex.encode(hash));
			
			Registration reg = registrationrepo.findByPhone(phonenumber);
			reg.setPassword(sha256hex);
			
		}
		catch(JSONException e){
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			Registration reg = registrationrepo.findByPhone(phonenumber);
			response.put("status", "success");
			response.put("address", reg.getAddress());
			response.put("firstname", reg.getFirstname());
			response.put("lastname", reg.getLastname());
			response.put("email", reg.getEmail());
			response.put("pincode", new Integer(reg.getPincode()).toString());
			response.put("phone", phonenumber);
			try {
				FcmTestingController.sendPushNotification("eaKLK5KI45Y:APA91bFB1DJ_d_Fc3x6QwErOoI0rbpmu7w2OUrX7R1kOtJtqAXbaeKufa2punTOAwidF25_RZIQVsj_gY3qwTWXC_fW1oOnxiWVY1msgJRWU5qKcPOcLHaY0bsvAmMx5fevPPldtoVtq", "hii", "Lokacart Minus");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}catch(JSONException je){
			je.printStackTrace();
		}
		return response.toString();
	}

			
	@Transactional
	@RequestMapping(value = "/changeprofile", method = RequestMethod.POST)
	public String changeProfile(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String firstname = null;
		String lastname = null;
		String email = null;
		String address = null;
		String phone = null;
		String pincode = null;
		try{
			jsonObject = new JSONObject(requestBody);
			firstname = jsonObject.getString("firstname");
			lastname = jsonObject.getString("lastname");
			if(!(jsonObject.getString("email").equals(""))){
				email = jsonObject.getString("email");
			}
			address = jsonObject.getString("address");
			phone = jsonObject.getString("phone");
			pincode = jsonObject.getString("pincode");
			Registration reg = registrationrepo.findByPhone(phone);
			
			reg.setAddress(address);
			reg.setEmail(email);
			reg.setFirstname(firstname);
			reg.setLastname(lastname);
			reg.setPincode(Integer.parseInt(pincode));
			
		}
		catch(JSONException je){
			je.printStackTrace();
		}
		
		try {
			responseJsonObject.put("status", "success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/shippingaddress", method = RequestMethod.POST)
	public String changeAddress(@RequestBody String requestBody){
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String address = null;
		String phone = null;
		try{
			jsonObject = new JSONObject(requestBody);
			address = jsonObject.getString("address");
			phone = jsonObject.getString("phone");
			
			Registration reg = registrationrepo.findByPhone(phone);
			
			reg.setDelAddress(address);
			
		}
		catch(JSONException je){
			je.printStackTrace();
		}
		
		try {
			responseJsonObject.put("status", "success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return responseJsonObject.toString();
	}
	
	
	@Transactional
	@RequestMapping(value="/logincheck", method = RequestMethod.POST)
	public String loginCheck(@RequestBody String requestBody) throws NoSuchAlgorithmException{
		JSONObject jsonObject = null;
		String phone = null;
		String password = null;
		JSONObject responseJsonObject = new JSONObject();
		try{
			jsonObject = new JSONObject(requestBody);
			phone = jsonObject.getString("phone");
			password = jsonObject.getString("password");
			Registration registrationPhone = null;
			registrationPhone = registrationrepo.findByPhone(phone);
			if(registrationPhone == null){
				try{
					responseJsonObject.put("status", "failure");
					responseJsonObject.put("error", "Phone number is not registered");
					return responseJsonObject.toString();
				}
				catch(JSONException e){
					e.printStackTrace();
				}
			}
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(
		  password.getBytes(StandardCharsets.UTF_8));
		String sha256hex = new String(Hex.encode(hash));
		//System.out.println(sha256hex);
		Registration registration = new Registration();
		try{
			registration =  registrationservice.getUserByPhoneAndPassword(phone, sha256hex);
			String firstname = registration.getFirstname();
			String lastname = registration.getLastname();
			String email = registration.getEmail();
			int pincode = registration.getPincode();
			responseJsonObject.put("status", "success");
			responseJsonObject.put("firstname", firstname);
			responseJsonObject.put("lastname", lastname);
			responseJsonObject.put("address", registration.getAddress());
			responseJsonObject.put("email", email);
			responseJsonObject.put("pincode", new Integer(pincode).toString());
			responseJsonObject.put("phone", phone);
		}
		catch(Exception e){
			try {
				responseJsonObject.put("status", "failure");
				responseJsonObject.put("error", "Incorrect password.");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/newregister", method = RequestMethod.POST)
	public String newUser(@RequestBody String requestBody) throws NoSuchAlgorithmException{
		JSONObject jsonObject = null;
		JSONObject responseJsonObject = new JSONObject();
		String firstname = null;
		String lastname = null;
		String email = null;
		String phone = null;
		String originalString = null;
		String address = null;
		int pincode = 0;
		try{
			jsonObject = new JSONObject(requestBody);
			firstname = jsonObject.getString("firstname");
			lastname = jsonObject.getString("lastname");
			if(!(jsonObject.getString("email").equals(""))){
				email = jsonObject.getString("email");
				System.out.println("hereemail");
			}
			originalString = jsonObject.getString("password");
			pincode = jsonObject.getInt("pincode");
			address = jsonObject.getString("address");
			Registration registration = null;
			Registration registrationEmail = null;
			try{
				//System.out.println("try up");
				phone = jsonObject.getString("phone");
				registration = registrationrepo.findByPhone(phone);
				System.out.println("her0e");
				if(email == null){					
					registrationEmail = null;
				}
				else{
					registrationEmail = registrationrepo.findFirstByEmail(email);
				}
				
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				byte[] hash = digest.digest(
				  originalString.getBytes(StandardCharsets.UTF_8));
				String sha256hex = new String(Hex.encode(hash));
				
				if(registration == null && registrationEmail == null){	
					Registration register = new Registration(firstname, lastname, phone, email, sha256hex, pincode, address);
						registrationservice.addUser(register);
						try {
							responseJsonObject.put("status", "success");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
				else if(registration != null && registrationEmail != null){
					responseJsonObject.put("status", "number and email already taken");
					return responseJsonObject.toString();
				}
				else if(registration != null){
					responseJsonObject.put("status", "number already taken");
					return responseJsonObject.toString();
				}
				else if(registrationEmail != null){
					responseJsonObject.put("status", " email already taken");
					return responseJsonObject.toString();
				}
			}
			catch(NullPointerException e){
				responseJsonObject.put("status", "number taken");
				return responseJsonObject.toString();
			}
		}
		catch(JSONException q){
			q.printStackTrace();
		}
		
		
		return responseJsonObject.toString();
	}
	
	
	// Util functions
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static Random rnd = new Random();
		
	String randomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

	@Transactional
	@RequestMapping(value = "/forgotlcppassword", method = RequestMethod.POST)
	public String forgotPassword(@RequestBody String requestBody){
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String phonenumber = null;
		try{
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
		}
		catch(JSONException je){
			je.printStackTrace();
		}
		Registration reg = registrationrepo.findByPhone(phonenumber);
		if(reg == null){
			try{
				responseJsonObject.put("otp", "null");
			}
			catch(JSONException e){
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		Organization billingOrg = smsApiKeysRepository.findFirstByOrderByApiIdDesc().getOrganization();
		if(reg.getPhone() != null){
			String otp = randomString(4);
			//int status = 1;
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(billingOrg);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");
			System.out.println("In here 1");
			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", "91"+ phonenumber);
			parameters.put("text", "Hello! Here's your OTP for Lokacart Plus: " + otp);
			try{
				System.out.println("In here 2");
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}
			try {
				responseJsonObject.put("otp", otp);

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		else{
			try {
				responseJsonObject.put("otp", "null");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
	}

	@Transactional
	@RequestMapping(value = "/forgotwebpassword", method = RequestMethod.POST)
	public String forgotPasswordWeb(@RequestBody String requestBody){
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String phonenumber = null;
		try{
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
		}
		catch(JSONException je){
			je.printStackTrace();
		}
		UserPhoneNumber userPhoneNumber = userPhoneNumberRepository.findByPhoneNumber("91"+phonenumber);
		if(userPhoneNumber.getUser() == null){
			try{
				responseJsonObject.put("otp", "null");
			}
			catch(JSONException e){
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		User user = userPhoneNumber.getUser();
		Organization billingOrg = smsApiKeysRepository.findFirstByOrderByApiIdDesc().getOrganization();
		if(user.getUserPhoneNumbers() != null){
			String otp = randomString(4);
			//int status = 1;
			SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(billingOrg);
			String authId = smsApiKeys.getAuthId();
			String authToken = smsApiKeys.getAuthToken();
			String sourceNumber = smsApiKeys.getSourceNumber();
			RestAPI api = new RestAPI(authId, authToken, "v1");
			System.out.println("In here 1");
			LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
			parameters.put("src", sourceNumber);
			parameters.put("dst", "91"+ phonenumber);
			parameters.put("text", "Hello! Here's your OTP for Lokacart Plus: " + otp);
			try{
				System.out.println("In here 2");
				MessageResponse msgResponse = api.sendMessage(parameters);
				System.out.println(msgResponse);
				System.out.println("Api ID : " + msgResponse.apiId);
				System.out.println("Message : " + msgResponse.message);
				if (msgResponse.serverCode == 202) {
					System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

				} else {
					System.out.println(msgResponse.error);
				}
			} catch (PlivoException e) {
				System.out.println(e.getLocalizedMessage());
			}
			try {
				responseJsonObject.put("otp", otp);

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
		else{
			try {
				responseJsonObject.put("otp", "null");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseJsonObject.toString();
		}
	}

	@Transactional
	@RequestMapping(value = "/generateOtp", method = RequestMethod.POST)
	public String generateOtp(@RequestBody String requestBody){
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String phonenumber = null;
		try{
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
		}
		catch(JSONException je){
			je.printStackTrace();
		}
		Organization billingOrg = smsApiKeysRepository.findFirstByOrderByApiIdDesc().getOrganization();
		String otp = randomString(4);
		SmsApiKeys smsApiKeys = smsApiKeysService.getApiKeyByOrganization(billingOrg);
		String authId = smsApiKeys.getAuthId();
		String authToken = smsApiKeys.getAuthToken();
		String sourceNumber = smsApiKeys.getSourceNumber();
		RestAPI api = new RestAPI(authId, authToken, "v1");
		System.out.println("In here 1");
		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
		parameters.put("src", sourceNumber);
		parameters.put("dst", "91"+ phonenumber);
		parameters.put("text", "Hello! Here's your OTP for Lokacart Plus: " + otp);
		try{
			System.out.println("In here 2");
			MessageResponse msgResponse = api.sendMessage(parameters);
			System.out.println(msgResponse);
			System.out.println("Api ID : " + msgResponse.apiId);
			System.out.println("Message : " + msgResponse.message);
			if (msgResponse.serverCode == 202) {
				System.out.println("Message UUID : " + msgResponse.messageUuids.get(0).toString());

			} else {
				System.out.println(msgResponse.error);
			}
		} catch (PlivoException e) {
			System.out.println(e.getLocalizedMessage());
		}
		try {
			responseJsonObject.put("otp", otp);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/changewebpassword", method = RequestMethod.POST)
	public String changePasswordWeb(@RequestBody String requestBody){
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String phonenumber = null;
		String password = null;
		try{
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			password = jsonObject.getString("password");
		}
		catch(JSONException je){
			je.printStackTrace();
		}
		User user = userPhoneNumberRepository.findByPhoneNumber("91"+phonenumber).getUser();
		user.setWebPassword(passwordEncoder.encode(password));
		try{
			responseJsonObject.put("status", "success");
		} catch(JSONException e){
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
	@Transactional
	@RequestMapping(value = "/checkNumber", method = RequestMethod.POST)
	public String checkNumber(@RequestBody String requestBody){
		JSONObject responseJsonObject = new JSONObject();
		JSONObject jsonObject = null;
		String phonenumber = null;
		String password = null;
		try{
			jsonObject = new JSONObject(requestBody);
			phonenumber = jsonObject.getString("phonenumber");
			password = jsonObject.getString("password");
		}
		catch(JSONException je){
			je.printStackTrace();
		}
		Registration reg = registrationrepo.findByPhone(phonenumber);
		try{
			if(reg == null)
				responseJsonObject.put("exist", "no");
			else
				responseJsonObject.put("exist", "yes");
			responseJsonObject.put("status", "success");
		} catch(JSONException e){
			e.printStackTrace();
		}
		return responseJsonObject.toString();
	}
	
}