package app.util;

public class ProductsManage {
	
	private String name;
	private String audioUrlWav;
	private String imageUrl;
	private int productId;
	private String audioUrl;
	private int status;
	private float unitRate;
	private int quantity;
	private String productType;
	
	public ProductsManage(String name, String audioUrlWav, String imageUrl, int productId, String audioUrl, int status, float unitRate, int quantity, String productType){
		this.name = name;
		this.audioUrlWav = audioUrlWav;
		this.imageUrl = imageUrl;
		this.productId = productId;
		this.audioUrl = audioUrl;
		this.status = status;
		this.unitRate = unitRate;
		this.quantity = quantity;
		this.productType = productType;
	}	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAudioUrlWav() {
		return audioUrlWav;
	}

	public void setAudioUrlWav(String audioUrlWav) {
		this.audioUrlWav = audioUrlWav;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getAudioUrl() {
		return audioUrl;
	}

	public void setAudioUrl(String audioUrl) {
		this.audioUrl = audioUrl;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public float getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(float unitRate) {
		this.unitRate = unitRate;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

}
