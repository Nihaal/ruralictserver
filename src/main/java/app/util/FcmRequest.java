package app.util;

import java.io.BufferedReader;

import java.io.IOException;

import java.io.InputStreamReader;

import java.io.OutputStreamWriter;

import java.net.HttpURLConnection;

import java.net.MalformedURLException;

import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;

import org.json.JSONObject;

public class FcmRequest {
		
	final static private String FCM_URL = "https://fcm.googleapis.com/fcm/send";
	//final static private String server_key = "AAAAr9WqZIM:APA91bFwWZ3tC7-bx-EnbGemA8gDnI19dlNM5MGKVpRLnE0-QsNCO_rmdgP0VSz7PKPQY9znGSFiW517yoJuycqFDI4JWLGVfc6L-PH9qEeH0M1dWOkzjR4ErVGaT90li2qxs2oOlXu7";
	final static private String server_key = "AAAANw3tnLg:APA91bGPIqhdFMJSwNRdjGEXDMA7Rx6jESU-VoYvvYr6lcImxaVjqwTiONTp24XZAjtptH_FLztmX6UwaxkVoKGcz5NA2QOe5y1h9_50KyZmKslIlN6Pe2_wmZjxXhhfeYl6cZT_G5qD";
	
	public static void send_FCM_Notification(String tokenId, String title, String message){
		try{
			URL url = new URL(FCM_URL);
			HttpURLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Authorization","key="+server_key);
			conn.setRequestProperty("Content-Type","application/json");
			JSONObject infoJson = new JSONObject();
			infoJson.put("title", title);
			infoJson.put("body", message);
			JSONObject json = new JSONObject();
			json.put("to",tokenId.trim());
			json.put("notification", infoJson);
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();
			int status = 0;
			if(conn != null){
				status = conn.getResponseCode();
			}
			if(status != 0){
				if(status == 200){
					BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					System.out.println("Android Notification Response : " + reader.readLine());
				}
				else if(status == 401){
					System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
				}
				else if(status == 501){
					System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
				}
				else if( status == 503){
					System.out.println("Notification Response : FCM Service is Unavailable  TokenId : " + tokenId);
				}
			}
		} catch(Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	public static void send_FCM_Broadcast(List<String> tokenIds, String title, String message, int id){
		try{
			URL url = new URL(FCM_URL);
			HttpURLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Authorization","key="+server_key);
			conn.setRequestProperty("Content-Type","application/json");
			JSONObject infoJson = new JSONObject();
			infoJson.put("title", title);
			infoJson.put("body", message);
			infoJson.put("tag", id);
			Iterator<String> iter = tokenIds.iterator();
			while(iter.hasNext()){
				JSONObject json = new JSONObject();
				String tokenId = iter.next().trim();
				json.put("to", tokenId);
				json.put("notification", infoJson);
				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
				wr.write(json.toString());
				wr.flush();
				int status = 0;
				if(conn != null){
					status = conn.getResponseCode();
				}
				if(status != 0){
					if(status == 200){
						BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						System.out.println("Android Notification Response : " + reader.readLine());
						continue;
					}
					else if(status == 401){
						System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
						continue;
					}
					else if(status == 501){
						System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
						continue;
					}
					else if( status == 503){
						System.out.println("Notification Response : FCM Service is Unavailable  TokenId : " + tokenId);
						continue;
					}
				}
			}
		} catch(Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
		
	public static void send_FCM_Broadcast_Topic(List<String> tokenIds, String title, String message){
		try{
			URL url = new URL(FCM_URL);
			HttpURLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Authorization","key="+server_key);
			conn.setRequestProperty("Content-Type","application/json");
			JSONObject infoJson = new JSONObject();
			infoJson.put("title", title);
			infoJson.put("body", message);
			infoJson.put("tag", 4);
			Iterator<String> iter = tokenIds.iterator();
			while(iter.hasNext()){
				JSONObject json = new JSONObject();
				String tokenId = iter.next().trim();
				json.put("to", "/topics/loka-plus");
				json.put("notification", infoJson);
				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
				wr.write(json.toString());
				wr.flush();
				int status = 0;
				if(conn != null){
					status = conn.getResponseCode();
				}
				if(status != 0){
					if(status == 200){
						BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						System.out.println("Android Notification Response : " + reader.readLine());
						continue;
					}
					else if(status == 401){
						System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
						continue;
					}
					else if(status == 501){
						System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
						continue;
					}
					else if( status == 503){
						System.out.println("Notification Response : FCM Service is Unavailable  TokenId : " + tokenId);
						continue;
					}
				}
			}
		} catch(Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
	public static void send_FCM_Broadcast_Multiple(List<String> tokenIds, String title, String message, int id){
		try{			
			JSONObject infoJson = new JSONObject();
			infoJson.put("title", title);
			infoJson.put("body", message);
			infoJson.put("tag", id);
			Iterator<String> iter = tokenIds.iterator();
			while(iter.hasNext()){
				URL url = new URL(FCM_URL);
				HttpURLConnection conn;
				conn = (HttpURLConnection) url.openConnection();
				conn.setUseCaches(false);
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Authorization","key="+server_key);
				conn.setRequestProperty("Content-Type","application/json");
				JSONObject json = new JSONObject();
				String tokenId = iter.next().trim();
				json.put("to", tokenId);
				json.put("notification", infoJson);
				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
				wr.write(json.toString());
				wr.flush();
				int status = 0;
				if(conn != null){
					status = conn.getResponseCode();
				}
				if(status != 0){
					if(status == 200){
						BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						System.out.println("Android Notification Response : " + reader.readLine());
						continue;
					}
					else if(status == 401){
						System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
						continue;
					}
					else if(status == 501){
						System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
						continue;
					}
					else if( status == 503){
						System.out.println("Notification Response : FCM Service is Unavailable  TokenId : " + tokenId);
						continue;
					}
				}
			}
		} catch(Exception ex) {
			System.out.println("Error : " + ex.getMessage());
		}
	}
	
}