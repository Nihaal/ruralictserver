package app.util;

public class OrderItemManage {
	
	
	private int stat;
	private int prodId;
	private int id;
	private String name;
	private int quantity;
	private float rate;
	

	public OrderItemManage(int stat, int id, int prodId, String name, int quantity, float rate) {
		this.prodId = prodId;
		this.id = id;
		this.stat = stat;
		this.name = name;
		this.quantity = quantity;
		this.rate = rate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getStat() {
		return stat;
	}

	public void setStat(int stat) {
		this.stat = stat;
	}

	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public float getRate() {
		return rate;
	}
	public void setRate(float rate) {
		this.rate = rate;
	}
	
}
