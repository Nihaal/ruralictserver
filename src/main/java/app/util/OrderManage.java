package app.util;

import java.sql.Timestamp;
import java.util.Date;

public class OrderManage {
	private int stat;
	private int orderId;
	private String name;
	private String text;
	private Boolean isPaid;
	private String comment;
	private String status;
	private String time;
	
	public OrderManage(int stat, int orderId, String name, String text, Boolean isPaid, String comment, String status, String time){
		this.stat = stat;
		this.orderId = orderId;
		this.name = name;
		this.text = text;
		this.isPaid = isPaid;
		this.comment = comment;
		this.status = status;
		this.time = time;
	}

	public OrderManage(int stat, int orderId, String name){
		this.stat = stat;
		this.orderId = orderId;
		this.name = name;
	}
	
	public OrderManage(int stat, int orderId, String name, String time ){
		this.stat = stat;
		this.orderId = orderId;
		this.name = name;
		this.time = time;
	}
	
	public OrderManage(int stat, int orderId, String name, String text, Boolean isPaid, String status, String time){
		this.stat = stat;
		this.orderId = orderId;
		this.name = name;
		this.text = text;
		this.isPaid = isPaid;
		this.status = status;
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getStat() {
		return stat;
	}

	public void setStat(int stat) {
		this.stat = stat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
