package app.util;

public class ProductManage {

	private String name;
	private int prodId;
	private Boolean stock;
	private float rate;
	private float quantity;
	
	public ProductManage(int prodId, String name, Boolean stock, float rate, float quantity){
		this.prodId = prodId;
		this.name = name;
		this.stock = stock;
		this.rate = rate;
		this.quantity = quantity;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public Boolean getStock() {
		return stock;
	}
	public void setStock(Boolean stock) {
		this.stock = stock;
	}
	public float getRate() {
		return rate;
	}
	public void setRate(float rate) {
		this.rate = rate;
	}
	public float getQuantity() {
		return quantity;
	}
	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}
	
}
