package app.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import app.entities.OrgEnquiry;

public interface OrgEnquiryRepository extends JpaRepository<OrgEnquiry, Integer> {
	
	@Override
	public OrgEnquiry findOne(Integer id);
	
	public OrgEnquiry findByEmail(@Param("email") String email);

}