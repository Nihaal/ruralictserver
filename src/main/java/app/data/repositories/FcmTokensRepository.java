package app.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entities.FcmTokens;

public interface FcmTokensRepository extends JpaRepository<FcmTokens, Integer> {
	
	@Override
	public FcmTokens findOne(Integer id);
	
	public FcmTokens findByNumber(String number);

	public List<FcmTokens> findAllByNumber(String number);
	
	public List<FcmTokens> findAllByNumberOrderByIdDesc(String number);
	
	public FcmTokens findByToken(String token);

}
