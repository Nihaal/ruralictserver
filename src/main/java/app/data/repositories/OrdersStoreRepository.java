package app.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import app.entities.Order;
import app.entities.OrdersStore;
import app.entities.UserPhoneNumber;

public interface OrdersStoreRepository extends JpaRepository<OrdersStore, Integer> {
	/*
	 * Default functions
	 */
	@Override
	public OrdersStore findOne(Integer id);
	
	public List<OrdersStore> findByOrder(Order order);
	
	@Override
	public void delete(@Param("orderStore") OrdersStore orderStore);
}
