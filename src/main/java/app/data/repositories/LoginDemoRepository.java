package app.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;

import app.entities.Loginuserdemo;

public interface LoginDemoRepository extends JpaRepository<Loginuserdemo, Integer>{

	public <S extends Loginuserdemo> S save(@Param("usrdmo") S usrdmo);
	
	public Loginuserdemo findByEmailid(@Param("email") String emailid);
	
}