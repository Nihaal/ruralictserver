package app.data.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
//import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;

import app.entities.Group;
import app.entities.LcpOrder;
import app.entities.Organization;
import app.entities.Registration;
import app.entities.LcpProduct;

public interface LcpOrderRepository extends JpaRepository<LcpOrder, Integer> {
	/*
	 * Default functions
	 */
	
	public List<LcpOrder> findByRegistration(Registration reg);

	@Override
	public LcpOrder findOne(Integer id);

	//@PostFilter("hasRole('ADMIN'+filterObject.organization.abbreviation)")
	@Override
	public List<LcpOrder> findAll();

	//@PostFilter("hasRole('ADMIN'+filterObject.organization.abbreviation)")
	@Override
	public Page<LcpOrder> findAll(Pageable pageable);

	//@PostFilter("hasRole('ADMIN'+filterObject.organization.abbreviation)")
	@Override
	public List<LcpOrder> findAll(Sort sort);

	//@PreAuthorize("hasRole('MEMBER'+#order.organization.abbreviation)")
	@Override
	public void delete(@Param("lcp_order") LcpOrder order);
	
	public List<LcpOrder> findByRegistrationOrderByOrderIdDesc(Registration reg);

	/*
	 * Search functions
	 */
     
	//public List<LcpOrder> findByOrganization(Organization organization);
	
	//public List<LcpOrder> findByOrganizationOrderByOrderIdDesc(Organization organization);
	
	//public List<LcpOrder> findByOrganizationAndStatus(Organization organization,String status);
	
	//public List<LcpOrder> findByOrganizationAndStatusOrderByOrderIdDesc(Organization organization,String status);
	
	//public List<LcpOrder> findByMessage_group(Group group);
	
	//public List<LcpOrder> findByOrganizationAndStatusAndText(Organization organization,String status, String text);
	
	//public List<LcpOrder> findByMessage_groupAndStatus(Group group,String status);
	
	//public List<Order> findByMessage_formatAndOrganization_abbreviation(@Param("format") String format,@Param("abbr") String abbr);
	
	//@PostFilter("hasRole('MEMBER'+filterObject.organization.abbreviation)")
//	@RestResource(rel="getOrdersForMember", path="getOrdersForMember")
//	public List<LcpOrder> findByMessage_formatAndStatusAndOrganization_abbreviationAndMessage_user_userPhoneNumbers_phoneNumber(@Param("format") String format,@Param("status") String status,@Param("abbr") String abbr,@Param("phonenumber")String phonenumber);
	
}