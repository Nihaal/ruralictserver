package app.data.repositories;

import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import app.entities.SiUnit;

public interface SiUnitRepository extends JpaRepository<SiUnit, Integer>{
	
	@Override
	public SiUnit findOne(Integer id);
	
	public SiUnit findBySiUnit(String arg);
	
	@Override
	public List<SiUnit> findAll();
	
	@Override
	public List<SiUnit> findAll(Sort sort);
	
	@Override
	public <S extends SiUnit> S save(@Param("siUnit") S siUnit);

	@Override
	public void delete(@Param("siUnit") SiUnit siUnit);

	public List<SiUnit> findAllByOrderByLcGroupDesc();
	
	public List<SiUnit> findAllByOrderByLcpGroupDesc();
	
	

}