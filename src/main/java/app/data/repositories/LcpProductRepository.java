package app.data.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import app.entities.LcpProduct;
import app.entities.ProductType;
import app.entities.Organization;

public interface LcpProductRepository extends JpaRepository<LcpProduct, Integer>{
	@Override
	public LcpProduct findOne(Integer id);
	
	@Override
	public List<LcpProduct> findAll();

	@Override
	public Page<LcpProduct> findAll(Pageable pageable);
	
	@Override
	public List<LcpProduct> findAll(Sort sort);

	public List<LcpProduct> findAllByOrderByProductIdDesc();
	
	public List<LcpProduct> findByOrganization(Organization organization);
	
	@Override
	public <S extends LcpProduct> S save(@Param("lcpproduct") S lcpproduct);

	@Override
	public void delete(@Param("lcpproduct") LcpProduct lcpproduct);
	
	public List<LcpProduct> findByNameContaining(String arg);

	public List<LcpProduct> findByNameContainingAndQuantityBetween(String arg, int from, int to);	
	
	public List<LcpProduct> findByNameContainingAndQuantityBetweenOrderByUnitRateAsc(String arg, int from, int to);	
	
	public List<LcpProduct> findByNameContainingAndQuantityBetweenOrderByUnitRateDesc(String arg, int from, int to);
	
	public List<LcpProduct> findByNameContainingOrderByUnitRateAsc(String arg);
	
	public List<LcpProduct> findByNameContainingOrderByUnitRateDesc(String arg);
	
	public LcpProduct findFirstByNameOrderByQuantityAsc(String name);
	
	public LcpProduct findFirstByNameOrderByQuantityDesc(String name);
	
	public List<LcpProduct> findByUnitRateBetween(float from, float to);
	
	public List<LcpProduct> findAllByOrderByUnitRateAsc();

	public List<LcpProduct> findAllByOrderByUnitRateDesc();
	
	public List<LcpProduct> findAllByNameAndQuantityBetween(String name, int from, int to);
	
	public LcpProduct findByProductId(int id);

	//@Query("select u from lcp_product where u.product_type_id = ?1")
	//public List<LcpProduct> findByAndSort(String productType, Sort sort);
	
	public List<LcpProduct> findAllByOrderByNameAsc();/*
	
	@RestResource(path="/productlist")
	public List<LcpProduct> findByproductType_organization_abbreviation(@Param("abbr")String abbr);*/

	public LcpProduct findFirstByProductTypeOrderByQuantityAsc(ProductType productType);

	public LcpProduct findFirstByProductTypeOrderByQuantityDesc(ProductType productType);
}