package app.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entities.Feedback;

public interface FeedbackRepository extends JpaRepository<Feedback, Integer> {
	
	@Override
	public Feedback findOne(Integer id);
	
	
}
