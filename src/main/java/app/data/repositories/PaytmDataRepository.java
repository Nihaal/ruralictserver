package app.data.repositories;

import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import app.entities.Organization;
import app.entities.PaytmData;

public interface PaytmDataRepository extends JpaRepository<PaytmData, Integer>{
	
	@Override
	public <S extends PaytmData> S save(@Param("paytm") S paytm);

	@Override
	public List<PaytmData> findAll(Sort sort);

	@Override
	public List<PaytmData> findAll();

	public PaytmData findByOrganization(Organization organization);
	
	@Override
	public void delete(@Param("paytm") PaytmData paytm);
		
}