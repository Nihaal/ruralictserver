package app.data.repositories;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;

//import app.entities.Organization;
import app.entities.LcpProductType;


public interface LcpProductTypeRepository extends JpaRepository<LcpProductType, Integer> {
	/*
	 * Default functions
	 */

	//@PostAuthorize("hasRole('ADMIN'+returnObject.organization.abbreviation)")
	@Override
	public LcpProductType findOne(Integer id);

	//@PostFilter("hasRole('ADMIN'+filterObject.organization.abbreviation)")
	@Override
	public List<LcpProductType> findAll();

//	@PostFilter("hasRole('ADMIN'+filterObject.organization.abbreviation)")
//	@Override
//	public Page<ProductType> findAll(Pageable pageable);

	//@PostFilter("hasRole('ADMIN'+filterObject.organization.abbreviation)")
	@Override
	public List<LcpProductType> findAll(Sort sort);

	//@PreAuthorize("hasRole('ADMIN'+#type.organization.abbreviation)")
	@Override
	public <S extends LcpProductType> S save(@Param("type") S type);

	//@PreAuthorize("hasRole('ADMIN'+#type.organization.abbreviation)")
	@Override
	public void delete(@Param("type") LcpProductType type);
	
	//public List<LcpProductType> findAllByOrganizationOrderBySequenceAsc(Organization organization);
	

	/*
	 * Search functions
	 */
	public List<LcpProductType> findAllByOrderByNameAsc();
	
	//public List<LcpProductType> findByorganization_abbreviationIgnoreCase(@Param("abbr") String abbr);
	
	//public LcpProductType findByNameAndOrganization(String name, Organization organization);

	//public List<LcpProductType> findByOrganization(Organization organization);
	
	//public LcpProductType findByOrganizationAndName(Organization organization, String name);
}


