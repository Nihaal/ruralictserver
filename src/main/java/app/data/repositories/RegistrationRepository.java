package app.data.repositories;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;

import app.entities.Loginuserdemo;
import app.entities.Product;
import app.entities.Registration;

public interface RegistrationRepository extends JpaRepository<Registration, Integer>{
	@Override
	public <S extends Registration> S save(@Param("registration") S registration);
	
	/*@Override
	public Registration findOne(Integer id);*/

	@Override
	public List<Registration> findAll(Sort sort);

	@Override
	public List<Registration> findAll();

	public Registration findByPhone(String phone);
	
	/*public Registration findByPId(String phone);*/
	
	public Registration findFirstByEmail(String email);
	
	@Override
	public void delete(@Param("registration") Registration registration);
	
	
	public Registration findByPhoneAndPassword(String phone, String password);
	
	
	
}