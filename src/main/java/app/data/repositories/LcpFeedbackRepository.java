package app.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import app.entities.LcpFeedback;

public interface LcpFeedbackRepository extends JpaRepository<LcpFeedback, Integer> {
	
	@Override
	public LcpFeedback findOne(Integer id);
	
	
}
